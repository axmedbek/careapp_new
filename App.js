import React, {useEffect} from 'react';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import rootReducer from './src/reducers/rootReducer';
import {checkLangExists, getLanguage, saveLang} from "./src/config/caching/actions/setting";
import 'moment/locale/da';
import 'moment/locale/en-in';
import moment from "moment";
import ScreenNavigationContainer from "./src/screens/ScreenNavigationContainer";
import OperationLoadingComponent from "./src/components/loadings/OperationLoadingComponent";
import {Root} from "native-base";

const store = createStore(rootReducer, applyMiddleware(thunk));

const App = () => {
    useEffect(() => {
        if (checkLangExists()) {
            saveLang('da');
        }
        moment.locale(getLanguage())
    }, []);

    return (
        <Provider store={store}>
            <Root>
                <ScreenNavigationContainer/>
            </Root>
            <OperationLoadingComponent/>
        </Provider>
    );
};


export default App;
