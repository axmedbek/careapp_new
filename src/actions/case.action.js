import {httpClient} from "../helpers/HttpClient";
import {
    GET_CASE_ACTIVITIES_URL,
    GET_CASE_PLANS_URL, GET_CASE_QUESTIONS_URL, GET_CASE_RECORDS_URL,
    GET_CASE_URL, GET_CASES_MIN_URL,
    GET_CASES_URL,
    GET_CONTACTS_URL, GET_GOALS_URL, GET_PLAN_STATUSES_URL,
    GET_SESSION_TYPES_URL
} from "../constants/urls";
import {
    GET_CASE_ACTIVITIES_FULFILLED,
    GET_CASE_ACTIVITIES_PENDING,
    GET_CASE_ACTIVITIES_REJECTED,
    GET_CASE_FULFILLED,
    GET_CASE_PENDING,
    GET_CASE_PLANS_FULFILLED,
    GET_CASE_PLANS_PENDING,
    GET_CASE_PLANS_REJECTED, GET_CASE_QUESTIONS_FULFILLED,
    GET_CASE_QUESTIONS_PENDING, GET_CASE_QUESTIONS_REJECTED,
    GET_CASE_RECORDS_FULFILLED,
    GET_CASE_RECORDS_PENDING,
    GET_CASE_RECORDS_REJECTED,
    GET_CASE_REJECTED,
    GET_CASES_FULFILLED,
    GET_CASES_MIN_FULFILLED,
    GET_CASES_MIN_PENDING,
    GET_CASES_MIN_REJECTED,
    GET_CASES_PENDING,
    GET_CASES_REJECTED,
    GET_CONTACTS_FULFILLED,
    GET_CONTACTS_PENDING,
    GET_CONTACTS_REJECTED,
    GET_GOALS_FULFILLED,
    GET_GOALS_PENDING,
    GET_GOALS_REJECTED,
    GET_SESSION_TYPES_FULFILLED,
    GET_SESSION_TYPES_PENDING,
    GET_SESSION_TYPES_REJECTED,
    GET_TRADING_PLANS_STATUSES_FULFILLED,
    GET_TRADING_PLANS_STATUSES_PENDING,
    GET_TRADING_PLANS_STATUSES_REJECTED,
} from "./action_types/case.type";

export function getCases() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CASES_PENDING,
                'success': GET_CASES_FULFILLED,
                'reject': GET_CASES_REJECTED
            },
            `${GET_CASES_URL}`,
            'get',
            null);
    }
}


export function getMinCases() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CASES_MIN_PENDING,
                'success': GET_CASES_MIN_FULFILLED,
                'reject': GET_CASES_MIN_REJECTED
            },
            `${GET_CASES_MIN_URL}`,
            'get',
            null);
    }
}

export function getCase(id) {
    let url = GET_CASE_URL.replace(":case", id);

    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CASE_PENDING,
                'success': GET_CASE_FULFILLED,
                'reject': GET_CASE_REJECTED
            },
            url, 'get', null);
    }
}

export function getSessionTypes(data) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_SESSION_TYPES_PENDING,
                'success': GET_SESSION_TYPES_FULFILLED,
                'reject': GET_SESSION_TYPES_REJECTED
            },
            `${GET_SESSION_TYPES_URL}`,
            'get',
            data);
    }
}


export function getContacts(id) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CONTACTS_PENDING,
                'success': GET_CONTACTS_FULFILLED,
                'reject': GET_CONTACTS_REJECTED
            },
            `${GET_CONTACTS_URL}`,
            'get',
            {
                case_id: id
            });
    }
}

export function getCasePlans(id) {
    let url = GET_CASE_PLANS_URL.replace(":case", id);

    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CASE_PLANS_PENDING,
                'success': GET_CASE_PLANS_FULFILLED,
                'reject': GET_CASE_PLANS_REJECTED
            },
            url,
            'get',null);
    }
}


export function getGoals(data) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_GOALS_PENDING,
                'success': GET_GOALS_FULFILLED,
                'reject': GET_GOALS_REJECTED
            },
            `${GET_GOALS_URL}`,
            'get',
            data);
    }
}

export function getTradingPlanStatuses(data) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_TRADING_PLANS_STATUSES_PENDING,
                'success': GET_TRADING_PLANS_STATUSES_FULFILLED,
                'reject': GET_TRADING_PLANS_STATUSES_REJECTED
            },
            `${GET_PLAN_STATUSES_URL}`,
            'get',
            data);
    }
}

export function getCaseActivities(case_id) {
    let url = GET_CASE_ACTIVITIES_URL.replace(':case', case_id);
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CASE_ACTIVITIES_PENDING,
                'success': GET_CASE_ACTIVITIES_FULFILLED,
                'reject': GET_CASE_ACTIVITIES_REJECTED
            },
            url,
            'get',
            null);
    }
}

export function getCaseRecords(case_id, start_date, end_date) {
    let url = GET_CASE_RECORDS_URL.replace(":case", case_id);

    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CASE_RECORDS_PENDING,
                'success': GET_CASE_RECORDS_FULFILLED,
                'reject': GET_CASE_RECORDS_REJECTED
            },
            url,
            'get',
            {
                starttime: start_date,
                endtime: end_date
            });
    }
}

export function getCaseQuestions(id) {
    let url = GET_CASE_QUESTIONS_URL.replace(":case", id);

    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CASE_QUESTIONS_PENDING,
                'success': GET_CASE_QUESTIONS_FULFILLED,
                'reject': GET_CASE_QUESTIONS_REJECTED
            },
            url,
            'get');
    }
}
