import {httpClient} from "../helpers/HttpClient";
import {
    CHAT_CONTACT_LIST_URL,
    CHAT_LIST_URL,
    CHAT_MESSAGES_URL,
    CHAT_MESSAGES_WITH_USER_ID_URL,
    CHAT_PROFILE_URL
} from "../constants/urls";
import {
    GET_ALL_CHAT_FULFILLED,
    GET_ALL_CHAT_PENDING,
    GET_ALL_CHAT_REJECTED,
    GET_ALL_CONTACT_FULFILLED,
    GET_ALL_CONTACT_PENDING,
    GET_ALL_CONTACT_REJECTED,
    GET_CHAT_DIALOGUE_FULFILLED,
    GET_CHAT_DIALOGUE_PENDING,
    GET_CHAT_DIALOGUE_REJECTED,
    GET_CHAT_MESSAGES_FULFILLED,
    GET_CHAT_MESSAGES_PENDING,
    GET_CHAT_MESSAGES_REJECTED
} from "./action_types/chat.type";

export function getAllChats(search) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_ALL_CHAT_PENDING,
                'success': GET_ALL_CHAT_FULFILLED,
                'reject': GET_ALL_CHAT_REJECTED
            },
            `${CHAT_LIST_URL}?search=${search}`,
            'get',
            null);
    }
}


export function getAllContacts(search) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_ALL_CONTACT_PENDING,
                'success': GET_ALL_CONTACT_FULFILLED,
                'reject': GET_ALL_CONTACT_REJECTED
            },
            `${CHAT_CONTACT_LIST_URL}?search=${search}`,
            'get',
            null);
    }
}


export function getChatMessages(id) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CHAT_MESSAGES_PENDING,
                'success': GET_CHAT_MESSAGES_FULFILLED,
                'reject': GET_CHAT_MESSAGES_REJECTED
            },
            `${CHAT_MESSAGES_URL}/${id}`,
            'get',
            null);
    }
}

export function getChatMessagesWithUserId(id) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CHAT_MESSAGES_PENDING,
                'success': GET_CHAT_MESSAGES_FULFILLED,
                'reject': GET_CHAT_MESSAGES_REJECTED
            },
            `${CHAT_MESSAGES_WITH_USER_ID_URL}/${id}`,
            'get',
            null);
    }
}


export function getChatDialogue(id) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CHAT_DIALOGUE_PENDING,
                'success': GET_CHAT_DIALOGUE_FULFILLED,
                'reject': GET_CHAT_DIALOGUE_REJECTED
            },
            `${CHAT_PROFILE_URL}/${id}`,
            'get',
            null);
    }
}

export function addMessageChatReducer(message) {
    return dispatch => {
        dispatch({
            type: 'ADD_MESSAGE_CHAT_REDUCER',
            payload: message
        });
    }
}
