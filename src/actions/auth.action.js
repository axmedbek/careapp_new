import {logoutService} from "../services/auth.service";
import {updateTokens} from "../config/caching/actions/token";
import {
    GET_PROFILE_INFO_FULFILLED,
    GET_PROFILE_INFO_PENDING,
    GET_PROFILE_INFO_REJECTED,
    LOGOUT_FULFILLED,
    LOGOUT_PENDING, LOGOUT_REJECTED
} from "./action_types/auth.type";
import {httpClient} from "../helpers/HttpClient";

import {PROFILE_INFO} from "../constants/urls";

export function getProfileInfo() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_PROFILE_INFO_PENDING,
                'success': GET_PROFILE_INFO_FULFILLED,
                'reject': GET_PROFILE_INFO_REJECTED
            },
            `${PROFILE_INFO}`,
            'get',
            null);
    }
}

export function logout(lang, token) {
    return dispatch => {
        dispatch({
            type: LOGOUT_PENDING
        });

        logoutService(token)
            .then(response => {
                updateTokens();
                dispatch({
                    type: LOGOUT_FULFILLED,
                    payload: response
                });
            })
            .catch(error => {
                updateTokens();
                dispatch({
                    type: LOGOUT_REJECTED,
                    payload: error
                });
            });
    }
}
