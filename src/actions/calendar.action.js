import {EVENTS_FULFILLED, EVENTS_PENDING, EVENTS_REJECTED} from "./action_types/calendar.type";
import {httpClient} from "../helpers/HttpClient";
import {GET_EVENTS_URL} from "../constants/urls";

export function getEvents() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': EVENTS_PENDING,
                'success': EVENTS_FULFILLED,
                'reject': EVENTS_REJECTED
            },
            `${GET_EVENTS_URL}`,
            'get',
            null);
    }
}
