import {httpClient} from "../helpers/HttpClient";
import {CONTRACTS_URL} from "../constants/urls";
import {GET_CONTRACTS_FULFILLED, GET_CONTRACTS_PENDING, GET_CONTRACTS_REJECTED} from "./action_types/contract.type";
import {getDeviceUser} from "../config/caching/actions/setting";

export function getContracts() {
    let url = CONTRACTS_URL.replace(":id",getDeviceUser().user_id);

    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CONTRACTS_PENDING,
                'success': GET_CONTRACTS_FULFILLED,
                'reject': GET_CONTRACTS_REJECTED
            },
            url,
            'get',
            null);
    }
}
