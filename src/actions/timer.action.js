import {
    GET_TIMER_FULFILLED,
    GET_TIMER_PENDING,
    GET_TIMER_REJECTED
} from "./action_types/case.type";
import {getTimeService} from "../services/timer.service";

export function getTimer(id) {
    return dispatch => {
        dispatch({
            type: GET_TIMER_PENDING
        });

        getTimeService(id)
            .then(response => {
                if (response.data.status === "success") {
                    dispatch({
                        type: GET_TIMER_FULFILLED,
                        payload: response.data.session
                    })
                }
                else {
                    dispatch({
                        type: GET_TIMER_FULFILLED,
                        payload: response.data
                    })
                }
            })
            .catch(error => {
                dispatch({
                    type: GET_TIMER_REJECTED,
                    payload: error
                })
            })
    }
}
