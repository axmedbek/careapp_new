import {httpClient} from "../helpers/HttpClient";
import {GET_CATEGORIES_URL, GET_MODERATORS_LIST} from "../constants/urls";
import {
    GET_CATEGORIES_FULFILLED,
    GET_CATEGORIES_PENDING,
    GET_CATEGORIES_REJECTED, GET_CITIZENS_FULFILLED, GET_CITIZENS_PENDING, GET_CITIZENS_REJECTED,
    GET_EMPLOYEE_FULFILLED,
    GET_EMPLOYEE_PENDING,
    GET_EMPLOYEE_REJECTED,
    GET_MODERATORS_FULFILLED,
    GET_MODERATORS_PENDING,
    GET_MODERATORS_REJECTED, GET_PARTNERS_FULFILLED, GET_PARTNERS_PENDING, GET_PARTNERS_REJECTED
} from "./action_types/common.type";

export function getCategories() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CATEGORIES_PENDING,
                'success': GET_CATEGORIES_FULFILLED,
                'reject': GET_CATEGORIES_REJECTED
            },
            `${GET_CATEGORIES_URL}`,
            'get',
            null);
    }
}

export function getModerators() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_MODERATORS_PENDING,
                'success': GET_MODERATORS_FULFILLED,
                'reject': GET_MODERATORS_REJECTED
            },
            `${GET_MODERATORS_LIST}`,
            'get',
            {type: 'partner'});
    }
}


export function getEmployee() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_EMPLOYEE_PENDING,
                'success': GET_EMPLOYEE_FULFILLED,
                'reject': GET_EMPLOYEE_REJECTED
            },
            `${GET_MODERATORS_LIST}`,
            'get',
            {type: 'partner'});
    }
}


export function getCitizens() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_CITIZENS_PENDING,
                'success': GET_CITIZENS_FULFILLED,
                'reject': GET_CITIZENS_REJECTED
            },
            `${GET_MODERATORS_LIST}`,
            'get',
            {type: 'partner'});
    }
}


export function getPartners() {
    return dispatch => {
        httpClient(dispatch, {
                'pending': GET_PARTNERS_PENDING,
                'success': GET_PARTNERS_FULFILLED,
                'reject': GET_PARTNERS_REJECTED
            },
            `${GET_MODERATORS_LIST}`,
            'get',
            {type: 'partner'});
    }
}
