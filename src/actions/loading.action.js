import {SET_OPERATION_LOADING} from "./action_types/system.type";

export const setOperationLoading = loading => {
    return dispatch => {
        dispatch({
            type: SET_OPERATION_LOADING,
            loading,
        });
    };
};
