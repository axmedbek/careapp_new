import {SHOW_TOAST_MESSAGE} from "./action_types/system.type";

export function showToast(status, message, toastType) {
    return dispatch => {
        dispatch({
            type: SHOW_TOAST_MESSAGE,
            status,
            message,
            toastType
        });
    };
}


export function changeNotificationStatus(status) {
    return dispatch => {
        dispatch({
            type: 'NOTIFICATION_STATUS',
            payload: status
        });
    };
}
