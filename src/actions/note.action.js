import {httpClient} from "../helpers/HttpClient";
import {
    ADD_FOLDER_FULFILLED,
    ADD_FOLDER_PENDING,
    ADD_FOLDER_REJECTED, ADD_NOTE_CONTRACT_FULFILLED, ADD_NOTE_CONTRACT_PENDING, ADD_NOTE_CONTRACT_REJECTED,
    ADD_NOTE_FULFILLED,
    ADD_NOTE_PENDING,
    ADD_NOTE_REJECTED,
    DELETE_FOLDER_FULFILLED,
    DELETE_FOLDER_PENDING,
    DELETE_FOLDER_REJECTED, DELETE_NOTE_CONTRACT_FULFILLED, DELETE_NOTE_CONTRACT_PENDING, DELETE_NOTE_CONTRACT_REJECTED,
    DELETE_NOTE_FULFILLED,
    DELETE_NOTE_PENDING,
    DELETE_NOTE_REJECTED,
    FOLDERS_FULFILLED,
    FOLDERS_PENDING,
    FOLDERS_REJECTED, NOTES_CONTRACT_FULFILLED, NOTES_CONTRACT_PENDING, NOTES_CONTRACT_REJECTED,
    NOTES_FULFILLED,
    NOTES_PENDING,
    NOTES_REJECTED
} from "./action_types/note.type";
import {
    CREATE_FOLDER_URL,
    CREATE_NOTE_URL, DELETE_FOLDER_URL, DELETE_NOTE_URL,
    EDIT_FOLDER_URL,
    EDIT_NOTE_URL,
    GET_CASE_NOTES_URL,
    GET_FOLDERS_URL, GET_PLAN_NOTES_URL,
} from "../constants/urls";

import {getNotesService} from "../services/note.service";

export function getFolders(data = {}) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': FOLDERS_PENDING,
                'success': FOLDERS_FULFILLED,
                'reject': FOLDERS_REJECTED
            },
            `${GET_FOLDERS_URL}`,
            'get',
            data);
    }
}

export function addFolder(data = {}) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_FOLDER_PENDING,
                'success': ADD_FOLDER_FULFILLED,
                'reject': ADD_FOLDER_REJECTED
            },
            CREATE_FOLDER_URL,
            'post',
            {
                title: data.title
            });
    }
}

export function updateFolder(data = {}) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_FOLDER_PENDING,
                'success': ADD_FOLDER_FULFILLED,
                'reject': ADD_FOLDER_REJECTED
            },
            `${EDIT_FOLDER_URL}/${data.id}`,
            'put',
            {
                title: data.title
            });
    }
}

export function deleteFolder(id = {}) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': DELETE_FOLDER_PENDING,
                'success': DELETE_FOLDER_FULFILLED,
                'reject': DELETE_FOLDER_REJECTED
            },
            `${DELETE_FOLDER_URL}/${id}`,
            'delete',
            null);
    }
}


export function getNotes(id) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': NOTES_PENDING,
                'success': NOTES_FULFILLED,
                'reject': NOTES_REJECTED
            },
            `${GET_FOLDERS_URL}/${id}/notes`,
            'get',
            null);
    }
}

export function getContractNotes(id){
    return dispatch => {
        httpClient(dispatch, {
                'pending': NOTES_CONTRACT_PENDING,
                'success': NOTES_CONTRACT_FULFILLED,
                'reject': NOTES_CONTRACT_REJECTED
            },
            `profiles/1/contracts/${id}/notes`,
            'get',
            null);
    }
}


export function addContractNote(id,data = {}) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_NOTE_CONTRACT_PENDING,
                'success': ADD_NOTE_CONTRACT_FULFILLED,
                'reject': ADD_NOTE_CONTRACT_REJECTED
            },
            `profiles/1/contracts/${id}/notes`,
            'post',
            {
                title: data.title,
                description: data.description
            });
    }
}


export function deleteContractNote(id,note_id) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': DELETE_NOTE_CONTRACT_PENDING,
                'success': DELETE_NOTE_CONTRACT_FULFILLED,
                'reject': DELETE_NOTE_CONTRACT_REJECTED
            },
            `profiles/1/contracts/${id}/notes/${note_id}`,
            'delete',
            null);
    }
}

export function getCaseNotes(id) {
    let url = GET_CASE_NOTES_URL.replace(":case", id);

    return dispatch => {
        httpClient(dispatch, {
                'pending': NOTES_PENDING,
                'success': NOTES_FULFILLED,
                'reject': NOTES_REJECTED
            },
            url,
            'get',
            null);
    }
}

export function getPlanNotes(id) {
    let url = GET_PLAN_NOTES_URL.replace(":plan", id);

    return dispatch => {
        httpClient(dispatch, {
                'pending': NOTES_PENDING,
                'success': NOTES_FULFILLED,
                'reject': NOTES_REJECTED
            },
            url,
            'get',
            null);
    }
}

export function addNote(data = {}) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_NOTE_PENDING,
                'success': ADD_NOTE_FULFILLED,
                'reject': ADD_NOTE_REJECTED
            },
            `${CREATE_FOLDER_URL}/${data.folder_id}/notes`,
            'post',
            {
                title: data.title,
                description: data.description
            });
    }
}

export function updateNote(data = {}) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_NOTE_PENDING,
                'success': ADD_NOTE_FULFILLED,
                'reject': ADD_NOTE_REJECTED
            },
            `${CREATE_FOLDER_URL}/${data.folder_id}/notes/${data.id}`,
            'put',
            {
                title: data.title,
                description: data.description
            });
    }
}

export function deleteNote(id, folder_id) {
    return dispatch => {
        httpClient(dispatch, {
                'pending': DELETE_NOTE_PENDING,
                'success': DELETE_NOTE_FULFILLED,
                'reject': DELETE_NOTE_REJECTED
            },
            `${DELETE_FOLDER_URL}/${folder_id}/notes/${id}`,
            'delete',
            null);
    }
}


export function addEditNoteForCase(case_id,data = {}) {
    let method = data.id ? 'put' : 'post';
    let url = GET_CASE_NOTES_URL.replace(":case", case_id);
    url = data.id ? `${url}/${data.id}` : url;

    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_NOTE_PENDING,
                'success': ADD_NOTE_FULFILLED,
                'reject': ADD_NOTE_REJECTED
            },
            url,
            method,
            data);
    }
}

export function addEditNoteForEvent(data = {}) {
    const dataURL = parseInt(data.id) > 0 ?
        `title=${data.title}&description=${data.description}&id=${data.id}&todo_id=${data.todo_id}` :
        `title=${data.title}&description=${data.description}&todo_id=${data.todo_id}`;

    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_NOTE_PENDING,
                'success': ADD_NOTE_FULFILLED,
                'reject': ADD_NOTE_REJECTED
            },
            `${parseInt(data.id) > 0 ? EDIT_NOTE_URL : CREATE_NOTE_URL}`,
            'get',
            dataURL);
    }
}

export function addEditNoteForPlan(plan_id,data = {}) {
    let method = data.id ? 'put' : 'post';
    let url = GET_PLAN_NOTES_URL.replace(":plan", plan_id);
    url = data.id ? `${url}/${data.id}` : url;

    return dispatch => {
        httpClient(dispatch, {
                'pending': ADD_NOTE_PENDING,
                'success': ADD_NOTE_FULFILLED,
                'reject': ADD_NOTE_REJECTED
            },
            url,
            method,
            data);
    }
}

