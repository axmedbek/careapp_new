export const GET_ALL_CHAT_PENDING = 'GET_ALL_CHAT_PENDING';
export const GET_ALL_CHAT_FULFILLED = 'GET_ALL_CHAT_FULFILLED';
export const GET_ALL_CHAT_REJECTED = 'GET_ALL_CHAT_REJECTED';


export const GET_ALL_CONTACT_PENDING = 'GET_ALL_CONTACT_PENDING';
export const GET_ALL_CONTACT_FULFILLED = 'GET_ALL_CONTACT_FULFILLED';
export const GET_ALL_CONTACT_REJECTED = 'GET_ALL_CONTACT_REJECTED';

export const GET_CHAT_MESSAGES_PENDING = 'GET_CHAT_MESSAGES_PENDING';
export const GET_CHAT_MESSAGES_FULFILLED = 'GET_CHAT_MESSAGES_FULFILLED';
export const GET_CHAT_MESSAGES_REJECTED = 'GET_CHAT_MESSAGES_REJECTED';


export const GET_CHAT_DIALOGUE_PENDING = 'GET_CHAT_DIALOGUE_PENDING';
export const GET_CHAT_DIALOGUE_FULFILLED = 'GET_CHAT_DIALOGUE_FULFILLED';
export const GET_CHAT_DIALOGUE_REJECTED = 'GET_CHAT_DIALOGUE_REJECTED';

