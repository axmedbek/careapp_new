export const LOGIN_PENDING = 'LOGIN_PENDING';
export const LOGIN_FULFILLED = 'LOGIN_FULFILLED';
export const LOGIN_REJECTED = 'LOGIN_REJECTED';

export const GET_PROFILE_INFO_PENDING = 'GET_PROFILE_INFO_PENDING';
export const GET_PROFILE_INFO_FULFILLED = 'GET_PROFILE_INFO_FULFILLED';
export const GET_PROFILE_INFO_REJECTED = 'GET_PROFILE_INFO_REJECTED';

export const UPDATE_PROFILE_INFO_PENDING = 'UPDATE_PROFILE_INFO_PENDING';
export const UPDATE_PROFILE_INFO_FULFILLED = 'UPDATE_PROFILE_INFO_FULFILLED';
export const UPDATE_PROFILE_INFO_REJECTED = 'UPDATE_PROFILE_INFO_REJECTED';

export const LOGOUT_PENDING = 'LOGOUT_PENDING';
export const LOGOUT_FULFILLED = 'LOGOUT_FULFILLED';
export const LOGOUT_REJECTED = 'LOGOUT_REJECTED';
