export function setSocketConnection (socket) {
    return dispatch => {
        dispatch ({
            type : 'SET_SOCKET_CONNECTION',
            payload : socket
        })
    }
}
