import {UPDATE_LANGUAGE} from "./action_types/system.type";

export const updateLanguage = language => {
  return dispatch => {
    dispatch({
      type: UPDATE_LANGUAGE,
      language,
    });
  };
};
