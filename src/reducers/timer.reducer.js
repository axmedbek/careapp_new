import {
    GET_TIMER_FULFILLED,
    GET_TIMER_PENDING,
    GET_TIMER_REJECTED, UPDATE_TIMER_FULFILLED,
    UPDATE_TIMER_PENDING, UPDATE_TIMER_REJECTED
} from "../actions/action_types/case.type";

const initialState = {
    timer: [],
    updateTimer: {},
    errors: {},
    fetching: false
};

const timer_reducer = (state = initialState, action) => {
    switch (action.type) {
        //    get timer
        case GET_TIMER_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_TIMER_FULFILLED :
            return {
                ...state,
                fetching: false,
                timer: action.payload
            };
        case GET_TIMER_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };

        // //    update timer
        // case UPDATE_TIMER_PENDING :
        //     return {
        //         ...state,
        //         errors: {},
        //         fetching: true
        //     };
        // case UPDATE_TIMER_FULFILLED :
        //     return {
        //         ...state,
        //         fetching: false,
        //         updateTimer: action.payload.data.data
        //     };
        // case UPDATE_TIMER_REJECTED :
        //     return {
        //         ...state,
        //         fetching: false,
        //         errors: action.payload
        //     };
        default :
            return state;
    }
};

export default timer_reducer;
