import {
    GET_CONTRACTS_FULFILLED,
    GET_CONTRACTS_PENDING,
    GET_CONTRACTS_REJECTED
} from "../actions/action_types/contract.type";
import {
    ADD_NOTE_CONTRACT_FULFILLED,
    ADD_NOTE_CONTRACT_PENDING,
    ADD_NOTE_CONTRACT_REJECTED,
    NOTES_CONTRACT_FULFILLED,
    NOTES_CONTRACT_PENDING,
    NOTES_CONTRACT_REJECTED
} from "../actions/action_types/note.type";

const initialState = {
    contracts: [],
    fetching: false,
    errors: {},

    notes: [],
    fetchingNotes: false,
    notesErrors: {},
    adding: false
};

const contract_reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CONTRACTS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CONTRACTS_FULFILLED :
            return {
                ...state,
                fetching: false,
                contracts: action.payload.data.data
            };
        case GET_CONTRACTS_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };


        case NOTES_CONTRACT_PENDING :
            return {
                ...state,
                notesErrors: {},
                fetchingNotes: true
            };
        case NOTES_CONTRACT_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                fetchingNotes: false,
                notes: action.payload.data.data
            };
        case NOTES_CONTRACT_REJECTED :
            return {
                ...state,
                fetchingNotes: false,
                notesErrors: action.payload
            };



        case ADD_NOTE_CONTRACT_PENDING :
            return {
                ...state,
                notesErrors: {},
                adding: true
            };
        case ADD_NOTE_CONTRACT_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                adding: false
            };
        case ADD_NOTE_CONTRACT_REJECTED :
            console.log(action.payload.response);
            return {
                ...state,
                adding: false,
                notesErrors: action.payload
            };
        default :
            return state;
    }
};

export default contract_reducer;
