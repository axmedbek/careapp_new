import {SHOW_TOAST_MESSAGE} from "../actions/action_types/system.type";

const initialState = {
    simpleToast: false,
    simpleToastMessage: 'No message',
    simpleToastType: 'success',
    notification_status: true
};

const settingReducer = (state = initialState, action) => {
    if (action === undefined) {
        return state;
    }
    if (action.type === SHOW_TOAST_MESSAGE) {
        return {
            ...state,
            simpleToast: action.status,
            simpleToastMessage: action.message,
            simpleToastType: action.toastType
        };
    } else if (action.type === 'NOTIFICATION_STATUS') {
        return {
            ...state,
            notification_status: action.payload
        };
    } else {
        return state;
    }
};
export default settingReducer;
