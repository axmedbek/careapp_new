import {
    GET_CASE_ACTIVITIES_FULFILLED,
    GET_CASE_ACTIVITIES_PENDING,
    GET_CASE_ACTIVITIES_REJECTED,
    GET_CASE_FULFILLED,
    GET_CASE_PENDING,
    GET_CASE_PLANS_FULFILLED,
    GET_CASE_PLANS_PENDING,
    GET_CASE_PLANS_REJECTED, GET_CASE_QUESTIONS_FULFILLED,
    GET_CASE_QUESTIONS_PENDING, GET_CASE_QUESTIONS_REJECTED,
    GET_CASE_RECORDS_FULFILLED,
    GET_CASE_RECORDS_PENDING,
    GET_CASE_RECORDS_REJECTED,
    GET_CASE_REJECTED,
    GET_CASES_FULFILLED,
    GET_CASES_MIN_FULFILLED,
    GET_CASES_MIN_PENDING,
    GET_CASES_MIN_REJECTED,
    GET_CASES_PENDING,
    GET_CASES_REJECTED,
    GET_GOALS_FULFILLED,
    GET_GOALS_PENDING,
    GET_GOALS_REJECTED,
    GET_SESSION_TYPES_FULFILLED,
    GET_SESSION_TYPES_PENDING,
    GET_SESSION_TYPES_REJECTED,
    GET_TRADING_PLANS_STATUSES_FULFILLED,
    GET_TRADING_PLANS_STATUSES_PENDING,
    GET_TRADING_PLANS_STATUSES_REJECTED,
} from "../actions/action_types/case.type";

const initialState = {
    cases: [],
    minCases: [],
    case: undefined,
    sessionTypes: [],
    errors: {},
    fetching: false,

    // plans
    plans: {},
    plansFetching: false,
    plansError: {},

    trading_plans_statuses: [],

//   goals
    goals: [],


//    activities
    activities: [],
    records: [],


    questions: {},
    questionFetching: false,
    questionErrors: {},
};

const case_reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CASES_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CASES_FULFILLED :
            return {
                ...state,
                fetching: false,
                cases: action.payload.data.data
            };
        case GET_CASES_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };


        //     get case
        case GET_CASE_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CASE_FULFILLED :
            return {
                ...state,
                fetching: false,
                case: action.payload.data
            };
        case GET_CASE_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };

        //    get session types
        case GET_SESSION_TYPES_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_SESSION_TYPES_FULFILLED :
            return {
                ...state,
                fetching: false,
                sessionTypes: action.payload.data.data
            };
        case GET_SESSION_TYPES_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };


        //    get case plans
        case GET_CASE_PLANS_PENDING :
            return {
                ...state,
                plansError: {},
                plansFetching: true
            };
        case GET_CASE_PLANS_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                plansFetching: false,
                plans: {
                    count: action.payload.data.count,
                    skip: action.payload.data.skip,
                    limit: action.payload.data.limit,
                    data: action.payload.data.data
                }
            };
        case GET_CASE_PLANS_REJECTED :
            console.log(action.payload.response);
            return {
                ...state,
                plansFetching: false,
                plansError: action.payload
            };


        //    get trading plans statuses
        case GET_TRADING_PLANS_STATUSES_PENDING :
            return {
                ...state,
                plansError: {},
                plansFetching: true
            };
        case GET_TRADING_PLANS_STATUSES_FULFILLED :
            return {
                ...state,
                plansFetching: false,
                trading_plans_statuses: action.payload.data.data
            };
        case GET_TRADING_PLANS_STATUSES_REJECTED :
            return {
                ...state,
                plansError: action.payload
            };



        //    get goals
        case GET_GOALS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_GOALS_FULFILLED :
            return {
                ...state,
                fetching: false,
                goals: action.payload.data.data
            };
        case GET_GOALS_REJECTED :
            return {
                ...state,
                errors: action.payload
            };


        //    get activities
        case GET_CASE_ACTIVITIES_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CASE_ACTIVITIES_FULFILLED :
            return {
                ...state,
                fetching: false,
                activities: action.payload.data.data
            };
        case GET_CASE_ACTIVITIES_REJECTED :
            return {
                ...state,
                errors: action.payload
            };


        //    get records
        case GET_CASE_RECORDS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CASE_RECORDS_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                fetching: false,
                records: action.payload.data
            };
        case GET_CASE_RECORDS_REJECTED :
            console.log(action.payload.response);
            return {
                ...state,
                fetching: false,
                errors: action.payload,
                records: []
            };


        //    get min cases list
        case GET_CASES_MIN_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CASES_MIN_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                fetching: false,
                minCases: action.payload.data.data
            };
        case GET_CASES_MIN_REJECTED :
            console.log(action.payload);
            return {
                ...state,
                errors: action.payload
            };


        //    get questions list
        case GET_CASE_QUESTIONS_PENDING :
            return {
                ...state,
                errors: {},
                questionFetching: true
            };
        case GET_CASE_QUESTIONS_FULFILLED :
            return {
                ...state,
                questionFetching: false,
                questions: action.payload.data.data
            };
        case GET_CASE_QUESTIONS_REJECTED :
            return {
                ...state,
                questionErrors: action.payload
            };
        default :
            return state;
    }
};

export default case_reducer;
