import {SET_OPERATION_LOADING} from "../actions/action_types/system.type";

const initialState = {
    operationLoading: false
};

const loadingReducer = (state = initialState, action) => {
    if (action === undefined) {
        return state;
    }
    if (action.type === SET_OPERATION_LOADING) {
        return {
            ...state,
            operationLoading: action.loading
        };
    } else {
        return state;
    }
};
export default loadingReducer;
