import {combineReducers} from 'redux';
import langReducer from './lang.reducer';
import user_reducer from "./user.reducer";
import loadingReducer from "./loading.reducer";
import event_reducer from "./events.reducer";
import note_reducer from "./note.reducer";
import {LOGOUT_FULFILLED} from "../actions/action_types/auth.type";
import settingReducer from "./setting.reducer";
import case_reducer from "./case.reducer";
import timer_reducer from "./timer.reducer";
import contact_reducer from "./contacts.reducer";
import common_reducer from "./common.reducer";
import chat_reducer from "./chat.reducer";
import socket_connection_reducer from "./socket.reducer";
import contract_reducer from "./contract.reducer";

const appReducer = combineReducers({
    lang: langReducer,
    user: user_reducer,
    loadingReducer: loadingReducer,
    eventReducer: event_reducer,
    noteReducer: note_reducer,
    settingReducer: settingReducer,
    caseReducer: case_reducer,
    timeReducer: timer_reducer,
    contactReducer: contact_reducer,
    commonReducer: common_reducer,
    chatReducer: chat_reducer,
    socket_connection: socket_connection_reducer,
    contractReducer: contract_reducer
});


const rootReducer = (state, action) => {
    // when a logout action is dispatched it will reset redux state
    if (action.type === LOGOUT_FULFILLED) {
        state = undefined;
    }

    return appReducer(state, action);
};

export default rootReducer;
