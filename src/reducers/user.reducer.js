import {
    GET_PROFILE_INFO_FULFILLED,
    GET_PROFILE_INFO_PENDING,
    GET_PROFILE_INFO_REJECTED,
    LOGIN_FULFILLED,
    LOGIN_PENDING,
    LOGIN_REJECTED, LOGOUT_FULFILLED,
    LOGOUT_PENDING, LOGOUT_REJECTED
} from "../actions/action_types/auth.type";
import {deleteDeviceUserFromStorage, setDeviceUser} from "../config/caching/actions/setting";

const initialState = {
    isLogged: false,
    user: {},
    fetching: false,
    errors: {}
};

const user_reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case LOGIN_FULFILLED :
            return {
                ...state,
                fetching: false,
                isLogged: true
            };
        case LOGIN_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };

        case GET_PROFILE_INFO_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_PROFILE_INFO_FULFILLED :
            setDeviceUser(action.payload.data.data);
            return {
                ...state,
                fetching: false,
                user: action.payload.data.data
            };
        case GET_PROFILE_INFO_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };
        case LOGOUT_PENDING :
            return {
                ...state,
                fetching: true
            };
        case LOGOUT_FULFILLED :
            deleteDeviceUserFromStorage();
            return {
                ...state,
                fetching: false
            };
        case LOGOUT_REJECTED :
            deleteDeviceUserFromStorage();
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };
        default :
            return state;
    }
};

export default user_reducer;
