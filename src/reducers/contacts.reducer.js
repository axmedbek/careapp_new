import {
    GET_CONTACTS_FULFILLED,
    GET_CONTACTS_PENDING,
    GET_CONTACTS_REJECTED,
} from "../actions/action_types/case.type";

const initialState = {
    contacts: [],
    errors: {},
    fetching: false
};

const contact_reducer = (state = initialState, action) => {
    switch (action.type) {
        //    get contacts
        case GET_CONTACTS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CONTACTS_FULFILLED :
            return {
                ...state,
                fetching: false,
                contacts: action.payload.data.data
            };
        case GET_CONTACTS_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };
        default :
            return state;
    }
};

export default contact_reducer;
