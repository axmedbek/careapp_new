const initialState = {
    socket: null
};

const socket_connection_reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_SOCKET_CONNECTION' :
            return {
                ...state,
                socket: action.payload
            };
        default :
            return state;
    }
};

export default socket_connection_reducer;
