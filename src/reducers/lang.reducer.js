import {getLanguageMessages} from '../helpers/standard.helper';
import {UPDATE_LANGUAGE} from "../actions/action_types/system.type";
import {getLanguage, updateLang} from "../config/caching/actions/setting";

const initialState = {
  locale: getLanguage(),
  messages: getLanguageMessages(),
};

const langReducer = (state = initialState, action) => {
  if (action === undefined) {
    return state;
  }
  if (action.type === UPDATE_LANGUAGE) {
    updateLang(action.language);
    return {
      ...state,
      locale: action.language,
      messages: getLanguageMessages(action.language),
    };
  } else {
    return state;
  }
};
export default langReducer;
