
import {
    GET_CATEGORIES_FULFILLED,
    GET_CATEGORIES_PENDING,
    GET_CATEGORIES_REJECTED,
    GET_CITIZENS_FULFILLED,
    GET_CITIZENS_PENDING,
    GET_CITIZENS_REJECTED,
    GET_EMPLOYEE_FULFILLED,
    GET_EMPLOYEE_PENDING,
    GET_EMPLOYEE_REJECTED,
    GET_MODERATORS_FULFILLED,
    GET_MODERATORS_PENDING,
    GET_MODERATORS_REJECTED, GET_PARTNERS_FULFILLED, GET_PARTNERS_PENDING, GET_PARTNERS_REJECTED
} from "../actions/action_types/common.type";

const initialState = {
    categories: [],
    moderators: [],
    employee: [],
    citizens: [],
    partners: [],
    errors: {},
    fetching: false
};

const common_reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CATEGORIES_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                fetching: false,
                categories: action.payload.data.data
            };
        case GET_CATEGORIES_REJECTED :
            console.log(action.payload.response);
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };


        //    moderators
        case GET_MODERATORS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_MODERATORS_FULFILLED :
            return {
                ...state,
                fetching: false,
                moderators: action.payload.data.data.moderator ? action.payload.data.data.moderator : []
            };
        case GET_MODERATORS_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };


        //    employee
        case GET_EMPLOYEE_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_EMPLOYEE_FULFILLED :
            return {
                ...state,
                fetching: false,
                employee: action.payload.data.data.employee ? action.payload.data.data.employee : []
            };
        case GET_EMPLOYEE_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };


        //    citizens
        case GET_CITIZENS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_CITIZENS_FULFILLED :
            return {
                ...state,
                fetching: false,
                citizens: action.payload.data.data.citizen ? action.payload.data.data.citizen : []
            };
        case GET_CITIZENS_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };



        //    partners
        case GET_PARTNERS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case GET_PARTNERS_FULFILLED :
            return {
                ...state,
                fetching: false,
                partners: action.payload.data.data.partner ? action.payload.data.data.partner : []
            };
        case GET_PARTNERS_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };
        default :
            return state;
    }
};

export default common_reducer;
