import {
    ADD_FOLDER_FULFILLED,
    ADD_FOLDER_PENDING,
    ADD_FOLDER_REJECTED,
    ADD_NOTE_FULFILLED,
    ADD_NOTE_PENDING,
    ADD_NOTE_REJECTED, DELETE_FOLDER_FULFILLED, DELETE_FOLDER_PENDING, DELETE_FOLDER_REJECTED,
    DELETE_NOTE_FULFILLED,
    DELETE_NOTE_PENDING, DELETE_NOTE_REJECTED,
    FOLDERS_FULFILLED,
    FOLDERS_PENDING,
    FOLDERS_REJECTED,
    NOTES_FULFILLED,
    NOTES_PENDING,
    NOTES_REJECTED
} from "../actions/action_types/note.type";

const initialState = {
    folders: [],
    notes: [],
    errors: {},
    fetching: false,
    deleting: false,
    adding: false
};

const note_reducer = (state = initialState, action) => {
    switch (action.type) {
        case FOLDERS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case FOLDERS_FULFILLED :
            return {
                ...state,
                fetching: false,
                folders: action.payload.data.data
            };
        case FOLDERS_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload,
                folders: []
            };
        case ADD_FOLDER_PENDING :
            return {
                ...state,
                errors: {},
                adding: true
            };
        case ADD_FOLDER_FULFILLED :
            return {
                ...state,
                adding: false
            };
        case ADD_FOLDER_REJECTED :
            console.log(action.payload.response);
            return {
                ...state,
                adding: false,
                errors: action.payload
            };
        // notes reducer
        case NOTES_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case NOTES_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                fetching: false,
                notes: action.payload.data.data
            };
        case NOTES_REJECTED :
            console.log(action.payload);
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };
        //    note add edit

        case ADD_NOTE_PENDING :
            return {
                ...state,
                errors: {},
                adding: true
            };
        case ADD_NOTE_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                adding: false
            };
        case ADD_NOTE_REJECTED :
            console.log(action.payload.response);
            return {
                ...state,
                adding: false,
                errors: action.payload
            };
        // folder delete
        case DELETE_FOLDER_PENDING :
            return {
                ...state,
                errors: {},
                deleting: true
            };
        case DELETE_FOLDER_FULFILLED :
            return {
                ...state,
                deleting: false
            };
        case DELETE_FOLDER_REJECTED :
            return {
                ...state,
                errors: action.payload,
                deleting: false
            };
        // note delete
        case DELETE_NOTE_PENDING :
            return {
                ...state,
                errors: {},
                deleting: true
            };
        case DELETE_NOTE_FULFILLED :
            return {
                ...state,
                deleting: false
            };
        case DELETE_NOTE_REJECTED :
            console.log(action.payload.response);
            return {
                ...state,
                fetching: false,
                deleting: action.payload
            };
        default :
            return state;
    }
};

export default note_reducer;
