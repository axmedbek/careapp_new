import {
    GET_ALL_CHAT_FULFILLED,
    GET_ALL_CHAT_PENDING,
    GET_ALL_CHAT_REJECTED,
    GET_ALL_CONTACT_FULFILLED,
    GET_ALL_CONTACT_PENDING,
    GET_ALL_CONTACT_REJECTED, GET_CHAT_DIALOGUE_FULFILLED, GET_CHAT_DIALOGUE_PENDING, GET_CHAT_DIALOGUE_REJECTED,
    GET_CHAT_MESSAGES_FULFILLED,
    GET_CHAT_MESSAGES_PENDING,
    GET_CHAT_MESSAGES_REJECTED
} from "../actions/action_types/chat.type";
import {checkMessageExistList} from "../helpers/standard.helper";


const initialState = {
    chats: [],
    errors: {},
    fetching: false,

    contacts: [],
    contactFetching: false,
    contactErrors: {},

    dialogue: null,
    dialogueFetching: false,
    dialogueErrors: {},

    messageList: [],
    currentPage: 0,
    totalPage: 0,
    messageFetching: false,
    messageErrors: {},
};

const chat_reducer = (state = initialState, action) => {
    switch (action.type) {
        //   get all chats
        case GET_ALL_CHAT_PENDING :
            return {
                ...state,
                fetching: true
            };
        case GET_ALL_CHAT_FULFILLED :
            return {
                ...state,
                fetching: false,
                chats: action.payload.data.data
            };
        case GET_ALL_CHAT_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };

        //    contact list
        case GET_ALL_CONTACT_PENDING :
            return {
                ...state,
                contactFetching: true
            };
        case GET_ALL_CONTACT_FULFILLED :
            return {
                ...state,
                contactFetching: false,
                contacts: action.payload.data.data
            };
        case GET_ALL_CONTACT_REJECTED :
            return {
                ...state,
                contactFetching: false,
                contactErrors: action.payload
            };


        //    messages list
        case GET_CHAT_MESSAGES_PENDING :
            return {
                ...state,
                messageFetching: true,
                messageList: []
            };
        case GET_CHAT_MESSAGES_FULFILLED :
            return {
                ...state,
                messageFetching: false,
                messageList: action.payload.data.data.data,
                currentPage: action.payload.data.data.current_page,
                totalPage: action.payload.data.data.last_page,
            };
        case GET_CHAT_MESSAGES_REJECTED :
            return {
                ...state,
                messageFetching: false,
                messageErrors: action.payload
            };

        case 'ADD_MESSAGE_CHAT_REDUCER' :
            let currentList = state.messageList;

            if (!checkMessageExistList(currentList,action.payload)) {
                currentList.unshift(action.payload);
            }

            return {
                ...state,
                messageList: currentList
            };




        //    dialogue
        case GET_CHAT_DIALOGUE_PENDING :
            return {
                ...state,
                dialogue: null,
                dialogueFetching: true
            };
        case GET_CHAT_DIALOGUE_FULFILLED :
            return {
                ...state,
                dialogueFetching: false,
                dialogue: action.payload.data.data
            };
        case GET_CHAT_DIALOGUE_REJECTED :
            return {
                ...state,
                dialogueFetching: false,
                dialogueErrors: action.payload
            };
        default :
            return state;
    }
};

export default chat_reducer;
