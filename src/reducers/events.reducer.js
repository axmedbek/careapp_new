import {EVENTS_FULFILLED, EVENTS_PENDING, EVENTS_REJECTED} from "../actions/action_types/calendar.type";
import {extractDatesForCalendar} from "../helpers/standard.helper";

const initialState = {
    events: [],
    dates: {},
    event: {},
    errors: {},
    fetching: false,
    count: 0,
    description: "",
    limit: 0,
    skip: 0
};

const event_reducer = (state = initialState, action) => {
    switch (action.type) {
        case EVENTS_PENDING :
            return {
                ...state,
                errors: {},
                fetching: true
            };
        case EVENTS_FULFILLED :
            return {
                ...state,
                fetching: false,
                events: action.payload.data.data,
                dates: extractDatesForCalendar(action.payload.data.data),
                count: action.payload.data.count,
                description: action.payload.data.description,
                limit: action.payload.data.limit,
                skip: action.payload.data.skip,
            };
        case EVENTS_REJECTED :
            return {
                ...state,
                fetching: false,
                errors: action.payload
            };
        default :
            return state;
    }
};

export default event_reducer;
