import React from 'react';
import {View} from "react-native";
import SimpleHeader from "../components/headers/SimpleHeader";
import {getDeviceUser} from "../config/caching/actions/setting";
import {truncateText} from "../helpers/standard.helper";

const AuthenticatedScreenHOC = ({ title,navigation,menuIcon,backIcon,backRoute,children }) => {

    return (
        <View style={{flex : 1}}>
            <SimpleHeader uri={{uri : getDeviceUser().avatar}} title={truncateText(title,20)} navigation={navigation}
                          menuIcon={menuIcon} backIcon={backIcon} backRoute={backRoute}/>
            {children}
        </View>
    );
};


export default AuthenticatedScreenHOC;
