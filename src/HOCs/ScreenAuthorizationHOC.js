import React,{useEffect} from 'react';
import {getAccessToken} from "../config/caching/actions/token";

const ScreenAuthorizationHoc = ({ navigation,children }) => {
    useEffect(() => {
        if (!getAccessToken()) {
            navigation.navigate('Login');
        }
    },[]);

    return children;
};

export default ScreenAuthorizationHoc;
