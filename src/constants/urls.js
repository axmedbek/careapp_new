// auth
export const LOGIN_URL = 'login';
export const PROFILE_INFO = 'profile/info';
export const PROFILE_AVATAR = 'profile/avatar';
export const PROFILE_PASSWORD = 'profile/password';
export const PROFILE_LOGOUT = 'profile/logout';

export const ACCOUNT_SET_DEVICE = '/account/setdevice';
export const NOTIFICATION_LIST = '/notifications/list';

// calendar
export const GET_EVENTS_URL = "todos";
export const CREATE_EVENT_URL = "todos";
export const EDIT_EVENT_URL = "todos";
export const GET_EVENT_INFO_URL = "todos";
export const DELETE_EVENTS_URL = "todos";

// notes
export const GET_FOLDERS_URL = "notefolders";
export const CREATE_FOLDER_URL = "notefolders";
export const EDIT_FOLDER_URL = "notefolders";
export const DELETE_FOLDER_URL = "notefolders";

export const EDIT_NOTE_CATEGORY_URL = "/notefolders/edit";

export const GET_NOTES_URL = "/notes/list";
export const CREATE_NOTE_URL = "/notes/create";
export const DELETE_NOTE_URL = "/notes/delete";
export const EDIT_NOTE_URL = "/notes/edit";


// case
export const GET_CASES_MIN_URL = "casesmin";
export const GET_CASES_URL = "cases";
export const GET_CASE_URL = "casesinfo/:case";
export const GET_CASE_PLANS_URL = "cases/:case/tradingplans";
export const GET_CASE_RECORDS_URL = "cases/:case/timerecords";
export const GET_CASE_QUESTIONS_URL = "cases/:case/timerecordings/popupquestions";
export const GET_CASE_ACTIVITIES_URL = "cases/:case/activities";
export const GET_ACTIVITIES_BUDGET_URL = "cases/:case/lastbudgetdate";
export const CREATE_PLAN_URL = "tradingplans/add";
export const CREATE_ACTIVITY_URL = "cases/:case/activities";
export const GET_CONTACTS_URL = "contacts";
export const TOGGLE_TIMER_URL = "cases/:case/timerecords/toggle";
export const GET_TIMER_URL = "cases/:case/timerecords/status";

export const GET_CASE_NOTES_URL = "cases/:case/notes";
export const GET_PLAN_NOTES_URL = "cases/:case/tradingplans/:plan/notes";

export const UPDATE_TIMER_URL = "cases/:case/timerecords/:id";


// data
export const GET_CATEGORIES_URL = "data/todocategories";
export const GET_MODERATORS_LIST = "users";
export const GET_GOALS_URL = "data/goals";
export const GET_PLAN_STATUSES_URL = "data/tradingplanstatuses";
export const UPDATE_TRADING_PLAN = "cases/:case/tradingplans";
export const GET_ACTIVITY_STATUSES_URL = "data/activitystatuses";
export const GET_SESSION_TYPES_URL = "data/sessiontypes";


export const ADD_CONTACT_URL = "contacts";
export const EDIT_CONTACT_URL = "contacts";
export const DELETE_CONTACT_URL = "contacts";


// file
export const UPLOAD_FILE_URL = "/files/upload";



export const CHAT_LIST_URL = 'chat/list';
export const CHAT_CONTACT_LIST_URL = 'chat/contact-list';
export const CHAT_SEND_MESSAGE_URL = 'chat/message/send';
export const CHAT_CREATE_GROUP_URL = 'chat/create/group';
export const CHAT_MESSAGES_URL = 'chat/messages';
export const CHAT_MESSAGES_WITH_USER_ID_URL = 'chat/users';
export const CHAT_PROFILE_URL = 'chat/profile';


export const CONTRACTS_URL = 'profiles/:id/contracts';




