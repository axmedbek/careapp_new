// settings here

import {Platform,Dimensions} from "react-native";
const platform = Platform.OS;

export const REGULAR_FONT = platform === "ios" ? "System" : "Roboto";
export const BOLD_FONT = platform === "ios" ? "System" : "Roboto";
export const SCREEN_WIDTH = Dimensions.get('window').width - 50;
export const SCREEN_HEIGHT = Dimensions.get('window').height - 50;
export const SCREEN_WIDTH_ORIGINAL = Dimensions.get('window').width;

export function fontSizer(length,fontSize) {
    if (length > 24) {
        return fontSize - 10;
    } else if (length > 16) {
        return fontSize - 5;
    } else {
        return fontSize;
    }
}

export function marginToper(length,top) {
    if (length > 24) {
        return top + 10;
    } else if (length > 16) {
        return top + 5;
    } else {
        return top;
    }
}
