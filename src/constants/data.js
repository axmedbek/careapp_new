export const types = [
    {
        id: "event",
        title: "Begivenhed",
    },
    {
        id: "todo",
        title: "Arbejdsopgave",
    },
];

export const statuses = [
    {
        value: 1,
        label: "Ikke startet",
        html_code: "#ff0000",
    },
    {
        value: 2,
        label: "Igangværende",
        html_code: "#ffc400",
    },
    {
        value: 3,
        label: "Afsluttet",
        html_code: "#1dc151",
    },
];
