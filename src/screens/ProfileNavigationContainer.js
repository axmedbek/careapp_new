import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {handleProfileRoutes} from "../routes/AppRoutes";
const Stack = createStackNavigator();

const ProfileNavigationContainer = () => {
    return (
        <Stack.Navigator>
            {
                handleProfileRoutes.map((route, index) => (
                    <Stack.Screen
                        key={++index}
                        name={route.name}
                        component={route.component}
                        options={{
                            headerShown: false,
                        }}
                    />
                ))
            }
        </Stack.Navigator>
    );
};

export default ProfileNavigationContainer;
