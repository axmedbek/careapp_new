import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {connect} from "react-redux";
import Icon from "react-native-vector-icons/AntDesign";
import {MAIN_COLOR} from "../constants/colors";
import {getBottomTitleName} from "../helpers/standard.helper";
import {bottomRoutes} from "../routes/AppRoutes";
import ScreenAuthorizationHoc from "../HOCs/ScreenAuthorizationHOC";

const Tab = createBottomTabNavigator();

const BottomNavigationContainer = ({navigation,lang}) => {

    return (
        <ScreenAuthorizationHoc navigation={navigation}>
            <Tab.Navigator
                tabBarOptions={{
                    activeTintColor: MAIN_COLOR,
                    inactiveTintColor: '#7c7c7c',
                    labelStyle: {
                        fontSize: 13
                    },
                    style: {
                        height: 60
                    }
                }}
            >
                {
                    bottomRoutes.map((item,key) => (
                        <Tab.Screen
                            key={++key}
                            name={item.name}
                            component={item.component}
                            options={{
                                tabBarLabel: getBottomTitleName(lang.messages,item.label),
                                tabBarIcon: ({color,size}) => (
                                    <Icon name={item.icon} color={color} style={{fontSize: 24}}/>
                                )
                            }}
                        />
                    ))
                }
            </Tab.Navigator>
        </ScreenAuthorizationHoc>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BottomNavigationContainer);
