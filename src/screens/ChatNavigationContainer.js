import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {handleChatRoutes} from "../routes/AppRoutes";
const Stack = createStackNavigator();

const ChatNavigationContainer = () => {
    return (
        <Stack.Navigator>
            {
                handleChatRoutes.map((route, index) => (
                    <Stack.Screen
                        key={++index}
                        name={route.name}
                        component={route.component}
                        options={{
                            headerShown: false,
                        }}
                    />
                ))
            }
        </Stack.Navigator>
    );
};

export default ChatNavigationContainer;
