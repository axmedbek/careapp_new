import React, {useEffect} from 'react';
import {createDrawerNavigator} from "@react-navigation/drawer";
import Home from "../pages/HomePages/Home";
import Contact from "../pages/HomePages/Contract";
import Guidelines from "../pages/HomePages/Guidelines";
import Values from "../pages/HomePages/Values";
import {connect} from "react-redux";
import CustomDrawerContent from "../components/drawer/CustomDrawerContent";
import {changeNotificationStatus} from "../actions/setting.action";

const Drawer = createDrawerNavigator();

const HomeScreenNavigator = ({navigation, changeNotificationStatus, lang}) => {
    useEffect(() => {
        changeNotificationStatus(true);
        navigation.addListener('focus', () => {
            changeNotificationStatus(true);
        });
    }, []);
    return (
        <Drawer.Navigator
            initialRouteName="Home"
            drawerType={'back'}
            drawerContent={(props) => <CustomDrawerContent {...props} />}
        >
            <Drawer.Screen name={"Home"} options={{
                title: lang.messages.home.title,
            }} component={Home}/>
            <Drawer.Screen name={"Contract"} options={{
                title: lang.messages.home.contract,
            }} component={Contact}/>
            <Drawer.Screen name={"Guidelines"} options={{
                title: lang.messages.home.guidelines,
            }} component={Guidelines}/>
            <Drawer.Screen name={"Values"} options={{
                title: lang.messages.home.values,
            }} component={Values}/>
        </Drawer.Navigator>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {
    changeNotificationStatus
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HomeScreenNavigator);
