import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {appAuthRoutes} from "../routes/AppRoutes";
import PushNotification from "react-native-push-notification";
import {connect} from "react-redux";

const Stack = createStackNavigator();

const ScreenNavigationContainer = ({ notification_status }) => {
    useEffect(() => {
        if (!notification_status) {
            console.log("disabled");
            PushNotification.abandonPermissions();
        }
    },[notification_status]);

    return (
        <NavigationContainer>
            <Stack.Navigator>
                {
                    appAuthRoutes.map((route, index) => (
                        <Stack.Screen
                            key={++index}
                            name={route.name}
                            component={route.component}
                            options={{
                                headerShown: false,
                            }}
                        />
                    ))
                }
            </Stack.Navigator>
        </NavigationContainer>
    );
};

const mapStateToProps = ({settingReducer}) => {
    return {
        notification_status: settingReducer.notification_status
    };
};

const mapDispatchToProps = {

};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ScreenNavigationContainer);
