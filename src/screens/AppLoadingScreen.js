import React, {useState, useEffect} from 'react';
import BottomNavigationContainer from "./BottomNavigationContainer";
import SimpleLoadingComponent from "../components/loadings/SimpleLoadingComponent";
import {getAccessToken} from "../config/caching/actions/token";
import {getValueFromSettingWithKey} from "../config/caching/actions/setting";


const AppLoadingScreen = ({navigation}) => {
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            if (!getAccessToken()) {
                navigation.navigate('Login');
            } else {
                if (getValueFromSettingWithKey("pinCode")) {
                    navigation.navigate('PinLogin');
                } else if (getValueFromSettingWithKey("fingerprint")) {
                    navigation.navigate('PinLogin');
                } else {
                    setLoading(false);
                }
            }
        }, 2000);
    }, []);

    if (!loading) {
        return <BottomNavigationContainer navigation={navigation}/>
    }
    return <SimpleLoadingComponent loading={loading}/>
};

export default AppLoadingScreen;
