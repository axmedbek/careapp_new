import React from 'react';
import {createDrawerNavigator} from "@react-navigation/drawer";
import {connect} from "react-redux";
import CustomDrawerContent from "../components/drawer/CustomDrawerContent";
import PersonOverview from "../pages/PeoplePages/PersonOverview";
import Economy from "../pages/PeoplePages/Economy";
import Timeregister from "../pages/PeoplePages/Timeregister";
import Contacts from "../pages/PeoplePages/Contacts";
import PersonInfo from "../pages/PeoplePages/PersonInfo";
import ContactInfo from "../pages/PeoplePages/ContactInfo";
import Notes from "../pages/PeoplePages/Notes";
import HandlePlanNavigationContainer from "./HandlePlanNavigationContainer";

const Drawer = createDrawerNavigator();

const PersonOverviewNavigationContainer = ({route,lang}) => {
    const { item } = route.params;

    return (
        <Drawer.Navigator
            drawerType={'back'}
            drawerPosition={"right"}
            drawerContent={(props) => <CustomDrawerContent {...props} />}
        >
            <Drawer.Screen name={"PersonOverview"} options={{
                title: lang.messages.personoverview.title,
            }} component={PersonOverview} initialParams={{item: item}}/>
            <Drawer.Screen name={"HandlePlanNavigation"} options={{
                title: lang.messages.personoverview.handleplan,
            }} component={HandlePlanNavigationContainer} initialParams={{item: item}}/>
            <Drawer.Screen name={"Economy"} options={{
                title: lang.messages.personoverview.economy,
            }} component={Economy} initialParams={{item: item}}/>
            <Drawer.Screen name={"Timeregister"} options={{
                title: lang.messages.personoverview.timeregister,
            }} component={Timeregister} initialParams={{item: item}}/>
            <Drawer.Screen name={"Contacts"} options={{
                title: lang.messages.personoverview.contacts,
            }} component={Contacts} initialParams={{item: item}}/>

            <Drawer.Screen name={"PersonalInfo"} options={{
                title: lang.messages.personoverview.info,
            }} component={PersonInfo} initialParams={{item: item}}/>

            <Drawer.Screen name={"ContactInfo"} options={{
                title: lang.messages.personoverview.contact_info,
            }} component={ContactInfo} initialParams={{item: item}}/>

            <Drawer.Screen name={"PersonNotes"} options={{
                title: lang.messages.personoverview.notes,
            }} component={Notes} initialParams={{item: item}}/>
        </Drawer.Navigator>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PersonOverviewNavigationContainer);
