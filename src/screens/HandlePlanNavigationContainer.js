import React from 'react';
import {handlePlanRoutes} from "../routes/AppRoutes";
import {createStackNavigator} from "@react-navigation/stack";
const Stack = createStackNavigator();

const HandlePlanNavigationContainer = ({ route }) => {
    const {item} = route.params;

    return (
        <Stack.Navigator>
            {
                handlePlanRoutes.map((route, index) => (
                    <Stack.Screen
                        key={++index}
                        name={route.name}
                        component={route.component}
                        initialParams={{item: item}}
                        options={{
                            headerShown: false,
                        }}
                    />
                ))
            }
        </Stack.Navigator>
    );
};

export default HandlePlanNavigationContainer;
