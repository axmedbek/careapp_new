import {StyleSheet} from 'react-native';
import {
    DIVIDER_COLOR,
    RED_COLOR,
    TEXT_COLOR
} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',
        height: 'auto',
        paddingTop: 2,
        paddingLeft: 5,
        paddingBottom: 5,
        textAlign: 'center',
        alignItems: 'center'
    },
    header: {
        fontSize: 20,
        fontFamily: REGULAR_FONT,
        color: TEXT_COLOR,
        textAlign: 'center',
    },
    input: {
        borderWidth: 1,
        width: '100%',
        borderRadius: 4,
        borderColor: DIVIDER_COLOR,
        marginTop: 20
    },
    text: {
        borderWidth: 1,
        width: '100%',
        borderRadius: 4,
        borderColor: DIVIDER_COLOR,
        marginTop: 20,
        height: 150
    },
    btnContainer: {
        flexDirection: 'row'
    },
    error: {
        borderWidth: 1,
        borderColor: RED_COLOR,
        color: RED_COLOR,
    },
    errorTxt: {
        paddingLeft: 10,
        paddingTop: 10,
        color: RED_COLOR
    }
});

export default styles;
