import React, {useState} from 'react';
import {View, Text, TextInput} from "react-native";
import styles from './style';
import StandardButton from "../../../components/buttons/StandardButton";
import {LIGHT_COLOR} from "../../../constants/colors";
import {concatObjects, isEmptyObject} from "../../../helpers/standard.helper";
import {Textarea, Toast} from 'native-base';

const NoteAddForm = ({hideModal, noteErrors, updateNote, addNote, item, folder_id, plan_id, event_id, case_id, contract_id}) => {
    const [title, setTitle] = useState(item ? item.title : '');
    const [text, setText] = useState(item ? item.description : '');

    const handleAddNote = () => {
        if (title.length < 1) {
            Toast.show({
                position: 'bottom',
                text: "Title is required!",
                buttonText: "OK",
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 5000
            });
        } else {
            if (item) {
                if (folder_id) {
                    updateNote({
                        description: text,
                        title: title,
                        id: item._id,
                        folder_id: folder_id
                    });
                } else if (contract_id) {
                    updateNote({
                        description: text,
                        title: title
                    });
                } else {
                    updateNote({
                        description: text,
                        title: title,
                        id: item._id,
                        folder_id: 1
                    });
                }
            } else {
                if (folder_id) {
                    addNote({
                        description: text,
                        title: title,
                        folder_id: folder_id
                    });
                } else if (plan_id) {
                    addNote(plan_id, {
                        description: text,
                        title: title
                    });
                } else if (event_id) {
                    addNote({
                        description: text,
                        title: title,
                        todo_id: event_id
                    });
                } else if (case_id) {
                    addNote(case_id, {
                        description: text,
                        title: title
                    });
                } else if (contract_id) {
                    addNote(contract_id, {
                        description: text,
                        title: title
                    });
                }
            }
        }
    };

    return (
        <View style={styles.container}>
            <TextInput placeholder={"Title"}
                       style={concatObjects(styles.input, !isEmptyObject(noteErrors) ? styles.error : {})} value={title}
                       onChangeText={e => setTitle(e)}/>
            <Textarea style={concatObjects(styles.text, !isEmptyObject(noteErrors) ? styles.error : {})} rowSpan={5}
                      bordered placeholder="Description" value={text} onChangeText={e => setText(e)}/>
            <Text style={styles.errorTxt}>{noteErrors.description}</Text>
            <View style={styles.btnContainer}>
                <StandardButton handleButton={hideModal} text={"ANNULER"}
                                style={{width: '46%', backgroundColor: LIGHT_COLOR}}
                                textStyle={{fontSize: 16, fontWeight: 'bold'}}/>
                <StandardButton text={"ACCEPTER"} style={{width: '46%'}}
                                handleButton={handleAddNote}
                                textStyle={{fontSize: 16, fontWeight: 'bold'}}/>
            </View>
        </View>
    );
};

export default NoteAddForm;
