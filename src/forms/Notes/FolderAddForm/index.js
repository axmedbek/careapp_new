import React from 'react';
import {View} from "react-native";
import {Button,Text} from "native-base";
import Input from "../../../components/inputs/Input";
import { Formik } from "formik";
import validation from "./validation";

const FolderAddForm = ({hideModal, addFolder,updateFolder, item, loading}) => {

    const handleAddNote = (values) => {
        if (item) {
            values.id = item._id;
            updateFolder(values)
        }
        else {
            addFolder(values);
        }
    };

    return (
        <Formik
            validationSchema={validation}
            initialValues={{
                title: item ? item.title : '',
            }}
            onSubmit={handleAddNote}
        >
            {({handleChange, handleBlur, handleSubmit, values, isValid}) => (
                <View>
                    <Input
                        onChangeText={handleChange("title")}
                        onBlur={handleBlur("title")}
                        value={values.title}
                        name="title"
                    />

                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "flex-end",
                            marginTop: -20
                        }}
                    >
                        <Button dark transparent onPress={hideModal}>
                            <Text>Annuler</Text>
                        </Button>

                        <Button success transparent disabled={!isValid || loading} onPress={handleSubmit}>
                            <Text>Accepter</Text>
                        </Button>
                    </View>
                </View>
            )}
        </Formik>
    );
};

export default FolderAddForm;
