import {StyleSheet} from 'react-native';
import {LIGHT_COLOR, RED_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: 300,
        height: 250,
        paddingTop: 20,
        paddingLeft: 5,
        textAlign: 'center',
        alignItems: 'center'
    },
    header: {
        fontSize: 20,
        fontFamily: REGULAR_FONT,
        color: TEXT_COLOR,
        textAlign: 'center',
    },
    input: {
        borderWidth: 1,
        width: '95%',
        borderRadius: 4,
        borderColor: LIGHT_COLOR,
        marginTop: 20
    },
    btnContainer: {
        flexDirection: 'row'
    },
    error: {
        borderWidth: 1,
        borderColor: RED_COLOR,
        color: RED_COLOR,
    },
    errorTxt: {
        paddingLeft: 10,
        paddingTop: 10,
        color: RED_COLOR
    }
});

export default styles;
