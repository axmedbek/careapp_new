import React,{useState} from 'react';
import {ScrollView} from "react-native";
import StandardInputComponent from "../../components/inputs/StandardInputComponent";
import StandardButton from "../../components/buttons/StandardButton";
import {createOrEditContact} from "../../services/case.service";
import {Toast} from "native-base";

const ContactAddEditForm = ({ contact,case_id,setOperationLoading,getContacts,setModal,lang }) => {

    const [title, setTitle] = useState(contact ? contact.title : '');
    const [titleError, setTitleError] = useState(false);
    const [firstName, setFirstName] = useState(contact ? contact.firstname : '');
    const [firstNameError, setFirstNameError] = useState(false);
    const [lastName, setLastName] = useState(contact ? contact.lastname : '');
    const [lastNameError, setLastNameError] = useState(false);
    const [phone, setPhone] = useState(contact ? contact.phone : '');
    const [phoneError, setPhoneError] = useState(false);
    const [email, setEmail] = useState(contact ? contact.email : '');

    const handleButton = () => {
        if (title.length < 1) {
            setTitleError(true);
        }

        if (firstName.length < 1) {
            setFirstNameError(true);
        }

        if (lastName.length < 1) {
            setLastNameError(true);
        }

        if (phone.length < 1) {
            setPhoneError(true);
        }


        if (title.length > 0 && firstName.length > 0 && lastName.length > 0 && phone.length > 0) {
            setOperationLoading(true);
            let id = contact ? ( contact.id > 0 ? contact.id : contact.user_id ) : null;
            createOrEditContact(id,title,firstName,lastName,phone,email,response => {
                Toast.show({
                    position: 'top',
                    text: response.data.description,
                    buttonText: lang.messages.common.ok,
                    type: 'success',
                    textStyle: {marginLeft: 10},
                    duration: 3000
                });
                setModal(false);
                setOperationLoading(false);
                getContacts(case_id);
            },(error) => {
                console.log(error.response);
                Toast.show({
                    position: 'top',
                    text: 'Oops.Something went wrong!',
                    buttonText: lang.messages.common.ok,
                    type: 'danger',
                    textStyle: {marginLeft: 10},
                    duration: 3000
                });
                setOperationLoading(false);
            });
        }
    };

    return (
        <ScrollView style={{paddingBottom: 20,paddingLeft: 10}}>
            <StandardInputComponent inputStyle={{width: '100%'}}
                                    valueError={titleError} setValueError={setTitleError}
                                    placeholder={"Title pa person"} value={title} setValue={setTitle}/>
            <StandardInputComponent inputStyle={{width: '100%'}}
                                    valueError={firstNameError} setValueError={setFirstNameError}
                                    placeholder={"Fornavn"} value={firstName} setValue={setFirstName}/>
            <StandardInputComponent inputStyle={{width: '100%'}}
                                    valueError={lastNameError} setValueError={setLastNameError}
                                    placeholder={"Efternavn"} value={lastName} setValue={setLastName}/>
            <StandardInputComponent inputStyle={{width: '100%'}}
                                    valueError={phoneError} setValueError={setPhoneError}
                                    placeholder={"Telefon"} type={"number"} value={phone} setValue={setPhone}/>
            <StandardInputComponent inputStyle={{width: '100%'}}
                                    placeholder={"Email"} value={email} setValue={setEmail}/>
            <StandardButton text={"Gem"} handleButton={handleButton} style={{marginLeft: -4}}/>
        </ScrollView>
    );
};

export default ContactAddEditForm;
