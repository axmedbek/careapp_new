import React, {useState} from 'react';
import {View} from "react-native";
import SimplePicker from "../../../components/selects/SimpleSelect";
import {convertGoalsForSelect, convertToSelectData} from "../../../helpers/standard.helper";
import StandardButton from "../../../components/buttons/StandardButton";

const PeopleTimeRegisterUpdate = ({lang, updateTimeRegister, sessionTypes, contacts}) => {

    const [typeValue, setTypeValue] = useState(null);
    const [typeValueError, setTypeValueError] = useState(false);
    const [contactValue, setContactValue] = useState(null);
    const [contactValueError, setContactValueError] = useState(false);

    const handleSubmitTimeRegister = () => {
        if (!typeValue) {
            setTypeValueError(true);
        } else {
            setTypeValueError(false);
        }

        if (typeValue === 1) {
            if (!contactValue) {
                setContactValueError(true);
            } else {
                setContactValueError(false);
            }
        }


        if (typeValue) {
            if (typeValue === 1 && !contactValue) {
                return;
            }
            updateTimeRegister(typeValue, contactValue);
        }
    };

    return (
        <View style={{width: 280, borderRadius: 0}}>
            <SimplePicker data={convertGoalsForSelect(sessionTypes)}
                          labelText={lang.messages.common.choose_type}
                          selectedValue={typeValue} setSelectedValue={setTypeValue}
                          placeHolder={"Choose a type"}
                          hasError={typeValueError}
            />
            {
                typeValue === 1 &&
                <SimplePicker data={convertToSelectData(contacts, true)}
                              labelText={lang.messages.common.choose_contact}
                              selectedValue={contactValue}
                              setSelectedValue={setContactValue}
                              placeHolder={"Choose a contact"}
                              hasError={contactValueError}
                />
            }
            <StandardButton text={"Gem"} handleButton={handleSubmitTimeRegister}/>
        </View>
    );
};

export default PeopleTimeRegisterUpdate;
