import {httpClientFileUpload, httpClientWithoutDispatch, httpClientWithoutDispatchForm} from "../helpers/HttpClient";
import {CHAT_CREATE_GROUP_URL, CHAT_SEND_MESSAGE_URL} from "../constants/urls";


export function chatFileSend(data, successCallback, errorCallback) {
    return httpClientFileUpload(`${CHAT_SEND_MESSAGE_URL}`, data).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}

export function chatMessageSend(data, successCallback, errorCallback) {
    return httpClientWithoutDispatch(`${CHAT_SEND_MESSAGE_URL}`, 'post',
        data).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}


export function createChatGroup(data, successCallback, errorCallback) {
    return httpClientFileUpload(`${CHAT_CREATE_GROUP_URL}`, data)
        .then(response => {
            successCallback(response);
        }).catch(response => {
            errorCallback(response);
        });
}
