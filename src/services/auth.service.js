import {httpClientFileUpload, httpClientPostWithoutDispatch, httpClientWithoutDispatch} from "../helpers/HttpClient";
import {LOGIN_URL, PROFILE_AVATAR, PROFILE_INFO, PROFILE_LOGOUT, PROFILE_PASSWORD} from "../constants/urls";

export function loginService(data, successCallback, errorCallback) {
    return httpClientPostWithoutDispatch(`${LOGIN_URL}`, data).then(response => {
        successCallback(response);
    }).catch(error => {
        errorCallback(error);
    });
}

export function logoutService() {
    return httpClientWithoutDispatch(`${PROFILE_LOGOUT}`, 'post');
}


export function updateProfileInfo(data, successCallback, errorCallback) {
    return httpClientWithoutDispatch(`${PROFILE_INFO}`, 'post', data).then(response => {
        successCallback(response);
    }).catch(error => {
        errorCallback(error);
    });
}


export function updateProfileAvatar(data, successCallback, errorCallback) {
    return httpClientFileUpload(`${PROFILE_AVATAR}`, data).then(response => {
        successCallback(response);
    }).catch(error => {
        errorCallback(error);
    });
}


export function updatePassword(data, successCallback, errorCallback) {
    return httpClientWithoutDispatch(`${PROFILE_PASSWORD}`, 'post', data).then(response => {
        successCallback(response);
    }).catch(error => {
        errorCallback(error);
    });
}
