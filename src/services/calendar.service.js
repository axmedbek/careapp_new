import {httpClientWithoutDispatch} from "../helpers/HttpClient";
import {CREATE_EVENT_URL, EDIT_EVENT_URL, GET_EVENT_INFO_URL} from "../constants/urls";
import {getLanguage} from "../config/caching/actions/setting";

export function createCalendarEvent(data, successCallback, errorCallback) {

    let params = {},
        method = 'post',
        url = `${CREATE_EVENT_URL}`;
    params.type = data.type;
    params.title = data.title;
    params.category = data.category;
    params.description = data.description;
    params.case = data.case;
    params.startdate = data.startdate;
    params.starttime = data.starttime;
    params.enddate = data.enddate;
    params.endtime = data.endtime;
    params.status = data.status;

    params.citizen = data.citizen;
    params.partner = data.partner;
    params.moderator = data.moderator;
    params.lang = getLanguage();


    if (data.id) {
        url = `${EDIT_EVENT_URL}/${data.id}`;
        method = 'put';
    }

    return httpClientWithoutDispatch(url, method, params)
        .then(response => {
            successCallback(response);
        }).catch(response => {
            errorCallback(response, false);
        });
}


export function getCalendarInfo(id, successCallback, errorCallback) {
    return httpClientWithoutDispatch(`${GET_EVENT_INFO_URL}/${id}`,
        'get', null)
        .then(response => {
            successCallback(response);
        }).catch(response => {
            errorCallback(response, false);
        });
}
