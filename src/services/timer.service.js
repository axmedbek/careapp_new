import {httpClientWithoutDispatch} from "../helpers/HttpClient";
import {GET_TIMER_URL, TOGGLE_TIMER_URL, UPDATE_TIMER_URL} from "../constants/urls";

export function getTimeService(id) {
    let url = GET_TIMER_URL.replace(":case", id);
    return httpClientWithoutDispatch(`${url}`, 'get', null);
}

export function timeToggle(id, longitude, latitude, successCallback, errorCallback) {
    let url = TOGGLE_TIMER_URL.replace(":case", id);

    console.log(url);
    return httpClientWithoutDispatch(url, 'post', {
        longitude: longitude,
        latitude: latitude
    })
        .then(response => {
            successCallback(response);
        })
        .catch(error => {
            errorCallback(error);
        });
}


export function updateTimerRegister(case_id, sessionId, type, contact, successCallback, errorCallback) {
    let url = UPDATE_TIMER_URL.replace(":case", case_id).replace(":id", sessionId);

    return httpClientWithoutDispatch(url, 'put', {
        type: type,
        contact: contact
    }).then(response => {
        successCallback(response);
    }).catch(error => {
        errorCallback(error);
    });
}


export function getTimerService(id, successCallback, errorCallback) {
    let url = GET_TIMER_URL.replace(":case", id);

    console.log(url);

    return httpClientWithoutDispatch(url, 'get', null).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}
