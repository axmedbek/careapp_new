import {httpClientWithoutDispatch} from "../helpers/HttpClient";

export function signContract(id) {
    return httpClientWithoutDispatch(`profiles/1/contracts/${id}/sign`, 'post', null)
}


export function updateSignContract(data) {
    return httpClientWithoutDispatch(`signature/1`, 'put', data)
}

export function getSignContract() {
    return httpClientWithoutDispatch(`signature/1`, 'get', null)
}
