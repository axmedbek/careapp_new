import {httpClientFileUpload, httpClientWithoutDispatch, httpClientWithoutDispatchForm} from "../helpers/HttpClient";
import {
    ADD_CONTACT_URL,
    CREATE_ACTIVITY_URL,
    DELETE_CONTACT_URL,
    EDIT_CONTACT_URL, GET_ACTIVITIES_BUDGET_URL,
    GET_CASE_PLANS_URL
} from "../constants/urls";
import axios from "axios";
import {getAccessToken} from "../config/caching/actions/token";


export function updatePlanStatus(case_id, id, type, successCallback, errorCallback) {
    let url = GET_CASE_PLANS_URL.replace(":case", case_id);

    return httpClientWithoutDispatch(`${url}/${id}`, 'put', {
        status: type
    }).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response);
    });
}


export function createPlan(id, goal, title, status, successCallback, errorCallback) {
    let url = GET_CASE_PLANS_URL.replace(":case", id);
    return httpClientWithoutDispatch(url, 'post',
        {
            status: status,
            goal: goal,
            plan: title
        }).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}

export function createNewActivity(case_id, data, successCallback, errorCallback) {
    let url = CREATE_ACTIVITY_URL.replace(":case", case_id);
    return httpClientFileUpload(url, data).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}


export function createOrEditContact(id, title, firstname, lastname, phone, email, successCallback, errorCallback) {
    let url = id ? EDIT_CONTACT_URL + "/" + id : ADD_CONTACT_URL;

    return httpClientWithoutDispatch(`${url}`, id ? 'put' : 'post',
        {
            title: title,
            firstname: firstname,
            lastname: lastname,
            phone: phone,
            email: email
        }).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}


export function deleteContact(id, successCallback, errorCallback) {
    return httpClientWithoutDispatch(`${DELETE_CONTACT_URL}/${id}`, 'delete',
        null).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}


export function caseSurveyUpdate(case_id, answers, successCallback, errorCallback) {
    const formData = new FormData();
    answers.map(answer => (
        formData.append('answers[]', answer)
    ));

    return httpClientWithoutDispatchForm(`cases/${case_id}/timerecords/popUpQuestionsAnswers`,
        'post', formData).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}


export function caseActivityLastBudgetDate(id, successCallback, errorCallback) {
    let url = GET_ACTIVITIES_BUDGET_URL.replace(":case", id);

    return httpClientWithoutDispatch(url, 'get', null)
        .then(response => {
            successCallback(response);
        }).catch(response => {
            errorCallback(response, false);
        });
}
