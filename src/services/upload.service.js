import {httpClientFileUpload} from "../helpers/HttpClient";
import {UPLOAD_FILE_URL} from "../constants/urls";
import {getAccessToken} from "../config/caching/actions/token";

export function uploadActivityFile(image,successCallback, errorCallback) {

    const formData = new FormData();
    formData.append('token', getAccessToken());
    formData.append('for', 'case');
    formData.append('file', {
        uri: image,
        type: 'multipart/form-data',
        name: 'testname.jpg',
    });

    return httpClientFileUpload(`${UPLOAD_FILE_URL}`, formData).then(response => {
        if (response.data.status === "success") {
            successCallback(response);
        }
        else {
            errorCallback(response);
        }
    }).catch(error => {
        errorCallback(error);
    });
}
