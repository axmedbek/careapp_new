import {httpClientWithoutDispatch} from "../helpers/HttpClient";
import {DELETE_EVENTS_URL} from "../constants/urls";

export function deleteEventService(id, successCallback, errorCallback) {
    return httpClientWithoutDispatch(`${DELETE_EVENTS_URL}/${id}`,
        'delete', null).then(response => {
        successCallback(response);
    }).catch(response => {
        errorCallback(response, false);
    });
}
