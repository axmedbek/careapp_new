import {Alert, PermissionsAndroid, Platform} from "react-native";
import Geolocation from "@react-native-community/geolocation";
import axios from "axios";

const IOS = Platform.OS === "ios";

export async function getLocationPermissionService (successCallback, errorCallback) {
    if (!IOS) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Location permission',
                    message: 'App needs to access your location',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                successCallback();
            } else {
                errorCallback();
                Alert.alert(
                    'Could not get permission',
                    'You have to enable location before using this feature.',
                    [{text: 'OK'}],
                    {cancelable: false},
                );
            }
        } catch (err) {
            errorCallback();
            Alert.alert(
                'Could not get permission',
                'You have to enable location before using this feature.',
                [{text: 'OK'}],
                {cancelable: false},
            );
        }
    } else {
        successCallback();
    }
};


export function getLocation(successCallback,errorCallback) {
    Geolocation.getCurrentPosition(
        async (position) => {
            const {longitude, latitude} = position.coords;
            successCallback(longitude,latitude);
        }, () => {
            axios.get('https://ipinfo.io/geo')
                .then(async response => {
                    let loc = response.data.loc.split(','),
                        longitude = loc[1],
                        latitude = loc[0];
                    successCallback(longitude,latitude);
                })
                .catch(() => {
                    errorCallback();
                    Alert.alert(
                        'Could not get permission',
                        'You have to enable location before using this feature.',
                        [{text: 'OK'}],
                        {cancelable: false},
                    );
                });
        }, {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 3600000,
        });
}
