import {httpClientWithoutDispatch} from "../helpers/HttpClient";
import {GET_NOTES_URL} from "../constants/urls";

export function getNotesService(data) {
    return httpClientWithoutDispatch(`${GET_NOTES_URL}`, 'get', data);
}


export function deleteContractNote(id, note_id) {
    return httpClientWithoutDispatch(`profiles/1/contracts/${id}/notes/${note_id}`, 'delete', null)
}

export function updateContractNote(id, note_id, data) {
    return httpClientWithoutDispatch(`profiles/1/contracts/${id}/notes/${note_id}`, 'put', data)
}
