import React from 'react';
import styles from './styles';
import {Text, TouchableOpacity, View} from "react-native";
import SimpleAvatar from "../../avatars/SimpleAvatar";
import Icon from "react-native-vector-icons/dist/Octicons";
import {fontSizer, marginToper} from "../../../constants/setting";
import BackIcon from "../../icons/BackIcon";
import {concatObjects} from "../../../helpers/standard.helper";

const SimpleHeader = ({ uri,title,navigation,menuIcon = false,backIcon = false,backRoute}) => {
    return (
        <View style={styles.container}>
            <Text style={Object.assign({},styles.title,
                {fontSize: fontSizer(title.length,22),
                    marginTop: marginToper(title.length,18)},
                (!backIcon && !menuIcon) && {textAlign: 'left',marginLeft: 10}
            )}>{title}</Text>
            {
                (!backIcon || !menuIcon) &&
                <TouchableOpacity style={styles.avatar} onPress={() => navigation.navigate('Account')}>
                    <SimpleAvatar uri={uri}/>
                </TouchableOpacity>
            }
            {
                menuIcon &&
                <TouchableOpacity style={concatObjects(styles.menu,(!backIcon || !menuIcon) ? {left: 14} : {right: 14})}
                                  onPress={() => navigation.openDrawer()}>
                    <Icon style={styles.menuIcon} name={"grabber"}/>
                </TouchableOpacity>
            }
            {
                backIcon &&
                <TouchableOpacity style={styles.back} onPress={backRoute ? backRoute : () => navigation.goBack()}>
                    <BackIcon style={styles.backIcon}/>
                </TouchableOpacity>
            }
        </View>
    );
};

export default SimpleHeader;
