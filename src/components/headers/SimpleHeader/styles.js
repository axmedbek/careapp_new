import {StyleSheet} from "react-native";
import {BACK_ICON_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: '98%',
        height: 64,
        marginLeft: 6,
        marginTop: 6,
        backgroundColor: 'white',
        borderRadius: 4,

        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    avatar: {
        position: 'absolute',
        right: 6,
        marginTop: 6,
        marginBottom: 10,
        borderWidth: 4,
        borderRadius: 30,
        borderColor: '#eee'
    },
    title: {
        position: 'absolute',
        width: '100%',
        marginTop: 16,
        // fontFamily: REGULAR_FONT,
        textAlign: 'center',
        color: TEXT_COLOR
    },
    menu: {
        position: 'absolute',
        // left: 14,
        top: 10
    },
    back: {
        position: 'absolute',
        left: 14,
        top: 10
    },
    menuIcon: {
        fontSize: 46,
        color: TEXT_COLOR
    },
    backIcon: {
        fontSize: 46,
        color: TEXT_COLOR,
        marginLeft: -15
    }
});

export default styles;
