import React from 'react';
import styles from './styles';
import {Text} from "react-native";

const FormHeaderDescriptor = ({ text }) => {
    return <Text style={styles.text}>{text}</Text>
};

export default FormHeaderDescriptor;
