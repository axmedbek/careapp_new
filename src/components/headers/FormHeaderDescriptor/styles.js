import {StyleSheet} from "react-native";
import {BLACK_COLOR} from "../../../constants/colors";
import {BOLD_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    text: {
        fontFamily: BOLD_FONT,
        fontStyle: 'normal',
        fontSize: 18,
        lineHeight: 28,
        letterSpacing: 0.2,
        color: BLACK_COLOR,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        marginLeft: 10
    }
});

export default styles;
