import React from 'react';
import styles from './styles';
import {Text, View} from "react-native";

const HeaderTextHolder = ({ text }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{text}</Text>
        </View>
    );
};

export default HeaderTextHolder;
