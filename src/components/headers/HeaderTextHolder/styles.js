import {StyleSheet} from "react-native";
import {BLACK_COLOR} from "../../../constants/colors";
import {BOLD_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        width: '100%',
        top: 24
    },
    text: {
        fontFamily: BOLD_FONT,
        fontStyle: 'normal',
        fontSize: 24,
        lineHeight: 28,
        letterSpacing: 0.2,
        color: BLACK_COLOR,
        textAlign: 'center'
    }
});

export default styles;
