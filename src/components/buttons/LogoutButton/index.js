import React from 'react';
import {TouchableOpacity} from "react-native";
import styles from "./styles";
import Icon from "react-native-vector-icons/Octicons";

const LogoutButton = ({handleSignIn}) => {
    return (
        <TouchableOpacity onPress={handleSignIn} style={styles.container}>
            <Icon style={styles.icon} name={"sign-out"}/>
        </TouchableOpacity>
    );
};

export default LogoutButton;
