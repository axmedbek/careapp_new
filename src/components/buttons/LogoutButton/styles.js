import {StyleSheet} from "react-native";
import {BLACK_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        right: 4,
        width: 50,
        height: 50,
        margin: 15,
        backgroundColor: 'white',
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',

        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    icon: {
        fontSize: 25,
        color: BLACK_COLOR,
        marginLeft: 5,
        marginTop: 4,
    }
});

export default styles;
