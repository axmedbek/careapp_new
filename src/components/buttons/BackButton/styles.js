import {StyleSheet} from "react-native";
import {BLACK_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        width: 50,
        height: 50,
        margin: 15,
        backgroundColor: 'white',
        borderRadius: 25,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        fontSize: 25,
        color: BLACK_COLOR,
        marginRight: 4
    }
});

export default styles;
