import React from 'react';
import {TouchableOpacity} from "react-native";
import styles from "./styles";
import Icon from "react-native-vector-icons/Ionicons";

const BackButton = ({handleSignIn}) => {
    return (
        <TouchableOpacity onPress={handleSignIn} style={styles.container}>
            <Icon style={styles.icon} name={"ios-arrow-back"}/>
        </TouchableOpacity>
    );
};

export default BackButton;
