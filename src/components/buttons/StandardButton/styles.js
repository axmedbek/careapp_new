import {StyleSheet} from "react-native";
import {MAIN_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    loginBtn: {
        backgroundColor: MAIN_COLOR,
        marginTop: 15,
        margin: 6,
        borderRadius: 4,
        padding: 12
    },
    loginTxt: {
        color: 'white',
        fontFamily: REGULAR_FONT,
        fontSize: 18,
        textAlign: 'center'
    },
});

export default styles;
