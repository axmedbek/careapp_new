import React from 'react';
import {Text, TouchableOpacity} from "react-native";
import styles from "./styles";
import {concatObjects, increase_brightness} from "../../../helpers/standard.helper";
import {MAIN_COLOR} from "../../../constants/colors";

const StandardButton = ({ handleButton,disabled,text,textStyle,...rest }) => {
    return (
        <TouchableOpacity
            activeOpacity={disabled ? 1 : 0}
            onPress={disabled ? () => console.log("disabled") : handleButton}
                          style={concatObjects(styles.loginBtn,rest.style,
                              disabled ? {
                                  backgroundColor: increase_brightness(MAIN_COLOR,50)
                              } : {})}>
            <Text style={concatObjects(styles.loginTxt,textStyle)}>{text}</Text>
        </TouchableOpacity>
    );
};

export default StandardButton;
