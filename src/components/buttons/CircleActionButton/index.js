import React from 'react';
import {TouchableOpacity} from "react-native";
import styles from "./styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const CircleActionButton = ({handlePress,icon}) => {
    return (
        <TouchableOpacity onPress={handlePress} style={styles.container} activeOpacity={.8}>
            <Icon style={styles.icon} name={icon}/>
        </TouchableOpacity>
    );
};

export default CircleActionButton;
