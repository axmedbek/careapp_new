import {StyleSheet} from "react-native";
import {MAIN_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        width: 60,
        height: 60,
        bottom: 15,
        right: 10,
        backgroundColor: MAIN_COLOR,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',

        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    icon: {
        fontSize: 30,
        color: 'white'
    }
});

export default styles;
