import React from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import {HORIZONTAL_PADDING_SCREEN, LIGHT_COLOR} from "../../../constants/colors";
import Text from "../../text/Text";


const ActivityCircle = ({ topText, bottomText, radius }) => {
    const spacing = radius / 9;

    return (
        <View
            style={{
                padding: spacing,
                borderWidth: spacing,
                borderRadius: radius,
                width: radius * 2,
                height: radius * 2,
                borderColor: "#dfedcd",
                backgroundColor: "#b4d789",
                alignItems: "center",
                justifyContent: "center",
                marginHorizontal: HORIZONTAL_PADDING_SCREEN,
            }}
        >
            <View
                style={{
                    width: (radius - spacing * 2) * 2,
                    height: (radius - spacing * 2) * 2,
                    borderRadius: radius - spacing * 2,
                    backgroundColor: "#8ac147",
                    alignItems: "center",
                    justifyContent: "center",
                }}
            >
                <Text
                    center
                    style={{
                        color: 'white',
                        fontSize: 35,
                        fontWeight: "bold",
                    }}
                >
                    {topText}
                </Text>
                <Text
                    center
                    style={{
                        color: 'white',
                        fontSize: 22,
                    }}
                >
                    {bottomText}
                </Text>
            </View>
        </View>
    );
};

ActivityCircle.propTypes = {
    topText: PropTypes.string.isRequired,
    bottomText: PropTypes.string.isRequired,
    radius: PropTypes.number.isRequired,
};

export default ActivityCircle;
