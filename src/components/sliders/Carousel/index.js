import React, {useState} from 'react';
import {View, Text, ScrollView, Image, StyleSheet} from "react-native";
import {SCREEN_HEIGHT, SCREEN_WIDTH} from "../../../constants/setting";
import {increase_brightness} from "../../../helpers/standard.helper";
import {BACK_ICON_COLOR, BLUE_COLOR} from "../../../constants/colors";
import ZoomableImage from "../../images/ZoomableImage";

const Carousel = ({images}) => {
    const [currentScroll, setCurrentScroll] = useState(1);

    const handleScrollView = (e) => {
        // the current offset, {x: number, y: number}
        const position = e.nativeEvent.contentOffset;

        // page index
        const index = Math.round(position.x / SCREEN_WIDTH) + 1;

        if (index !== currentScroll) {
            setCurrentScroll(index);
        }
    };


    return (
        <View
            style={styles.scrollContainer}
        >
            <Text style={{
                alignSelf: 'center', textAlign: 'center', backgroundColor: BLUE_COLOR, color: 'white', width: 100,
                marginTop: 8, marginBottom: 8, borderRadius: 4, padding: 4
            }}>
                {currentScroll}/{images.length}
            </Text>
            <ScrollView
                onScroll={handleScrollView}
                horizontal
                pagingEnabled={true}
                showsHorizontalScrollIndicator={true}
                style={{height: 400}}
            >
                {images.map((image, index) => (
                    <Image key={++index}
                                   style={styles.image}
                                   source={{uri: image}}
                                   imageHeight={SCREEN_HEIGHT - 100}
                                   imageWidth={SCREEN_WIDTH + 50}/>
                ))}
            </ScrollView>
        </View>
    );
};

export default Carousel;


const styles = StyleSheet.create({
    scrollContainer: {
        height: SCREEN_HEIGHT,
        backgroundColor: increase_brightness(BACK_ICON_COLOR, 70)
    },
    image: {
        width: SCREEN_WIDTH + 50,
        height: SCREEN_HEIGHT - 100,
        resizeMode: 'contain'
    },
});
