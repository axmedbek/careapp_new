import React from 'react';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const TreeIcon = ({ ...rest }) => {
    return <Icon name={"file-tree"} {...rest}/>
};

export default TreeIcon;
