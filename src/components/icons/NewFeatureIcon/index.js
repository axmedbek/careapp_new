import React from 'react';
import Icon from "react-native-vector-icons/Foundation";
import {RED_COLOR} from "../../../constants/colors";

const NewFeatureIcon = () => {
    return <Icon name={"burst-new"} color={RED_COLOR} style={{fontSize: 20}}/>
};

export default NewFeatureIcon;
