import React from 'react';
import Icon from "react-native-vector-icons/dist/Entypo";

const BackIcon = ({ ...rest }) => {
    return <Icon name={"chevron-small-left"} {...rest}/>;
};

export default BackIcon;
