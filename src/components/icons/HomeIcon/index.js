import React from 'react';
import Icon from "react-native-vector-icons/dist/Octicons";

const HomeIcon = ({ item,styles }) => {
    return <Icon style={item.active ? styles.iconActive : styles.icon} name={"home"}/>
};

export default HomeIcon;
