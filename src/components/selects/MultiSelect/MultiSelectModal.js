import React from 'react';
import {FlatList, View} from "react-native";
import styles from './style';
import CoreModal from "../../modals/CoreModal";
import StandardButton from "../../buttons/StandardButton";
import MultiSelectItem from "./MultiSelectItem";
import Modal from "../../modals/Modal";
import {MAIN_COLOR} from "../../../constants/colors";


const MultiSelectModal = ({modalVisible, closeModal, title, data, selectAll, select, selected, check, itemCheck}) => {
    return (
        <Modal title={title} closeModal={() => closeModal(false)} modalVisible={modalVisible}>
            <View style={styles.modalContainer}>
                <FlatList
                    data={data}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={true}
                    style={styles.modalContent}
                    renderItem={({item, index}) =>
                        <MultiSelectItem index={index} item={item}
                                         selected={selected} selectAll={selectAll}
                                         select={select} check={check} itemCheck={itemCheck}
                        />}
                />
                <StandardButton text={"OK"} handleButton={() => closeModal(false)} style={styles.okBtn}
                textStyle={{color: MAIN_COLOR,fontSize: 14}}/>
            </View>
        </Modal>
    );
};

export default MultiSelectModal;
