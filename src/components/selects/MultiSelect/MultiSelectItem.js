import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./style";
import Icon from "react-native-vector-icons/MaterialIcons";

const MultiSelectItem = ({index, item, selected, select, selectAll, check,itemCheck}) => {
    return (
        <View>
            {
                !index &&
                <TouchableOpacity
                    onPress={selectAll}
                    style={styles.listItem}>
                    <View style={styles.check}/>
                    <Icon
                        name={'check-box'}
                        color={check ? 'green' : 'white'}
                        size={24}
                        style={styles.checkIcon}
                    />
                    <Text
                        numberOfLines={1}
                        ellipsizeMode={'tail'}
                        style={[styles.itemTxt, check ? styles.selectedItem : {}]}>
                        Select All
                    </Text>
                </TouchableOpacity>
            }
            <TouchableOpacity
                onPress={() => select(item.id)}
                style={styles.listItem}>
                <View style={styles.check}/>
                <Icon
                    name={itemCheck ? 'check-box' : 'check-box'}
                    color={selected.includes(item.id) ? 'green' : 'white'}
                    size={24}
                    style={styles.checkIcon}
                />
                <Text
                    numberOfLines={1}
                    ellipsizeMode={'tail'}
                    style={[styles.itemTxt, selected.includes(item.id) ? styles.selectedItem : {}]}>
                    {item.firstname + ' ' + item.lastname}
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export default MultiSelectItem;
