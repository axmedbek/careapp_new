import {StyleSheet} from 'react-native';
import {increase_brightness} from "../../../helpers/standard.helper";
import {BACK_ICON_COLOR, DIVIDER_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {SCREEN_WIDTH} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '98%',
        flexDirection: 'row',
        alignItems: 'center',
        height: 45,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: DIVIDER_COLOR,
        borderRadius: 5,
        marginVertical: 10
    },
    title: {
        fontSize: 17,
        marginLeft: 10,
        color: TEXT_COLOR
    },
    modalContainer: {
        width: SCREEN_WIDTH
    },
    modalHeader: {
        justifyContent: 'center',
        marginTop: 10
    },
    modalContent: {
        maxHeight: 250,
        backgroundColor: '#e5e7ea'
    },
    listItem: {
        width: '100%',
        height: 60,
        backgroundColor: 'white',
        marginBottom: 1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    check: {
        width: 19,
        height: 19,
        marginLeft: 2,
        marginRight: 10,
        borderWidth: 1,
        borderColor: 'green',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        zIndex: 1,
    },
    okBtn: {
        alignSelf: 'flex-end',
        width: 60,
        marginLeft: 0,
        backgroundColor: 'white',
        marginTop: 0
    },
    checkIcon: {
        position: 'absolute',
        marginLeft: 0
    },
    itemTxt: {
        fontSize: 15,
        color: 'black',
        marginHorizontal: 5,
        marginBottom: 4,
        textAlign: 'center'
    },
    selectedItem: {
        color: 'green',
        fontWeight: 'bold'
    }
});

export default styles;
