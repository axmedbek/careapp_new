import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import MultiSelectModal from "./MultiSelectModal";
import styles from './style';
import {getOnlyUniqueData} from "../../../helpers/standard.helper";
import Icon from 'react-native-vector-icons/MaterialIcons';


const MultiSelect = ({data, title, select, ids}) => {

        const [modalVisible, setModalVisible] = useState(false);
        const [selected, setSelected] = useState([]);
        const [check, setCheck] = useState(false);
        const [itemCheck, setItemCheck] = useState(false);

        useEffect(() => {
            if (ids.length > 0 && ids.length === data.length) {
                setCheck(true);
            }
        }, [data]);


        useEffect(() => {
            setSelected(getOnlyUniqueData(ids))
        }, [ids]);


        const onSelect = (index) => {
            let temp = selected;
            if (!temp.includes(index)) {
                temp.push(index);
            } else {
                temp.splice(temp.indexOf(index), 1);
            }

            setCheck(data.length === temp.length);

            if (temp.length > 0) {
                select(temp);
            } else {
                select([]);
                setCheck(false);
            }

            // setCheck(!check);
            setItemCheck(!itemCheck);
            setSelected(temp);
        };

        const selectAll = () => {
            let tmp = [];
            if (!check) {
                data.map(item => (
                    tmp.push(item.id)
                ));
                select(tmp);
            }
            else {
                select([]);
            }

            setCheck(!check);
            setSelected(tmp);
        };

        const openModal = () => {
            setModalVisible(true);
        };

        const closeModal = () => {
            setModalVisible(false);
        };


        return (
            <View>
                {
                    data.length > 0 &&
                    <TouchableOpacity onPress={openModal} style={styles.container}>
                        <Text style={styles.title}>{title}</Text>
                        <Icon
                            name={'keyboard-arrow-down'}
                            color={'grey'}
                            size={20}
                            style={{position:'absolute',right:10}}
                        />
                    </TouchableOpacity>
                }

                <MultiSelectModal
                    title={title}
                    modalVisible={modalVisible}
                    check={check}
                    closeModal={closeModal}
                    data={data}
                    select={onSelect}
                    selectAll={selectAll}
                    selected={selected}
                    itemCheck={itemCheck}
                />
            </View>
        );
    }
;

export default MultiSelect;
