import React from 'react';
import {View,Image,Text} from "react-native";
import {BACK_ICON_COLOR, DIVIDER_COLOR, LIGHT_DIVIDER_COLOR, MAIN_COLOR, TEXT_COLOR} from "../../../constants/colors";

const SelectableOptionItem = ({ item,isSelected }) => {
    return (
        <View
            style={{
                flexDirection: "row",
                alignItems: "center",
                backgroundColor: isSelected ? MAIN_COLOR : "transparent",
                paddingHorizontal: 15,
                paddingVertical: 10,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: DIVIDER_COLOR,
            }}
        >
            <Image
                source={{ uri: item.avatar }}
                style={{ width: 30, height: 30, borderRadius: 30 / 2, marginRight: 10 }}
            />

            <Text
                style={{
                    fontSize: 14,
                    color: isSelected ? 'white' : TEXT_COLOR,
                }}
            >
                {item.title}
            </Text>
        </View>
    )
};

export default SelectableOptionItem;
