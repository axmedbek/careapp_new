import React from 'react';
import {View,Text} from "react-native";
import {BACK_ICON_COLOR, DIVIDER_COLOR, MAIN_COLOR, TEXT_COLOR} from "../../../constants/colors";

const SelectableCategoryItem = ({ item, isSelected }) => {
    return (
        <View
            style={{
                flexDirection: "row",
                alignItems: "center",
                backgroundColor: isSelected ? MAIN_COLOR : "transparent",
                paddingHorizontal: 10,
                paddingVertical: 5,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: DIVIDER_COLOR
            }}
        >
            <View
                style={{
                    width: 10,
                    height: 10,
                    borderRadius: 10 / 2,
                    backgroundColor: isSelected ? 'white' : item.html_code === "#" ? MAIN_COLOR : item.html_code,
                    marginRight: 10,
                }}
            />

            <Text
                style={{
                    fontSize: 14,
                    color: isSelected ? 'white' : TEXT_COLOR,
                }}
            >
                {item.title}
            </Text>
        </View>
    );
};

export default SelectableCategoryItem;
