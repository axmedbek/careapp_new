import React from "react";
import {FlatList, Text, TouchableOpacity, View, ActivityIndicator} from "react-native";
import {REGULAR_FONT} from "../../../constants/setting";
import {BACK_ICON_COLOR, DIVIDER_COLOR, LIGHT_DIVIDER_COLOR, RED_COLOR, TEXT_COLOR} from "../../../constants/colors";

const SelectableOptions = ({
                               options,
                               selectedOption,
                               onOptionSelected,
                               onOptionUnselected,
                               renderItem,
                               loading,
                               label,
                               hasError
                           }) => {
    return (
        <View>
            {
                label &&
                <Text style={{
                    fontFamily: REGULAR_FONT,
                    fontSize: 18,
                    color: TEXT_COLOR,
                    paddingLeft: 2,
                    paddingTop: 6
                }}>{label}</Text>
            }
            <FlatList
                data={options}
                showsHorizontalScrollIndicator={false}
                extraData={selectedOption}
                keyExtractor={item => item.id.toString()}
                horizontal
                style={hasError ? {borderWidth: 2,borderColor: RED_COLOR,padding: 4} : {}}
                contentContainerStyle={{
                    marginVertical: 15,
                }}
                ListEmptyComponent={<View style={{padding: 10}}>{loading && <ActivityIndicator/>}</View>}
                renderItem={({item}) => {
                    const selected = selectedOption === item.id;

                    return (
                        <TouchableOpacity
                            onPress={() => {
                                if (selected) {
                                    onOptionUnselected();
                                } else {
                                    onOptionSelected(item.id);
                                }
                            }}
                            style={{
                                marginRight: 15
                            }}
                            activeOpacity={1}
                        >
                            {renderItem(item, selected)}
                        </TouchableOpacity>
                    );
                }}
            />
        </View>
    );
};

export default SelectableOptions;
