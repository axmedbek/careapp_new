import {StyleSheet} from 'react-native';
import {REGULAR_FONT, SCREEN_WIDTH} from "../../../constants/setting";
import {DIVIDER_COLOR, SECONDARY_BG_COLOR, TEXT_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    label: {
        fontFamily: REGULAR_FONT,
        fontSize: 18,
        color: TEXT_COLOR,
        marginLeft: 10,
        marginTop: 10
    },
    selectContainer: {
        // width: SCREEN_WIDTH - 10,
        borderWidth: 1,
        borderColor: DIVIDER_COLOR,
        borderRadius: 4,
        margin: 8,
        fontWeight: 'bold'
    }
});

export default styles;
