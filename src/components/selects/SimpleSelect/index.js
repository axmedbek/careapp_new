import React from 'react';
import {Picker} from "native-base";
import {BACK_ICON_COLOR, DIVIDER_COLOR, RED_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {Text,View} from "react-native";
import {concatObjects, increase_brightness} from "../../../helpers/standard.helper";
import styles from './style';


const SimplePicker = ({data, placeHolder, labelText, selectedValue, setSelectedValue, hasError,style}) => {
    return (
        <View style={concatObjects(styles.container,style)}>
            {
                labelText &&
                <Text style={styles.label}>
                    {labelText}
                </Text>
            }
           <View style={concatObjects(styles.selectContainer,
               hasError ? {borderWidth: 2,borderColor: RED_COLOR} : {borderColor: DIVIDER_COLOR},
               labelText ? {} : {marginLeft: 0})}>
               <Picker
                   mode="dropdown"
                   style={{borderColor: 'red',borderWidth: 1,fontWeight: 'bold'}}
                   selectedValue={selectedValue}
                   onValueChange={e => setSelectedValue(e)}
               >
                   {
                       placeHolder &&
                       <Picker.Item label={placeHolder} value={null} color={TEXT_COLOR}/>
                   }
                   {
                       data.map(item => (
                           <Picker.Item key={item.id} label={item.title} value={item.id}/>
                       ))
                   }
               </Picker>
           </View>
        </View>
    );
};

export default SimplePicker;
