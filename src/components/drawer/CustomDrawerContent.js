import React from 'react';
import {DrawerContentScrollView, DrawerItemList} from '@react-navigation/drawer';
import {MAIN_COLOR, TEXT_COLOR} from "../../constants/colors";
import LangDrawerItem from "./LangDrawerItem";
import {REGULAR_FONT} from "../../constants/setting";

function CustomDrawerContent(props) {
    return (
        <DrawerContentScrollView {...props} style={{paddingTop: 10}}>
            <DrawerItemList
                {...props}
                activeTintColor={'white'}
                inactiveTintColor={TEXT_COLOR}
                labelStyle={{fontSize: 18}}
                activeBackgroundColor={MAIN_COLOR}
            />
            <LangDrawerItem/>
        </DrawerContentScrollView>
    );
}

export default CustomDrawerContent;
