import React from 'react';
import {View} from "react-native";
import LanguageContainer from "../../containers/LanguageContainer";

const LangDrawerItem = () => {
    return (
        <View>
            <View style={{marginTop: 20,borderTopWidth: 1, borderTopColor: 'rgba(185,185,185,0.4)'}}/>
            <View style={{marginTop: 100}}>
                <LanguageContainer/>
            </View>
        </View>
    );
};

export default LangDrawerItem;
