import React from 'react';
import {TextInput, View} from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import {connect} from "react-redux";
import styles from './styles';
import {RED_COLOR} from "../../../constants/colors";

const PasswordInputComponent = ({lang,password,setPassword,passwordError,
                                    setPasswordError, icon = "lock",
                                    placeholder = lang.messages.login.password,
                                    passwordRef,handleSignIn}) => {
    return (
        <View style={Object.assign({},styles.container,passwordError ? {borderColor : RED_COLOR,borderWidth : 2} : {})}>
            <Icon style={Object.assign({},styles.icon,passwordError ? {color : RED_COLOR} : {})} active name={icon}/>
            <TextInput
                ref={passwordRef}
                style={styles.text}
                placeholder={placeholder}
                secureTextEntry
                returnKeyType="go"
                name={"password"}
                value={password}
                onSubmitEditing={handleSignIn}
                onChangeText={e => {
                    if (e.length > 0) {
                        setPasswordError(false);
                    }
                    else {
                        setPasswordError(true);
                    }
                    setPassword(e)
                }}
            />
        </View>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PasswordInputComponent);
