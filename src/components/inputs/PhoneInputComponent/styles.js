import {StyleSheet} from "react-native";
import {LIGHT_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    container : {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
        borderWidth: 1,
        borderColor: LIGHT_COLOR,
        borderRadius: 4
    },
    icon: {
        fontSize: 20,
        margin: 10,
        marginTop: 12
    },
    text: {
        fontSize: 16,
        width: 300
    }
});

export default styles;
