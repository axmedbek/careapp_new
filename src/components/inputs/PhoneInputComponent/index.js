import React from 'react';
import {TextInput, View} from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import {connect} from "react-redux";
import styles from './styles';
import {RED_COLOR} from "../../../constants/colors";
import {Input} from "native-base";

const PhoneInputComponent = ({lang,phone,setPhone,icon,phoneError,setPhoneError,passwordRef}) => {
    return (
        <View style={Object.assign({},styles.container,phoneError ? {borderColor : RED_COLOR,borderWidth : 2} : {})}>
            <Icon style={Object.assign({},styles.icon,phoneError ? {color : RED_COLOR} : {})} active name={icon}/>
            <TextInput
                style={styles.text}
                keyboardType={'phone-pad'}
                placeholder={lang.messages.login.phone}
                name={"phone"}
                value={phone}
                returnKeyType="next"
                onSubmitEditing={() => {
                    passwordRef && passwordRef.current.focus();
                }}
                onChangeText={e => {
                    if (e.length > 0) {
                        setPhoneError(false);
                    }
                    else {
                        setPhoneError(true);
                    }
                    setPhone(e)
                }}
            />
        </View>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PhoneInputComponent);
