import React from 'react';
import {Platform, TextInput, Text, View} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from './styles';
import {RED_COLOR} from "../../../constants/colors";
import {concatObjects} from "../../../helpers/standard.helper";

const StandardInputComponent = ({name, value, setValue, valueError, labelTxt, setValueError, icon, type, placeholder,inputStyle,containerInputStyle}) => {
    return (
        <View style={Object.assign({}, styles.container,{})}>
            {
                labelTxt &&
                <Text style={styles.label}>{labelTxt}</Text>
            }
            <View style={concatObjects(styles.containerInput,containerInputStyle)}>
                {
                    icon &&
                    <Icon style={Object.assign({}, styles.icon, valueError ? {color: RED_COLOR} : {})} active
                          name={icon}/>
                }
                <TextInput
                    keyboardType={type === 'number'  ? (Platform.OS === 'ios' ? 'decimal-pad' : 'number-pad') : type}
                    style={concatObjects(styles.text,inputStyle,valueError ? {borderColor: RED_COLOR, borderWidth: 2} : {})}
                    placeholder={placeholder}
                    returnKeyType="go"
                    name={name}
                    value={value}
                    onChangeText={e => {
                        if (valueError) {
                            if (e.length > 0) {
                                setValueError(false);
                            } else {
                                setValueError(true);
                            }
                        }
                        setValue(e)
                    }}
                />
            </View>
        </View>
    );
};

export default StandardInputComponent;
