import {StyleSheet} from "react-native";
import {BACK_ICON_COLOR, DIVIDER_COLOR, SECONDARY_BG_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT, SCREEN_WIDTH} from "../../../constants/setting";
import {increase_brightness} from "../../../helpers/standard.helper";

const styles = StyleSheet.create({
    container: {
        // write something
        marginTop: 10
    },
    containerInput: {
        flexDirection: 'row',
        margin: 10,
        marginLeft: 0,
        borderWidth: 1,
        borderColor: SECONDARY_BG_COLOR,
        borderRadius: 4,
    },
    icon: {
        fontSize: 20,
        margin: 10,
        marginTop: 12
    },
    text: {
        fontSize: 16,
        width: SCREEN_WIDTH - 10,
        paddingLeft: 8,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: DIVIDER_COLOR
    },
    label: {
        fontFamily: REGULAR_FONT,
        fontSize: 18,
        color: TEXT_COLOR,
        marginLeft: 10,
        marginTop: 10
    }
});

export default styles;
