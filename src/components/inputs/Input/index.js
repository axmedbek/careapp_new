import React from "react";
import { View } from "react-native";
import { ErrorMessage } from "formik";
import { Input as NativeInput } from "react-native-elements";
import {LIGHT_DIVIDER_COLOR} from "../../../constants/colors";
import ErrorText from "../../text/ErrorText";

const Input = ({ name, inputContainerStyle, containerStyle, ...rest }) => (
    <View>
        <NativeInput
            inputContainerStyle={[
                {
                    borderWidth: 1,
                    borderColor: LIGHT_DIVIDER_COLOR,
                    borderRadius: 5,
                },
                inputContainerStyle && inputContainerStyle,
            ]}
            containerStyle={[
                {
                    marginBottom: 10,
                    width: "100%",
                },
                containerStyle && containerStyle,
            ]}
            {...rest}
        />
        <ErrorMessage name={name} component={ErrorText} />
    </View>
);

export default Input;
