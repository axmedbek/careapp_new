import React from 'react';
import {View} from "react-native";
import styles from './styles';
import {Spinner} from 'native-base';
import {MAIN_COLOR} from "../../../constants/colors";

const DataLoadingComponent = ({loading}) => {
    return (
        <>
            {
                loading &&
                <View style={styles.container}>
                    <Spinner size={30} color={MAIN_COLOR}/>
                </View>
            }
        </>
    );
};

export default DataLoadingComponent;
