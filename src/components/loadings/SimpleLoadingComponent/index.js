import React from 'react';
import {View,StatusBar} from "react-native";
import styles from './styles';
import {Spinner} from 'native-base';
import {MAIN_COLOR} from "../../../constants/colors";

const SimpleLoadingComponent = ({loading}) => {
    return (
        <>
            {
                loading &&
                <View style={styles.container}>
                    <StatusBar backgroundColor={MAIN_COLOR} barStyle={"light-content"} />
                    <Spinner size={60} color={"#ffffff"}/>
                </View>
            }
        </>
    );
};

export default SimpleLoadingComponent;
