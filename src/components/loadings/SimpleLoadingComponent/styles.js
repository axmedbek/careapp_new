import {StyleSheet} from "react-native";
import {MAIN_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: MAIN_COLOR,
        elevation: 20
    }
});

export default styles;
