import {StyleSheet} from "react-native";
import {MAIN_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(198,198,198,0.44)',
        elevation: 5
    },
    txt: {
        backgroundColor: '#404040',
        padding: 10,
        borderRadius: 4,
        color: 'white',
        fontFamily: REGULAR_FONT,
        fontSize: 18
    }
});

export default styles;
