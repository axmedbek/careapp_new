import React from 'react';
import {View,Text} from "react-native";
import styles from "./styles";

const PermissionLoadingComponent = ({loading}) => {
    return (
        <>
            {
                loading &&
                <View style={styles.container}>
                    <Text style={styles.txt}>Location Pending...</Text>
                </View>
            }
        </>
    );
};

export default PermissionLoadingComponent;
