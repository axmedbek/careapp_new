import React from 'react';
import {View} from "react-native";
import {connect} from "react-redux";
import styles from './styles';
import {Spinner} from 'native-base';
import {MAIN_COLOR} from "../../../constants/colors";

const OperationLoadingComponent = ({loading}) => {
    return (
        <>
            {
                loading &&
                <View style={styles.container}>
                    <Spinner size={60} color={MAIN_COLOR}/>
                </View>
            }
        </>
    );
};

const mapStateToProps = ({loadingReducer}) => {
    return {
        loading : loadingReducer.operationLoading
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(OperationLoadingComponent);
