import React from "react";
import PropTypes from "prop-types";
import { Text as NativeText } from "react-native-elements";
import {TEXT_COLOR} from "../../../constants/colors";

const Text = ({ style, children, center, h1, h2, h3, h4 }) => (
    <NativeText
        h1={h1}
        h2={h2}
        h3={h3}
        h4={h4}
        style={[
            {
                color: TEXT_COLOR,
                textAlign: center ? "center" : "left",
            },
            !h1 && !h2 && !h3 && !h4 && { fontSize: 18 },
            style && style,
        ]}
    >
        {children}
    </NativeText>
);

Text.propTypes = {
    style: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.object), PropTypes.object]),
    h1: PropTypes.bool,
    h2: PropTypes.bool,
    h3: PropTypes.bool,
    h4: PropTypes.bool,
    center: PropTypes.bool,
    children: PropTypes.string.isRequired,
};

Text.defaultProps = {
    style: {},
    h1: false,
    h2: false,
    h3: false,
    h4: false,
    center: false,
};

export default Text;
