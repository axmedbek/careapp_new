import React from "react";
import PropTypes from "prop-types";
import {RED_COLOR} from "../../../constants/colors";
import Text from "../Text";

const ErrorText = ({ children, style }) => (
    <Text
        style={[
            {
                color: RED_COLOR,
                marginTop: -20,
                marginBottom: 10,
                // textAlign: "center",
                fontSize: 14,
            },
            style,
        ]}
    >
        {children}
    </Text>
);

ErrorText.propTypes = {
    children: PropTypes.string.isRequired,
    style: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.object), PropTypes.object]),
};

ErrorText.defaultProps = {
    style: {},
};

export default ErrorText;
