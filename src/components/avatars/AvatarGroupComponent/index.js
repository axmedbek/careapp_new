import React from 'react';
import {View} from "react-native";
import {Thumbnail} from 'native-base';
import {LIGHT_COLOR} from "../../../constants/colors";
import {concatObjects} from "../../../helpers/standard.helper";

const AvatarGroupComponent = ({avatars, style, borderColor = LIGHT_COLOR,avatarStyle = {}}) => {
    return (
        <View style={style}>
            {
                avatars.map((avatar,index) => (
                    <Thumbnail style={concatObjects({marginLeft: -10, borderWidth: 1, borderColor: borderColor},avatarStyle)}
                               key={++index+"-"+avatar.avatar+"-"+avatar.id} small
                               source={{uri: avatar.avatar}}/>
                ))
            }
        </View>
    );
};

export default AvatarGroupComponent;
