import React from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Avatar} from 'react-native-elements';

const StackedAvatars = ({avatars, maxCountToShow}) => (
    <View style={{flexDirection: 'row'}}>
        {avatars.slice(0, maxCountToShow).map((avatar, index) => (
            <Avatar
                key={avatar.url+Math.random()}
                source={{uri: avatar.url}}
                size="small"
                rounded
                containerStyle={{marginLeft: index === 0 ? 0 : -10}}
            />
        ))}
        {avatars.length > maxCountToShow && (
            <Avatar
                key={`avatar${Math.random()}`}
                size="small"
                rounded
                title={`+${avatars.length - maxCountToShow}`}
                containerStyle={{marginLeft: 5}}
                ImageComponent={FastImage}
            />
        )}
    </View>
);

StackedAvatars.propTypes = {
    avatars: PropTypes.arrayOf(
        PropTypes.shape({
            url: PropTypes.string.isRequired,
        }),
    ).isRequired,
    maxCountToShow: PropTypes.number,
};

StackedAvatars.defaultProps = {
    maxCountToShow: 3,
};

export default StackedAvatars;
