import React from 'react';
import {Thumbnail} from "native-base";

const SimpleAvatar = ({uri, width = 45, height = 45, ...rest}) => {
    return (
        <Thumbnail source={uri} style={{
            width: 40,
            height: 40,
            ...rest.style
        }}/>
    );
};

export default SimpleAvatar;
