import React from 'react';
import {StyleSheet, View} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {BLACK_COLOR, DIVIDER_COLOR} from "../../../constants/colors";
import {concatObjects, increase_brightness} from "../../../helpers/standard.helper";

const MultiImageAvatar = ({ size = null }) => {

    return (
        <View style={concatObjects(styles.container,size === "small" ? {
            width: 50,
            height: 50,
            borderRadius: 25
        } : {})}>
            <Icon name={"account-group"} style={styles.icon}/>
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        width: 60,
        height: 60,
        backgroundColor: '#0D8ABC',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        fontSize: 30,
        color: 'white'
    }
});

export default MultiImageAvatar;
