import React, {useEffect} from 'react';
import {increase_brightness} from "../../../helpers/standard.helper";
import {BACK_ICON_COLOR} from "../../../constants/colors";
import {Text} from "react-native";
import {REGULAR_FONT} from "../../../constants/setting";
import TimeCalculator from "../../../containers/People/TimeCalculator";

let timer = null;

const SimpleTimer = ({time, setTime, isEnabled, label}) => {

    useEffect(() => {
        startTimer();
        return () => clearInterval(timer);
    }, []);

    const startTimer = () => {
        timer = setInterval(() => {
            setTime(time => time + 1)
        }, 1000);
    };

    return (
        <>
            {
                isEnabled
                    ? <TimeCalculator time={time}/>
                    : <Text style={{
                        paddingLeft: 20,
                        color: increase_brightness(BACK_ICON_COLOR, 60),
                        fontFamily: REGULAR_FONT,
                        lineHeight: 20
                    }}>
                        {label}
                    </Text>
            }
        </>
    )
};

export default SimpleTimer;
