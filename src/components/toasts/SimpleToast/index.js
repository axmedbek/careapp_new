import React from 'react';
import {View, Text} from "react-native";
import styles from './style';
import {connect} from "react-redux";
import {showToast} from "../../../actions/setting.action";
import CoreModal from "../../modals/CoreModal";
import StandardButton from "../../buttons/StandardButton";
import Icon from "react-native-vector-icons/FontAwesome";
import {concatObjects, getToastStyleFromType} from "../../../helpers/standard.helper";

const SimpleToast = ({lang,show,message,type,showToast}) => {

    return (
        show &&
        <CoreModal hideModal={() => showToast(false)} visible={show} hideIcon={true}>
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Icon style={concatObjects(styles.icon,getToastStyleFromType(type))} name={"warning"}/>
                    <Text style={styles.text}>{message}</Text>
                </View>
                <StandardButton
                    text={lang.messages.common.ok}
                    handleButton={() => showToast(false)}
                    style={styles.btn}
                />
            </View>
        </CoreModal>
    );
};

const mapStateToProps = ({lang,settingReducer}) => {
    return {
        lang,
        show: settingReducer.simpleToast,
        message: settingReducer.simpleToastMessage,
        type: settingReducer.simpleToastType,
    };
};

const mapDispatchToProps = {
    showToast
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SimpleToast);
