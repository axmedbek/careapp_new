import {StyleSheet} from "react-native";
import {TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        width: 300,
        height: 'auto'
    },
    headerContainer: {
        flexDirection: 'row',
        width: 300
    },
    icon: {
        fontSize: 40,
        marginLeft: 20,
        marginTop: 25
    },
    text: {
        fontSize: 20,
        padding: 10,
        marginTop: 10,
        fontFamily: REGULAR_FONT,
        lineHeight: 25,
        width: 240,
        color: TEXT_COLOR
    },
    btnContainer: {
        width: '100%',
        alignItems: 'center'
    },
    btn: {
        backgroundColor: '#DAD9E1',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16
    }
});

export default styles;
