import React from 'react';
import {View, Text} from "react-native";
import styles from './style';
import CoreModal from "../../modals/CoreModal";
import StandardButton from "../../buttons/StandardButton";
import Icon from "react-native-vector-icons/FontAwesome";
import {concatObjects, getToastStyleFromType, isEmptyObject} from "../../../helpers/standard.helper";
import DataLoadingComponent from "../../loadings/DataLoadingComponent";

const ConfirmToast = ({show,message,type,item,handClose,deleteHandle,errors,loading,id}) => {

    const handleButton = () => {
        if (id || item) {
            const deletedId = id ? id : item._id;
            deleteHandle(deletedId,item.folder);
        }
        else {
            deleteHandle();
        }
    };


    return (
        show &&
        <CoreModal hideModal={handClose} visible={show} hideIcon={true}>
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Icon style={concatObjects(styles.icon,getToastStyleFromType(type))} name={"warning"}/>
                    <Text style={styles.text}>{message}</Text>
                </View>
                {
                    errors &&
                    <Text style={concatObjects(styles.errorTxt,isEmptyObject(errors) ? {display : "none"} : {display : "block"})}>
                        {errors.description}
                    </Text>
                }
                <View style={{flexDirection: 'row'}}>
                   <StandardButton
                       text={"Cancel"}
                       handleButton={handClose}
                       style={styles.btnCancel}
                   />
                   <StandardButton
                       text={"Confirm"}
                       handleButton={handleButton}
                       style={styles.btnHandle}
                   />
               </View>
            </View>
            <DataLoadingComponent loading={loading}/>
        </CoreModal>
    );
};


export default ConfirmToast;
