import {StyleSheet} from "react-native";
import {MAIN_COLOR, RED_COLOR, RED_MID_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {BOLD_FONT, REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        width: 300,
        height: 'auto',
        padding: 10
    },
    headerContainer: {
        flexDirection: 'row',
        width: 300
    },
    icon: {
        fontSize: 40,
        marginLeft: 20,
        marginTop: 25
    },
    text: {
        fontSize: 20,
        padding: 10,
        marginTop: 10,
        fontFamily: REGULAR_FONT,
        lineHeight: 25,
        width: 220,
        color: TEXT_COLOR
    },
    btnContainer: {
        width: '100%',
        alignItems: 'center'
    },
    btnCancel: {
        backgroundColor: RED_COLOR,
        width: '46%',
        height: 35,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    btnHandle: {
        backgroundColor: MAIN_COLOR,
        width: '46%',
        height: 35,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    errorTxt: {
        textAlign: 'center',
        backgroundColor: RED_MID_COLOR,
        color: RED_COLOR,
        padding: 10,
        borderRadius: 4,
        fontFamily: BOLD_FONT,
    }
});

export default styles;
