import React from "react";
import PropTypes from "prop-types";
import { Modal, TouchableOpacity } from "react-native";
import { Content } from "native-base";

const NativeModal = ({ children, visible, onBackDropPressed, onRequestClose, style }) => (
    <Modal
        animationType="fade"
        hardwareAccelerated
        transparent
        visible={visible}
        onRequestClose={onRequestClose}
    >
        <TouchableOpacity
            style={{
                flex: 1,

                backgroundColor: "rgba(0,0,0,0.3)",
            }}
            activeOpacity={1}
            onPress={onBackDropPressed}
        >
            <Content contentContainerStyle={{ flex: 1, justifyContent: "center" }}>
                <TouchableOpacity
                    style={[
                        {
                            borderRadius: 5,
                            marginHorizontal: 15,
                            elevation: 10,
                            minHeight: 100,
                            overflow: "hidden",
                            backgroundColor: "#fff",
                        },
                        style && style,
                    ]}
                    activeOpacity={1}
                >
                    {children}
                </TouchableOpacity>
            </Content>
        </TouchableOpacity>
    </Modal>
);

NativeModal.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
    visible: PropTypes.bool.isRequired,
    onBackDropPressed: PropTypes.func,
    onRequestClose: PropTypes.func,
    style: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.object), PropTypes.object]),
};

NativeModal.defaultProps = {
    onBackDropPressed: () => null,
    onRequestClose: () => null,
    style: {},
};

export default NativeModal;
