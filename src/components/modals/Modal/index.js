import React from 'react';
import {H3} from "native-base";
import NativeModal from "../NativeModal";

const Modal = ({ modalVisible,closeModal,title,children }) => {
    return (
        <NativeModal
            visible={modalVisible}
            onBackDropPressed={closeModal}
            onRequestClose={closeModal}
            style={{
                paddingHorizontal: 15,
                paddingTop: 10,
            }}
        >
            <H3 style={{ marginBottom: 15,fontWeight: 'bold' }}>{title}</H3>
            {children}
        </NativeModal>
    );
};

export default Modal;
