import {StyleSheet} from 'react-native';
import {MAIN_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(134,133,116,0.7)'
    },
    container: {
        backgroundColor: 'white',
        borderRadius: 6,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
    iconContainer: {
        zIndex: 1,
        alignSelf: 'flex-end',
        position: 'absolute',
        right: 5,
        // elevation: 10
    },
    icon: {
        zIndex: 1,
        alignSelf: 'flex-end',
        position: 'absolute',
        right: 2,
        width: 25,
        height: 25,
        fontSize: 20,
        paddingTop: 2,
        paddingLeft: 3,
        color: TEXT_COLOR
    },
    titleContainer: {
        backgroundColor: MAIN_COLOR,
        height: 40,
        paddingTop: 6,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    titleTxt: {
        color: 'white',
        fontSize: 20,
        fontFamily: REGULAR_FONT,
        fontStyle: 'italic'
    },
    titleIcon: {
        fontSize: 22,
        color: 'white',
        marginTop: 2,
        marginRight: 10
    },
    titleHeader: {
        color: 'white',
        marginTop: 8,
        marginRight: 4
    }
});

export default styles;
