import React from 'react';
import {View, Modal, Text} from 'react-native';
import styles from './style';
import Icon from "react-native-vector-icons/AntDesign";
import {concatObjects} from "../../../helpers/standard.helper";
import DataLoadingComponent from "../../loadings/DataLoadingComponent";

const CoreModal = ({visible, hideModal, hideIcon, modalStyle, titleText, children,loading}) => {
    return (
        <Modal
            animationType="slide"
            visible={visible}
            // onRequestClose={hideModal}
            transparent={true}
        >
            <View style={styles.centeredView}>
                <View style={concatObjects(styles.container, modalStyle)}>
                    {!hideIcon &&
                    <Icon style={concatObjects(styles.icon, titleText ? styles.titleHeader : {})} name={"close"}
                          onPress={hideModal}/>}
                    {titleText && <View
                        style={concatObjects(styles.titleContainer, modalStyle && modalStyle.borderRadius
                            ? {borderRadius: modalStyle.borderRadius} : {},)}>
                        {/*<Icon name={"infocirlceo"} style={styles.titleIcon}/>*/}
                        <Text style={styles.titleTxt}>{titleText}</Text>
                    </View>}
                    {children}
                </View>
                <DataLoadingComponent loading={loading}/>
            </View>
        </Modal>
    );
};

export default CoreModal;
