import React, {useState} from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import {View, Text, TouchableOpacity, Platform} from 'react-native';
import moment from "moment";
import {concatObjects, increase_brightness} from "../../../helpers/standard.helper";
import {BACK_ICON_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";


const SimpleDateTimePicker = ({date, placeholder, setDate, label, format = "LLL", mode = 'date',minimumDate = null, style}) => {
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    return (
        <View style={concatObjects({
            width: '100%',
            marginTop: 4,
            marginBottom: 4
        }, style)}>
            <Text style={{fontFamily: REGULAR_FONT, fontSize: 18, padding: 6, color: TEXT_COLOR}}>{label}</Text>
            <TouchableOpacity onPress={() => setShow(true)}
                              style={{
                                  borderColor: increase_brightness(BACK_ICON_COLOR, 70),
                                  borderWidth: 1,
                                  borderRadius: 4,
                                  backgroundColor: 'white',
                                  justifyContent: 'center',
                                  paddingLeft: 10,
                                  height: 50
                              }}>
                <Text style={{fontFamily: REGULAR_FONT, fontSize: 16}}>
                    {date ? moment(date).format(format) : placeholder}
                </Text>
            </TouchableOpacity>
            {
                show &&
                <DateTimePicker
                    value={date ? new Date(date) : new Date()}
                    mode={mode}
                    is24Hour={true}
                    minimumDate={minimumDate}
                    display="default"
                    onChange={onChange}
                />
            }
        </View>
    )
};

export default SimpleDateTimePicker;
