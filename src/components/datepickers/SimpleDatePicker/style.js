import {StyleSheet} from "react-native";
import {LIGHT_COLOR, SECONDARY_BG_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT, SCREEN_WIDTH} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        // write something
    },
    containerInput: {
        flexDirection: 'row',
        margin: 10,
        borderWidth: 1,
        borderColor: SECONDARY_BG_COLOR,
        borderRadius: 4,
    },
    icon: {
        fontSize: 20,
        margin: 10,
        marginTop: 12
    },
    text: {
        fontSize: 16,
        width: SCREEN_WIDTH - 10,
        paddingLeft: 8
    },
    label: {
        fontFamily: REGULAR_FONT,
        fontSize: 18,
        color: TEXT_COLOR,
        marginLeft: 10,
        marginBottom: 10,
        marginTop: 10
    }
});

export default styles;
