import React from 'react';
import {DatePicker} from "native-base";
import {BACK_ICON_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {View, Text} from "react-native";
import styles from './style';
import {increase_brightness} from "../../../helpers/standard.helper";
import moment from "moment";


const SimpleDatePicker = ({label, placeholder,setDate,date,containerStyle}) => {
    return (
        <View style={containerStyle}>
            <Text style={styles.label}>{label}</Text>
            <View style={{
                height: 50,
                paddingTop: 2,
                borderWidth: 1,
                borderRadius: 4,
                marginLeft: 10,
                marginRight: 10,
                borderColor: increase_brightness(BACK_ICON_COLOR, 60)
            }}>
                <DatePicker
                    defaultDate={new Date()}
                    locale={"da"}
                    timeZoneOffsetInMinutes={undefined}
                    formatChosenDate={date => {return moment(date).format('LL');}}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText={placeholder}
                    textStyle={{color: TEXT_COLOR}}
                    placeHolderTextStyle={{color: "#d3d3d3"}}
                    onDateChange={(e) => setDate(e)}
                    disabled={false}
                    value={date}
                />
            </View>
        </View>
    );
};

export default SimpleDatePicker;
