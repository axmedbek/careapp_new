import Login from "../pages/Login";
import Profile from "../pages/Account/Profile";
import HomeScreenNavigator from "../screens/HomeScreenNavigator";
import Notes from "../pages/NotePages/Notes";
import People from "../pages/PeoplePages/People";
import Calendar from "../pages/Calendar";
import AppLoadingScreen from "../screens/AppLoadingScreen";
import Note from "../pages/NotePages/Note";
import PersonOverviewNavigationContainer from "../screens/PersonOverviewNavigationContainer";
import HandlePlan from "../pages/PeoplePages/HandlePlanPages/HandlePlan";
import EditPlan from "../pages/PeoplePages/HandlePlanPages/EditPlan";
import PlanNotes from "../pages/PeoplePages/HandlePlanPages/PlanNotes";
import ActivityAdd from "../pages/PeoplePages/Economy/ActivityAdd";
import CameraScanner from "../containers/People/CameraScanner";
import CustomImageCropper from "../containers/People/CustomImageCropper";
import ActivityImageSlider from "../pages/PeoplePages/ActivityImageSlider";
import CreateEvent from "../pages/CreateEvent";
import EventNote from "../pages/EventNote";
import Setting from "../pages/Account/Setting";
import Password from "../pages/Account/Password";
import Fingerprint from "../pages/Account/Fingerprint";
import Pincode from "../pages/Account/Pincode";
import PinLogin from "../pages/Login/sub/PinLogin";
import ProfileImageCropper from "../pages/Account/Profile/ProfileImageCropper";
import ChatList from "../pages/ChatPage/ChatList";
import ProfileNavigationContainer from "../screens/ProfileNavigationContainer";
import ChatInbox from "../pages/ChatPage/ChatInbox";
import ContactList from "../pages/ChatPage/ContactList";
import CreateChatGroup from "../pages/ChatPage/CreateChatGroup";
import AddGroupUser from "../pages/ChatPage/AddGroupUser";
import ChatGroupImageCropper from "../pages/ChatPage/ChatGroupImageCropper";
import ChatProfile from "../pages/ChatPage/ChatProfile";
import BottomNavigationContainer from "../screens/BottomNavigationContainer";
import ContractShowPage from "../pages/HomePages/Contract/ContractShowPage";
import CreateSignaturePage from "../pages/HomePages/Contract/CreateSignaturePage";
import ContractNotePage from "../pages/HomePages/Contract/ContractNotePage";


export const appAuthRoutes = [
    // bottom navigation container
    {
        'name': 'AppLoadingScreen',
        'component': AppLoadingScreen
    },
    // common routes - this routes should be call from anywhere
    {
        'name': 'Account',
        'component': ProfileNavigationContainer
    },
    {
        'name': 'HomeScreenNavigator',
        'component': BottomNavigationContainer
    },
    {
        'name': 'PersonOverviewNavigationContainer',
        'component': PersonOverviewNavigationContainer
    },
    {
      'name': 'ContractShowPage',
      'component': ContractShowPage
    },
    {
        'name': 'CreateSignaturePage',
        'component': CreateSignaturePage
    },
    {
        'name': 'ContractNotePage',
        'component': ContractNotePage
    },
    {
        'name': 'ChatInbox',
        'component': ChatInbox
    },
    {
        'name': 'ContactList',
        'component': ContactList
    },
    {
        'name': 'CreateChatGroup',
        'component': CreateChatGroup
    },
    {
        'name': 'AddGroupUser',
        'component': AddGroupUser
    },
    {
        'name': 'ChatProfile',
        'component': ChatProfile,
    },
    {
        'name': 'Note',
        'component': Note
    },
    {
        'name': 'ActivityAdd',
        'component': ActivityAdd
    },
    {
        'name': 'CameraScanner',
        'component': CameraScanner
    },
    {
        'name': 'CustomImageCropper',
        'component': CustomImageCropper
    },
    {
        'name': 'ActivityImageSlider',
        'component': ActivityImageSlider
    },
    {
        'name': 'ChatGroupImageCropper',
        'component': ChatGroupImageCropper
    },
    {
        'name': 'CreateEvent',
        'component': CreateEvent
    },
    {
        'name': 'EventNote',
        'component': EventNote
    },
    // authentication routes ( sign-in , sign-up and etc.)
    {
        'name': 'Login',
        'component': Login
    },
    {
        'name': 'PinLogin',
        'component': PinLogin
    }
];

export const bottomRoutes = [
    // bottom routes
    {
        'name': 'Home',
        'component': HomeScreenNavigator,
        'icon': 'home',
        'label': 'home'
    },
    {
        'name': 'Notes',
        'component': Notes,
        'icon': 'form',
        'label': 'notes'
    },
    {
        'name': 'People',
        'component': People,
        'icon': 'heart',
        'label': 'people'
    },
    {
        'name': 'Calendar',
        'component': Calendar,
        'icon': 'calendar',
        'label': 'calendar'
    },
    {
        'name': 'Inbox',
        'component': ChatList,
        'icon': 'message1',
        'label': 'inbox'
    }
];

export const handlePlanRoutes = [
    {
        'name': 'HandlePlans',
        'component': HandlePlan,
        'icon': null,
        'label': 'handleplan'
    },
    {
        'name': 'EditPlan',
        'component': EditPlan,
        'icon': null,
        'label': 'editplan'
    },
    {
        'name': 'PlanNotes',
        'component': PlanNotes,
        'icon': null,
        'label': 'plannotes'
    },
];

export const handleChatRoutes = [];

export const handleProfileRoutes = [
    {
        'name': 'Account',
        'component': Setting,
        'icon': null,
        'label': 'account'
    },
    {
        'name': 'Profile',
        'component': Profile,
        'icon': null,
        'label': 'profile'
    },
    {
        'name': 'ProfileImageCropper',
        'component': ProfileImageCropper,
        'icon': null,
        'label': 'profileimagecropper'
    },
    {
        'name': 'Password',
        'component': Password,
        'icon': null,
        'label': 'password'
    },
    {
        'name': 'Fingerprint',
        'component': Fingerprint,
        'icon': null,
        'label': 'fingerprint'
    },
    {
        'name': 'Pincode',
        'component': Pincode,
        'icon': null,
        'label': 'pincode'
    }
];
