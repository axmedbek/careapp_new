import React from 'react';
import { ListItem } from "react-native-elements";
import Swipeout from "react-native-swipeout";
import {truncateText} from "../../../helpers/standard.helper";
import {ACCENT_COLOR} from "../../../constants/colors";

const FolderItem = ({navigation, handleEdit, handleRemove, item}) => {
    return (
        <Swipeout
            backgroundColor='transparent'
            autoClose={true}
            right={[
                {
                    onPress: () => handleEdit(),
                    text: 'Redigere'
                },
                {
                    backgroundColor:'red',
                    onPress: () => handleRemove(),
                    text: 'Slette'
                }
            ]}
        >
            <ListItem
                title={truncateText(item.title, 30)}
                badge={{
                    value: item.count,
                    containerStyle: {
                        borderRadius: 10
                    },
                    badgeStyle:{
                        backgroundColor: ACCENT_COLOR,
                    },
                }}
                onPress={() =>
                    navigation.navigate("Note", {
                        id: item.id,
                        title: item.title
                    })
                }
                onLongPress={handleRemove}
                containerStyle={{
                    backgroundColor: "transparent",
                    borderRadius: 10
                }}
                underlayColor="transparent"
                bottomDivider
                chevron
            />
        </Swipeout>
    );
};

export default FolderItem;
