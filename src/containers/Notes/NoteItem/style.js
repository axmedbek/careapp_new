import {StyleSheet} from "react-native";
import {BLUE_COLOR, MAIN_COLOR, RED_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    iconGroup: {
        flexDirection: 'row',
        position: 'absolute',
        right: 10
    },
    edit: {
        fontSize: 28,
        color: BLUE_COLOR,
        marginRight: 8,
    },
    delete: {
        fontSize: 28,
        color: RED_COLOR
    },
    count: {
        backgroundColor: MAIN_COLOR,
        color: 'white',
        width: 'auto',
        height: 24,
        borderRadius: 4,
        textAlign: 'center',
        marginRight: 10,
        fontFamily: REGULAR_FONT,
        fontSize: 14,
        paddingTop: 3,
        paddingLeft: 8,
        paddingRight: 8
    }
});

export default styles;
