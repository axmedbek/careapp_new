import React, {useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Swipeout from "react-native-swipeout";
import {TEXT_COLOR} from "../../../constants/colors";
import {List, ListItem} from 'native-base';
import {BOLD_FONT} from "../../../constants/setting";
import Modal from "../../../components/modals/Modal";


const NoteItem = ({handleEdit, handleRemove, item}) => {
    const [showInfo, setShowInfo] = useState(false);

    return (
        <>
            <Swipeout
                backgroundColor='transparent'
                autoClose={true}
                right={
                    [
                        {
                            onPress: () => handleEdit(),
                            text: 'Redigere',
                        },
                        {
                            backgroundColor: 'red',
                            onPress: () => handleRemove(),
                            text: 'Slette',
                        },
                    ]
                }
            >
                <TouchableOpacity
                    onPress={() => setShowInfo(true)}
                    style={{
                        height: 65,
                        width: '100%',
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <View>
                        <Text
                            numberOfLines={1}
                            ellipsizeMode={'tail'}
                            style={{
                                fontSize: 17,
                                marginLeft: 20,
                                color: TEXT_COLOR,
                                marginRight: 100,
                            }}
                        >
                            {item.title}
                        </Text>
                        <Text
                            numberOfLines={1}
                            ellipsizeMode={'tail'}
                            style={{
                                marginLeft: 20,
                                marginRight: 100,
                            }}
                        >
                            {item.description}
                        </Text>
                    </View>
                    <Text
                        style={{
                            fontSize: 14,
                            position: 'absolute',
                            right: 10,
                        }}
                    >
                        {item.date}
                    </Text>
                </TouchableOpacity>
            </Swipeout>
            <Modal closeModal={() => setShowInfo(false)} modalVisible={showInfo} title={"Note information"}>
                <View style={{width: '100%', minHeight: 200, height: 'auto', borderRadius: 4, backgroundColor: 'white'}}>
                    <View>
                        <List>
                            <ListItem itemDivider>
                                <Text style={{fontFamily: BOLD_FONT, color: TEXT_COLOR}}>Title</Text>
                            </ListItem>
                            <ListItem>
                                <Text style={{color: '#8d8d8d'}}>{item.title}</Text>
                            </ListItem>
                            <ListItem itemDivider>
                                <Text style={{fontFamily: BOLD_FONT, color: TEXT_COLOR}}>Description</Text>
                            </ListItem>
                            <ListItem>
                                <Text style={{color: '#8d8d8d'}}>{item.description}</Text>
                            </ListItem>
                            <ListItem itemDivider>
                                <Text style={{fontFamily: BOLD_FONT, color: TEXT_COLOR}}>Date</Text>
                            </ListItem>
                            <ListItem>
                                <Text style={{color: '#8d8d8d'}}>{item.date}</Text>
                            </ListItem>
                        </List>
                    </View>
                </View>
            </Modal>

        </>
    );
};

export default NoteItem;
