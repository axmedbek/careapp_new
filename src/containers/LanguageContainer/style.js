import {StyleSheet} from 'react-native';
import {MAIN_COLOR} from "../../constants/colors";
import {REGULAR_FONT} from "../../constants/setting";

const styles = StyleSheet.create({
  langContainer: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    width: '100%',
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  langBtn: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderColor: MAIN_COLOR,
    borderWidth: 2,
    borderRadius: 4,
    width: 48,
    height: 48,
    margin: 4,
    paddingLeft: 8
  },
  langTxt: {
    color: MAIN_COLOR,
    fontFamily: REGULAR_FONT,
    fontSize: 18,
  },
  activeLang: {
    backgroundColor: MAIN_COLOR,
    justifyContent: 'center',
    borderColor: MAIN_COLOR,
    borderWidth: 2,
    borderRadius: 4,
    width: 48,
    height: 48,
    paddingLeft: 8
  },
  activeLangTxt: {
    color: 'white',
    fontFamily: REGULAR_FONT,
    fontSize: 18,
  },
});

export default styles;
