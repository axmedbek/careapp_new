import React,{useEffect} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './style';
import {updateLanguage} from "../../actions/lang.action";
import {connect} from "react-redux";
import {setOperationLoading} from "../../actions/loading.action";

const LanguageContainer = ({lang, setOperationLoading, updateLanguage, style}) => {

    useEffect(() => {
        setOperationLoading(false);
    }, [lang]);


    const handleChangeLang = newLang => {
        if (newLang !== lang.locale) {
            setOperationLoading(true);
            updateLanguage(newLang);
        }
    };

    return (
        <View style={Object.assign({}, styles.langContainer, style)}>
            <TouchableOpacity
                style={lang.locale === 'en' ? styles.activeLang : styles.langBtn}
                onPress={() => handleChangeLang('en')}>
                <Text style={lang.locale === 'en' ? styles.activeLangTxt : styles.langTxt}>
                    EN
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={lang.locale === 'da' ? styles.activeLang : styles.langBtn}
                onPress={() => handleChangeLang('da')}>
                <Text style={lang.locale === 'da' ? styles.activeLangTxt : styles.langTxt}>
                    DA
                </Text>
            </TouchableOpacity>
        </View>
    );
};


const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {
    updateLanguage,
    setOperationLoading
};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(LanguageContainer);
