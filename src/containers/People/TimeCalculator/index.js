import React from 'react';
import {Text, View} from "react-native";
import styles from './style';


const TimeCalculator = ({time}) => {
    let hours = Math.floor(time / 3600);
    let minutes = Math.floor((time - hours * 3600) / 60);
    let seconds = Math.floor(time - hours * 3600 - minutes * 60);

    hours = hours < 10 ? ` 0${hours} ` : hours;
    minutes = minutes < 10 ? ` 0${minutes} ` : minutes;
    seconds = seconds < 10 ? ` 0${seconds} ` : seconds;

    return (
        <View style={styles.container}>
            <View style={styles.timeContainer}>
                <Text style={styles.timeTxt}>{hours}</Text>
            </View>
            <View style={styles.timeContainer}>
                <Text style={styles.timeTxt}>{minutes}</Text>
            </View>
            <View style={styles.timeContainer}>
                <Text style={styles.timeTxt}>{seconds}</Text>
            </View>
        </View>
    );
};

export default TimeCalculator;
