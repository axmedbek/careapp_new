import {StyleSheet} from 'react-native';
import {BLUE_COLOR, LIGHT_DIVIDER_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: 150,
        backgroundColor: BLUE_COLOR,
        height: 40,
        alignItems: 'center',
        borderRadius: 6
    },
    timeContainer: {
        borderRightWidth: 1,
        borderRightColor: LIGHT_DIVIDER_COLOR,
        paddingLeft: 2,
        paddingRight: 2,
        justifyContent: 'center',
        height: 40,
        borderRadius: 6,
        width: '33%'
    },
    timeTxt: {
        color: 'white',
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: REGULAR_FONT,
        fontSize: 18
    }
});

export default styles;
