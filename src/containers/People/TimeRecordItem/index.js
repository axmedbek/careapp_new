import React from 'react';
import {View} from "react-native";
import {TEXT_COLOR} from "../../../constants/colors";
import Text from "../../../components/text/Text";

const TimeRecordItem = ({ item }) => {
    return (
        <View
            style={{
                height: 85,
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 10,
            }}
        >
            <View
                style={{
                    flex: 2,
                }}
            >
                <Text
                    style={{
                        color: TEXT_COLOR,
                        fontSize: 16,
                        fontWeight: 'bold',
                    }}
                >
                    {item.startdate}
                </Text>
                <Text
                    style={{

                        fontSize: 15,
                    }}
                >
                    {`${item.starttime} ${item.endtime}`}
                </Text>
                <Text
                    style={{
                        color: TEXT_COLOR,
                        fontSize: 15,
                    }}
                >
                    {item.type.title}
                </Text>

            </View>
            <View
                style={{
                    flex: 1,
                    fontSize: 15.2,
                }}
            >
                <Text
                    style={{
                        textAlign: 'right',
                        fontSize: 15,
                    }}
                >
                    {item.duration}
                </Text>
            </View>
        </View>
    );
};

export default TimeRecordItem;
