import React from 'react';
import AmazingCropper, { DefaultFooter } from 'react-native-amazing-cropper';
import {subsElementFromArray} from "../../../helpers/standard.helper";

const CustomImageCropper = ({route,navigation}) => {
    const {type,image,images,updateImages} = route.params;

    const imgWidth = image.width;
    const imgHeight = image.height;

    const onDone = (e) => {
        if (type === 'editing') {
            updateImages(subsElementFromArray(images,image.path,e),true);
        }
        else {
            updateImages(e);
        }
        navigation.navigate('ActivityAdd');
    };

    const onError = () => {
        alert("Something went wrong while cropping screen opening");
    };

    const onCancel = () => {
        navigation.navigate('ActivityAdd')
    };

    return (
        <AmazingCropper
            footerComponent={<DefaultFooter doneText='FINISH' rotateText='ROTATE' cancelText='BACK' />}
            onDone={onDone}
            onError={onError}
            onCancel={onCancel}
            imageUri={image.path}
            imageWidth={imgWidth}
            imageHeight={imgHeight}
            NOT_SELECTED_AREA_OPACITY={0.3}
            BORDER_WIDTH={20}
        />
    );
};

export default CustomImageCropper;
