import React from 'react';
import {TouchableOpacity, Image} from "react-native";
import {BACK_ICON_COLOR} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";

const CustomImagePreview = ({setLoadedImage, setModalImageLoading, image}) => {

    const handlePreviewImage = () => {
        setLoadedImage(image);
        setModalImageLoading(true);
    };

    return (
        <TouchableOpacity
            style={{backgroundColor: increase_brightness(BACK_ICON_COLOR, 70), marginLeft: 4, marginRight: 4}}
            onPress={handlePreviewImage}>
            <Image source={{uri: image}} style={{width: 60, height: 60, resizeMode: 'contain'}}/>
        </TouchableOpacity>
    )
};

export default CustomImagePreview;
