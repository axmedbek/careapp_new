import React from "react";
import {View, TouchableOpacity} from "react-native";
import styles from './style';
import SimpleAvatar from "../../../components/avatars/SimpleAvatar";
import PeopleCircleProgress from "../PeopleCircleProgress";
import Text from "../../../components/text/Text";
import {TEXT_COLOR} from "../../../constants/colors";
import {BOLD_FONT} from "../../../constants/setting";

const PeopleCard = ({navigation, item, handleLeftPress, handleRightPress}) => {
    return (
        <TouchableOpacity activeOpacity={.9} style={styles.card}
                          onPress={() => navigation.navigate('PersonOverviewNavigationContainer', {'item': item})}>
            <View style={styles.cardHeader}>
                <Text style={styles.user}>{item.citizen.firstname} {item.citizen.lastname}</Text>
                <View style={styles.avatar}>
                    <SimpleAvatar uri={{uri: item.citizen.avatar.small}}/>
                </View>
            </View>
            <View style={{flexDirection: "row",marginTop: 40}}>
                <View style={{flex: 1}}>
                    <Text center style={{minHeight: 42}}>{item.column1.title}</Text>
                    <Text
                        center
                        h4
                        style={{
                            fontWeight: "normal",
                            marginTop: 10,
                            marginBottom: 15,
                        }}
                    >
                        {item.column1.value}
                    </Text>
                    <Text center
                          style={{
                              fontWeight: "bold",
                              marginTop: 0,
                              marginBottom: 0,
                          }}>
                        {item.amount_circle.upper_text}
                    </Text>
                </View>
                <View style={{flex: 1}}>
                    <Text center style={{minHeight: 42}}>{item.column2.title}</Text>
                    <Text
                        center
                        h4
                        style={{
                            fontWeight: "normal",
                            marginTop: 10,
                            marginBottom: 15,
                        }}
                    >
                        {item.column2.value}
                    </Text>
                    <Text center
                          style={{
                              fontWeight: "bold",
                              marginTop: 0,
                              marginBottom: 0,
                          }}>
                        {item.timer_circle.upper_text}
                    </Text>
                </View>
            </View>
            <View style={styles.circleContainer}>
                <PeopleCircleProgress
                    percent={item.amount_circle.percent}
                    color={item.amount_circle.color}
                    under_text={item.amount_circle.under_text}
                    handleLeftPress={handleLeftPress}
                >
                    <View style={styles.circleContent}>
                        <Text style={{fontSize: 16}}>{item.amount.left}/{item.amount.right}</Text>
                        <Text style={{fontSize: 16}}>DDK</Text>
                    </View>
                </PeopleCircleProgress>
                <PeopleCircleProgress
                    percent={item.timer_circle.percent}
                    color={item.timer_circle.color}
                    under_text={item.timer_circle.under_text}
                    handleRightPress={handleRightPress}
                >
                    <View style={styles.circleContent}>
                        <Text style={{color: TEXT_COLOR, fontFamily: BOLD_FONT,fontSize: 16}}>{item.timer_circle.week}</Text>
                        <Text style={{fontSize: 16}}>{Math.round(item.timer.left)}/{item.timer.right}</Text>
                        <Text style={{fontSize: 16}}>{item.timer.text}</Text>
                    </View>
                </PeopleCircleProgress>
            </View>
        </TouchableOpacity>
    );
};

export default PeopleCard;
