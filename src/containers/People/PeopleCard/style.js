import {StyleSheet} from 'react-native';
import {BACK_ICON_COLOR, BLUE_COLOR, MAIN_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    card: {
        margin: 15,
        padding: 20,
        backgroundColor: 'white',
        borderRadius: 6,

        shadowColor: BLUE_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 2
    },
    cardHeader: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    user: {
        width: '80%',
        marginTop: 10,
        marginLeft: 8,
        color: TEXT_COLOR,
        fontFamily: REGULAR_FONT,
        fontSize: 18,
    },
    avatar: {
        position: 'absolute',
        right: 10,
        borderWidth: 1,
        borderRadius: 40,
        borderColor: increase_brightness(BACK_ICON_COLOR,80)
    },
    dateContainer: {
        marginTop: 30,
        flexDirection: 'row',
        backgroundColor: increase_brightness(MAIN_COLOR,90),
        borderRadius: 6
    },
    date: {
        width: '50%',
        justifyContent: 'center',
        padding: 6
    },
    dateHeaderTxt: {
        textAlign: 'center',
        fontFamily: REGULAR_FONT,
        color: MAIN_COLOR,
        fontSize: 16,
    },
    dateBottomTxt: {
        marginTop: 4,
        fontFamily: REGULAR_FONT,
        color: MAIN_COLOR,
        textAlign: 'center',
        borderWidth: 1,
        borderRadius: 20,
        borderColor: increase_brightness(MAIN_COLOR,80),
        padding: 4,
        marginLeft: 10,
        marginRight: 10
    },
    circleContainer: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    circleContent: {
        paddingTop: 20,
        height: 80,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default styles;
