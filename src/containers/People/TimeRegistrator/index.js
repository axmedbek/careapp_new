import React, {useState, useEffect} from "react";
import {View, Text, Switch, AppState} from "react-native";
import styles from './style';
import SimpleTimer from "../../../components/timers/SimpleTimer";
import {BLUE_COLOR} from "../../../constants/colors";
import {connect} from "react-redux";
import CoreModal from "../../../components/modals/CoreModal";
import {getTimerService, timeToggle, updateTimerRegister} from "../../../services/timer.service";
import PeopleTimeRegisterUpdate from "../../../forms/People/PeopleTimeRegisterUpdate";
import {getLocation, getLocationPermissionService} from "../../../services/location.service";
import {Toast} from "native-base";
import SurveyCaseModal from "../../../modals/People/SurveyCaseModal";
import {caseSurveyUpdate} from "../../../services/case.service";
import {getValueFromSettingWithKey, saveSetting} from "../../../config/caching/actions/setting";
import {isEmptyArray} from "formik";

const TimeRegistrator = ({case_id, lang, contacts, sessionTypes, setScreenLoading, questions}) => {

    const [isEnabled, setIsEnabled] = useState(false);
    const [sessionId, setSessionId] = useState(0);
    const [selectedTime, setSelectedTime] = useState(0);
    const [timeRegisterModal, setTimeRegisterModal] = useState(false);
    const [surveyModal, setSurveyModal] = useState(false);

    useEffect(() => {
        getSyncTimerWithApi();
        AppState.addEventListener('change', _handleAppStateChange);
        return () => {
            AppState.removeEventListener('change', _handleAppStateChange);
        };
    }, []);

    const _handleAppStateChange = (nextAppState) => {
        if (nextAppState === 'active') {
            getSyncTimerWithApi();
            if (getValueFromSettingWithKey("open_popup_timeregister") === "1") {
                if (!timeRegisterModal) setTimeRegisterModal(true);
            } else if (getValueFromSettingWithKey("open_popup_timeregister") === "2") {
                if (!surveyModal && questions.length > 0) setSurveyModal(true);
            }
        }
    };

    const toggleSwitch = async () => {
        if (!isEnabled) setSelectedTime(0);
        setScreenLoading(true);
        setIsEnabled(!isEnabled);
        await getLocationPermission(!isEnabled);
    };


    const timeToggleAPIOperation = (case_id, longitude, latitude) => {
        timeToggle(case_id, longitude, latitude, (response) => {
            console.log(response.data.data);
            if (!isEmptyArray(response.data.data)) {
                setSessionId(response.data.data.session.id);
                setScreenLoading(false);
                if (isEnabled) {
                    saveSetting("open_popup_timeregister", "1");
                    saveSetting("active_case_id", case_id.toString());
                    saveSetting("active_session_id", sessionId.toString());
                    setTimeRegisterModal(true);
                }
            }
        }, (error) => {
            console.log(error);
            setScreenLoading(false);
            setIsEnabled(!isEnabled);
            if (isEnabled) {
                saveSetting("open_popup_timeregister", "1");
                saveSetting("active_case_id", case_id.toString());
                saveSetting("active_session_id", sessionId.toString());
                setTimeRegisterModal(true);
            }
        })
    };

    const getLocationPermission = async () => {
        await getLocationPermissionService(() => {
            toggleTimer();
        }, () => {
            setScreenLoading(false);
            setIsEnabled(false);
        });
    };


    const toggleTimer = () => {
        getLocation((longitude, latitude) => {
            timeToggleAPIOperation(case_id, longitude, latitude);
        }, () => {
            setScreenLoading(false);
            setIsEnabled(!isEnabled);
        });
    };


    const getSyncTimerWithApi = () => {
        getTimerService(case_id, (response) => {
            if (!isEmptyArray(response.data.data)) {
                setIsEnabled(true);
                setSessionId(response.data.data.session.id);
                setSelectedTime(response.data.data.session.elapse_seconds);
            }
            setScreenLoading(false);
            console.log(response);
        }, (error) => {
            console.log(error);
            setScreenLoading(false);
            Toast.show({
                position: 'bottom',
                text: "Oops.Something went wrong!",
                buttonText: lang.messages.common.ok,
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        });
    };

    const handleSurveyUpdate = (data) => {
        saveSetting("open_popup_timeregister", "3");
        caseSurveyUpdate(case_id, data, response => {
            setSurveyModal(false);
            Toast.show({
                position: 'bottom',
                text: response.data.description,
                buttonText: lang.messages.common.ok,
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        }, error => {
            setSurveyModal(false);
            Toast.show({
                position: 'bottom',
                text: "Oops.Something went wrong!",
                buttonText: lang.messages.common.ok,
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        })
    };

    const updateTimerRegisterHandle = (typeValue, contactValue) => {
        saveSetting("open_popup_timeregister", "2");
        saveSetting("active_case_id", case_id.toString());
        updateTimerRegister(case_id, sessionId, typeValue, contactValue, (response) => {
            setTimeRegisterModal(false);
            setSurveyModal(true);
            Toast.show({
                position: 'bottom',
                text: response.data.description,
                buttonText: lang.messages.common.ok,
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        }, (error) => {
            console.log(error.response);
            setTimeRegisterModal(false);
            Toast.show({
                position: 'bottom',
                text: "Oops.Something went wrong!",
                buttonText: lang.messages.common.ok,
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        })
    };

    return (
        <View style={styles.container}>
            <View style={styles.timerBtnContainer}>
                <Text style={styles.timerTitle}>{lang.messages.common.timeregister}</Text>
                <Switch
                    trackColor={{false: "#767577", true: BLUE_COLOR}}
                    thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch}
                    style={{
                        transform: [{scaleX: 1.6}, {scaleY: 1.6}],
                        position: 'absolute',
                        bottom: 15,
                        left: '30%'
                    }}
                    value={isEnabled}
                />
            </View>
            <View style={styles.timerContainer}>
                <SimpleTimer time={selectedTime} setTime={setSelectedTime} style={styles.timer}
                             isEnabled={isEnabled} label={lang.messages.common.timer_info}/>
            </View>
            <CoreModal visible={timeRegisterModal}
                       modalStyle={{borderRadius: 4, padding: 10}}
                       hideModal={() => setTimeRegisterModal(false)} hideIcon={true}>
                <PeopleTimeRegisterUpdate lang={lang}
                                          updateTimeRegister={updateTimerRegisterHandle}
                                          contacts={contacts}
                                          sessionTypes={sessionTypes}
                />
            </CoreModal>
            {
                questions.length > 0 &&
                <CoreModal visible={surveyModal}
                           titleText={"Survey Case"}
                           modalStyle={{borderRadius: 4, padding: 10}}
                           hideModal={() => setSurveyModal(false)} hideIcon={true}>
                    <SurveyCaseModal handleSurveyUpdate={handleSurveyUpdate}
                                     surveys={questions}/>
                </CoreModal>
            }
        </View>
    );
};

const mapStateToProps = ({lang, caseReducer, contactReducer}) => {
    return {
        lang,
        caseItem: caseReducer.case,
        contacts: contactReducer.contacts,
        sessionTypes: caseReducer.sessionTypes,
        questions: caseReducer.questions
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TimeRegistrator);
