import {StyleSheet} from 'react-native';
import {
    BLUE_COLOR,
    LIGHT_DIVIDER_COLOR,
    TEXT_COLOR
} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 80,
        backgroundColor: 'white',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 20,
        borderRadius: 6,

        shadowColor: BLUE_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 2
    },
    timerBtnContainer: {
        width: '40%',
        borderRightWidth: 1,
        borderRightColor: LIGHT_DIVIDER_COLOR,
        textAlign: 'center',
    },
    timerContainer: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    timer: {
        color: 'white',
        padding: 10,
        backgroundColor: BLUE_COLOR,
        borderRadius: 6,
        textAlign: 'center',
        fontFamily: REGULAR_FONT,
        fontSize: 20,

        shadowColor: BLUE_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 8,
        elevation: 4
    },
    timerTitle: {
        fontFamily: REGULAR_FONT,
        color: TEXT_COLOR,
        marginTop: 4,
        fontSize: 16,
        textAlign: 'center'
    }
});

export default styles;
