
import React, { useRef, useState, useEffect } from "react"
import {View, StyleSheet, Text, TouchableOpacity, Image, PermissionsAndroid} from "react-native"
import PDFScanner from "@woonivers/react-native-document-scanner"
import {REGULAR_FONT, SCREEN_WIDTH} from "../../../constants/setting";
import {concatObjects, increase_brightness} from "../../../helpers/standard.helper";
import {MAIN_COLOR, RED_COLOR} from "../../../constants/colors";

export default function CameraScanner({ route,navigation }) {
    const {updateImages} = route.params;

    const pdfScannerElement = useRef(null);
    const [data, setData] = useState({});
    const [allowed, setAllowed] = useState(false);

    useEffect(() => {
        async function requestCamera() {
            const result = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: 'Camera permission',
                    message: 'App needs to access your camera',
                },
            );
            if (result === PermissionsAndroid.RESULTS.GRANTED)  setAllowed(true)
        }
        requestCamera()
    }, []);

    function handleOnPressRetry() {
        setData({})
    }
    function handleOnPress() {
        pdfScannerElement.current.capture()
    }

    function handleOnPressAccept() {
        updateImages(data.croppedImage);
        navigation.navigate('ActivityAdd')
    }

    if (!allowed) {
        return (
            <View style={styles.permissions}>
                <Text>You must accept camera permission</Text>
            </View>
        )
    }
    if (data.croppedImage) {
        return (
            <React.Fragment>
                <Image source={{ uri: data.croppedImage }} style={concatObjects(styles.preview,{
                    // height: data.height
                })} />
                <View style={styles.resultButtons}>
                    <TouchableOpacity onPress={handleOnPressAccept} style={styles.acceptButton}>
                        <Text style={styles.buttonResultText}>Accept</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={handleOnPressRetry} style={styles.retryButton}>
                        <Text style={styles.buttonResultText}>Retry</Text>
                    </TouchableOpacity>
                </View>
            </React.Fragment>
        )
    }
    return (
        <React.Fragment>
            <PDFScanner
                ref={pdfScannerElement}
                style={styles.scanner}
                onPictureTaken={setData}
                overlayColor="rgba(255,130,0, 0.7)"
                enableTorch={false}
                quality={1}
                detectionCountBeforeCapture={5}
                detectionRefreshRateInMS={50}
            />
            <TouchableOpacity onPress={handleOnPress} style={styles.button}>
                <Text style={styles.buttonText}>Take picture</Text>
            </TouchableOpacity>
        </React.Fragment>
    )
}

const styles = StyleSheet.create({
    scanner: {
        flex: 1,
        aspectRatio: undefined
    },
    resultButtons: {
        flexDirection: 'row',
        position: "absolute",
        bottom: 32,
        alignSelf: "center"
    },
    retryButton: {
        backgroundColor: increase_brightness(RED_COLOR,30),
        padding: 10,
        borderRadius: 10,
        marginLeft: 4
    },
    acceptButton: {
        backgroundColor: increase_brightness(MAIN_COLOR,30),
        padding: 10,
        borderRadius: 10,
        marginLeft: 4
    },
    buttonResultText: {
        fontFamily: REGULAR_FONT,
        color: 'white',
        fontSize: 32,
    },
    button: {
        backgroundColor: increase_brightness(MAIN_COLOR,80),
        alignSelf: "center",
        position: "absolute",
        bottom: 32,
        borderRadius: 10,
        padding: 10
    },
    buttonText: {
        fontFamily: REGULAR_FONT,
        color: MAIN_COLOR,
        fontSize: 32,
    },
    preview: {
        marginTop: 10,
        alignSelf: "center",
        width: SCREEN_WIDTH,
        height: '80%',
        resizeMode: "contain"
    },
    permissions: {
        flex:1,
        justifyContent: "center",
        alignItems: "center"
    }
});
