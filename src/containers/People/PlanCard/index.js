import React from 'react';
import {View,TouchableOpacity} from "react-native";
import {Card} from "react-native-elements";
import {DIVIDER_COLOR, LIGHT_COLOR} from "../../../constants/colors";
import Text from "../../../components/text/Text";

const PlanCard = ({ title,status,goal,color,onPress }) => {
    const Wrapper = onPress ? TouchableOpacity : View;

    return (
        <View
            style={{
                width: "50%",
                padding: 10,
            }}
        >
            <Wrapper onPress={onPress} activeOpacity={0.8}>
                <Card containerStyle={{ borderRadius: 5, margin: 0 }}>
                    <View
                        style={{
                            height: 160,
                            alignItems: "center",
                            justifyContent: "space-between",
                        }}
                    >
                        <View style={{marginLeft: 10,marginRight: 10,flexDirection: 'row'}}>
                            <Text style={{fontSize: 18,marginRight: 10}}>{goal}</Text>
                            <View
                                style={{
                                    width: 20,
                                    height: 20,
                                    borderRadius: 10,
                                    borderWidth: 3,
                                    borderColor: color,
                                    marginTop: 4,
                                    marginBottom: 10,
                                }}
                            />
                        </View>
                        <View style={{backgroundColor: DIVIDER_COLOR,height: 2,width: '100%',marginBottom: 20,marginTop: 10}}/>

                        <Text center>{title}</Text>
                        <Text center style={{ fontSize: 12, color: LIGHT_COLOR, marginTop: 15 }}>
                            {status}
                        </Text>
                    </View>
                </Card>
            </Wrapper>
        </View>
    );
};

export default PlanCard;
