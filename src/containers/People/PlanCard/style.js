import {StyleSheet} from 'react-native';
import {BACK_ICON_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '45%',
        height: 200,
        marginTop: 10,
        borderRadius: 4,
        marginLeft: 14,

        shadowColor: '#b2b2b2',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    title: {
        fontFamily: REGULAR_FONT,
        color: TEXT_COLOR,
        margin: 6,
        textAlign: 'center',
        fontSize: 20
    },
    divider: {
        borderBottomWidth: 1,
        borderBottomColor: '#d4d4d4',
        marginBottom: 8
    },
    dateTxt: {
        fontFamily: REGULAR_FONT,
        color: BACK_ICON_COLOR,
        position: 'absolute',
        bottom: 10,
        alignSelf: 'center'
    },
    statusTxt: {
        fontFamily: REGULAR_FONT,
        fontStyle: 'italic',
        color: BACK_ICON_COLOR,
        alignSelf: 'center'
    },
    contentStyle: {
        fontFamily: REGULAR_FONT,
        color: TEXT_COLOR,
        fontSize: 20,
        textAlign: 'center',
        margin: 20
    }
});

export default styles;
