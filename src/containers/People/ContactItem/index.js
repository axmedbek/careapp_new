import React from 'react';
import {LIGHT_COLOR,} from "../../../constants/colors";
import {callNumber} from "../../../helpers/standard.helper";
import Swipeout from 'react-native-swipeout';
import { ListItem } from "react-native-elements";


const ContactItem = ({contact, handleEdit, handleRemove}) => {
    return (
        <Swipeout
            autoClose={true}
            backgroundColor='transparent'
            right={contact.id!==0?
                [

                    {
                        onPress: () => handleEdit(contact),
                        text: 'Redigere',
                    },
                    {
                        backgroundColor:'red',
                        onPress: () => handleRemove(contact),
                        text: 'Slette',
                    },

                ]
                :
                [
                    {
                        backgroundColor:'red',
                        onPress: () => handleRemove(contact),
                        text: 'Slette',
                    },

                ]
            }
        >

            <ListItem
                title={contact.title + ' ' +  contact.firstname + ' ' +  contact.lastname}
                subtitle={contact.phone}
                subtitleStyle={{
                    color: LIGHT_COLOR,
                }}
                rightIcon={{
                    name: "phone",
                    color: LIGHT_COLOR,
                }}
                containerStyle={{
                    backgroundColor: "transparent",
                }}
                underlayColor="transparent"
                bottomDivider
                onLongPress={() => handleRemove(contact)}
                onPress={() => callNumber(contact.phone)}
            />
        </Swipeout>
    );
};

export default ContactItem;
