import {StyleSheet} from 'react-native';
import {BACK_ICON_COLOR, BLACK_COLOR,TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        margin: 4,
        backgroundColor: 'white',
        height: 100,
        padding: 10,
        borderRadius: 4,

        shadowColor: BLACK_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 8,
        elevation: 4
    },
    title: {
        fontFamily: REGULAR_FONT,
        fontSize: 18,
        color: TEXT_COLOR
    },
    statusIcon: {
        marginTop: 4,
        color: 'white',
        width: 30,
        height: 30,
        borderRadius: 15,
        paddingLeft: 9,
        paddingTop: 8
    },
    amount: {
        position: 'absolute',
        right: 10,
        bottom: 10,
        fontFamily: REGULAR_FONT,
        fontSize: 18,
    },
    statusTxt: {
        marginTop: 9,
        marginLeft: 6,
        fontFamily: REGULAR_FONT,
        color: BACK_ICON_COLOR,
        fontSize: 16
    },
    fileIcon: {
        position: 'absolute',
        right: 10,
        top: 10,
        fontSize: 30,
        color: BACK_ICON_COLOR
    },
    dateTxt: {
        position: 'absolute',
        fontFamily: REGULAR_FONT,
        fontSize: 15,
        bottom: 8,
        left: 12,
        color: BACK_ICON_COLOR
    }
});

export default styles;
