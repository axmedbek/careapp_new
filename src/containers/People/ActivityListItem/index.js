import React from 'react';
import {View, Text, TouchableOpacity} from "react-native";
import {LIGHT_COLOR, TEXT_COLOR} from "../../../constants/colors";
import Icon from "react-native-vector-icons/MaterialIcons";


const ActivityListItem = ({item, handleClick}) => {
    return (
        <TouchableOpacity
            onPress={handleClick}
            style={{
                height: 80,
                width: '100%',
                paddingHorizontal: 10,
                justifyContent: 'center',
            }}
        >
            <Text
                style={{
                    fontSize: 15.5,
                }}
            >
                {item.title}
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View
                    style={{
                        width: 14,
                        height: 14,
                        borderRadius: 7,
                        backgroundColor: item.status.color,
                        color: TEXT_COLOR,
                    }}
                >
                </View>
                <Text
                    style={{
                        marginLeft: 5,
                        fontSize: 14.5,
                        color: LIGHT_COLOR,
                    }}
                >
                    {item.status.text}
                </Text>
            </View>
            <Text
                style={{
                    fontSize: 14,
                    color: LIGHT_COLOR,
                }}
            >
                {item.date}
            </Text>
            <Text
                style={{
                    position: 'absolute',
                    right: 10,
                    fontSize: 15,
                    bottom: 10
                }}
            >
                {item.amount.value}
            </Text>
            <Text
                style={{
                    position: 'absolute',
                    right: 10,
                    fontSize: 18,
                    top: 10
                }}
            >
                {item.files.length > 0 ? <Icon name={"attach-file"} style={{fontSize: 20}}/> : ''}
            </Text>
        </TouchableOpacity>
    );
};

export default ActivityListItem;
