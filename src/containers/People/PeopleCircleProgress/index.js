import React from 'react';
import {Text, View,TouchableOpacity, StyleSheet} from "react-native";
import AnimatedCircularProgress from 'react-native-animated-circular-progress';
import {LIGHT_COLOR, LIGHT_DIVIDER_COLOR, MAIN_BG_COLOR} from "../../../constants/colors";

const PeopleCircleProgress = ({percent, color, under_text,handleLeftPress,handleRightPress,children}) => {
    return (
        <View style={styles.circle}>
            <TouchableOpacity onPress={handleLeftPress ? handleLeftPress : handleRightPress}>
                <AnimatedCircularProgress
                    backgroundColor={LIGHT_DIVIDER_COLOR}
                    color={color}
                    startDeg={0}
                    endDeg={percent}
                    radius={60}
                    innerRadius={50}
                    duration={1000}
                >
                    {children}
                </AnimatedCircularProgress>
            </TouchableOpacity>
            <Text style={styles.underTxt}>{under_text}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    circle: {
        marginRight: '8%',
        marginLeft: '8%',
        alignItems: 'center'
    },
    upperTxt: {
        marginTop: 10,
        marginBottom: 10,
        fontSize: 16
    },
    underTxt: {
        marginTop: 15,
        marginBottom: 4,
        fontSize: 16
    }
});

export default PeopleCircleProgress;
