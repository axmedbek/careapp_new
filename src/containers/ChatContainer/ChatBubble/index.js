import React from 'react';
import {TextInput, TouchableOpacity, View} from "react-native";
import styles from "../../../pages/ChatPage/ChatInbox/style";
import {concatObjects} from "../../../helpers/standard.helper";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const ChatBubble = ({sendMessage, openOptions, inputHeight, setInputHeight, setMessageValue, messageValue}) => {
    return (
        <View style={styles.bottom}>
            <TextInput style={concatObjects(styles.messageBox, {
                height: Math.max(50, inputHeight)
            })} placeholder={"Write something.."}
                       value={messageValue}
                       multiline={true}
                       onContentSizeChange={(event) => {
                           setInputHeight(event.nativeEvent.contentSize.height)
                       }}
                       onChangeText={e => setMessageValue(e)}/>
            <View style={styles.operationBtn}>
                <TouchableOpacity style={styles.fileContainer} onPress={openOptions}>
                    <MaterialIcons style={styles.file} name={"attach-file"}/>
                </TouchableOpacity>
                <TouchableOpacity style={styles.sendContainer} onPress={sendMessage}>
                    <MaterialIcons style={styles.send} name={"send"}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default ChatBubble;
