import React from 'react';
import {Text, StyleSheet, View, TouchableOpacity, Image, Linking} from "react-native";
import {BACK_ICON_COLOR, BLACK_COLOR} from "../../../constants/colors";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import moment from "moment";
moment.locale('de');

import {checkImageOrFile, increase_brightness} from "../../../helpers/standard.helper";

const fileHolder = require('../../../assets/file.png');

const MessageItem = ({message}) => {

    console.log(message);

    const fileContainer = () => {
        return (
            <TouchableOpacity onPress={() => Linking.openURL(message.body)}>
                {
                    checkImageOrFile(message.body)
                        ? <Image source={{uri: message.body}} style={{width: 200, height: 150}}/>
                        : <Image source={fileHolder} style={{width: 200, height: 150}}/>
                }
            </TouchableOpacity>
        )
    };

    return (
        <>
            {
                message.is_operation ?
                    <TouchableOpacity style={styles.operation}>
                        <Text style={styles.operationMessage}>{message.body}</Text>
                        <View style={styles.bottom}>
                            <Text style={styles.date}>{moment(message.updated_at).startOf('hour').fromNow()}</Text>
                        </View>
                    </TouchableOpacity>
                    :
                    (
                        message.own ?
                            <TouchableOpacity style={styles.own}>
                                {
                                    message.type === "file"
                                        ? fileContainer()
                                        : <Text style={styles.message}>{message.body}</Text>
                                }
                                <View style={styles.bottom}>
                                    <Text
                                        style={styles.date}>{moment(message.updated_at).format('YYYY-MM-DD HH:mm:ss')}</Text>
                                    <Icon style={styles.read} name={"check-all"}/>
                                </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.user}>
                                <Text style={styles.username}>{message.user.firstname} {message.user.lastname}</Text>
                                {
                                    message.type === "file"
                                        ? fileContainer()
                                        : <Text style={styles.message}>{message.body}</Text>
                                }
                                <View style={styles.bottom}>
                                    {
                                        message.type === "file" &&
                                        <TouchableOpacity style={{position: 'absolute', right: 4}}>
                                            <Icon name={"download"} style={{fontSize: 25, color: BLACK_COLOR}}/>
                                        </TouchableOpacity>
                                    }
                                    <Text
                                        style={styles.date}>{moment(message.updated_at).format('YYYY-MM-DD HH:mm:ss')}</Text>
                                </View>
                            </TouchableOpacity>
                    )
            }
        </>
    );
};

const styles = StyleSheet.create({
    operation: {
        // alignSelf: 'flex-end',
        backgroundColor: increase_brightness(BLACK_COLOR, 10),
        marginRight: 15,
        marginLeft: 15,
        marginTop: 25,
        // minWidth: 150,
        borderRadius: 8,

        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 10,
        paddingRight: 10,

        shadowColor: BLACK_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 8,
        elevation: 2
    },
    own: {
        alignSelf: 'flex-end',
        backgroundColor: increase_brightness('#caf592', 30),
        marginRight: 15,
        marginTop: 25,
        minWidth: 200,
        borderRadius: 8,

        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 10,
        paddingRight: 10,

        shadowColor: BLACK_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 8,
        elevation: 2
    },
    user: {
        backgroundColor: 'white',
        alignSelf: 'flex-start',
        marginLeft: 15,
        marginTop: 25,
        minWidth: 200,
        borderRadius: 8,

        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 10,
        paddingRight: 10,

        shadowColor: BLACK_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 8,
        elevation: 2
    },
    bottom: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    date: {
        marginTop: 4,
        color: BACK_ICON_COLOR,
        fontStyle: 'italic'
    },
    read: {
        marginTop: 8,
        position: 'absolute',
        right: 4,
        fontSize: 16,
        color: 'green'
    },
    operationMessage: {
        // maxWidth: 200,
        color: 'white',
        lineHeight: 18
    },
    message: {
        maxWidth: 200,
        color: increase_brightness('#242424', 20),
        lineHeight: 18
    },
    username: {
        fontSize: 16,
        color: '#0093E9',
        marginBottom: 6
    }
});

export default MessageItem;
