import React from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "../../../pages/ChatPage/ChatList/style";

const ContactListItem = ({navigation, contact}) => {

    return (
        <TouchableOpacity style={styles.chatContainer} onPress={() => navigation.navigate('ChatInbox',
            {
                id: contact.id,
                title: `${contact.firstname} ${contact.lastname}`,
                is_group: false,
                avatar: contact.avatar.small,
                users: [contact.id],
                dialogue: 0
            })}>
            <Image source={{uri: contact.avatar.small}} style={styles.chatImage}/>
            <View style={styles.chatContent}>
                <View style={styles.chatHeader}>
                    <Text style={styles.chatTitle}>{contact.firstname} {contact.lastname}</Text>
                </View>
                <Text style={styles.desc}>{contact.type}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default ContactListItem;
