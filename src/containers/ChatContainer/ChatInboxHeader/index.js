import React from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "../../../pages/ChatPage/ChatInbox/style";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MultiImageAvatar from "../../../components/avatars/MultiImageAvatar";
import Entypo from "react-native-vector-icons/Entypo";

const ChatInboxHeader = ({navigation, title, dialogue, is_group, avatar}) => {
    return (
        <View style={styles.header}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <MaterialIcons style={styles.backIcon} name={"arrow-back"}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('ChatProfile', {dialogue: dialogue})}>
                {
                    is_group
                        ? avatar ? <Image style={styles.headerImage} source={{uri: avatar}}/> :
                        <MultiImageAvatar size={"small"}/>
                        : <Image style={styles.headerImage} source={{uri: avatar}}/>

                }
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('ChatProfile', {dialogue: dialogue})}>
                <View>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.status}>user</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuContainer} onPress={() => console.log("---")}>
                <Entypo style={styles.menu} name={"dots-three-vertical"}/>
            </TouchableOpacity>
        </View>
    );
};

export default ChatInboxHeader;
