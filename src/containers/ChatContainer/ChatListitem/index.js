import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import styles from "../../../pages/ChatPage/ChatList/style";
import moment from "moment";
import {
    getChatAvatar,
    getUserArrayFromChat,
    truncateText
} from "../../../helpers/standard.helper";
import {isObject} from "formik";


const ChatListitem = ({ navigation,chat }) => {
    return (
        <TouchableOpacity style={styles.chatContainer} onPress={() => navigation.navigate('ChatInbox',{
            id: chat.id,
            title: chat.title,
            avatar: chat.image_url,
            users: getUserArrayFromChat(chat),
            dialogue: chat._id,
            is_group: chat.type !== "dialogue"
        })}>
            {getChatAvatar(chat)}
            <View style={styles.chatContent}>
                <View style={styles.chatHeader}>
                    <Text style={styles.chatTitle}>{truncateText(chat.title,20)}</Text>
                    <Text style={styles.chatDate}>{moment(chat.updated_at).fromNow()}</Text>
                </View>
                <Text style={styles.desc}>{isObject(chat.message)
                    ? truncateText(chat.message.body,20)
                    : truncateText(chat.message,20)}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default ChatListitem;
