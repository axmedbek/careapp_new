import React, {useEffect, useState, useRef} from 'react';
import {ScrollView, View, Image, Text, Keyboard, BackHandler} from 'react-native';
import styles from './style';
import LanguageContainer from '../LanguageContainer';
import {connect} from 'react-redux';
import {getProfileInfo} from "../../actions/auth.action";
import StandardButton from "../../components/buttons/StandardButton";
import PhoneInputComponent from "../../components/inputs/PhoneInputComponent";
import PasswordInputComponent from "../../components/inputs/PasswordInputComponent";
import {showToast} from "../../actions/setting.action";
import DataLoadingComponent from "../../components/loadings/DataLoadingComponent";
import {Toast} from 'native-base';
import {BOLD_FONT} from "../../constants/setting";
import {loginService} from "../../services/auth.service";
import {saveTokens} from "../../config/caching/actions/token";


const logo = require('../../assets/images/logo/logo.png');


const LoginContainer = ({navigation, lang, getProfileInfo}) => {

    const [phone, setPhone] = useState('');
    const [phoneError, setPhoneError] = useState(false);
    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState(false);
    const [isKeyboardVisible, setKeyboardVisible] = useState(false);
    const [loading, setLoading] = useState(false);

    const passwordRef = useRef(null);

    useEffect(() => {

        BackHandler.addEventListener("hardwareBackPress", () => {
            BackHandler.exitApp();
            return true
        });


        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                setKeyboardVisible(true); // or some other action
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setKeyboardVisible(false); // or some other action
            }
        );

        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
            BackHandler.removeEventListener("hardwareBackPress");
        };
    }, []);


    const handleSignIn = () => {
        if (phone.length < 1 || password.length < 1) {
            if (phone.length < 1) setPhoneError(true);
            if (password.length < 1) setPasswordError(true);
            Toast.show({
                position: 'top',
                text: lang.messages.validations.login,
                buttonText: lang.messages.common.ok,
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
            return;
        }
        setLoading(true);

        loginService({
            'lang': lang.locale,
            'phone': phone,
            'password': password
        }, response => {
            saveTokens(response.data.access_token);
            getProfileInfo();
            setLoading(false);
            navigation.navigate('HomeScreenNavigator');
        }, error => {
            setLoading(false);
            Toast.show({
                position: 'top',
                text: 'Phone or password is incorrect.',
                buttonText: lang.messages.common.ok,
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
        });
    };

    const imageStyleHide = Object.assign({}, styles.logoImage, {display: 'none'});

    return (
        <View style={styles.container}>
            <DataLoadingComponent loading={loading}/>
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.imageContainer}>
                        <Image
                            style={isKeyboardVisible ? imageStyleHide : styles.logoImage}
                            source={logo}
                        />
                        <Text style={isKeyboardVisible ? {fontSize: 30, fontFamily: BOLD_FONT} : styles.title}>
                            {lang.messages.common.welcome}
                        </Text>
                        <Text style={styles.desc}>{lang.messages.login.desc}</Text>
                    </View>
                    <View>
                        <PhoneInputComponent phoneError={phoneError}
                                             setPhoneError={setPhoneError}
                                             phone={phone}
                                             setPhone={setPhone}
                                             icon={"user"}
                                             passwordRef={passwordRef}/>
                        <PasswordInputComponent passwordError={passwordError}
                                                setPasswordError={setPasswordError}
                                                password={password}
                                                setPassword={setPassword}
                                                passwordRef={passwordRef}
                                                handleSignIn={handleSignIn}
                        />
                        <StandardButton text={lang.messages.login.submit}
                                        handleButton={handleSignIn}/>
                    </View>
                </View>
            </ScrollView>
            {
                isKeyboardVisible || <LanguageContainer/>
            }
        </View>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {
    showToast,
    getProfileInfo
};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(LoginContainer);
