import {StyleSheet} from 'react-native';
import {BOLD_FONT, REGULAR_FONT} from "../../constants/setting";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 30,
  },
  logoImage: {
    width: 300,
    height: 160,
  },
  title: {
    fontSize: 30,
    fontFamily: BOLD_FONT,
    marginTop: 50,
  },
  desc: {
    fontFamily: REGULAR_FONT,
    fontSize: 20,
    marginTop: 10,
  }
});

export default styles;
