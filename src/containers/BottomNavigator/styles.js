import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        height: 65,
        backgroundColor: 'white',
        width: '100%',
        paddingRight: 10,
        borderColor: '#ccd3e0',
        borderWidth: 1
    }
});

export default styles;
