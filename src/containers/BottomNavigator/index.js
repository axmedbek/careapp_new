import React from 'react';
import styles from './styles';
import {View} from "react-native";
import MenuList from "../Menu/MenuList";
import {useRoute} from '@react-navigation/native';


const BottomNavigator = ({lang,navigation}) => {
    const route = useRoute();

    return (
        <View style={styles.container}>
            <MenuList navigation={navigation} list={[
                {title: lang.messages.home.title, route: 'Home', icon: 'home', active: route.name === 'Home'},
                {title: lang.messages.notes.title, route: 'Notes', icon: 'note', active: route.name === 'Notes'},
                {title: lang.messages.people.title, route: 'People', icon: 'heart', active: route.name === 'People'},
                {title: lang.messages.calendar.title, route: 'Calendar', icon: 'calendar', active: route.name === 'Calendar'},
                {title: lang.messages.inbox.title, route: 'Inbox', icon: 'inbox', active: route.name === 'Inbox'}
            ]}/>
        </View>
    );
};


export default BottomNavigator;
