import React, {useState} from 'react';
import {TouchableOpacity} from 'react-native';
import EventCard from "../../EventCard";
import ConfirmToast from "../../../components/toasts/ConfirmToast";
import {getDeviceUser} from "../../../config/caching/actions/setting";
import {Toast} from "native-base";
import {deleteEventService} from "../../../services/event.service";
import {getEvents} from "../../../actions/calendar.action";

const EventItem = ({navigation, isSmall, event, handleDeleteEvent}) => {

    return (
        <>
            <TouchableOpacity
                activeOpacity={0.8}
                onLongPress={() => handleDeleteEvent(event)}
                onPress={() => navigation.navigate('CreateEvent', {event: event, dateString: ''})}>
                <EventCard
                    isSmall={isSmall}
                    event={event}
                />
            </TouchableOpacity>
        </>
    );
};

export default EventItem;
