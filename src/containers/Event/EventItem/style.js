import {StyleSheet} from "react-native";
import {
    BACK_ICON_COLOR,
    BLACK_COLOR,
    GRAY_COLOR,
    LIGHT_COLOR,
    TEXT_COLOR
} from "../../../constants/colors";
import {increase_brightness,} from "../../../helpers/standard.helper";
import {REGULAR_FONT} from "../../../constants/setting";
import React from "react";

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: 210,
        width: 'auto',
        margin: 10,
        borderRadius: 4,
        padding: 15,

        shadowColor: BLACK_COLOR,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 8,
        elevation: 4
    },
    header: {
        flexDirection: 'row'
    },
    titleTxt: {
        fontSize: 18,
        marginTop: 4,
        marginBottom: 4
    },
    contentTxt: {
        width: 'auto',
        fontFamily: REGULAR_FONT,
        fontSize: 15,
        lineHeight: 18,
        color: LIGHT_COLOR
    },
    avatarGroupStyle: {
        position: 'absolute',
        flexDirection: 'row'
    },
    // contentIcon: {
    //     position: 'absolute',
    //     right: -3,
    //     borderRadius: 15,
    //     width: 30,
    //     height: 30,
    //     textAlign: 'center',
    //     justifyContent: 'center',
    //     fontSize: 25,
    //     paddingTop: 2,
    //     color: 'white'
    // },
    footer: {
        flexDirection: 'row',
        marginTop: 10
    },
    footerIdContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: increase_brightness(BACK_ICON_COLOR, 90),
        color: TEXT_COLOR,
        // padding: 4,
        paddingLeft: 4,
        paddingRight: 4,
        borderRadius: 4,

        width: '35%'
    },
    footerIdTxt: {
        // backgroundColor: increase_brightness(BACK_ICON_COLOR,90),
        color: increase_brightness(TEXT_COLOR, 20),
        padding: 4,
        fontSize: 14
        // borderRadius: 4
    },
    footerIdIcon: {
        color: increase_brightness(TEXT_COLOR, 20),
        marginTop: 5,
        marginLeft: 4,
        fontSize: 18
    },
    footerDateContainer: {
        flexDirection: 'row',
        backgroundColor: increase_brightness(BACK_ICON_COLOR, 90),
        marginLeft: 6,
        borderRadius: 4,
        width: '65%',
        justifyContent: 'center'
    },
    footerDateIcon: {
        marginTop: 5,
        marginLeft: 6,
        color: increase_brightness(TEXT_COLOR, 20),
        fontSize: 18
    },
    footerDateTxt: {
        marginLeft: 6,
        marginTop: 4,
        marginRight: 6,
        color: increase_brightness(TEXT_COLOR, 20),
        fontFamily: REGULAR_FONT,
        fontSize: 14
    },
    footerAuthorContainer: {
        position: 'absolute',
        right: -5,
        flexDirection: 'row',
        backgroundColor: increase_brightness(BACK_ICON_COLOR, 90),
        height: 27,
        borderRadius: 4
    },
    divider: {
        height: 1,
        backgroundColor: LIGHT_COLOR,
        marginTop: 15,
        marginBottom: 6
    },
    authorContainer: {
        flexDirection: 'row',
        marginTop: 10
    },
    authorIcon: {
        fontSize: 30,
        color: GRAY_COLOR,
        backgroundColor: 'white'
    },
    authorTxt: {
        fontFamily: REGULAR_FONT,
        fontSize: 14,
        height: 28,
        padding: 6,
        borderRadius: 4,
        color: BACK_ICON_COLOR
    },
    // smallAvatar: {
    //     width: 40,
    //     height: 40,
    //     borderRadius: 10,
    //     borderColor: SECONDARY_BG_COLOR
    // },
    // smallAvatarContainer: {
    //     bottom: 14,
    //     right: 4,
    //     elevation: 10
    // },
    // avatarContainer: {
    //     right: 10,
    //     top: 12,
    //     elevation: 10
    // },
    startDateLabel: {
        position: 'absolute',
        top: 0,
        fontSize: 12,
        // textDecorationLine: 'underline',
        textAlign: 'left',
        fontWeight: 'bold',
        marginTop: -13,
        paddingRight: 100,
        // width: 50,height: 20
    },
    endDateLabel: {
        position: 'absolute',
        top: 0,
        fontSize: 12,
        textDecorationLine: 'underline',
        textAlign: 'left',
        fontWeight: 'bold',
        marginTop: -13,
        paddingLeft: 100,
        width: 50,
        height: 20
    }
});

export default styles;


// <View style={concatObjects(styles.container, isSmall ? {width: 250, height: 250} : {})}>
//     <AvatarGroupComponent borderColor={LIGHT_DIVIDER_COLOR} avatars={getDataWithSelectedCount(event.users, 3)}
//                           avatarStyle={isSmall ? styles.smallAvatar : {}}
//                           style={concatObjects(styles.avatarGroupStyle,
//                               isSmall ? styles.smallAvatarContainer : styles.avatarContainer)}/>
//     <View style={concatObjects(styles.header, isSmall ? {width: '100%'} : {})}>
//         <Icon style={concatObjects(styles.headerIcon, {color: event.category.bg_color})}
//               name={event.type === "event" ? "event-available" : "bookmark"}/>
//         <View style={Object.assign({}, styles.headerTxtContainer, {
//             backgroundColor: increase_brightness(event.category.bg_color, 90)
//         }, isSmall ? {width: 200} : {})}>
//             <Text style={concatObjects(styles.headerTxt, {
//                 color: event.category.bg_color
//             })}>{event.category.title}</Text>
//         </View>
//     </View>
//     <View style={styles.contentContainer}>
//         <Text>{event.title}</Text>
//         <Text style={styles.contentTxt}>
//             {
//                 event.description.length > 0
//                     ? truncateText(event.description, 100)
//                     : not_found_description
//             }
//         </Text>
//         <View style={concatObjects(styles.contentIcon, {
//             backgroundColor: event.status.color
//         })}/>
//     </View>
//     <View style={concatObjects(styles.footer, isSmall ?
//         {flexDirection: 'column', position: 'absolute', left: 4, bottom: 20, width: 200} : {})}>
//         <View
//             style={concatObjects(styles.footerIdContainer, isSmall ? {minWidth: '70%'} : {}, event.type === "event" ? {width: '50%'} : {})}>
//             <Text style={{
//                 position: 'absolute',
//                 width: '100%', marginTop: -13,
//                 fontSize: 12, textAlign: 'center'
//             }}><Text style={{
//                 fontSize: 12,
//                 // backgroundColor: increase_brightness(BACK_ICON_COLOR, 90),
//                 color: TEXT_COLOR, borderRadius: 4, fontWeight: 'bold', textDecorationLine: 'underline'
//             }}>Creator</Text></Text>
//             <View style={{flexDirection: 'row'}}>
//                 {/*<Icon name={"person"} style={styles.footerIdIcon}/>*/}
//                 <Text style={styles.footerIdTxt}>{truncateText(event.creator.fullname, 12)}</Text>
//             </View>
//         </View>
//         <View style={concatObjects(styles.footerDateContainer, isSmall ? {
//             width: '70%', marginLeft: 0, marginTop: 4, height: 25, justifyContent: 'flex-start'
//         } : {}, event.type === "event" ? {width: '50%'} : {})}>
//             {/*<Icon style={styles.footerDateIcon} name={"alarm"}/>*/}
//             <Text style={concatObjects(styles.startDateLabel,
//                 {
//                     backgroundColor: increase_brightness(event.category.bg_color, 90),
//                     color: event.category.bg_color
//                 })}>Start dato</Text>
//             {
//                 event.type === "todo" &&
//                 <Text style={concatObjects(styles.endDateLabel,
//                     {
//                         backgroundColor: increase_brightness(event.category.bg_color, 90),
//                         color: event.category.bg_color
//                     })}>Deadline</Text>
//             }
//             <Text style={styles.footerDateTxt}>{moment(new Date(event.date)).format("LL")}</Text>
//             {
//                 event.type === "todo" &&
//                 <>
//                     <View
//                         style={{width: 1, height: '100%', backgroundColor: increase_brightness(LIGHT_COLOR, 70)}}/>
//                     <Text style={styles.footerDateTxt}>{moment(new Date(event.date_deadline)).format("LL")}</Text>
//                 </>
//             }
//         </View>
//     </View>
// </View>
