import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
        marginTop: 10
    }
});

export default styles;
