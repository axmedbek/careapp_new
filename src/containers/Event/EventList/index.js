import React, {useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import EventItem from "../EventItem";
import {getEvents} from "../../../actions/calendar.action";
import {connect} from "react-redux";
import {getDeviceUser} from "../../../config/caching/actions/setting";
import {Toast} from "native-base";
import {deleteEventService} from "../../../services/event.service";
import ConfirmToast from "../../../components/toasts/ConfirmToast";
import ListItemEmpty from "../../ListItemEmpty";

const EventList = ({eventLoading, navigation, data, getEvents,events,
                       horizontal = false, showsHorizontalScrollIndicator = false,
                       isSmall = false,reloadEvent, ...rest}) => {

    const [deletedEvent, setDeletedEvent] = useState(null);

    const refresh = () => {
        getEvents();
    };

    useEffect(() => {
        if (reloadEvent) {
            if (!eventLoading) {
                reloadEvent();
            }
        }
    },[events]);

    const handleDeleteEvent = (event) => {
        if (getDeviceUser().type !== "employee") {
            setDeletedEvent(event);
        } else {
            if (parseInt(event.creator.id) === parseInt(getDeviceUser().user_id)) {
                setDeletedEvent(event);
            } else {
                Toast.show({
                    position: 'top',
                    text: "You can delete only events which you have created !",
                    buttonText: "OK",
                    type: 'danger',
                    textStyle: {marginLeft: 10},
                    duration: 4000
                });
            }
        }
    };

    const handleDeleteEventProcess = () => {
        deleteEventService(deletedEvent.id, () => {
            Toast.show({
                position: 'top',
                text: deletedEvent.type + " was deleted successfully!",
                buttonText: "OK",
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
            getEvents();
        }, (error) => {
            Toast.show({
                position: 'top',
                text: "Event/Todo couldn't delete!",
                buttonText: "OK",
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        });
        setDeletedEvent(null);
    };

    return (
        <>
            <FlatList
                data={data}
                keyExtractor={item => item.id.toString()}
                onRefresh={refresh}
                refreshing={eventLoading}
                ListEmptyComponent={<ListItemEmpty loading={eventLoading} text="No events"/>}
                renderItem={({item}) => <EventItem event={item} isSmall={isSmall} navigation={navigation}
                                                   handleDeleteEvent={handleDeleteEvent}/>}
                horizontal={horizontal}
                showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
                {...rest}
            />
            <ConfirmToast
                show={deletedEvent !== null}
                handClose={() => setDeletedEvent(null)}
                type={"info"}
                message={"Do you sure to delete this " + (deletedEvent ? deletedEvent.type : "-")}
                item={deletedEvent}
                deleteHandle={handleDeleteEventProcess}
                errors={null}
            />
        </>
    );
};

const mapStateToProps = ({eventReducer}) => {
    return {
        eventLoading: eventReducer.fetching,
        events: eventReducer.events
    };
};

const mapDispatchToProps = {
    getEvents
};

export default connect(mapStateToProps, mapDispatchToProps)(EventList);
