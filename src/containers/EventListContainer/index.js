import React, {useEffect, useState} from 'react';
import {FlatList, StatusBar, View} from "react-native";
import {getEventsActiveDate} from "../../helpers/standard.helper";
import EventItem from "../Event/EventItem";
import {getEvents} from "../../actions/calendar.action";
import {connect} from "react-redux";
import ListItemEmpty from "../ListItemEmpty";
import AuthenticatedScreenHOC from "../../HOCs/AuthenticatedScreenHOC";
import {MAIN_COLOR} from "../../constants/colors";
import {getValueFromSettingWithKey, saveSetting} from "../../config/caching/actions/setting";
import CoreModal from "../../components/modals/CoreModal";
import PeopleTimeRegisterUpdate from "../../forms/People/PeopleTimeRegisterUpdate";
import SurveyCaseModal from "../../modals/People/SurveyCaseModal";
import {getCaseQuestions, getContacts, getSessionTypes} from "../../actions/case.action";
import {caseSurveyUpdate} from "../../services/case.service";
import {Toast} from "native-base";
import {updateTimerRegister} from "../../services/timer.service";
import EventList from "../Event/EventList";
import {getProfileInfo} from "../../actions/auth.action";

const EventListContainer = ({
                                lang, navigation, getEvents, user, events,
                                contacts, sessionTypes, questions, getContacts,
                                getCaseQuestions, getSessionTypes, getProfileInfo
                            }) => {

    const [timeRegisterModal, setTimeRegisterModal] = useState(false);
    const [surveyModal, setSurveyModal] = useState(false);

    useEffect(() => {
        if (sessionTypes && sessionTypes.length > 0) {
            if (getValueFromSettingWithKey("open_popup_timeregister") === "1") {
                if (!timeRegisterModal) setTimeRegisterModal(true);
            } else if (getValueFromSettingWithKey("open_popup_timeregister") === "2") {
                if (!surveyModal && questions.length > 0) setSurveyModal(true);
            }
        }
    }, [sessionTypes]);


    useEffect(() => {
        getCaseQuestions(getValueFromSettingWithKey("active_case_id"));
        getContacts();
        getSessionTypes();
        getEvents();
    }, []);

    // useEffect(() => {
    //     if (user.isLogged) {
    //         getEvents();
    //         getProfileInfo();
    //     }
    // }, [user.isLogged]);

    const handleSurveyUpdate = (data) => {
        saveSetting("open_popup_timeregister", "3");
        caseSurveyUpdate(getValueFromSettingWithKey("active_case_id"), data, response => {
            setSurveyModal(false);
            Toast.show({
                position: 'bottom',
                text: response.data.description,
                buttonText: lang.messages.common.ok,
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        }, error => {
            setSurveyModal(false);
            Toast.show({
                position: 'bottom',
                text: "Oops.Something went wrong!",
                buttonText: lang.messages.common.ok,
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        })
    };

    const updateTimerRegisterHandle = (typeValue, contactValue) => {
        saveSetting("open_popup_timeregister", "2");
        saveSetting("active_case_id", getValueFromSettingWithKey("active_case_id"));

        updateTimerRegister(getValueFromSettingWithKey("active_case_id"),
            getValueFromSettingWithKey("active_session_id"), typeValue,
            contactValue, (response) => {
                setTimeRegisterModal(false);
                setSurveyModal(true);
                Toast.show({
                    position: 'bottom',
                    text: response.data.description,
                    buttonText: lang.messages.common.ok,
                    type: 'success',
                    textStyle: {marginLeft: 10},
                    duration: 4000
                });
            }, (error) => {
                setTimeRegisterModal(false);
                console.log(error.response);
                Toast.show({
                    position: 'bottom',
                    text: "Oops.Something went wrong!",
                    buttonText: lang.messages.common.ok,
                    type: 'danger',
                    textStyle: {marginLeft: 10},
                    duration: 4000
                });
            }
        )
    };


    return (
        <AuthenticatedScreenHOC title={lang.messages.home.title}
                                navigation={navigation}
                                lang={lang}
                                menuIcon={true}>
            <StatusBar backgroundColor={MAIN_COLOR}/>
            <EventList
                navigation={navigation}
                data={getEventsActiveDate(events)}
                showsHorizontalScrollIndicator={true}
            />
            <CoreModal visible={timeRegisterModal}
                       modalStyle={{borderRadius: 4, padding: 10}}
                       hideModal={() => setTimeRegisterModal(false)} hideIcon={true}>
                <PeopleTimeRegisterUpdate lang={lang}
                                          updateTimeRegister={updateTimerRegisterHandle}
                                          contacts={contacts}
                                          sessionTypes={sessionTypes}
                />
            </CoreModal>
            <CoreModal visible={surveyModal}
                       titleText={"Survey Case"}
                       modalStyle={{borderRadius: 4, padding: 10}}
                       hideModal={() => setSurveyModal(false)} hideIcon={true}>
                {
                    questions.length > 0 &&
                    <SurveyCaseModal handleSurveyUpdate={handleSurveyUpdate}
                                     surveys={questions}/>
                }
            </CoreModal>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, user, eventReducer, contactReducer, caseReducer}) => {
    return {
        lang,
        events: eventReducer.events,
        eventLoading: eventReducer.fetching,
        user,
        contacts: contactReducer.contacts,
        sessionTypes: caseReducer.sessionTypes,
        questions: caseReducer.questions
    };
};

const mapDispatchToProps = {
    getEvents,
    getContacts,
    getSessionTypes,
    getCaseQuestions,
    getProfileInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(EventListContainer);
