import React from 'react';
import Icon from "react-native-vector-icons/MaterialIcons";
import {concatObjects} from "../../helpers/standard.helper";

const EventTypeIcon = ({color, type}) => {
    return (
        <Icon style={concatObjects({fontSize: 30}, {color: color})}
              name={type === "event" ? "event-available" : "bookmark"}/>
    );
};

export default EventTypeIcon;
