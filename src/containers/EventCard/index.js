import React from 'react';
import {View} from 'react-native';
import Text from "../../components/text/Text";
import {concatObjects, truncateText} from "../../helpers/standard.helper";
import styles from '../../containers/Event/EventItem/style';
import EventTypeIcon from "./EventTypeIcon";
import CategoryTitle from "./CategoryTitle";
import AvatarGroup from "./AvatarGroup";
import StatusIcon from "./StatusIcon";
import EventFooter from "./EventFooter";
import moment from "moment";

const EventCard = ({event, isSmall}) => {
    return (
        <View style={concatObjects(styles.container, isSmall ? {
            width: 300
        } : {})}>
            <View style={styles.header}>
                <EventTypeIcon color={event.category.bg_color} type={event.type}/>
                <CategoryTitle color={event.category.bg_color} title={event.category.title}/>
                <AvatarGroup users={event.users}/>
            </View>
            <View>
                <Text style={styles.titleTxt}>{truncateText(event.title, 25)}</Text>
                <Text style={styles.contentTxt}>{truncateText(event.description, isSmall ? 110 : 125)}</Text>
                <StatusIcon color={event.status.color}/>
            </View>
            <EventFooter
                url={event.creator.avatar.small}
                name={event.creator.fullname}
                end_date={event.date_deadline}
                start_date={moment(event.date_raw).format("DD-MM-YYYY")}
                color={event.category.bg_color}
                isTodo={event.type === "todo"}
            />
        </View>
    )
};

export default EventCard;
