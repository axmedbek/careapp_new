import React from 'react';
import Text from "../../components/text/Text";
import {concatObjects, increase_brightness} from "../../helpers/standard.helper";
import {StyleSheet} from "react-native";

const CategoryTitle = ({ title,color }) => {
    return (
        <Text style={concatObjects(styles.headerTxt,
            {color: 'white',backgroundColor: color})}>
            {title}
        </Text>
    );
};

export default CategoryTitle;

const styles = StyleSheet.create({
    headerTxt: {
        fontWeight: 'bold',
        fontSize: 15,
        marginTop: 2,
        borderRadius: 4,
        paddingLeft: 6,
        paddingRight: 6,
        paddingTop: 2,
        height: 26
    }
});
