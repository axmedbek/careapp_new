import React from 'react';
import {StyleSheet, View} from "react-native";
import {concatObjects} from "../../helpers/standard.helper";

const StatusIcon = ({ color }) => {
    return (
        <View style={concatObjects(styles.contentIcon, {
            backgroundColor: color
        })}/>
    );
};

export default StatusIcon;

const styles = StyleSheet.create({
    contentIcon: {
        position: 'absolute',
        top: 10,
        right: 0,
        borderRadius: 15,
        width: 30,
        height: 30,
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 25,
        color: 'white'
    }
});
