import React from 'react';
import {StyleSheet, View} from "react-native";
import Text from "../../components/text/Text";
import SimpleAvatar from "../../components/avatars/SimpleAvatar";
import {concatObjects, truncateText} from "../../helpers/standard.helper";
import {DIVIDER_COLOR} from "../../constants/colors";
import {SCREEN_WIDTH_ORIGINAL} from "../../constants/setting";

const EventFooter = ({url, color, name, start_date, end_date,isTodo}) => {
    return (
        <>
            <View style={styles.divider}/>

            <View style={styles.container}>
                <View style={{flexDirection: 'row', width: '65%'}}>
                    <SimpleAvatar uri={{uri: url}} height={30} width={30} style={{marginTop: 6}}/>
                    <View style={styles.avatarContainer}>
                        <Text style={concatObjects(styles.avatarLabelTxt, {
                            backgroundColor: color
                        })}>Creator</Text>
                        <Text style={styles.text}>{truncateText(name, 16)}</Text>
                    </View>
                </View>
                <View style={{width: '35%', alignItems: 'flex-end', flexDirection: 'row'}}>
                    <View style={concatObjects(styles.dateContainer,!isTodo ? {
                        position: 'absolute',
                        right: 0
                    } : {})}>
                        <Text style={concatObjects(styles.dateLabelTxt, {
                            backgroundColor: color
                        })}>Start date</Text>
                        <Text style={styles.text}>{start_date}</Text>
                    </View>
                    {/*{*/}
                    {/*    isTodo &&*/}
                    {/*    <View style={styles.dateContainer}>*/}
                    {/*        <Text style={concatObjects(styles.dateLabelTxt, {*/}
                    {/*            backgroundColor: color*/}
                    {/*        })}>End date</Text>*/}
                    {/*        <Text style={styles.text}>{end_date}</Text>*/}
                    {/*    </View>*/}
                    {/*}*/}
                </View>
            </View>
        </>
    );
};

export default EventFooter;


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 10,
        left: 12,
        width: '100%'
    },
    dateLabelTxt: {
        fontSize: 14,
        color: 'white',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        padding: 2,
        paddingLeft: 6,
        paddingRight: 8
    },
    avatarContainer: {
        marginLeft: 6,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: DIVIDER_COLOR
    },
    avatarLabelTxt: {
        fontSize: 14,
        color: 'white',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        padding: 2,
        paddingLeft: 6
    },
    divider: {
        position: 'absolute',
        bottom: 65,
        width: '100%',
        height: 1,
        backgroundColor: DIVIDER_COLOR
    },
    text: {
        fontSize: 14,
        paddingLeft: 4,
        paddingRight: 6
    },
    dateContainer: {
        marginLeft: 4,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: DIVIDER_COLOR
    }
});

