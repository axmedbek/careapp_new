import React from 'react';
import AvatarGroupComponent from "../../components/avatars/AvatarGroupComponent";
import {LIGHT_DIVIDER_COLOR, SECONDARY_BG_COLOR} from "../../constants/colors";
import {concatObjects, getDataWithSelectedCount} from "../../helpers/standard.helper";
import {StyleSheet} from "react-native";

const AvatarGroup = ({ users,isSmall }) => {
    return (
        <AvatarGroupComponent borderColor={LIGHT_DIVIDER_COLOR} avatars={getDataWithSelectedCount(users, 3)}
                              avatarStyle={isSmall ? styles.smallAvatar : {}}
                              style={concatObjects(styles.avatarGroupStyle,
                                  isSmall ? styles.smallAvatarContainer : styles.avatarContainer)}/>
    );
};

export default AvatarGroup;

const styles = StyleSheet.create({
    smallAvatar: {
        width: 40,
        height: 40,
        borderRadius: 10,
        borderColor: SECONDARY_BG_COLOR
    },
    smallAvatarContainer: {
        bottom: 14,
        right: 4,
        elevation: 10
    },
    avatarContainer: {
        right: 0,
        top: 0,
        elevation: 10
    },
    avatarGroupStyle: {
        position: 'absolute',
        flexDirection: 'row'
    }
});
