import React from 'react';
import {View, Text} from "react-native";
import {RED_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const FormAPIError = ({show, message}) => {
    return (
        show &&
        <View style={{backgroundColor: RED_COLOR,marginLeft: 10,marginRight: 10,height: 40,borderRadius: 4}}>
            <Text style={{
                textAlign: 'center',
                fontFamily: REGULAR_FONT,
                fontSize: 18,
                color: 'white',
                paddingTop: 6
            }}>
                {message}
            </Text>
        </View>
    );
};

export default FormAPIError;
