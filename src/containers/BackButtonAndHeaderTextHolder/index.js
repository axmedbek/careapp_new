import React from 'react';
import BackButton from "../../components/buttons/BackButton";
import HeaderTextHolder from "../../components/headers/HeaderTextHolder";
import {View} from "react-native";

const BackButtonAndHeaderTextHolder = ({ handleSignIn,text }) => {
    return (
        <View>
            <BackButton handleSignIn={handleSignIn}/>
            <HeaderTextHolder text={text}/>
        </View>
    );
};

export default BackButtonAndHeaderTextHolder;
