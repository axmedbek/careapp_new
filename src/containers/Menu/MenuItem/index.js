import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import Icon from "react-native-vector-icons/dist/Octicons";
import styles from './style.js'

const MenuItem = ({navigation,item}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={() => navigation.navigate(item.route)}>
            <Icon style={item.active ? styles.iconActive : styles.icon} name={item.icon}/>
            <Text style={item.active ? styles.textActive : styles.text}>{item.title}</Text>
        </TouchableOpacity>
    );
};

export default MenuItem;
