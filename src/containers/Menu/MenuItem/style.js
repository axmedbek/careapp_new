import {StyleSheet} from "react-native";
import {MAIN_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {REGULAR_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        width: '20%',
        marginLeft: 10,
        alignItems: 'center',
        marginTop: 10
    },
    icon: {
        fontSize: 25,
        color: TEXT_COLOR
    },
    iconActive: {
        fontSize: 25,
        color: MAIN_COLOR
    },
    text: {
        fontFamily: REGULAR_FONT,
        color: TEXT_COLOR,
        marginTop: 4
    },
    textActive: {
        fontFamily: REGULAR_FONT,
        marginTop: 4,
        color: MAIN_COLOR
    }
});

export default styles;
