import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: 20,
        marginRight: 20
    }
});

export default styles;
