import React from 'react';
import {View} from 'react-native';
import MenuItem from "../MenuItem";
import styles from './style';

const MenuList = ({navigation,list}) => {
    return (
        <View style={styles.container}>
            {
                list.map((item, index) => (
                    <MenuItem navigation={navigation} key={++index} item={item}/>
                ))
            }
        </View>
    );
};

export default MenuList;
