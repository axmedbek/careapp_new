import React from 'react';
import {ActivityIndicator, View, Text} from "react-native";
import {MAIN_COLOR} from "../../constants/colors";

const ListItemEmpty = ({text, loading}) => {
    return (
        <View
            style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
            }}
        >
            {loading ? (
                <ActivityIndicator color={MAIN_COLOR}/>
            ) : (
                <Text
                    style={{
                        textAlign: "center",
                        lineHeight: 30,
                        paddingHorizontal: 60,
                    }}
                >
                    {text}
                </Text>
            )}
        </View>
    );
};

export default ListItemEmpty;
