import React, {useState, useEffect} from 'react';
import {View, Text, FlatList} from 'react-native';
import {connect} from "react-redux";
import AuthenticatedScreenHOC from "../../HOCs/AuthenticatedScreenHOC";
import DataLoadingComponent from "../../components/loadings/DataLoadingComponent";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import NoteItem from "../../containers/Notes/NoteItem";
import ListItemEmpty from "../../containers/ListItemEmpty";
import CircleActionButton from "../../components/buttons/CircleActionButton";
import CoreModal from "../../components/modals/CoreModal";
import NoteAddForm from "../../forms/Notes/NoteAddForm";
import ConfirmToast from "../../components/toasts/ConfirmToast";
import {addEditNoteForEvent, deleteNote, getNotes} from "../../actions/note.action";
import styles from "../NotePages/Note/style";
import Modal from "../../components/modals/Modal";


const EventNote = ({lang, route, navigation, addEditNoteForEvent, deleteNote, noteErrors, deleting, adding, notes, loading, getNotes}) => {
    const {id, title} = route.params;

    const [addModal, setAddModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [item, setItem] = useState(null);


    useEffect(() => {
        getNotes(`todo_id=${id}`);
    }, []);

    useEffect(() => {
        if (!deleting) {
            setDeleteModal(false);
            getNotes(`todo_id=${id}`);
        }
    }, [deleting]);


    useEffect(() => {
        if (!adding) {
            setAddModal(false);
            getNotes(`todo_id=${id}`);
        }
    }, [adding]);

    const handleEdit = (item) => {
        setItem(item);
        setAddModal(true);
    };

    const handleRemove = (item) => {
        setItem(item);
        setDeleteModal(true);
    };

    const refresh = () => {
        getNotes(`todo_id=${id}`);
    };

    return (
        <AuthenticatedScreenHOC title={`${title}`} navigation={navigation} lang={lang} backIcon={true}>
            <DataLoadingComponent loading={loading && deleteModal}/>
            <View style={styles.container}>
                <FlatList
                    data={notes}
                    renderItem={({item}) =>
                        <NoteItem item={item}
                                  handleEdit={() => handleEdit(item)}
                                  handleRemove={() => handleRemove(item)}
                        />}
                    contentContainerStyle={{flexGrow: 1}}
                    ListEmptyComponent={<ListItemEmpty loading={loading} text="No mapper"/>}
                    keyExtractor={item => item.id.toString()}
                    onRefresh={refresh}
                    refreshing={loading}
                />
                <CircleActionButton icon={"note-plus"} handlePress={() => {
                    setItem(null);
                    setAddModal(true);
                }}/>
                <Modal
                    closeModal={() => setAddModal(false)}
                    modalVisible={addModal}
                    title={item ? "Edit Note" : "Add Note"}
                >
                    <NoteAddForm
                        hideModal={() => setAddModal(false)}
                        addEditNote={addEditNoteForEvent}
                        noteErrors={{}}
                        loading={adding}
                        event_id={id}
                        item={item}
                    />
                </Modal>
                <ConfirmToast
                    show={deleteModal}
                    handClose={() => setDeleteModal(false)}
                    type={"info"}
                    message={"Do you sure to delete this note"}
                    item={item}
                    deleteHandle={deleteNote}
                    errors={noteErrors}
                />
            </View>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, noteReducer}) => {
    return {
        lang,
        notes: noteReducer.notes,
        loading: noteReducer.fetching,
        deleting: noteReducer.deleting,
        adding: noteReducer.adding,
        noteErrors: noteReducer.errors,
        addFolderLoading: noteReducer.addFolderLoading,
    };
};

const mapDispatchToProps = {
    getNotes,
    addEditNoteForEvent,
    deleteNote
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EventNote);
