import React from 'react';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import Carousel from "../../../components/sliders/Carousel";
import {getOnlyImageUrlsFromActivity} from "../../../helpers/standard.helper";

const ActivityImageSlider = ({route,navigation}) => {
    const {images} = route.params;

    return (
        <AuthenticatedScreenHOC title={"Activity Image Slider"} navigation={navigation}
                                menuIcon={false} backIcon={true}>
            <Carousel images={getOnlyImageUrlsFromActivity(images)}/>
        </AuthenticatedScreenHOC>
    );
};

export default ActivityImageSlider;
