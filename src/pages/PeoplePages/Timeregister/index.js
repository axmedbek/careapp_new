import React, {useState, useEffect} from 'react';
import {View, FlatList} from "react-native";
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {DIVIDER_COLOR} from "../../../constants/colors";
import {getCaseRecords} from "../../../actions/case.action";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import TimeRecordItem from "../../../containers/People/TimeRecordItem";
import SimpleDateTimePicker from "../../../components/datepickers/SimpleDateTimePicker";
import moment from "moment";
import StandardButton from "../../../components/buttons/StandardButton";
import Text from "../../../components/text/Text";

const Timeregister = ({route, navigation, lang, getCaseRecords, errors, loading, records}) => {
    const {item} = route.params;

    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);

    useEffect(() => {
        getCaseRecords(item.id);
    }, []);

    const refresh = () => {
        if (endDate && startDate) {
            getCaseRecords(item.id, moment(startDate).format("YYYY-MM-DD hh:mm:ss"), moment(endDate).format("YYYY-MM-DD hh:mm:ss"));
        } else if (endDate) {
            getCaseRecords(item.id, moment(new Date()).format("YYYY-MM-DD hh:mm:ss"), moment(endDate).format("YYYY-MM-DD hh:mm:ss"));
        } else if (startDate) {
            getCaseRecords(item.id, moment(startDate).format("YYYY-MM-DD hh:mm:ss"), moment(new Date()).format("YYYY-MM-DD hh:mm:ss"));
        } else {
            getCaseRecords(item.id);
        }
    };

    const handleDateChange = (type, e) => {
        if (type === 'start') {
            setStartDate(e);
            getCaseRecords(item.id, moment(e).format("YYYY-MM-DD hh:mm:ss"), moment(endDate ? endDate : new Date()).format("YYYY-MM-DD hh:mm:ss"));
        } else {
            setEndDate(e);
            getCaseRecords(item.id, moment(startDate ? startDate : new Date()).format("YYYY-MM-DD hh:mm:ss"), moment(e).format("YYYY-MM-DD hh:mm:ss"));
        }
    };

    const handleClearDates = () => {
        setStartDate(null);
        setEndDate(null);
        getCaseRecords(item.id);
    };

    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.timeregister} navigation={navigation}
                                menuIcon={true} backIcon={true}>
            <View style={{flexDirection: 'row', width: '96%', marginTop: -20, marginLeft: 6}}>
                <SimpleDateTimePicker placeholder={"Start Date"} style={{width: '36%'}}
                                      date={startDate} setDate={(e) => handleDateChange('start', e)}
                                      format={"YYYY-MM-DD"}
                />
                <View style={{width: '2%'}}/>
                <SimpleDateTimePicker placeholder={"End Date"} style={{width: '36%'}}
                                      date={endDate} setDate={(e) => handleDateChange('end', e)}
                                      format={"YYYY-MM-DD"}
                />
                <StandardButton text={"Clear"} style={{width: '24%',height: 47, marginTop: 42}} handleButton={handleClearDates}/>
            </View>
            <View style={{marginVertical: 15,flexDirection: 'row'}}>
                <Text style={{fontWeight: 'bold', marginRight: 10,marginLeft: 10}}>Total: </Text>
                <Text>{records.total_duration ? records.total_duration.text : 'no records'}</Text>
            </View>
            <FlatList
                data={records.data}
                renderItem={({item}) => <TimeRecordItem item={item}/>}
                contentContainerStyle={{flexGrow: 1, paddingBottom: 30}}
                ListEmptyComponent={<ListItemEmpty loading={loading} text="No records"/>}
                keyExtractor={item => item.id.toString()}
                onRefresh={refresh}
                ItemSeparatorComponent={() => <View
                    style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: DIVIDER_COLOR,
                    }}>

                </View>}
                refreshing={loading}
            />
        </AuthenticatedScreenHOC>
    );
};


const mapStateToProps = ({lang, caseReducer}) => {
    return {
        lang,
        records: caseReducer.records,
        errors: caseReducer.errors,
        loading: caseReducer.fetching
    };
};

const mapDispatchToProps = {
    getCaseRecords
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Timeregister);
