import React, {useState,useEffect} from 'react';
import {View, FlatList} from "react-native";
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {DIVIDER_COLOR} from "../../../constants/colors";
import {getCase} from "../../../actions/case.action";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import styles from './style';
import Text from "../../../components/text/Text";


const ContactInfo = ({route, navigation, lang, getCase, caseItem, loading}) => {
    const {item} = route.params;
    const [data,setData] = useState([]);

    useEffect(() => {
        if (caseItem) {
            if (caseItem.contact_persons.length > 0) {
                const obj = caseItem.contact_persons;
                const data =[
                    {'id': 'Fornavn', 'name':obj[0].firstname},
                    {'id': 'Efternavn', 'name':obj[0].lastname},
                    {'id': 'Telefonnummer', 'name':obj[0].phone},
                    {'id': 'Email adresse', 'name':obj[0].email},
                    //  {'id': 'Gender', 'name':obj[0].gender},
                ];
                setData(data);
            }
        }
    },[caseItem]);

    useEffect(() => {
        getCase(item.id);
    }, []);


    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.contact_info} navigation={navigation}
                                menuIcon={true} backIcon={true}>
            <FlatList
                data={data}
                ItemSeparatorComponent={() => <View
                    style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: DIVIDER_COLOR,
                    }}>

                </View>}
                renderItem={({item}) =>
                    <View
                        style={{
                            height:80,
                            width:'100%',
                            paddingHorizontal: 15,
                            justifyContent:'center',
                        }}
                    >

                        <View>
                            <Text style={styles.id}>{item.id}</Text>

                            <Text style={styles.name}>{item.name}</Text>
                        </View>


                    </View>
                }
                contentContainerStyle={{flexGrow: 1, paddingTop: 10,}}
                ListEmptyComponent={
                    <ListItemEmpty loading={loading} text="No Info"/>
                }
                keyExtractor={item => item.id.toString()}
            />
            <DataLoadingComponent loading={loading}/>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, caseReducer}) => {
    return {
        lang,
        caseItem: caseReducer.case,
        loading: caseReducer.fetching,
    };
};

const mapDispatchToProps = {
    getCase
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ContactInfo);
