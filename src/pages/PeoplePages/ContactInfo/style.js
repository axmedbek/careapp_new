import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    title:{
        marginLeft:10,
        fontWeight: 'bold',
        fontSize:19,
        color:'black',
    },
    id:{
        fontWeight:'bold',
        fontSize:17,
        color:'black',
        marginLeft:10,
    },
    name:{
        fontSize:16,
        marginLeft:10,

    },
});

export default styles;
