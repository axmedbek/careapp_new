import React, {useEffect,useState} from 'react';
import {ScrollView} from "react-native";
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import PeopleCard from "../../../containers/People/PeopleCard";
import TimeRegistrator from "../../../containers/People/TimeRegistrator";
import {getCase, getCaseQuestions, getContacts, getSessionTypes} from "../../../actions/case.action";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import {getEventsActiveDate} from "../../../helpers/standard.helper";
import EventList from "../../../containers/Event/EventList";




const PersonOverview = ({route, navigation, events, lang, getContacts, getCase, getSessionTypes,getCaseQuestions}) => {
    const {item} = route.params;

    const [screenLoading,setScreenLoading] = useState(true);

    useEffect(() => {
        getSessionTypes();
        getContacts();
        getCase(item.id);
        getCaseQuestions(item.id);
    }, []);

    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.title} navigation={navigation}
                                menuIcon={true} backIcon={true} backRoute={() => navigation.navigate('People')}>
            <ScrollView>
                <TimeRegistrator case_id={item.id} setScreenLoading={setScreenLoading}/>
                <PeopleCard navigation={navigation}
                            item={item}
                            handleRightPress={() => navigation.navigate('Timeregister',{item: item})}
                            handleLeftPress={() => navigation.navigate('Economy',{item: item})}
                />
                <EventList
                    navigation={navigation}
                    data={getEventsActiveDate(events)}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    isSmall={true}
                    style={{paddingLeft: 6,marginRight: 10,marginBottom: 20}}
                />
            </ScrollView>
            <DataLoadingComponent loading={screenLoading}/>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, eventReducer, caseReducer, contactReducer}) => {
    return {
        lang,
        events: eventReducer.events,
        caseItem: caseReducer.case,
        contacts: contactReducer.contacts,
        sessionTypes: caseReducer.sessionTypes
    };
};

const mapDispatchToProps = {
    getCase,
    getSessionTypes,
    getContacts,
    getCaseQuestions
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PersonOverview);
