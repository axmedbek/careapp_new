import React, {useEffect, useState} from 'react';
import {View, Text, FlatList} from "react-native";
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {DIVIDER_COLOR} from "../../../constants/colors";
import {getCase} from "../../../actions/case.action";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import styles from './style';


const PersonInfo = ({route, navigation, lang, getCase, caseItem, loading}) => {
    const {item} = route.params;
    const [data, setData] = useState([]);

    useEffect(() => {
        if (caseItem) {
            if (caseItem.citizen) {
                const obj = caseItem.citizen;
                const data = [
                    {'id': 'Fornavn', 'name': obj.firstname},
                    {'id': 'Efternavn', 'name': obj.lastname},
                    {'id': 'Telefonnummer', 'name': obj.phone},
                    {'id': 'Email adresse', 'name': obj.email},
                    //  {'id': 'Gender', 'name':obj.gender},
                ];
                if (typeof obj.address !== 'undefined') {
                    data.push({'id': 'Adresse', 'name': obj.address});
                }
                setData(data);
            }
        }

    }, [caseItem]);

    useEffect(() => {
        console.log(item.id);
        getCase(item.id);
    }, []);


    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.info} navigation={navigation}
                                menuIcon={true} backIcon={true}>
            <FlatList
                data={data}
                ItemSeparatorComponent={() => <View
                    style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: DIVIDER_COLOR,
                    }}>

                </View>}
                renderItem={({item}) => <View
                    style={{
                        height: 80,
                        width: '100%',
                        paddingHorizontal: 15,
                        justifyContent: 'center',
                    }}
                >
                    <View>
                        <Text style={styles.id}>{item.id}</Text>

                        <Text style={styles.name}>{item.name}</Text>
                    </View>


                </View>}
                contentContainerStyle={{flexGrow: 1, paddingTop: 10,}}
                ListEmptyComponent={
                    <ListItemEmpty loading={loading} text="No Info"/>
                }
                keyExtractor={item => item.id.toString()}
            />
            <DataLoadingComponent loading={loading}/>
        </AuthenticatedScreenHOC>
    );
};


const mapStateToProps = ({lang, caseReducer}) => {
    return {
        lang,
        caseItem: caseReducer.case,
        loading: caseReducer.fetching,
    };
};

const mapDispatchToProps = {
    getCase
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PersonInfo);
