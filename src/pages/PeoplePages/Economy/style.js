import {StyleSheet} from 'react-native';
import {MAIN_COLOR} from "../../../constants/colors";
import {BOLD_FONT, REGULAR_FONT} from "../../../constants/setting";
import {increase_brightness} from "../../../helpers/standard.helper";

const styles = StyleSheet.create({
    activeTab: {
        backgroundColor: MAIN_COLOR
    },
    tab: {
        backgroundColor: increase_brightness(MAIN_COLOR,90),
    },
    tabTextTitle: {
        color: MAIN_COLOR,
        fontFamily: BOLD_FONT,
        fontSize: 20,
    },
    tabActiveTextTitle: {
        color: 'white',
        fontFamily: BOLD_FONT,
        fontSize: 20,
    },
    tabTextDesc: {
        color: MAIN_COLOR,
        fontFamily: REGULAR_FONT,
        fontSize: 16,
    }
});

export default styles;
