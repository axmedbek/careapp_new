import {StyleSheet} from 'react-native';
import {increase_brightness} from "../../../../helpers/standard.helper";
import {BACK_ICON_COLOR, TEXT_COLOR} from "../../../../constants/colors";
import {REGULAR_FONT} from "../../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        margin: 20,
        padding: 10,
        paddingBottom: 10,
        borderRadius: 4,
        backgroundColor: 'white',
    },
    upload: {
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row'
    },
    uploadDesc: {
        fontFamily: REGULAR_FONT,
        fontSize: 18,
        color: TEXT_COLOR,
    },
    uploadBtn: {
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 30,
        backgroundColor: increase_brightness(BACK_ICON_COLOR, 70),
        width: 60,
        height: 60,
        marginLeft: 10
    },
    uploadIcon: {
        fontSize: 40,
        color: BACK_ICON_COLOR,
        marginTop: 8
    }
});

export default styles;
