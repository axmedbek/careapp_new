import React, {useEffect,useState} from 'react';
import AuthenticatedScreenHOC from "../../../../HOCs/AuthenticatedScreenHOC";
import {View, Text, TouchableOpacity, Image, ScrollView} from "react-native";
import StandardInputComponent from "../../../../components/inputs/StandardInputComponent";
import StandardButton from "../../../../components/buttons/StandardButton";
import Icon from "react-native-vector-icons/AntDesign";
import ImagePicker from 'react-native-image-crop-picker';
import CustomImagePreview from "../../../../containers/People/CustomImagePreview";
import AddActivityModal from "../../../../modals/People/AddActivityModal";
import styles from './style';
import {SCREEN_WIDTH} from "../../../../constants/setting";
import {ActionSheet, Badge} from 'native-base';
import ImagePreviewModal from "../../../../modals/People/ImagePreviewModal";
import {deleteElementFromArray} from "../../../../helpers/standard.helper";
import {caseActivityLastBudgetDate, createNewActivity} from "../../../../services/case.service";
import moment from "moment";
import DataLoadingComponent from "../../../../components/loadings/DataLoadingComponent";
import SimpleDateTimePicker from "../../../../components/datepickers/SimpleDateTimePicker";
import {LIGHT_DIVIDER_COLOR, MAIN_COLOR} from "../../../../constants/colors";


const ActivityAdd = ({route,navigation}) => {
    const {item,getCaseActivities} = route.params;

    const case_id = item.id;

    const [title, setTitle] = useState('');
    const [errorTitle, setErrorTitle] = useState(false);
    const [date, setDate] = useState(new Date());
    const [minDate, setMinDate] = useState(new Date().getTime());
    const [amount, setAmount] = useState(null);
    const [errorAmount, setErrorAmount] = useState(false);
    const [modalLoading, setModalLoading] = useState(false);
    const [modalImageLoading, setModalImageLoading] = useState(false);
    const [loadedImage, setLoadedImage] = useState(null);
    const [images, setImages] = useState([]);

    const [loading, setLoading] = useState(false);
    const [optionsOpen, setOptionsOpen] = useState(false);


    useEffect(() => {
        setLoading(true);
        caseActivityLastBudgetDate(case_id,response => {
            console.log(response);
            setLoading(false);
            setMinDate(new Date(response.data*1000));
        },error => {
            console.log(error.response);
            setLoading(false);
        });
    }, []);


    const updateImages = (image,status) => {
        // make replace
        if (status) {
            setImages(image);
        }
        else {
            setImages([...images, image]);
        }
    };

    const handleOpenCamera = () => {
        ImagePicker.openCamera({
            compressImageQuality: 0.5,
            cropping: false
        }).then(image => {
            navigation.navigate('CustomImageCropper', {type: 'save', image: image, updateImages: updateImages});
            setModalLoading(false);
        });
    };


    const handleGetDirectlyReceipt = () => {
        // setModalLoading(false);
        navigation.navigate('CameraScanner', {updateImages: updateImages});
    };

    const continueEditingImage = (image) => {
        setModalImageLoading(false);
        Image.getSize(image, (width, height) => {
            navigation.navigate('CustomImageCropper', {type: 'editing', image: {path: image,height: height,width: width},
                updateImages: updateImages,images: images});
        });
    };


    const handleOpenGallery = () => {
        ImagePicker.openPicker({
            cropping: false
        }).then(image => {
            navigation.navigate('CustomImageCropper', {type: 'save', image: image, updateImages: updateImages});
            setModalLoading(false);
        });
    };

    const handleDeleteImage = (image) => {
        setImages(deleteElementFromArray(images, image));
        setModalImageLoading(false);
    };


    const handleAddSubmit = async () => {
        let status = false;
        if (title.length < 1) {
            setErrorTitle(true);
            status = true;
        }

        if (!amount) {
            setErrorAmount(true);
            status = true;
        }


        if (!status) {
            setLoading(true);

            const formData = new FormData();
            images.map(async image => {
                formData.append('files[]', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'activity_image.jpg',
                });
            });

            formData.append("title",title);
            formData.append("date",moment(date).format("YYYY-MM-DD"));
            formData.append("amount",amount);

            await createNewActivity(case_id,formData,response => {
                console.log(response);
                getCaseActivities(case_id);
                setLoading(false);
                navigation.navigate('Economy',{item: item});
            },error => {
                console.log(error.response);
                setLoading(false);
            })
        }
    };

    const openOptions = () => {
        setOptionsOpen(true);

        const BUTTONS = ['Get Receipt Directly','Open Camera', 'Open Gallery', 'Cancel'];
        const CANCEL_INDEX = 3;

        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                title: 'Select images to upload',
            },
            buttonIndex => {
                if (buttonIndex === 0) {
                    handleGetDirectlyReceipt();
                } else if (buttonIndex === 1) {
                    handleOpenCamera();
                } else if (buttonIndex === 2) {
                    handleOpenGallery();
                }
            },
        );
        setOptionsOpen(false);
    };



    return (
        <AuthenticatedScreenHOC title={"Refusion"}
                                navigation={navigation}
                                menuIcon={false} backIcon={true}>
            <DataLoadingComponent loading={loading}/>
            <View style={styles.container}>
                <StandardInputComponent placeholder={"Klik for at skrive..."}
                                        value={title}
                                        setValue={setTitle}
                                        labelTxt={"Overskrift"}
                                        valueError={errorTitle}
                                        setValueError={setErrorTitle}
                                        inputStyle={{width: '100%'}}
                />
                <SimpleDateTimePicker label={"Dato"} date={date} setDate={setDate} format={"DD-MM-YYYY"} style={{width: '97%'}} minimumDate={minDate}/>
                <StandardInputComponent placeholder={"Amount"}
                                        type={"number"}
                                        value={amount}
                                        setValue={setAmount}
                                        labelTxt={"Beskriv beskrivelse"}
                                        valueError={errorAmount}
                                        setValueError={setErrorAmount}
                                        inputStyle={{width: '100%'}}
                />
                <View style={styles.upload}>
                    <Text style={styles.uploadDesc}>Klik for at tage billede af bilaget </Text>
                    <Badge success style={{
                        height: 26,
                        width: 26,
                        borderRadius: 13,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: MAIN_COLOR
                    }}>
                        <Text style={{color: 'white'}}>{images.length}</Text>
                    </Badge>
                </View>
                <View style={{flexDirection: 'row', alignSelf: 'center', width: SCREEN_WIDTH}}>
                    {/*<TouchableOpacity onPress={() => setModalLoading(true)} style={styles.uploadBtn}>*/}
                    {/*    <Icon name={"plus"} style={styles.uploadIcon}/>*/}
                    {/*</TouchableOpacity>*/}
                    <TouchableOpacity
                        onPress={openOptions}
                        style={{
                            height: 100,
                            width: 100,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        disabled={optionsOpen}
                    >
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                height: 60,
                                width: 60,
                                borderRadius: 60 / 2,
                                backgroundColor: LIGHT_DIVIDER_COLOR,
                            }}
                        >
                            <Icon name="plus" size={30}/>
                        </View>
                    </TouchableOpacity>
                    <ScrollView horizontal={true} style={{marginRight: 10}}>
                        {
                            images.map((image, index) => (
                                <CustomImagePreview key={++index} image={image}
                                                    setLoadedImage={setLoadedImage}
                                                    setModalImageLoading={setModalImageLoading}/>
                            ))
                        }
                    </ScrollView>
                </View>
                <StandardButton text={"Tilføj"} handleButton={handleAddSubmit} style={{marginLeft: 0}}/>

                <AddActivityModal handleOpenCamera={handleOpenCamera}
                                  handleOpenGallery={handleOpenGallery}
                                  handleGetDirectlyReceipt={handleGetDirectlyReceipt}
                                  modalLoading={modalLoading}
                                  setModalLoading={setModalLoading}
                />

                <ImagePreviewModal setModalLoading={setModalImageLoading} modalLoading={modalImageLoading}
                                   continueEditingImage={continueEditingImage}
                                   image={loadedImage} handleDeleteImage={handleDeleteImage}/>
            </View>
        </AuthenticatedScreenHOC>
    );
};

export default ActivityAdd;
