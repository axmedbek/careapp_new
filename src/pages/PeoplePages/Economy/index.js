import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {getCaseActivities} from "../../../actions/case.action";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import CircleActionButton from "../../../components/buttons/CircleActionButton";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import Carousel from "react-native-snap-carousel";
import {SCREEN_WIDTH_ORIGINAL} from "../../../constants/setting";
import ActivityCircle from "../../../components/sliders/ActivityCircle";
import ActivityListItem from "../../../containers/People/ActivityListItem";
import {getActivityListWithIndex} from "../../../helpers/standard.helper";


const Economy = ({navigation, route, lang, getCaseActivities, loading, activities}) => {

    const {item} = route.params;

    const [index, setIndex] = useState(0);

    useEffect(() => {
        getCaseActivities(item.id);
    }, []);

    const handleClickSlider = (images) => {
        navigation.navigate('ActivityImageSlider', {images})
    };

    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.economy} navigation={navigation}
                                menuIcon={true} backIcon={true}>
            <DataLoadingComponent loading={loading}/>
            <View style={{flex: 1}}>
                <View
                    style={{
                        marginBottom: 30,
                        marginTop: 20,
                    }}
                >
                    <Carousel
                        data={activities}
                        renderItem={({item}) => (
                            <View
                                style={{
                                    alignItems: "center",
                                }}
                            >
                                <ActivityCircle
                                    radius={100}
                                    topText={item.total.total}
                                    bottomText={item.month}
                                />
                            </View>
                        )}
                        sliderWidth={SCREEN_WIDTH_ORIGINAL}
                        itemWidth={SCREEN_WIDTH_ORIGINAL - 130}
                        firstItem={0}
                        onSnapToItem={i => setIndex(i)}
                    />
                </View>

                <FlatList
                    data={getActivityListWithIndex(activities,index)}
                    renderItem={({item}) =>
                        <ActivityListItem item={item} handleClick={item.files ? () => handleClickSlider(item.files) : () => console.log("no files")}/>
                    }
                    contentContainerStyle={{flexGrow: 1}}
                    ItemSeparatorComponent={() => <View
                        style={{
                            width: '100%',
                            height: 1,
                            backgroundColor: '#D8D8D8'
                        }}>

                    </View>}
                    ListEmptyComponent={
                        <ListItemEmpty loading={loading} text="No activity"/>
                    }
                    keyExtractor={item => item.id.toString()}
                />
            </View>
            <CircleActionButton icon={"plus"} handlePress={() => navigation.navigate('ActivityAdd', {
                item: item,
                getCaseActivities
            })}/>
        </AuthenticatedScreenHOC>
    );
};


const mapStateToProps = ({lang, caseReducer}) => {
    return {
        lang,
        activities: caseReducer.activities,
        loading: caseReducer.fetching
    };
};

const mapDispatchToProps = {
    getCaseActivities
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Economy);
