import React, {useEffect, useState} from 'react';
import {FlatList, View} from "react-native";
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {getContacts} from "../../../actions/case.action";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import ContactItem from "../../../containers/People/ContactItem";
import CircleActionButton from "../../../components/buttons/CircleActionButton";
import CoreModal from "../../../components/modals/CoreModal";
import {SCREEN_WIDTH} from "../../../constants/setting";
import {deleteContact} from "../../../services/case.service";
import {Toast} from "native-base";
import ContactAddEditForm from "../../../forms/ContactAddEditForm";
import ConfirmToast from "../../../components/toasts/ConfirmToast";
import {setOperationLoading} from "../../../actions/loading.action";
import Modal from "../../../components/modals/Modal";

const Contacts = ({route, navigation, lang, getContacts, loading, contacts}) => {
    const {item} = route.params;

    const [modal, setModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [contact, setContact] = useState(null);
    const [operationLoading, setOperationLoading] = useState(false);

    useEffect(() => {
        getContacts(item.id);
    }, [item]);

    const refresh = () => {
        getContacts(item.id);
    };


    const handleEdit = (item) => {
        setContact(item);
        setModal(true);
    };

    const handleRemove = (item) => {
        setContact(item);
        setDeleteModal(true);
    };

    const deleteContactHandle = () => {
        setOperationLoading(true);
        deleteContact(contact.id, response => {
            Toast.show({
                position: 'top',
                text: response.data.description,
                buttonText: lang.messages.common.ok,
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
            setDeleteModal(false);
            setOperationLoading(false);
            getContacts(`case=${item.id}`);
        }, (error) => {
            console.log(error.response);
            Toast.show({
                position: 'top',
                text: 'Oops.Something went wrong!',
                buttonText: lang.messages.common.ok,
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
            setDeleteModal(false);
            setOperationLoading(false);
        });
    };


    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.contacts} navigation={navigation}
                                menuIcon={true} backIcon={true}>
            <FlatList
                data={contacts}
                renderItem={({item}) => <ContactItem contact={item} handleEdit={handleEdit}
                                                     handleRemove={handleRemove}/>}
                contentContainerStyle={{flexGrow: 1, paddingBottom: 30}}
                ListEmptyComponent={<ListItemEmpty loading={loading} text="No contacts"/>}
                keyExtractor={item => `${item.id}-${item.user_id}`}
                ItemSeparatorComponent={() => <View
                    style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: '#D8D8D8'
                    }}>

                </View>}
                onRefresh={refresh}
                refreshing={loading}
            />


            <CircleActionButton icon={"plus"} handlePress={() => {
                setModal(true);
                setContact(null);
            }}/>

            <Modal
                modalVisible={modal}
                closeModal={() => setModal(false)}
                title={"Tilføj kontakt"}>
                <ContactAddEditForm
                    setModal={setModal}
                    lang={lang}
                    case_id={item.id}
                    getContacts={getContacts}
                    setOperationLoading={setOperationLoading}
                    contact={contact}
                />
            </Modal>

            <ConfirmToast
                show={deleteModal}
                handClose={() => setDeleteModal(false)}
                type={"info"}
                message={"Do you sure to delete this contact"}
                item={contact}
                deleteHandle={deleteContactHandle}
                errors={{}}
                loading={operationLoading}
                id={contact ? contact.id > 0 ? contact.id : contact.user_id  : 0}
            />

        </AuthenticatedScreenHOC>
    );
};


const mapStateToProps = ({lang, contactReducer}) => {
    return {
        lang,
        contacts: contactReducer.contacts,
        loading: contactReducer.fetching
    };
};

const mapDispatchToProps = {
    getContacts,
    setOperationLoading
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Contacts);
