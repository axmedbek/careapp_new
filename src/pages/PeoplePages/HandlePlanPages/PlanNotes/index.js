import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {connect} from "react-redux";
import styles from '../../../../pages/NotePages/Note/style';
import AuthenticatedScreenHOC from "../../../../HOCs/AuthenticatedScreenHOC";
import DataLoadingComponent from "../../../../components/loadings/DataLoadingComponent";
import NoteItem from "../../../../containers/Notes/NoteItem";
import ListItemEmpty from "../../../../containers/ListItemEmpty";
import CircleActionButton from "../../../../components/buttons/CircleActionButton";
import NoteAddForm from "../../../../forms/Notes/NoteAddForm";
import ConfirmToast from "../../../../components/toasts/ConfirmToast";
import {addEditNoteForPlan, deleteNote, getNotes, getPlanNotes} from "../../../../actions/note.action";
import Modal from "../../../../components/modals/Modal";

const PlanNotes = ({lang, route, navigation, addEditNoteForPlan, deleteNote, noteErrors, deleting, adding, notes, loading, getPlanNotes}) => {
    const {plan} = route.params;

    const [addModal, setAddModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [item, setItem] = useState(null);


    useEffect(() => {
        getPlanNotes(plan.id);
    }, []);

    useEffect(() => {
        if (!deleting) {
            setDeleteModal(false);
            getPlanNotes(plan.id);
        }
    }, [deleting]);


    useEffect(() => {
        if (!adding) {
            setAddModal(false);
            getPlanNotes(plan.id);
        }
    }, [adding]);

    const handleEdit = (item) => {
        setItem(item);
        setAddModal(true);
    };

    const handleRemove = (item) => {
        setItem(item);
        setDeleteModal(true);
    };

    const refresh = () => {
        getPlanNotes(plan.id);
    };

    return (
        <AuthenticatedScreenHOC title={`${plan.plan} plan`} navigation={navigation} lang={lang} backIcon={true}>
            <View style={styles.container}>
                <FlatList
                    data={notes}
                    renderItem={({item}) =>
                        <NoteItem item={item}
                                  handleEdit={() => handleEdit(item)}
                                  handleRemove={() => handleRemove(item)}
                        />}
                    contentContainerStyle={{flexGrow: 1}}
                    ListEmptyComponent={<ListItemEmpty loading={loading} text="No mapper"/>}
                    keyExtractor={item => item.id.toString()}
                    onRefresh={refresh}
                    refreshing={loading}
                />
                <CircleActionButton icon={"note-plus"} handlePress={() => {
                    setItem(null);
                    setAddModal(true);
                }}/>
                <Modal
                    closeModal={() => setAddModal(false)}
                    modalVisible={addModal}
                    title={item ? "Edit Note" : "Add Note"}
                >
                    <NoteAddForm
                        hideModal={() => setAddModal(false)}
                        addNote={addEditNoteForPlan}
                        noteErrors={{}}
                        loading={adding}
                        plan_id={plan.id}
                        item={item}
                    />
                </Modal>
                <ConfirmToast
                    show={deleteModal}
                    handClose={() => setDeleteModal(false)}
                    type={"info"}
                    message={"Do you sure to delete this note"}
                    item={item}
                    deleteHandle={deleteNote}
                    errors={noteErrors}
                />
                <DataLoadingComponent loading={loading && deleteModal}/>
            </View>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, noteReducer}) => {
    return {
        lang,
        notes: noteReducer.notes,
        loading: noteReducer.fetching,
        deleting: noteReducer.deleting,
        adding: noteReducer.adding,
        noteErrors: noteReducer.errors,
        addFolderLoading: noteReducer.addFolderLoading,
    };
};

const mapDispatchToProps = {
    getPlanNotes,
    addEditNoteForPlan,
    deleteNote
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PlanNotes);
