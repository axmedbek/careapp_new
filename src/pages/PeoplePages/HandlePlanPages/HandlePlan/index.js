import React, {useEffect, useState} from 'react';
import {FlatList} from "react-native";
import AuthenticatedScreenHOC from "../../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import ListItemEmpty from "../../../../containers/ListItemEmpty";
import PlanCard from "../../../../containers/People/PlanCard";
import {getCasePlans, getGoals, getTradingPlanStatuses} from "../../../../actions/case.action";
import CircleActionButton from "../../../../components/buttons/CircleActionButton";
import CoreModal from "../../../../components/modals/CoreModal";
import PlanAddModal from "../../../../modals/People/PlanAddModal";
import {truncateText} from "../../../../helpers/standard.helper";

const HandlePlan = ({route, navigation, fetching, plans, getGoals,getTradingPlanStatuses, getCasePlans, lang}) => {
    const {item} = route.params;

    const [modal, setModal] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getCasePlans(item.id);
        getTradingPlanStatuses();
        getGoals();
    }, []);


    const refresh = () => {
        getCasePlans(item.id);
    };

    console.log(plans);

    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.handleplan} navigation={navigation}
                                menuIcon={true} backIcon={true}>
            <FlatList
                data={plans.data}
                numColumns={2}
                renderItem={({item}) => <PlanCard
                    title={truncateText(item.plan, 20)}
                    status={item.status.title}
                    color={item.status.color}
                    goal={item.goal.title}
                    onPress={() =>
                        navigation.navigate("EditPlan", {
                            plan: item
                        })
                    }
                />}
                contentContainerStyle={{ flexGrow: 1, paddingBottom: 30 }}
                ListEmptyComponent={<ListItemEmpty loading={fetching}/>}
                keyExtractor={(item, index) => index.toString()}
                onRefresh={refresh}
                refreshing={fetching}
            />
            <CircleActionButton icon={"plus"} handlePress={() => setModal(true)}/>

            <CoreModal titleText={"Plan Add"} hideModal={() => setModal(false)} visible={modal}
                       modalStyle={{borderRadius: 4}} loading={loading}>
                <PlanAddModal case_id={item.id} setModal={setModal} setLoading={setLoading}/>
            </CoreModal>
        </AuthenticatedScreenHOC>
    );
};


const mapStateToProps = ({caseReducer, lang}) => {
    return {
        lang,
        plans: caseReducer.plans,
        fetching: caseReducer.plansFetching
    };
};

const mapDispatchToProps = {
    getCasePlans,
    getGoals,
    getTradingPlanStatuses
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HandlePlan);

