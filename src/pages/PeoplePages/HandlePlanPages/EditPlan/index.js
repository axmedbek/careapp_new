import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import AuthenticatedScreenHOC from "../../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import StandardButton from "../../../../components/buttons/StandardButton";
import styles from './style';
import SimplePicker from "../../../../components/selects/SimpleSelect";
import {convertGoalsForSelect, convertToSelectData, getToastMessage} from "../../../../helpers/standard.helper";
import {updatePlanStatus} from "../../../../services/case.service";
import DataLoadingComponent from "../../../../components/loadings/DataLoadingComponent";
import {getCasePlans} from "../../../../actions/case.action";


const EditPlan = ({route, getCasePlans, navigation, trading_plans_statuses, lang}) => {
    const {plan, item} = route.params;

    const [loading, setLoading] = useState(false);
    const [selectedValue, setSelectedValue] = useState(null);
    const [selectedValueError, setSelectedValueError] = useState(false);

    useEffect(() => {
        setSelectedValue(plan.status.value);
    }, []);

    const handleUpdate = () => {
        if (!selectedValue) {
            setSelectedValueError(true);
        } else {
            setLoading(true);
            setSelectedValueError(false);
            console.log(plan);
            updatePlanStatus(item.id,plan.id, selectedValue, () => {
                setLoading(false);
                getCasePlans(item.id);
                navigation.goBack();
            }, (error) => {
                setLoading(false);
                getToastMessage('Oops.Something went wrong!');
            })
        }
    };

    return (
        <AuthenticatedScreenHOC title={`${lang.messages.personoverview.editplan}`}
                                navigation={navigation}
                                menuIcon={true} backIcon={true}>
            <DataLoadingComponent loading={loading}/>
            <View style={styles.container}>
                <View style={styles.txtContent}>
                    <Text style={styles.label}>Forhold</Text>
                    <Text style={styles.txtStyle}>{plan.goal.title}</Text>
                    <Text style={styles.label}>Beskrivelse</Text>
                    <Text style={styles.txtStyle}>{plan.plan}</Text>
                    <Text style={styles.label}>Status</Text>
                    <SimplePicker data={convertGoalsForSelect(trading_plans_statuses)}
                                  placeHolder={"Choose a status"}
                                  hasError={selectedValueError}
                                  setSelectedValue={setSelectedValue} selectedValue={selectedValue}/>
                </View>
                <StandardButton text={"Gem"} handleButton={handleUpdate}/>
                <StandardButton text={"Notes"} handleButton={() => navigation.navigate('PlanNotes',{plan: plan})}
                                style={styles.noteBtn} textStyle={styles.noteBtnText}/>
            </View>
        </AuthenticatedScreenHOC>
    );
};


const mapStateToProps = ({lang, caseReducer}) => {
    return {
        lang,
        trading_plans_statuses: caseReducer.trading_plans_statuses
    };
};

const mapDispatchToProps = {
    getCasePlans
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EditPlan);
