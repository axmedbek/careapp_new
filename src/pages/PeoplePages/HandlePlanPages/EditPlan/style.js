import {StyleSheet} from 'react-native';
import {BACK_ICON_COLOR, MAIN_COLOR, TEXT_COLOR} from "../../../../constants/colors";
import {REGULAR_FONT} from "../../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        margin: 10,
        padding: 10
    },
    label: {
        fontFamily: REGULAR_FONT,
        color: TEXT_COLOR,
        fontSize: 22
    },
    txtStyle: {
        fontFamily: REGULAR_FONT,
        color: BACK_ICON_COLOR,
        fontSize: 18,
        marginBottom: 15
    },
    noteBtn: {
        borderWidth: 1,
        borderColor: MAIN_COLOR,
        backgroundColor: 'white'
    },
    noteBtnText: {
        color: MAIN_COLOR
    },
    txtContent: {
        paddingLeft: 6
    }
});

export default styles;
