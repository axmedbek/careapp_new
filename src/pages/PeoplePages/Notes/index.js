import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {connect} from "react-redux";
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import NoteItem from "../../../containers/Notes/NoteItem";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import CircleActionButton from "../../../components/buttons/CircleActionButton";
import NoteAddForm from "../../../forms/Notes/NoteAddForm";
import ConfirmToast from "../../../components/toasts/ConfirmToast";
import {addEditNoteForCase, deleteNote, getCaseNotes} from "../../../actions/note.action";
import styles from "../../NotePages/Note/style";
import Modal from "../../../components/modals/Modal";

const Notes = ({lang, route, navigation, addEditNoteForCase, deleteNote, noteErrors, deleting, adding, notes, loading, getCaseNotes}) => {
    const {item} = route.params;

    const [addModal, setAddModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [selectedItem, setSelectedItem] = useState(null);


    useEffect(() => {
        getCaseNotes(item.id);
    }, []);

    useEffect(() => {
        if (!deleting) {
            setDeleteModal(false);
            getCaseNotes(item.id);
        }
    }, [deleting]);


    useEffect(() => {
        if (!adding) {
            setAddModal(false);
            getCaseNotes(item.id);
        }
    }, [adding]);

    const handleEdit = (item) => {
        setSelectedItem(item);
        setAddModal(true);
    };

    const handleRemove = (item) => {
        setSelectedItem(item);
        setDeleteModal(true);
    };

    const refresh = () => {
        getCaseNotes(item.id);
    };

    return (
        <AuthenticatedScreenHOC title={lang.messages.personoverview.notes} navigation={navigation} lang={lang} backIcon={true}>
            <DataLoadingComponent loading={loading && deleteModal}/>
            <View style={styles.container}>
                <FlatList
                    data={notes}
                    renderItem={({item}) =>
                        <NoteItem item={item}
                                  handleEdit={() => handleEdit(item)}
                                  handleRemove={() => handleRemove(item)}
                        />}
                    contentContainerStyle={{flexGrow: 1}}
                    ListEmptyComponent={<ListItemEmpty loading={loading} text="No mapper"/>}
                    keyExtractor={item => item.id.toString()}
                    ItemSeparatorComponent={() => <View
                        style={{
                            width: '100%',
                            height: 1,
                            backgroundColor: '#D8D8D8'
                        }}>

                    </View>}
                    onRefresh={refresh}
                    refreshing={loading}
                />
                <CircleActionButton icon={"note-plus"} handlePress={() => {
                    setSelectedItem(null);
                    setAddModal(true);
                }}/>
                <Modal
                    closeModal={() => setAddModal(false)}
                    modalVisible={addModal}
                    title={item ? "Edit Note" : "Add Note"}
                >
                    <NoteAddForm
                        hideModal={() => setAddModal(false)}
                        addNote={addEditNoteForCase}
                        noteErrors={{}}
                        loading={adding}
                        case_id={item.id}
                        item={selectedItem}
                    />
                </Modal>
                <ConfirmToast
                    show={deleteModal}
                    handClose={() => setDeleteModal(false)}
                    type={"info"}
                    message={"Do you sure to delete this note"}
                    item={selectedItem}
                    deleteHandle={deleteNote}
                    errors={noteErrors}
                />
            </View>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, noteReducer}) => {
    return {
        lang,
        notes: noteReducer.notes,
        loading: noteReducer.fetching,
        deleting: noteReducer.deleting,
        adding: noteReducer.adding,
        noteErrors: noteReducer.errors,
        addFolderLoading: noteReducer.addFolderLoading,
    };
};

const mapDispatchToProps = {
    getCaseNotes,
    addEditNoteForCase,
    deleteNote
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Notes);
