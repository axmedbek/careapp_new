import React, {useEffect} from 'react';
import {FlatList} from 'react-native';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import PeopleCard from "../../../containers/People/PeopleCard";
import {getCases} from "../../../actions/case.action";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import {changeNotificationStatus} from "../../../actions/setting.action";

const People = ({lang, getCases, cases, changeNotificationStatus,caseLoading, navigation}) => {

    useEffect(() => {
        getCases();
        changeNotificationStatus(true);
        navigation.addListener('focus', () => {
            changeNotificationStatus(true);
        });
    }, []);

    const refresh = () => {
        getCases();
    };

    return (
        <AuthenticatedScreenHOC title={lang.messages.people.title} navigation={navigation}>
            <FlatList
                data={cases}
                renderItem={({item}) =>
                    <PeopleCard item={item} navigation={navigation}
                                handleLeftPress={() =>
                                    navigation.navigate('PersonOverviewNavigationContainer', {'item': item})}
                                handleRightPress={() =>
                                    navigation.navigate('PersonOverviewNavigationContainer', {'item': item})}/>}
                contentContainerStyle={{flexGrow: 1, paddingBottom: 30}}
                ListEmptyComponent={<ListItemEmpty loading={caseLoading} text="No people"/>}
                keyExtractor={item => item.id.toString()}
                onRefresh={refresh}
                refreshing={caseLoading}
            />
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, caseReducer}) => {
    return {
        lang,
        cases: caseReducer.cases,
        caseLoading: caseReducer.fetching
    };
};

const mapDispatchToProps = {
    getCases,
    changeNotificationStatus
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(People);
