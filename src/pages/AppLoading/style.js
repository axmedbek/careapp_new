import {StyleSheet} from 'react-native';
import {MAIN_COLOR} from "../../constants/colors";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: MAIN_COLOR,
        alignItems: "center",
        justifyContent: "center",
    },
});

export default styles;
