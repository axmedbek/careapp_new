import React from 'react';
import {View, ActivityIndicator, StatusBar} from "react-native";
import styles from './style';
import {MAIN_COLOR} from "../../constants/colors";

const AppLoading = ({loading}) => {
    const content =
        <View style={styles.container}>
            <StatusBar backgroundColor={MAIN_COLOR}/>
            <ActivityIndicator size={"large"} color="#ffffff"/>
        </View>;

    return loading ? content : "";
};

export default AppLoading;
