import {StyleSheet} from 'react-native';
import {TEXT_COLOR} from "../../constants/colors";
import {REGULAR_FONT} from "../../constants/setting";

const styles = StyleSheet.create({
    calendar: {
      padding: 10
    },
    bottomText: {
        color: TEXT_COLOR,
        fontFamily: REGULAR_FONT,
        marginTop: 30,
        marginVertical: 10,
        paddingHorizontal: 10,
        fontSize: 16,
    }
});

export default styles;
