import React, {useState, useEffect} from 'react';
import {View, ScrollView, Text, FlatList} from 'react-native';
import AuthenticatedScreenHOC from "../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import styles from './style';
import CircleActionButton from "../../components/buttons/CircleActionButton";
import {getEvents} from "../../actions/calendar.action";
import EventItem from "../../containers/Event/EventItem";
import {getEventsActiveDate} from "../../helpers/standard.helper";
import {Calendar as WixCalendar, LocaleConfig} from "react-native-calendars";
import {MAIN_COLOR, RED_COLOR} from "../../constants/colors";
import DataLoadingComponent from "../../components/loadings/DataLoadingComponent";
import EventList from "../../containers/Event/EventList";
import {changeNotificationStatus} from "../../actions/setting.action";


LocaleConfig.locales['da'] = {
    monthNames: ['Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November ', 'December'],
    monthNamesShort: ['Jan.', 'Febr.', 'Marts', 'April', 'Maj', 'Juni', 'Juli.', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'],
    dayNamesShort: ['Søn', 'Man.', 'Tirs.', 'Ons.', 'Tors.', 'Fre.', 'Lør.'],
    today: 'i dag'
};
LocaleConfig.defaultLocale = 'da';

const Calendar = ({lang, navigation, changeNotificationStatus, dates, events, getEvents}) => {

    const [selectedDateEvents, setSelectedDateEvents] = useState({});
    const [selectedDate, setSelectedDate] = useState('');
    const [selectedDay, setSelectedDay] = useState(null);
    const [selectedEvents, setSelectedEvents] = useState(null);
    const [loading, setLoading] = useState(false);


    useEffect(() => {
        getEvents();
        changeNotificationStatus(true);
        navigation.addListener('focus', () => {
            changeNotificationStatus(true);
        });
    }, []);

    useEffect(() => {
        setLoading(false);
    }, [selectedEvents]);


    const selectTime = (time) => {
        if (time) {
            setLoading(true);
            let obj = {};
            obj[time.dateString] = {selected: true, marked: true, selectedColor: 'grey'};
            let data = [];
            events.map(e => {
                if (time.dateString === e.date_raw) {
                    data[data.length] = e;
                }
            });
            setSelectedDateEvents(obj);
            setSelectedDate(time.dateString);
            setSelectedEvents(data);
        }
    };

    return (
        <AuthenticatedScreenHOC title={lang.messages.calendar.title} navigation={navigation} lang={lang}>
            <ScrollView style={{paddingLeft: 6, marginRight: 6, marginTop: 10}}>
                <View style={styles.calendar}>
                    <WixCalendar
                        theme={{
                            arrowColor: MAIN_COLOR,
                            monthTextColor: MAIN_COLOR,
                            selectedDayBackgroundColor: RED_COLOR,
                            todayTextColor: 'white',
                            todayBackgroundColor: RED_COLOR
                        }}
                        firstDay={1}
                        markedDates={{
                            ...selectedDateEvents,
                            ...dates
                        }}
                        markingType="multi-dot"
                        onDayPress={(day) => {
                            setSelectedDay(day);
                            selectTime(day)
                        }}
                    />
                </View>

                <Text style={styles.bottomText}>KOMMENDE BEGIVENHEDER</Text>
                <EventList
                    navigation={navigation}
                    data={selectedEvents ? selectedEvents : getEventsActiveDate(events)}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    isSmall={true}
                    reloadEvent={() => selectTime(selectedDay)}
                />
            </ScrollView>
            <CircleActionButton
                icon={"plus"}
                handlePress={() => navigation.navigate("CreateEvent", {dateString: selectedDate})}
            />
            <DataLoadingComponent loading={loading}/>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, eventReducer}) => {
    return {
        lang,
        events: eventReducer.events,
        dates: eventReducer.dates,
    };
};

const mapDispatchToProps = {
    getEvents,
    changeNotificationStatus
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Calendar);
