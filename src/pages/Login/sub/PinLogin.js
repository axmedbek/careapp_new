import React, {useEffect, useState} from 'react';
import {BackHandler, Image, Text, TouchableOpacity, Vibration, View} from "react-native";
import styles from "../../Account/Pincode/styles";
import {concatObjects, errorMessage} from "../../../helpers/standard.helper";
import Icon from "react-native-vector-icons/MaterialIcons";
import {getDeviceUser, getValueFromSettingWithKey} from "../../../config/caching/actions/setting";
import {GRAY_COLOR} from "../../../constants/colors";
import {compareString} from "../../../config/encryption";
import ReactNativeBiometrics from "react-native-biometrics";

const PinLogin = ({navigation}) => {
    const [pin, setPin] = useState('');

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", () => {
            BackHandler.exitApp();
            return true
        });
        return () => {
            BackHandler.removeEventListener("hardwareBackPress");
        };
    }, []);


    useEffect(() => {
        if (pin.length === 4) {
            if (compareString(getValueFromSettingWithKey("pinCode"), pin)) {
                navigation.navigate('HomeScreenNavigator');
            } else {
                Vibration.vibrate();
                setPin('');
            }
        }
    }, [pin]);

    const handleKeyPress = (key) => {
        if (key === 'cancel') {
            navigation.navigate('Login');
        } else if (key === 'clear') {
            setPin('');
        } else {
            if (pin.length < 4) {
                setPin(pin + key);
            }
        }
    };

    const handleForget = () => {
        navigation.navigate('Login');
    };

    const handleFingerLogin = () => {
        ReactNativeBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint'})
            .then((resultObject) => {
                const {success} = resultObject;
                if (success) {
                    navigation.navigate('HomeScreenNavigator');
                } else {
                    errorMessage('User cancelled biometric prompt');
                }
            })
            .catch(() => {
                errorMessage('Biometrics failed');
            })
    };

    return (
        <View style={styles.main}>
            <Image style={styles.avatar} source={{uri: getDeviceUser().avatar}}/>
            <View style={{alignItems: 'center'}}>
                <Text style={styles.title}>{getDeviceUser().firstname} {getDeviceUser().lastname}</Text>
                <View style={styles.codeContainer}>
                    <View style={concatObjects(styles.code, pin.length > 0 ? styles.activeCode : {})}/>
                    <View style={concatObjects(styles.code, pin.length > 1 ? styles.activeCode : {})}/>
                    <View style={concatObjects(styles.code, pin.length > 2 ? styles.activeCode : {})}/>
                    <View style={concatObjects(styles.code, pin.length > 3 ? styles.activeCode : {})}/>
                </View>
            </View>

            <View style={styles.keyboard}>
                <View style={styles.keyRow}>
                    <TouchableOpacity onPress={() => handleKeyPress(1)}><Text
                        style={styles.key}>1</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress(2)}><Text
                        style={styles.key}>2</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress(3)}><Text
                        style={styles.key}>3</Text></TouchableOpacity>
                </View>
                <View style={styles.keyRow}>
                    <TouchableOpacity onPress={() => handleKeyPress(4)}><Text
                        style={styles.key}>4</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress(5)}><Text
                        style={styles.key}>5</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress(6)}><Text
                        style={styles.key}>6</Text></TouchableOpacity>
                </View>
                <View style={styles.keyRow}>
                    <TouchableOpacity onPress={() => handleKeyPress(7)}><Text
                        style={styles.key}>7</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress(8)}><Text
                        style={styles.key}>8</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress(9)}><Text
                        style={styles.key}>9</Text></TouchableOpacity>
                </View>
                <View style={styles.keyRow}>
                    <TouchableOpacity onPress={() => handleKeyPress('cancel')}><Text
                        style={styles.cancelKey}>Cancel</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress(0)}><Text
                        style={styles.key}>0</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => handleKeyPress('clear')}><Text
                        style={styles.clearKey}>Clear</Text></TouchableOpacity>
                </View>
                <View style={concatObjects(styles.keyRow, {
                    backgroundColor: 'white',
                    marginLeft: 20, marginRight: 20, paddingTop: 10, paddingBottom: 10, borderRadius: 4
                })}>
                    <TouchableOpacity style={{flexDirection: 'row', width: '50%', paddingLeft: 4}}
                                      onPress={handleForget}>
                        <Icon name={"lock"} style={{fontSize: 30, marginRight: 4, marginTop: 4, color: GRAY_COLOR}}/>
                        <Text style={{fontSize: 14, width: '80%', color: GRAY_COLOR}}>Forget your access code ?</Text>
                    </TouchableOpacity>
                    {
                        getValueFromSettingWithKey("fingerprint") &&
                        <TouchableOpacity style={{flexDirection: 'row', width: '50%', marginLeft: 10}}
                                          onPress={handleFingerLogin}>
                            <Icon name={"fingerprint"}
                                  style={{fontSize: 30, marginRight: 4, marginTop: 4, color: GRAY_COLOR}}/>
                            <Text style={{fontSize: 14, width: '80%', color: GRAY_COLOR}}>Use fingerprint for
                                login</Text>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        </View>
    );
};

export default PinLogin;
