import React from 'react';
import LoginContainer from "../../../containers/Login";

const StandardLogin = ({ navigation }) => {
    return (
        <LoginContainer navigation={navigation}/>
    );
};

export default StandardLogin;
