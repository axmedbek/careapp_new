import React from 'react';
import LoginContainer from '../../containers/Login';
import {View, StatusBar} from 'react-native';
import styles from './style';
import {MAIN_COLOR} from "../../constants/colors";

const Login = ({navigation}) => {
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={MAIN_COLOR} barStyle={"light-content"}/>
            <LoginContainer navigation={navigation}/>
        </View>
    );
};

export default Login;
