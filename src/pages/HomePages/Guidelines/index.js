import React from 'react';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {Accordion, View, Text} from "native-base";
import {getLanguage} from "../../../config/caching/actions/setting";
import {LIGHT_DIVIDER_COLOR, TEXT_COLOR} from "../../../constants/colors";

const dataArrayDa = [
    {
        title: "Værdigrundlag",
        content: [
            "CareCompagniet føler et stort medansvar på det sociale område og ønsker at bistå kommunerne med rådgivning, planlægning og være den naturlige go-to partner på socialområdet.",
            "CareCompagniet har kompetent personale der kan træde til og løfte opgaven for kommunen når de ikke selv har de fornødne eller ønsker at frigive ressourcer.",
            "CareCompagniet er troværdige og loyale overfor ethvert samarbejde. Vi følger alle opgaver til dørs og overholder alle aftaler. Tavshedspligten er naturlig og ikke blot kontraktuel.",
            "CareCompagniet udarbejder handleplaner i samarbejde med kommune og borger. Disse tager udgangspunkt i borgerens nuværende situation og hvilke målsætninger der skal opnås. Planerne følges tæt af den tilknyttede støtteperson, der rapporterer til den enkelte sagsbehandler hvert kvartal.",
            "CareCompagniets målsætninger er, at skabe synlige resultater ved at fokusere på at opnå målsætningerne i handleplanen. Således styrkes borgeren i sin selvstændighed, og kan således skabe et værdigt liv på egen hånd ved det forebyggende arbejde. ",
        ],
    },
    {
        title: "Forventninger til dig som ansat",
        content: [
            "CareCompagniet forventer, at du i kontakten til borgeren samt samarbejdspartneren er; imødekommende, professionel i dit sprogbrug og din skriftlighed. Det forventes, at du er nærværende samt er i besiddelse af empatiske evner og formår at skabe en struktureret arbejdstilgang. Det forventes desuden at du agerer i overensstemmelse med CareCompagniets værdigrundlag. ",
            "I CareCompagniet vægter vi i høj grad relationen til borgeren. Det er vigtigt at du som ansat agerer som en aktivt lyttende person, der formår at bistå med støtte i vanskelige situationer. Ved opstart af opgaven afholdes en forventningsafstemning af borgerens behov og den ansattes rolle. Denne afstemning vil udgøre en retningslinje til brug for din indsats. Du har efterfølgende selv ansvaret for løbende at afholde forventningssamtaler med borgeren/familien. Forventningssamtaler afholdes enten hvert kvartal eller månedligt i forbindelse med aflevering af statusrapport eller evalueringsrapport. \n",
        ],
    },
    {
        title: "Møde med samarbejdspartner",
        content: [
            "CareCompagniet forventer, at din påklædning er præsentabel og at dit udseende som helhed fremstår professionelt i forbindelse med faglige møder samt i kontakten til borgeren. Det forventes desuden, at du ved hvert møde, har CareCompagniets skriveblok samt kuglepen liggende fremme på bordet til notater og relevante sagsakter. Herunder forventes det, at du tager notater af møder med samarbejdspartneren, som du efterfølgende registrer i CareCompagniets sagssystem ARC Manager.\n",
            "CareCompagniet forventer, at du adskiller dit privatliv fra dit arbejdsliv ved møder med samarbejdspartneren. Herunder er det nødvendigt at understrege, at det ikke er tilladt, at oplyse borgeren eller samarbejdspartneren om ens egne private problemer (sygdom, stress, familieproblemer, problemer på andet arbejde ect.). I tilfælde af private problemer, skal disse håndteres indenfor CareCompagniets rammer. i tilfælde af problemer som påvirker din arbejdsindsats, er du forpligtet til at rette henvendelse til CareCompagniet. Vi forventer en åben dialog, således at vi i fællesskab kan håndtere ansvaret overfor borgeren, samarbejdspartneren og dig. ",
        ],
    },
    {
        title: "CareCompagniet er til rådighed for dig ",
        content: [
            "Der vil løbende være kontakt mellem dig og administrationen, for at sikre en kontinuerlig dialog og for at gøre dig tryg i den stillede opgave. Administrationen er behjælpelig med spørgsmål omkring de ansattes opgaver, eksempelvis faglig sparring, juridisk vejledning, håndtering af frustrationer mm. Du har mulighed for faglig sparring og støtte enten over telefonen og/eller ved møde på kontoret. Husk at du aldrig står alene, vi er til rådighed for dig.    \n",
        ],
    },
    {
        title: "E-mail",
        content: [
            "Du vil ved opstart få oprettet en e-mail hos CareCompagniet med din egen signatur. Der henvises til kontrakten, hvoraf fremgangsmåden er beskrevet. Ligeledes forventes det, at du på din telefon opretter en arbejdssignatur til din arbejdsmail, som indeholder navn, din titel, dine- og CareCompagniets kontaktinformationer. Ved al skriftlig kontrakt til CareCompagniet samt samarbejdspartneren benyttes CareCompagniets e-mail. Du bedes ved vigtig korrespondance til samarbejdspartner, der kan have betydning for den fremtidige indsats, tilføjes CareCompagniets administration som cc i e-mailen (administration@careapp.dk).\n",
        ],
    },
    {
        title: "CRM-system og app",
        content: [
            "Du vil ved opstart få oprettet et login til ARC, og få downloadet den tilhørende app. Det er et krav fra ledelsen i CareCompagniet, at du anvender ARC i dit arbejde med borgeren, dvs. laver sagsnotater, laver tidsregistreringer, håndterer refusioner og følger op på handleplansmål m.v. Derudover er det et krav, at du anvende ARC som platform for al kommunikation vedr. din opgave med borgeren. Dette er blandt andet for at sikre, at du som medarbejder og at vi som social organisation overholder GDPR-lovgivningen om persondata. \n",
        ],
    },
    {
        title: "GDPR & personfølsom data",
        content: [
            "CareCompagniet bruger ARC til alle administrative arbejdsgange, som sikre at alt data opbevares korrekt og ikke kommer til tredje parts skue. Du er som en del af CareCompagniet forpligtet til at bruge de værktøjer som ARC tilbyder, for at undgå at du opbevare din borgers persondata forkert. Du må under ingen omstændigheder skrive og sende mails eller beskeder som indeholder personfølsom data, såsom cpr.nr. på en borger.\n",
        ],
    },
    {
        title: "Timeforbrug",
        content: [
            "Som ansat i CareCompagniet har du selv styringen i forbindelse med samværet med borgeren. Du tilrettelægger selv dine timer sammen med borgeren i henhold til din kontrakt, medmindre andet er aftalt i kontrakten. Som udgangspunkt forventes det, at du har ugentlig kontakt/samvær med borgeren for at fastholde den tætte relation og sikre en kontinuerlig indsats. \n",
            "Den ansatte er forpligtet til at registrere den tid der anvendes på opgaven i ARC, dvs. både samvær, telefonsamtaler, møder m.v. Som ansat har du selv ansvaret for at bruge det givne timetal, som er bevilget til at løfte opgaven. Såfremt den ansatte bruger færre timer end kontrakten lyder på, bliver honoraret udbetalt i overensstemmelse med tidsregistreringen i ARC. Hvis den ansatte over en periode har modtaget et for højt honorar i forhold til de faktiske timer, som den ansatte reelt har brugt på opgaven, vil der være krav om tilbagebetaling af honoraret.  \n",
        ],
    },
    {
        title: "Aktivitetsbilag",
        content: [
            "Såfremt du har fået tildelt et aktivitetsbeløb på opgaven, gælder følgende: Aktivitetsbeløbet er til fornøjelser samt aktiviteter sammen med borgeren. Den ansatte står selv for udlæg af aktivitetsbeløbet, og får udlægget tilbagebetalt sammen med månedens honorar, når bilagene er registreret og oploadet i ARC. Her henvises til kontrakten, hvor det uddybes. \n",
        ],
    },
    {
        title: "Statusrapport / Evalueringsrapport",
        content: [
            "Det forventes, at du hvert kvartal udarbejder en statusrapport eller en månedlig evalueringsrapport, afhængig af hvad din kontrakt foreskriver. Rapporten bekriver indsatsens forløb og progression, hvor relevante informationer om borgerens situation beskrives. Statusrapporten gennemgås af administrationen for kvalitetssikring inden den sendes til samarbejdspartneren. Ved spørgsmål til indholdet gennemgås rapporten i fælleskab, herunder har du mulighed for sparring og drøftelse af indsatsen. Der henvises til statusskabelon, tilhørende vejledning samt et eksempel på en statusrapport. Statusrapporten skal desuden som udgangspunkt altid gennemgås med borgeren, medmindre andet er aftalt. \n",
        ],
    },
    {
        title: "Ferie",
        content: [
            "Såfremt du skal afholde ferie, skal dette meddeles CareCompagniet på mail (administration@careapp.dk) senest 14 dage inden afholdelse af ferie. Såfremt ferien ikke meddeles 14 dage inden, kan CareCompagniet ikke godkende din ferie. Timeforbruget under dit fravær flyttes til før eller efter ferien. Det forventes at du selv tilrettelægger dine timer med borgeren, således at timeantallet for opgaven opretholdes. Såfremt ferien er længere end 1 uge bliver dette ikke lønnet, da CareCompagniet er forpligtet til at sætte en anden medarbejder / vikar på opgaven, så længe ferie afholdes. Derudover skal du huske at notere din ferie i din kalender i ARC. ",
        ],
    },
    {
        title: "Sygdom",
        content: [
            "Ved sygdom skal du selv sørge for at placere dine tabte samværstimer med borgeren før eller efter sygeperioden, hvorfor du ikke bliver godtgjort for sygedage. Såfremt at der er tale om et sygdomsforløb på mere end 2 uger, er du forpligtet til at rette henvendelse til CareCompagniets administration, således at der kan sættes en vikar på opgaven, så længe sygdomsforløbet varer.  ",
        ],
    },

];
const dataArrayEn = [
    {
        title: "Core Values",
        content: [

            "CareCompagniet feels a great co-responsibility in the social field and wants to assist the municipalities with advice, planning and being the natural go-to partner in the social area.",
            "CareCompagniet has competent staff who can step in and lift the task for the municipality when they do not themselves have the necessary or want to release resources.",
            "CareCompagniet is trustworthy and loyal to any collaboration. We follow every task at the door and comply with all agreements. The duty of confidentiality is natural and not merely contractual.",
            "CareCompagniet draws up action plans in collaboration with the municipality and the citizen. These are based on the citizen's current situation and what goals must be achieved. The plans are closely followed by the associated support person who reports to the individual caseworker every quarter.",
            "The goals of CareCompagnie are to create visible results by focusing on achieving the goals in the action plan. Thus, the citizen is strengthened in his independence and can thus create a worthy life on his own by the preventive work."
        ],
    },
    {
        title: "Expectations for you as an employee",
        content: [
            "CareCompagniet expects you to be in contact with the citizen as well as the partner; welcoming, professional in your language and writing. You are expected to be present as well as possess empathic abilities and be able to create a structured work approach. In addition, you are expected to you act in accordance with CareCompagniet's values. ",
            "In CareCompagniet, we attach great importance to the relationship with the citizen. It is important that you, as an employee, act as an active listening person who can assist with support in difficult situations. At the start of the task, an expectation balance of the citizen's needs and the role of the employee is held. This poll will serve as a guideline to use for your efforts. You are subsequently responsible for conducting expectation interviews with the citizen / family on an ongoing basis. Expectations are held either quarterly or monthly in the submission of the progress report or evaluation report.",
        ],
    },
    {
        title: "Meeting with a partner",
        content: [
            "CareCompagniet expects your attire to be presentable and your appearance as a whole to appear professional in professional meetings as well as in contact with the citizen. In addition, at each meeting, you will have CareCompagniet's writing pad and ball pen lying on the table for notes and relevant case files. Below, you are expected to take notes of meetings with the partner, which you subsequently record in the CareCompagnies case system ARC Manager.",
            "CareCompagniet expects you to separate your private life from your working life in meetings with your partner. It is important to emphasize that it is not permissible to inform the citizen or partner about one's own private problems (illness, stress, family problems, other problems) work ect.) In the case of private problems, these must be handled within the framework of CareCompagniet. In the event of problems affecting your work performance, you are obliged to contact CareCompagniet. We expect an open dialogue so that we can handle jointly. the responsibility towards the citizen, the partner and you. ",
        ],
    },
    {
        title: "CareCompagniet is available to you",
        content: [
            "There will be ongoing contact between you and the administration, to ensure a continuous dialogue and to make you comfortable in the task. The administration helps with questions about the tasks of the employees, such as professional sparring, legal guidance, handling frustrations, etc. You have the opportunity for professional sparring and support either over the phone and / or at an office meeting. Remember that you are never alone, we are available to you.",
        ],
    },
    {
        title: "E-mail",
        content: [
            "At startup, you will receive an email from CareCompagniet with your own signature. Reference is made to the contract outlining the procedure. Likewise, on your phone, you are expected to create a work signature for your work email that includes your name, your title, your and CareCompany's contact information. With all written contract to CareCompagniet and the partner, CareCompagniet's e-mail is used. You are requested by important correspondence with a partner that may have an impact on the future effort, the CareCompagniet administration is added as cc in the e-mail (administration@careapp.dk).",
        ],
    },
    {
        title: "CRM system and app",
        content: [
            "At startup, you will get an ARC login and download the corresponding app. It is a requirement of the management of CareCompagniet that you use ARC in your work with the citizen, ie. makes case notes, makes time records, handles reimbursements and follows up on action plan goals, etc. In addition, it is a requirement that you use the ARC as a platform for all communication regarding. your task with the citizen. This is, among other things, to ensure that you as an employee and that we as a social organization comply with the GDPR personal data legislation.",
        ],
    },
    {
        title: "GDPR & PeoplePages Sensitive Data",
        content: [
            "CareCompagniet uses ARC for all administrative workflows, which ensure that all data is stored correctly and does not come to third party view. As part of CareCompagniet, you are required to use the tools offered by ARC to avoid incorrectly storing your citizen's personal data. Under no circumstances may you write and send e-mails or messages containing personal sensitive data, such as Cpr. on a citizen.",
        ],
    },
    {
        title: "Time Consumption",
        content: [
            "As an employee of CareCompagniet, you yourself have control over the relationship with the citizen. You plan your hours with the citizen yourself according to your contract, unless otherwise agreed in the contract. As a starting point, you are expected to have weekly contact / contact with citizen in order to maintain the close relationship and ensure continuous efforts. ",
            "The employee is obliged to record the time spent on the task at ARC, ie both contact, telephone calls, meetings etc. As an employee, you are responsible for using the given hourly number allocated to lift the task. If the employee If the employee spends fewer hours than the contract, the fee will be paid in accordance with the time registration in the ARC. If, for a period of time, the employee has received too high a fee compared to the actual hours actually spent by the employee on the task, there will be requirements on the repayment of the fee. ",
        ],
    },
    {
        title: "Activities",
        content: [
            "If you have been allocated an activity amount for the assignment, the following applies: The activity amount is for pleasure as well as activities with the citizen. The employee is responsible for the outlay of the activity amount, and will be reimbursed along with the month's fee, once the annexes are registered and uploaded in the ARC. Here, reference is made to the contract in which it is elaborated.",
        ],
    },
    {
        title: "Status Report / Evaluation Report",
        content: [
            "You are expected to prepare a quarterly or monthly evaluation report every quarter, depending on what your contract prescribes. The report describes the course and progression of the effort, which describes relevant information about the citizen's situation. The progress report is reviewed by the Quality Assurance Administration before being sent to the partner. For questions about the content, the report is reviewed jointly, including the possibility of sparring and discussion of the effort. Refer to the status template, related guide, and an example of a status report. In addition, the status report must always be reviewed with the citizen, unless otherwise agreed.",
        ],
    },
    {
        title: "Vacation",
        content: [
            "If you are planning a holiday, this must be notified to CareCompagniet by e-mail (administration@careapp.dk) at least 14 days before the holiday. If the holiday is not announced 14 days before, CareCompagniet cannot approve your holiday. The hours spent during your absence will be moved to before or after the holiday. It is expected that you organize your own hours with the citizen so that the number of hours for the task is maintained. If the holiday is longer than 1 week, this will not be paid, as CareCompagniet is obliged to appoint another employee / temporary worker as long as the holiday is held. Additionally, be sure to note your vacation in your ARC calendar.",
        ],
    },
    {
        title: "Disease",
        content: [
            "In case of illness, you must make sure that you lose your contact hours with the citizen before or after the sick period, which is why you will not be reimbursed for sick days. If there is a disease course of more than 2 weeks, you are obliged to contact the CareCompagniet administration so that a substitute can be assigned to the task as long as the disease course lasts.",
        ],
    },

];

const Guidelines = ({lang, navigation}) => {

    const renderContent = item => (
        <View style={{paddingHorizontal: 15, paddingVertical: 20}}>
            {item.content.map((content, index) => (
                <Text key={++index} style={{fontSize: 16, color: TEXT_COLOR}}>{content}</Text>
            ))}
        </View>
    );

    return (
        <AuthenticatedScreenHOC title={lang.messages.home.guidelines}
                                navigation={navigation}
                                lang={lang}
                                menuIcon={true}
        >
            <Accordion
                dataArray={getLanguage() === 'da' ? dataArrayDa : dataArrayEn}
                renderContent={renderContent}
                expanded={0}
                headerStyle={{backgroundColor: LIGHT_DIVIDER_COLOR}}
                style={{padding: 14}}
            />
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Guidelines);
