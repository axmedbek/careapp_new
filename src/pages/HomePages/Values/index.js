import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {SECONDARY_BG_COLOR} from "../../../constants/colors";
import {SCREEN_WIDTH, SCREEN_WIDTH_ORIGINAL} from "../../../constants/setting";

const Values = ({lang, navigation}) => {
    return (
        <AuthenticatedScreenHOC title={lang.messages.home.values}
                                navigation={navigation}
                                lang={lang}
                                menuIcon={true}
        >
            <ScrollView
                contentContainerStyle={{
                    paddingHorizontal: 16,
                    backgroundColor: SECONDARY_BG_COLOR,
                    alignItems: "center",
                }}
            >
                <View style={{
                    backgroundColor: '#84B13A',
                    width: SCREEN_WIDTH,
                    height: SCREEN_WIDTH,
                    borderRadius: SCREEN_WIDTH / 2,
                    marginTop: 30,
                    marginBottom: 30
                }}>
                    <Text style={{
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginTop: 50
                    }}>MEDANSVAR</Text>
                    <Text style={{
                        width: 250,
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: "100",
                        fontSize: 16,
                        marginTop: 10,
                        lineHeight: 22
                    }}>Vi føler et stort medansvar på
                        det sociale ornrSde, og ønsker
                        at støtte børn, unge og familier,
                        som har brug for hjælp. Vi arbej-
                        der for at skabe gennemsigtighed
                        på det sociale område, cg ønsker
                        at være den naturlige
                        go-to partner.
                    </Text>
                </View>

                <View style={{
                    backgroundColor: '#000000',
                    width: SCREEN_WIDTH,
                    height: SCREEN_WIDTH,
                    borderRadius: SCREEN_WIDTH/2,
                    marginBottom: 30
                }}>
                    <Text style={{
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginTop: 60,
                        color: 'white'
                    }}>RESULTATER</Text>
                    <Text style={{
                        width: 250,
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: "100",
                        fontSize: 16,
                        marginTop: 10,
                        lineHeight: 22,
                        color: 'white'
                    }}>Vi skaber synlige resultater ved
                        at fokusere på målsætnngerne
                        i handleplanen. Således skabes
                        de positive ændringer, hvor
                        borgeren styrkes i sin trivsel
                        og selvstændighed.
                    </Text>
                </View>


                <View style={{
                    backgroundColor: '#76353B',
                    width: SCREEN_WIDTH,
                    height: SCREEN_WIDTH,
                    borderRadius: SCREEN_WIDTH/2,
                    marginBottom: 30
                }}>
                    <Text style={{
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginTop: 70,
                        color: 'white'
                    }}>NÆRVÆR</Text>
                    <Text style={{
                        width: 250,
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: "100",
                        fontSize: 16,
                        marginTop: 10,
                        lineHeight: 22,
                        color: 'white'
                    }}>
                        Vi har kompetente ildsjæle. der
                        kan træde til ag løfte opgaven.
                        Vi moder mennesker i deres
                        virkelighed, og danner
                        rammerne med mennesket
                        i centrum.
                    </Text>
                </View>


                <View style={{
                    backgroundColor: '#E5C884',
                    width: SCREEN_WIDTH,
                    height: SCREEN_WIDTH,
                    borderRadius: SCREEN_WIDTH/2,
                    marginBottom: 30
                }}>
                    <Text style={{
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginTop: 70
                    }}>UDVIKLING</Text>
                    <Text style={{
                        width: 250,
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: "100",
                        fontSize: 16,
                        marginTop: 10,
                        lineHeight: 22
                    }}>
                        Via ildsjæle, et stærkt
                        værdigrundlag og digitale
                        værktøjer sikrer vi en
                        kontinuerlig udviking i
                        arbejdet med det enkelte
                        menneske.
                    </Text>
                </View>


                <View style={{
                    backgroundColor: '#E4B600',
                    width: SCREEN_WIDTH,
                    height: SCREEN_WIDTH,
                    borderRadius: SCREEN_WIDTH/2,
                    marginBottom: 30
                }}>
                    <Text style={{
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginTop: 70
                    }}>TROVÆRDIGHED</Text>
                    <Text style={{
                        width: 250,
                        textAlign: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        fontWeight: "100",
                        fontSize: 16,
                        marginTop: 10,
                        lineHeight: 22
                    }}>
                        Vi er troværdige og loyale overfor
                        ethvert samarbejde. Vi følger alle
                        opgaver til dørs og overholder alle
                        aftaler.Tavshedspligten er
                        naturlig og ikke blot kontraktuel.
                    </Text>
                </View>
            </ScrollView>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang}) => {
    return {
        lang
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Values);
