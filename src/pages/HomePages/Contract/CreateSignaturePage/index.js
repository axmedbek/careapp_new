import {StyleSheet, Text, View,Image, TouchableOpacity} from 'react-native';
import SignatureCapture from 'react-native-signature-capture';
import React, {useEffect, useRef, useState} from "react";
import {BACK_ICON_COLOR, GRAY_COLOR, LIGHT_COLOR, LIGHT_DIVIDER_COLOR} from "../../../../constants/colors";
import {getSignContract, updateSignContract} from "../../../../services/contract.service";

const CreatesSignaturePage = ({navigation}) => {

    const [image,setImage] = useState(null);

    useEffect(() => {
        getSignContract().then(response => {
            setImage(response.data.data.signature);
        }).catch(err => {
            console.log(err);
        })
    },[]);

    const signRef = useRef();

    const _onSaveEvent = (e) => {
        updateSignContract({
            imageData64: e.encoded
        }).then(response => {
            setImage(e.encoded);
            console.log(response);
        }).catch(err => {
            console.log(err);
        })
    }

    const _onDragEvent = () => {
        //
    }

    const resetSign = () => {
        signRef.current.resetImage();
    };

    const saveSign = () => {
        signRef.current.saveImage();
    };

    return (
        <View style={{flex: 1}}>
            <View style={{height: 250,backgroundColor: 'white'}}>
                <Image source={{uri: `data:image/jpeg;base64,${image}`}} style={{width: '100%',height: '100%',resizeMode: 'contain'}}/>
            </View>
            <View style={{flex: 1}}>
                <SignatureCapture
                    style={[{flex: 1}, styles.signature]}
                    ref={signRef}
                    onSaveEvent={_onSaveEvent}
                    onDragEvent={_onDragEvent}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={true}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>
                <TouchableOpacity onPress={() => navigation.goBack()}
                                  style={{
                                      position: 'absolute', top: 5, right: 10,justifyContent: 'center',alignItems: 'center',
                                      backgroundColor: BACK_ICON_COLOR, width: 80, height: 40,borderRadius: 4
                                  }}>
                    <Text style={{color: 'white'}}>BACK</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    signature: {
        flex: 1,
        borderColor: '#000033',
        borderWidth: 1,
    },
    buttonStyle: {
        flex: 1, justifyContent: "center", alignItems: "center", height: 50,
        backgroundColor: "#eeeeee",
        margin: 10
    }
});

export default CreatesSignaturePage;
