import React, {useEffect} from 'react';
import {Text, ScrollView, View, TouchableOpacity} from 'react-native';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {BLUE_COLOR, DIVIDER_COLOR, MAIN_COLOR, YELLOW_COLOR} from "../../../constants/colors";
import {getContracts} from "../../../actions/contract.action";
import CircleActionButton from "../../../components/buttons/CircleActionButton";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import {signContract} from "../../../services/contract.service";
import { Toast } from "native-base";

const Contract = ({contracts, getContracts, lang, fetching, navigation}) => {

    useEffect(() => {
        getContracts();
    }, []);


    const handleSign = (id) => {
        signContract(id).then(response => {
            getContracts();
            Toast.show({
                text: 'Successfully signed',
                buttonText: 'Okay',
                type: "success"
            })
        }).catch(err => {
            Toast.show({
                text: 'Error happened!',
                buttonText: 'Okay',
                type: "danger"
            })
        })
    }


    return (
        <AuthenticatedScreenHOC title={lang.messages.home.contract}
                                navigation={navigation}
                                lang={lang}
                                menuIcon={true}
        >
            <ScrollView contentContainerStyle={{flex: 1, padding: 10, paddingLeft: 15}}>
                <View>
                    <View style={{flexDirection: 'row', width: '100%'}}>
                        <Text style={{width: '50%', fontWeight: 'bold'}}>Subject</Text>
                        <Text style={{width: '20%', fontWeight: 'bold'}}>Status</Text>
                        <Text style={{width: '30%', fontWeight: 'bold'}}>Operation</Text>
                    </View>
                    <ScrollView>
                        {
                            contracts.map(item => (
                                <View style={{flexDirection: 'column', width: '100%'}}>
                                    <View style={{flexDirection: 'row', width: '100%', marginTop: 5}}>
                                        <Text style={{width: '50%'}}>{item.subject}</Text>
                                        <View style={{width: '20%'}}>
                                            <View style={{width: 20,height: 20,borderRadius: 10,backgroundColor:
                                                    (parseInt(item.status) === 3 ? '#8AFA07' : (parseInt(item.status) === 2 ? '#FABB10' : '#EB4435'))}}/>
                                        </View>
                                        <View style={{width: '30%', flexDirection: 'column'}}>
                                            <TouchableOpacity
                                                onPress={() => navigation.navigate('ContractShowPage', {htmlContent: item.content})}
                                                style={{
                                                    backgroundColor: '#FFB822',
                                                    height: 30,
                                                    borderRadius: 4,
                                                    alignItems: 'center',
                                                    marginBottom: 10,
                                                    paddingTop: 4
                                                }}>
                                                <Text>Contract</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => navigation.navigate('ContractNotePage',{id: item.id})}
                                                style={{
                                                    backgroundColor: '#35A3F6',
                                                    height: 30,
                                                    borderRadius: 4,
                                                    alignItems: 'center',
                                                    marginBottom: 10,
                                                    paddingTop: 4
                                                }}>
                                                <Text style={{color: 'white'}}>Notes</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{
                                                backgroundColor: '#33BFA3',
                                                height: 30,
                                                borderRadius: 4,
                                                alignItems: 'center',
                                                marginBottom: 10,
                                                paddingTop: 4
                                            }} onPress={() => handleSign(item.id)}>
                                                <Text style={{color: 'white'}}>Sign</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{
                                        backgroundColor: DIVIDER_COLOR,
                                        height: 2,
                                        width: '100%',
                                        marginBottom: 20,
                                        marginTop: 10
                                    }}/>
                                </View>
                            ))
                        }
                    </ScrollView>
                </View>
            </ScrollView>
            <View style={{marginTop: 50}}/>
            <CircleActionButton icon={"signature-freehand"}
                                handlePress={() => navigation.navigate('CreateSignaturePage')}/>
                                <DataLoadingComponent loading={fetching}/>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({contractReducer, lang}) => {
    return {
        lang,
        contracts: contractReducer.contracts,
        fetching: contractReducer.fetching,
    };
};

const mapDispatchToProps = {
    getContracts
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Contract);
