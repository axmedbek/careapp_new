import React from 'react';
import {ScrollView, useWindowDimensions} from "react-native";
import HTML from "react-native-render-html";
import AuthenticatedScreenHOC from "../../../../HOCs/AuthenticatedScreenHOC";

const ContractShowPage = ({navigation, route}) => {
    const {htmlContent} = route.params;
    const contentWidth = useWindowDimensions().width - 50;

    const tagsStyles = {img: {resizeMode: 'cover',maxWidth: contentWidth,minWidth: contentWidth}};

    return (
        <AuthenticatedScreenHOC title={"Contract Show"} navigation={navigation}
                                menuIcon={false} backIcon={true}>
            <ScrollView style={{flex: 1}}>
                <HTML html={htmlContent}
                      contentWidth={contentWidth}
                      tagsStyles={tagsStyles}
                />
            </ScrollView>
        </AuthenticatedScreenHOC>
    );
};

export default ContractShowPage;
