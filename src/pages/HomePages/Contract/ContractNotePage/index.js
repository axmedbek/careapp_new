import React, {useEffect, useState} from 'react';
import {FlatList, View} from "react-native";
import DataLoadingComponent from "../../../../components/loadings/DataLoadingComponent";
import styles from "../../../NotePages/Note/style";
import NoteItem from "../../../../containers/Notes/NoteItem";
import ListItemEmpty from "../../../../containers/ListItemEmpty";
import CircleActionButton from "../../../../components/buttons/CircleActionButton";
import Modal from "../../../../components/modals/Modal";
import NoteAddForm from "../../../../forms/Notes/NoteAddForm";
import ConfirmToast from "../../../../components/toasts/ConfirmToast";
import AuthenticatedScreenHOC from "../../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import {addContractNote, getContractNotes} from "../../../../actions/note.action";
import {deleteContractNote, updateContractNote} from "../../../../services/note.service";

const ContractNotePage = ({route, navigation, updateNote, getContractNotes, addContractNote, deleting, noteErrors, adding, notes, loading}) => {
    const {id} = route.params;

    const [addModal, setAddModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [item, setItem] = useState(null);


    useEffect(() => {
        getContractNotes(id);
    }, []);

    useEffect(() => {
        if (!deleting) {
            setDeleteModal(false);
            getContractNotes(id);
        }
    }, [deleting]);


    useEffect(() => {
        if (!adding) {
            setAddModal(false);
            getContractNotes(id);
        }
    }, [adding]);

    const handleEdit = (item) => {
        setItem(item);
        setAddModal(true);
    };

    const handleRemove = (item) => {
        setItem(item);
        setDeleteModal(true);
    };

    const refresh = () => {
        getContractNotes(id);
    };

    const handleDeleteContractNote = (note_id) => {
        deleteContractNote(id,note_id).then(response => {
            getContractNotes(id);
            setDeleteModal(false);
        },err => {
            console.log(err);
            setDeleteModal(false);
        })
    }

    const handleUpdateContractNote = (note_id,data) => {
        updateContractNote(id,note_id,data).then(response => {
            getContractNotes(id);
            setAddModal(false);
        },err => {
            console.log(err);
            setAddModal(false);
        })
    }

    return (
        <AuthenticatedScreenHOC title={'Notes'} navigation={navigation} backIcon={true}>
            <DataLoadingComponent loading={loading && deleteModal}/>
            <View style={styles.container}>
                <FlatList
                    data={notes}
                    renderItem={({item}) =>
                        <NoteItem item={item}
                                  handleEdit={() => handleEdit(item)}
                                  handleRemove={() => handleRemove(item)}
                        />}
                    contentContainerStyle={{flexGrow: 1}}
                    ListEmptyComponent={<ListItemEmpty loading={loading} text="No mapper"/>}
                    keyExtractor={item => item.id.toString()}
                    onRefresh={refresh}
                    ItemSeparatorComponent={() => <View
                        style={{
                            width: '100%',
                            height: 1,
                            backgroundColor: '#D8D8D8'
                        }}>

                    </View>}
                    refreshing={loading}
                />
                <CircleActionButton icon={"note-plus"} handlePress={() => {
                    setItem(null);
                    setAddModal(true);
                }}/>
                <Modal
                    closeModal={() => setAddModal(false)}
                    modalVisible={addModal}
                    title={item ? "Edit Note" : "Add Note"}
                >
                    <NoteAddForm
                        hideModal={() => setAddModal(false)}
                        addNote={addContractNote}
                        updateNote={(data) => handleUpdateContractNote(item.id,data)}
                        noteErrors={{}}
                        loading={adding}
                        contract_id={id}
                        item={item}
                    />
                </Modal>
                <ConfirmToast
                    show={deleteModal}
                    handClose={() => setDeleteModal(false)}
                    type={"info"}
                    message={"Do you sure to delete this note"}
                    deleteHandle={() => handleDeleteContractNote(item.id)}
                    errors={noteErrors}
                />
            </View>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({contractReducer, lang}) => {
    return {
        lang,
        notes: contractReducer.notes,
        loading: contractReducer.fetchingNotes,
        deleting: contractReducer.deleting,
        adding: contractReducer.adding,
        noteErrors: contractReducer.errors
    };
};

const mapDispatchToProps = {
    getContractNotes,
    addContractNote
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ContractNotePage);
