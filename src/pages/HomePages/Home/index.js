import React, {useEffect} from 'react';
import EventListContainer from "../../../containers/EventListContainer";
import {createSocket} from "../../../config/WebSocket";
import PushNotification from "react-native-push-notification";
import {setSocketConnection} from "../../../actions/socket.action";
import {connect} from "react-redux";
import {getUserArrayFromChat} from "../../../helpers/standard.helper";
import {getDeviceUser} from "../../../config/caching/actions/setting";
import {loadNotification} from "../../../config/NotifService";

const Home = ({navigation, setSocketConnection, notification_status, socket_connection}) => {

    useEffect(() => {
        if (!socket_connection.socket) {
            setSocketConnection(createSocket());
        }
    }, []);

    useEffect(() => {
        if (socket_connection.socket) {
            socket_connection.socket.on('send notification', response => {
                if (parseInt(response.data.created_by) !== parseInt(getDeviceUser().user_id)) {
                    let users = getUserArrayFromChat(response.data);
                    loadNotification(response.data.notify.title, response.data.notify.description, users, response.data);
                }
            });

            PushNotification.configure({
                onNotification: function (notification) {
                    navigation.navigate('ChatInbox', {
                        id: notification.id,
                        title: notification.link.title,
                        avatar: notification.link.image_url,
                        users: notification.users,
                        dialogue: notification.link._id,
                        is_group: notification.link.type !== "dialogue"
                    })
                },
                popInitialNotification: true,
            });
        }
    }, [socket_connection]);

    return <EventListContainer navigation={navigation}/>
};

const mapStateToProps = ({socket_connection, settingReducer}) => {
    return {
        socket_connection: socket_connection,
        notification_status: settingReducer.notification_status
    };
};

const mapDispatchToProps = {
    setSocketConnection
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Home);
