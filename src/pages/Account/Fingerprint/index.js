import React, {useEffect, useState} from 'react';
import {StatusBar, Switch, Text, View} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import {BLUE_COLOR, MAIN_COLOR} from "../../../constants/colors";
import ReactNativeBiometrics from 'react-native-biometrics'
import {Toast} from "native-base";
import {
    deleteValueFromSettingWithKey,
    getValueFromSettingWithKey,
    saveSetting
} from "../../../config/caching/actions/setting";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";


const Fingerprint = ({navigation}) => {

    console.log(getValueFromSettingWithKey("fingerprint"));

    const [isEnabled,setIsEnabled] = useState(getValueFromSettingWithKey("fingerprint") === "has");
    const [hide,setHide] = useState(true);
    const [loading,setLoading] = useState(false);

    useEffect(() => {
        ReactNativeBiometrics.isSensorAvailable()
            .then((resultObject) => {
                const {available, biometryType} = resultObject;

                if (available && biometryType === ReactNativeBiometrics.TouchID) {
                    setHide(false);
                    console.log('TouchID is supported');
                } else if (available && biometryType === ReactNativeBiometrics.FaceID) {
                    setHide(false);
                    console.log('FaceID is supported');
                } else if (available && biometryType === ReactNativeBiometrics.Biometrics) {
                    setHide(false);
                    console.log('Biometrics is supported');

                    // let epochTimeSeconds = Math.round((new Date()).getTime() / 1000).toString();
                    // let payload = epochTimeSeconds + 'care compagniet';
                    //
                    // ReactNativeBiometrics.createSignature({
                    //     promptMessage: 'Login into application by fingerprint',
                    //     payload: payload
                    // })
                    //     .then((resultObject) => {
                    //         const {success, signature} = resultObject;
                    //         if (success) {
                    //             console.log(signature);
                    //             saveSetting("fingerprint", "has");
                    //             navigation.push("Account");
                    //         }
                    //     })
                } else {
                    setHide(true);
                    Toast.show({
                        position: 'bottom',
                        text: 'Biometrics not supported',
                        buttonText: 'OK',
                        type: 'danger',
                        textStyle: {marginLeft: 10},
                        duration: 4000
                    });
                }
            })
    }, []);


    const toggleSwitch = () => {
        setLoading(true);
        setIsEnabled(!isEnabled);
        if (!isEnabled) {
            saveSetting("fingerprint", "has");
            Toast.show({
                position: 'bottom',
                text: 'Biometrics login is activated!',
                buttonText: 'OK',
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        }
        else {
            deleteValueFromSettingWithKey("fingerprint");
            Toast.show({
                position: 'bottom',
                text: 'Biometrics login is deactivated!',
                buttonText: 'OK',
                type: 'info',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
        }

        setTimeout(() => {
            setLoading(false);
            navigation.goBack();
        },2000);
    };


    return (
        <View style={{backgroundColor: MAIN_COLOR, height: '100%'}}>
            <StatusBar barStyle="dark-content" hidden={false}/>
            <Icon name={"close"} style={{
                position: 'absolute', fontSize: 40,
                color: 'white', right: 20, top: 10
            }} onPress={() => navigation.goBack()}/>
            <View style={{alignItems: 'center', marginTop: 150}}>
                <Icon name={"fingerprint"} style={{color: 'white', fontSize: 80}}/>
                <View style={{
                    width: '80%', alignItems: 'center', textAlign: 'justify',
                    alignContent: 'center'
                }}>
                    <Text style={{
                        color: 'white', fontSize: 22, marginLeft: 10,
                        textAlign: 'center', marginTop: 20, lineHeight: 30
                    }}>
                        Login into application by fingerprint
                    </Text>
                    <Text style={{color: 'white', fontSize: 14, marginTop: 30}}>
                        Use fingerprint for login
                    </Text>
                </View>
            </View>
            {
                !hide &&
                <View>
                    <Switch
                        trackColor={{false: "#767577", true: BLUE_COLOR}}
                        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={toggleSwitch}
                        style={{
                            transform: [{scaleX: 2.2}, {scaleY: 2.2}],
                            position: 'absolute',
                            bottom: -150,
                            left: '45%'
                        }}
                        value={isEnabled}
                    />
                </View>
            }
            <DataLoadingComponent loading={loading}/>
        </View>
    );
};

export default Fingerprint;
