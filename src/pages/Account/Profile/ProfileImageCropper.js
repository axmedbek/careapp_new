import React from 'react';
import AmazingCropper, {DefaultFooter} from "react-native-amazing-cropper";
import {updateProfileAvatar} from "../../../services/auth.service";
import {Toast} from "native-base";

const ProfileImageCropper = ({route, navigation}) => {
    const {image, setAvatar, getProfileInfo} = route.params;

    const imgWidth = image.width;
    const imgHeight = image.height;

    const onDone = (e) => {
        setAvatar(e);
        const formData = new FormData();
        formData.append('avatar', {
            uri: e,
            type: 'multipart/form-data',
            name: 'uploaded_avatar_image.jpg',
        });
        updateProfileAvatar(formData, response => {
            getProfileInfo();
            Toast.show({
                position: 'top',
                text: 'Avatar was successfully updated.',
                buttonText: 'OK',
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
        }, error => {
            console.log(error.response);
            Toast.show({
                position: 'top',
                text: 'Avatar could not update.',
                buttonText: 'OK',
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
        });
        navigation.navigate('Profile');
    };

    const onError = () => {
        alert("Something went wrong while cropping screen opening");
    };

    const onCancel = () => {
        navigation.navigate('Profile');
    };

    return (
        <AmazingCropper
            footerComponent={<DefaultFooter doneText='FINISH' rotateText='ROTATE' cancelText='BACK'/>}
            onDone={onDone}
            onError={onError}
            onCancel={onCancel}
            imageUri={image.path}
            imageWidth={imgWidth}
            imageHeight={imgHeight}
            NOT_SELECTED_AREA_OPACITY={0.3}
            BORDER_WIDTH={20}
        />
    );
};

export default ProfileImageCropper;
