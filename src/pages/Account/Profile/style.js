import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    logoutContainer: {
        position: 'absolute',
        bottom: 10,
        width: '100%'
    }
});

export default styles;
