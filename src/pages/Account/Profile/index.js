import React, {useState, useEffect} from 'react';
import {ScrollView, TextInput, TouchableOpacity, View} from "react-native";
import styles from './style';
import StandardButton from "../../../components/buttons/StandardButton";
import SimpleAvatar from "../../../components/avatars/SimpleAvatar";
import FormHeaderDescriptor from "../../../components/headers/FormHeaderDescriptor";
import BackButton from "../../../components/buttons/BackButton";
import HeaderTextHolder from "../../../components/headers/HeaderTextHolder";
import LogoutButton from "../../../components/buttons/LogoutButton";
import {getProfileInfo, logout} from "../../../actions/auth.action";
import {setOperationLoading} from "../../../actions/loading.action";
import {connect} from "react-redux";
import {getDeviceUser} from "../../../config/caching/actions/setting";
import ScreenAuthorizationHoc from "../../../HOCs/ScreenAuthorizationHOC";
import {getAccessToken} from "../../../config/caching/actions/token";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import Icon from "react-native-vector-icons/FontAwesome";
import {SECONDARY_BG_COLOR} from "../../../constants/colors";
import {SCREEN_WIDTH_ORIGINAL} from "../../../constants/setting";
import SimplePicker from "../../../components/selects/SimpleSelect";
import {ActionSheet, Toast} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import {updateProfileInfo} from "../../../services/auth.service";


const Profile = ({navigation, getProfileInfo, logout, user}) => {
    const [logoutLoading, setLogoutLoading] = useState(false);
    const [name, setName] = useState(getDeviceUser().firstname);
    const [surname, setSurname] = useState(getDeviceUser().lastname);
    const [email, setEmail] = useState(getDeviceUser().email);
    const [phone, setPhone] = useState(getDeviceUser().phone);
    const [gender, setGender] = useState(getDeviceUser().gender);
    const [avatar, setAvatar] = useState(getDeviceUser().avatar);

    const handleLogout = () => {
        setLogoutLoading(true);
        logout();
    };

    useEffect(() => {
        if (!getAccessToken()) {
            navigation.navigate('Login');
            setLogoutLoading(false);
        }
    }, [user]);

    const openOptions = () => {
        const BUTTONS = ['Open Camera', 'Open Gallery', 'Cancel'];
        const CANCEL_INDEX = 2;

        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                title: 'Select image to upload',
            },
            buttonIndex => {
                if (buttonIndex === 0) {
                    handleOpenCamera();
                } else if (buttonIndex === 1) {
                    handleOpenGallery();
                }
            },
        );
    };

    const handleOpenGallery = () => {
        ImagePicker.openPicker({
            cropping: false
        }).then(image => {
            navigation.navigate('ProfileImageCropper', {setAvatar: setAvatar, image: image,getProfileInfo: getProfileInfo});
        });
    };


    const handleOpenCamera = () => {
        ImagePicker.openCamera({
            compressImageQuality: 0.5,
            cropping: false
        }).then(image => {
            navigation.navigate('ProfileImageCropper', {setAvatar: setAvatar, image: image,getProfileInfo: getProfileInfo});
        });
    };

    const handleUpdateInfo = () => {
        setLogoutLoading(true);
        updateProfileInfo({
            firstname: name,
            lastname: surname,
            email: email,
            phone: phone,
            gender: gender
        }, response => {
            getProfileInfo();
            Toast.show({
                position: 'top',
                text: 'Profile was successfully updated.',
                buttonText: 'OK',
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
            setLogoutLoading(false);
        }, error => {
            Toast.show({
                position: 'top',
                text: 'Profile could not update.',
                buttonText: 'OK',
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
            setLogoutLoading(false);
        });
    };


    return (
        <ScreenAuthorizationHoc navigation={navigation}>
            <DataLoadingComponent loading={logoutLoading}/>
            <View style={styles.container}>
                <HeaderTextHolder text={"Profile"}/>
                <BackButton handleSignIn={() => navigation.goBack()}/>
                <LogoutButton handleSignIn={handleLogout}/>
                <ScrollView contentContainerStyle={{alignItems: 'center'}}
                            style={{marginTop: 70, marginBottom: 10, textAlign: 'center'}}>
                    <TouchableOpacity onPress={handleOpenGallery}>
                        <SimpleAvatar uri={{uri: avatar}}
                                      style={{
                                          marginBottom: 20, width: 100, height: 100,
                                          borderRadius: 50
                                      }}/>
                    </TouchableOpacity>
                    <FormHeaderDescriptor text={"Personal information"}/>
                    <View style={{
                        flexDirection: 'row', width: SCREEN_WIDTH_ORIGINAL - 30, borderWidth: 1,
                        borderColor: SECONDARY_BG_COLOR, marginTop: 10, marginBottom: 20
                    }}>
                        <Icon name={"user"} style={{fontSize: 22, marginLeft: 10, marginTop: 14, width: '10%'}}/>
                        <TextInput placeholder={"Enter your name"}
                                   value={name}
                                   onChangeText={e => setName(e)}
                                   style={{fontSize: 18, width: '90%'}}
                        />
                    </View>
                    <View style={{
                        flexDirection: 'row', width: SCREEN_WIDTH_ORIGINAL - 30, borderWidth: 1,
                        borderColor: SECONDARY_BG_COLOR, marginBottom: 20
                    }}>
                        <Icon name={"user"} style={{fontSize: 22, marginLeft: 10, marginTop: 14, width: '10%'}}/>
                        <TextInput placeholder={"Enter your surname"}
                                   value={surname}
                                   onChangeText={e => setSurname(e)}
                                   style={{fontSize: 18}}
                        />
                    </View>
                    <View style={{
                        flexDirection: 'row', width: SCREEN_WIDTH_ORIGINAL - 30, borderWidth: 1,
                        borderColor: SECONDARY_BG_COLOR, marginBottom: 20
                    }}>
                        <Icon name={"envelope"} style={{fontSize: 20, marginLeft: 8, marginTop: 14, width: '10%'}}/>
                        <TextInput placeholder={"Enter your email"}
                                   value={email}
                                   onChangeText={e => setEmail(e)}
                                   style={{fontSize: 18}}
                        />
                    </View>
                    <View style={{
                        flexDirection: 'row', width: SCREEN_WIDTH_ORIGINAL - 30, borderWidth: 1,
                        borderColor: SECONDARY_BG_COLOR, marginBottom: 20
                    }}>
                        <Icon name={"phone"} style={{fontSize: 20, marginLeft: 8, marginTop: 14, width: '10%'}}/>
                        <TextInput placeholder={"Enter your phone"}
                                   value={phone}
                                   onChangeText={e => setPhone(e)}
                                   style={{fontSize: 18}}
                        />
                    </View>
                    <View style={{
                        flexDirection: 'row', width: '94%', marginLeft: 8
                    }}>
                        <SimplePicker data={[
                            {'id': 'm', 'title': 'Male'},
                            {'id': 'f', 'title': 'Female'}
                        ]}
                                      placeHolder={"Choose gender"}
                                      selectedValue={gender}
                                      setSelectedValue={setGender}
                                      style={{width: '100%'}}
                        />
                    </View>
                    <View style={{flexDirection: 'row', marginBottom: 20}}>
                        <StandardButton text={"Update"} handleButton={handleUpdateInfo} style={{width: '95%'}}/>
                    </View>
                </ScrollView>
            </View>
        </ScreenAuthorizationHoc>
    );
};

const mapStateToProps = ({user}) => {
    return {
        user
    }
};

const mapDispatchToProps = {
    setOperationLoading,
    logout,
    getProfileInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
