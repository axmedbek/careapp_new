import React, {useEffect, useState} from 'react';
import {Image, TouchableOpacity, View} from "react-native";
import ScreenAuthorizationHoc from "../../../HOCs/ScreenAuthorizationHOC";
import Text from "../../../components/text/Text";
import styles from './styles';
import {concatObjects} from "../../../helpers/standard.helper";
import {Toast} from "native-base";
import {encryptString} from "../../../config/encryption";
import {saveSetting} from "../../../config/caching/actions/setting";
const logo = require('../../../assets/images/logo/logo.png');


const Pincode = ({navigation}) => {

    const [pin, setPin] = useState('');
    const [repeat, setRepeat] = useState('');

    useEffect(() => {
        if (repeat.length === 4) {
            if (repeat === pin) {
                saveSetting('pinCode',encryptString(pin));
                alert("Pin code was successfully changed!");
                navigation.push('Account');
            }
            else {
                setPin('');
                setRepeat('');
                Toast.show({
                    position: 'bottom',
                    text: 'Repeated pin code not equal!',
                    buttonText: 'OK',
                    type: 'danger',
                    textStyle: {marginLeft: 10},
                    duration: 3000
                });
            }
        }
    },[repeat]);

    const handleKeyPress = (key) => {
        if (key === 'cancel') {
            navigation.goBack();
        } else if (key === 'clear') {
            setPin('');
            setRepeat('');
        } else {
            if (pin.length > 3) {
                if (repeat.length < 4) {
                    setRepeat(repeat+key);
                }
            }
            else {
                setPin(pin+key);
            }
        }
    };


    return (
        <ScreenAuthorizationHoc navigation={navigation}>
            <View style={{flex:1,alignItems: 'center', marginTop: 30}}>
                <Image
                    style={styles.logo}
                    source={logo}
                />
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.title}>Set new pin/pass code</Text>
                    <View style={styles.codeContainer}>
                        <View style={concatObjects(styles.code,pin.length > 0 ? styles.activeCode : {})}/>
                        <View style={concatObjects(styles.code,pin.length > 1 ? styles.activeCode : {})}/>
                        <View style={concatObjects(styles.code,pin.length > 2 ? styles.activeCode : {})}/>
                        <View style={concatObjects(styles.code,pin.length > 3 ? styles.activeCode : {})}/>
                    </View>
                </View>
                <View style={concatObjects({alignItems: 'center'},pin.length > 3 ? {opacity: 1} : {opacity: 0})}>
                    <Text style={styles.repeatTitle}>Repeat pin/pass code</Text>
                    <View style={styles.codeContainer}>
                        <View style={concatObjects(styles.code,repeat.length > 0 ? styles.activeCode : {})}/>
                        <View style={concatObjects(styles.code,repeat.length > 1 ? styles.activeCode : {})}/>
                        <View style={concatObjects(styles.code,repeat.length > 2 ? styles.activeCode : {})}/>
                        <View style={concatObjects(styles.code,repeat.length > 3 ? styles.activeCode : {})}/>
                    </View>
                </View>
                <View style={styles.keyboard}>
                    <View style={styles.keyRow}>
                        <TouchableOpacity onPress={() => handleKeyPress(1)}><Text
                            style={styles.key}>1</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress(2)}><Text
                            style={styles.key}>2</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress(3)}><Text
                            style={styles.key}>3</Text></TouchableOpacity>
                    </View>
                    <View style={styles.keyRow}>
                        <TouchableOpacity onPress={() => handleKeyPress(4)}><Text
                            style={styles.key}>4</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress(5)}><Text
                            style={styles.key}>5</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress(6)}><Text
                            style={styles.key}>6</Text></TouchableOpacity>
                    </View>
                    <View style={styles.keyRow}>
                        <TouchableOpacity onPress={() => handleKeyPress(7)}><Text
                            style={styles.key}>7</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress(8)}><Text
                            style={styles.key}>8</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress(9)}><Text
                            style={styles.key}>9</Text></TouchableOpacity>
                    </View>
                    <View style={styles.keyRow}>
                        <TouchableOpacity onPress={() => handleKeyPress('cancel')}><Text
                            style={styles.cancelKey}>Cancel</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress(0)}><Text
                            style={styles.key}>0</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => handleKeyPress('clear')}><Text
                            style={styles.clearKey}>Clear</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScreenAuthorizationHoc>
    );
};

export default Pincode;
