import {StyleSheet} from "react-native";
import {DIVIDER_COLOR, MAIN_COLOR, MAIN_COLOR_TRANSPARENT, MAIN_MID_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    main: {
        alignItems: 'center',
        marginTop: 20,
        flex: 1
    },
    avatar: {
        width: 250,
        height: 100,
        resizeMode: 'contain',
        borderRadius: 250 * 80 / 2
    },
    logo: {
        width: 250,
        height: 80,
        resizeMode: 'contain'
    },
    title: {
        marginTop: 50,
        fontSize: 22,
        letterSpacing: 0.8,
        marginBottom: 5
    },
    repeatTitle: {
        fontSize: 22,
        letterSpacing: 0.8,
        marginBottom: 5
    },
    codeContainer: {
        flexDirection: 'row',
        padding: 10,
        marginBottom: 10
    },
    code: {
        width: 16,
        height: 16,
        backgroundColor: DIVIDER_COLOR,
        borderRadius: 8,
        marginLeft: 15
    },
    activeCode: {
        backgroundColor: MAIN_COLOR
    },
    keyboard: {
        position: 'absolute',
        bottom: 0,
        paddingBottom: 20,
        backgroundColor: MAIN_COLOR,
        width: '100%',
        alignItems: 'center',
        paddingTop: 20
    },
    keyRow: {
        flexDirection: 'row'
    },
    key: {
        fontSize: 33,
        color: 'white',
        marginLeft: 45,
        marginRight: 45,
        marginBottom: 30,
    },
    cancelKey: {
        fontSize: 28,
        color: 'white',
        marginRight: 10
    },
    clearKey: {
        marginLeft: 20,
        fontSize: 28,
        color: 'white',
        marginRight: 10
    }
});

export default styles;
