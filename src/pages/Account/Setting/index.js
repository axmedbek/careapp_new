import React, {useEffect, useState} from 'react';
import {Linking, ScrollView, Text, TouchableOpacity, View} from "react-native";
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import Icon from "react-native-vector-icons/MaterialIcons";
import SimplePicker from "../../../components/selects/SimpleSelect";
import {
    deleteValueFromSettingWithKey,
    getLanguage,
    getValueFromSettingWithKey
} from "../../../config/caching/actions/setting";
import {BACK_ICON_COLOR, DIVIDER_COLOR, MAIN_COLOR, RED_COLOR} from "../../../constants/colors";
import ConfirmToast from "../../../components/toasts/ConfirmToast";
import {SCREEN_WIDTH} from "../../../constants/setting";
import {setOperationLoading} from "../../../actions/loading.action";
import {logout} from "../../../actions/auth.action";
import {connect} from "react-redux";
import {updateLanguage} from "../../../actions/lang.action";


const Setting = ({navigation,lang,setOperationLoading,updateLanguage}) => {

    const [deleteCred, setDeletedCred] = useState(false);
    const [deleteFinger, setDeleteFinger] = useState(false);
    const [selectedLang, setSelectedLang] = useState(getLanguage());

    const openURL = (url) => {
        Linking.openURL(url).catch((err) => console.error('An error occurred', err));
    };

    useEffect(() => {
        setOperationLoading(true);
        updateLanguage(selectedLang);
        setOperationLoading(false);
    }, [selectedLang]);


    const handleDeletePincode = () => {
        deleteValueFromSettingWithKey("pinCode");
        alert("Pin code was deleted successfully");
        setDeletedCred(false);
    };

    const handleDeleteFingerprint = () => {
        deleteValueFromSettingWithKey("fingerprint");
        alert("Fingerprint was deleted successfully");
        setDeleteFinger(false);
    };

    return (
        <AuthenticatedScreenHOC title={"Settings"} navigation={navigation}
                                menuIcon={false} backIcon={true} backRoute={() => navigation.navigate('HomeScreenNavigator')}>
            <ScrollView style={{marginLeft: 4}}>
                <View style={{paddingTop: 15, paddingLeft: 10, paddingRight: 50}}>
                    <Text style={{fontSize: 14, color: '#197d19'}}>{lang.messages.setting.general}</Text>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name={"language"}
                              style={{fontSize: 35, color: MAIN_COLOR, marginTop: 15, marginRight: 6}}/>
                        <SimplePicker data={[
                            {'id': 'en', 'title': 'English'},
                            {'id': 'da', 'title': 'Danish'}
                        ]}
                                      placeHolder={lang.messages.setting.choose_lang}
                                      selectedValue={selectedLang}
                                      setSelectedValue={setSelectedLang}
                                      style={{width: '100%'}}
                        />
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <Icon name={"person"} style={{fontSize: 35, color: MAIN_COLOR}}/>
                        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                            <Text style={{fontSize: 16, marginLeft: 10, marginTop: 8}}>{lang.messages.setting.info}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{marginTop: 10, marginBottom: 15, backgroundColor: DIVIDER_COLOR, height: 1}}/>

                <View style={{paddingTop: 15, paddingLeft: 10, paddingRight: 50}}>
                    <Text style={{fontSize: 14, color: '#197d19'}}>{lang.messages.setting.security}</Text>
                    <View style={{flexDirection: 'row', width: SCREEN_WIDTH}}>
                        <Icon name={"fiber-pin"}
                              style={{fontSize: 35, color: MAIN_COLOR, marginTop: 14, marginRight: 15}}/>
                        <TouchableOpacity style={{marginTop: 12}}
                                          onPress={() => navigation.push('Pincode')}>
                            <Text style={{fontSize: 16}}>{lang.messages.setting.change_pin}</Text>
                            {
                                getValueFromSettingWithKey("pinCode") ?
                                    <Text style={{color: BACK_ICON_COLOR, fontSize: 12}}>{lang.messages.setting.added}</Text> :
                                    <Text style={{color: BACK_ICON_COLOR, fontSize: 12}}>{lang.messages.setting.not_added}</Text>
                            }
                        </TouchableOpacity>
                        {
                            getValueFromSettingWithKey("pinCode") &&
                            <TouchableOpacity style={{
                                position: 'absolute',
                                right: 0,
                                flexDirection: 'row',
                                backgroundColor: RED_COLOR, height: 30,
                                marginTop: 15, borderRadius: 4, paddingTop: 5, paddingRight: 10
                            }} onPress={() => setDeletedCred(true)}>
                                <Icon name={"delete"}
                                      style={{color: 'white', fontSize: 14, marginLeft: 4, marginTop: 2}}/>
                                <Text style={{color: 'white', paddingLeft: 4, paddingRight: 4}}>{lang.messages.setting.delete}</Text>
                            </TouchableOpacity>
                        }
                    </View>

                    <View style={{flexDirection: 'row', width: SCREEN_WIDTH}}>
                        <Icon name={"fingerprint"}
                              style={{fontSize: 35, color: MAIN_COLOR, marginTop: 14, marginRight: 15}}/>
                        <TouchableOpacity style={{marginTop: 12}} onPress={() => navigation.navigate('Fingerprint')}>
                            <Text style={{fontSize: 16}}>{lang.messages.setting.login_fp}</Text>
                            {
                                getValueFromSettingWithKey("fingerprint") ?
                                    <Text style={{color: BACK_ICON_COLOR, fontSize: 12}}>{lang.messages.setting.added}</Text> :
                                    <Text style={{color: BACK_ICON_COLOR, fontSize: 12}}>{lang.messages.setting.not_added}</Text>
                            }
                        </TouchableOpacity>
                    </View>

                    <View style={{flexDirection: 'row'}}>
                        <Icon name={"lock"}
                              style={{fontSize: 35, color: MAIN_COLOR, marginTop: 14, marginRight: 15}}/>
                        <TouchableOpacity style={{marginTop: 20}} onPress={() => navigation.navigate('Password')}>
                            <Text style={{fontSize: 17}}>{lang.messages.setting.change_pass}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{marginTop: 10, marginBottom: 15, backgroundColor: DIVIDER_COLOR, height: 1}}/>

                <View style={{paddingTop: 15, paddingLeft: 10, paddingRight: 50}}>
                    <Text style={{fontSize: 14, color: '#197d19'}}>{lang.messages.setting.about}</Text>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name={"cloud"} style={{fontSize: 35, color: MAIN_COLOR, marginTop: 15, marginRight: 15}}/>
                        <View style={{marginTop: 6}}>
                            <Text style={{color: BACK_ICON_COLOR}}>{lang.messages.setting.version}</Text>
                            <Text style={{letterSpacing: 0.8, fontSize: 16}}>1.1.2</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name={"copyright"}
                              style={{fontSize: 35, color: MAIN_COLOR, marginTop: 15, marginRight: 15}}/>
                        <TouchableOpacity style={{marginTop: 8}} onPress={() => openURL("http://carecompagniet.dk/")}>
                            <Text style={{color: BACK_ICON_COLOR}}>{lang.messages.setting.copy}</Text>
                            <Text style={{letterSpacing: 0.8, fontSize: 16}}>Carecompagniet</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <ConfirmToast
                    show={deleteCred}
                    handClose={() => setDeletedCred(false)}
                    type={"info"}
                    message={"Do you sure to delete pin code ?"}
                    item={null}
                    deleteHandle={handleDeletePincode}
                    errors={null}
                />

                <ConfirmToast
                    show={deleteFinger}
                    handClose={() => setDeleteFinger(false)}
                    type={"info"}
                    message={"Do you sure to delete finger ?"}
                    item={null}
                    deleteHandle={handleDeleteFingerprint}
                    errors={null}
                />
            </ScrollView>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({ lang }) => {
    return {
        lang
    }
};

const mapDispatchToProps = {
    setOperationLoading,
    updateLanguage,
    logout
};

export default connect(mapStateToProps,mapDispatchToProps)(Setting);
