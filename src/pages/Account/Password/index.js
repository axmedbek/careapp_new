import React, {useState} from 'react';
import {ScrollView, View} from "react-native";
import FormHeaderDescriptor from "../../../components/headers/FormHeaderDescriptor";
import PasswordInputComponent from "../../../components/inputs/PasswordInputComponent";
import StandardButton from "../../../components/buttons/StandardButton";
import ScreenAuthorizationHoc from "../../../HOCs/ScreenAuthorizationHOC";
import HeaderTextHolder from "../../../components/headers/HeaderTextHolder";
import BackButton from "../../../components/buttons/BackButton";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import {updatePassword} from "../../../services/auth.service";
import {Toast} from "native-base";

const Password = ({navigation}) => {
    const [logoutLoading, setLogoutLoading] = useState(false);
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const handlePasswordChange = () => {
        setLogoutLoading(true);
        updatePassword({
            password: password,
            password_confirmation: confirmPassword
        }, response => {
            Toast.show({
                position: 'top',
                text: 'Password was successfully updated.',
                buttonText: 'OK',
                type: 'success',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
            setLogoutLoading(false);
        }, error => {
            console.log(error.response);
            Toast.show({
                position: 'top',
                text: 'Password could not update.',
                buttonText: 'OK',
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 3000
            });
            setLogoutLoading(false);
        });
    };

    return (
        <ScreenAuthorizationHoc navigation={navigation}>
            <HeaderTextHolder text={"Password"}/>
            <BackButton handleSignIn={() => navigation.goBack()}/>
            <ScrollView contentContainerStyle={{alignItems: 'center',marginBottom: 80}}
                        style={{marginTop: 100, textAlign: 'center'}}>
                <FormHeaderDescriptor text={"Change password"}/>
                <View style={{flexDirection: 'row'}}>
                    <PasswordInputComponent passwordError={false}
                                            setPasswordError={() => console.log("--")}
                                            password={password}
                                            setPassword={setPassword}/>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <PasswordInputComponent passwordError={false}
                                            setPasswordError={() => console.log("--")}
                                            password={confirmPassword}
                                            setPassword={setConfirmPassword}
                                            placeholder={"Password again"}
                    />
                </View>
                <View style={{flexDirection: 'row',}}>
                    <StandardButton text={"Change password"} handleButton={handlePasswordChange}
                                    style={{width: '95%'}}/>
                </View>
            </ScrollView>
            <DataLoadingComponent loading={logoutLoading}/>
        </ScreenAuthorizationHoc>
    );
};

export default Password;
