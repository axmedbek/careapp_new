import React,{useEffect,useState} from 'react';
import {View, FlatList} from 'react-native';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import styles from './style';
import CircleActionButton from "../../../components/buttons/CircleActionButton";
import {addFolder, deleteFolder, getFolders, updateFolder} from "../../../actions/note.action";
import FolderItem from "../../../containers/Notes/FolderItem";
import FolderAddForm from "../../../forms/Notes/FolderAddForm";
import ConfirmToast from "../../../components/toasts/ConfirmToast";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import {isEmptyObject} from "../../../helpers/standard.helper";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import Modal from "../../../components/modals/Modal";
import {changeNotificationStatus} from "../../../actions/setting.action";

const Notes = ({lang, navigation,folders,loading,deleting,deleteFolder,changeNotificationStatus,
                   getFolders,addFolder,updateFolder,adding,noteErrors}) => {
    const [addModal,setAddModal] = useState(false);
    const [item,setItem] = useState(null);
    const [deleteModal,setDeleteModal] = useState(false);

    useEffect(() => {
        getFolders();
        changeNotificationStatus(true);
        navigation.addListener('focus', () => {
            changeNotificationStatus(true);
        });
    }, []);

    useEffect(() => {
        if (!deleting) {
            getFolders();
            setDeleteModal(false);
        }
    }, [deleting]);

    useEffect(() => {
        if (!adding && isEmptyObject(noteErrors)) {
            getFolders();
            setAddModal(false);
        }
    }, [adding]);

    const refresh = () => {
        getFolders();
    };

    const handleEdit = (item) => {
        setItem(item);
        setAddModal(true);
    };

    const handleRemove = (item) => {
        setItem(item);
        setDeleteModal(true);
    };

    return (
        <AuthenticatedScreenHOC title={lang.messages.notes.title} navigation={navigation} lang={lang}>
            <DataLoadingComponent loading={adding || deleting}/>
            <View style={styles.container}>
                <FlatList
                    data={folders}
                    renderItem={({ item }) =>
                        <FolderItem item={item}
                                    handleEdit={() => handleEdit(item)}
                                    handleRemove={() => handleRemove(item)}
                                    navigation={navigation}
                        />}
                    contentContainerStyle={{flexGrow: 1}}
                    ListEmptyComponent={<ListItemEmpty loading={loading} text="No mapper"/>}
                    keyExtractor={item => item.id.toString()}
                    onRefresh={refresh}
                    refreshing={loading}
                />
                <CircleActionButton handlePress={() => {
                    setItem(null);
                    setAddModal(true);
                }} icon={"folder-plus"}/>
                <Modal
                    modalVisible={addModal}
                    closeModal={() => setAddModal(false)}
                    title={item ? "Edit Folder" : "Add New Folder"}
                >
                    <FolderAddForm
                        hideModal={() => setAddModal(false)}
                        addFolder={addFolder}
                        updateFolder={updateFolder}
                        noteErrors={noteErrors}
                        loading={loading}
                        getFolders={getFolders}
                        item={item}
                    />
                </Modal>
                <ConfirmToast
                    show={deleteModal}
                    handClose={() => setDeleteModal(false)}
                    type={"info"}
                    message={"Do you sure to delete this folder"}
                    item={item}
                    deleteHandle={deleteFolder}
                    errors={noteErrors}
                />
            </View>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang,noteReducer}) => {
    return {
        lang,
        folders: noteReducer.folders,
        loading: noteReducer.fetching,
        noteErrors: noteReducer.errors,
        deleting: noteReducer.deleting,
        adding: noteReducer.adding,
        addFolderLoading: noteReducer.addFolderLoading
    };
};

const mapDispatchToProps = {
    getFolders,
    addFolder,
    updateFolder,
    deleteFolder,
    changeNotificationStatus
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Notes);
