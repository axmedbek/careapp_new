import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import AuthenticatedScreenHOC from "../../../HOCs/AuthenticatedScreenHOC";
import {connect} from "react-redux";
import styles from './style';
import CircleActionButton from "../../../components/buttons/CircleActionButton";
import NoteAddForm from "../../../forms/Notes/NoteAddForm";
import {addNote, deleteNote, getNotes, updateNote} from "../../../actions/note.action";
import NoteItem from "../../../containers/Notes/NoteItem";
import ConfirmToast from "../../../components/toasts/ConfirmToast";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import Modal from "../../../components/modals/Modal";


const Note = ({lang, route, navigation, updateNote, addNote, deleteNote, noteErrors, deleting, adding, notes, loading, getNotes}) => {
    const {id, title} = route.params;

    const [addModal, setAddModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [item, setItem] = useState(null);


    useEffect(() => {
        getNotes(id);
    }, []);

    useEffect(() => {
        if (!deleting) {
            setDeleteModal(false);
            getNotes(id);
        }
    }, [deleting]);


    useEffect(() => {
        if (!adding) {
            setAddModal(false);
            getNotes(id);
        }
    }, [adding]);

    const handleEdit = (item) => {
        setItem(item);
        setAddModal(true);
    };

    const handleRemove = (item) => {
        setItem(item);
        setDeleteModal(true);
    };

    const refresh = () => {
        getNotes(id);
    };

    return (
        <AuthenticatedScreenHOC title={`${title} folder`} navigation={navigation} lang={lang} backIcon={true}>
            <DataLoadingComponent loading={loading && deleteModal}/>
            <View style={styles.container}>
                <FlatList
                    data={notes}
                    renderItem={({item}) =>
                        <NoteItem item={item}
                                  handleEdit={() => handleEdit(item)}
                                  handleRemove={() => handleRemove(item)}
                        />}
                    contentContainerStyle={{flexGrow: 1}}
                    ListEmptyComponent={<ListItemEmpty loading={loading} text="No mapper"/>}
                    keyExtractor={item => item.id.toString()}
                    onRefresh={refresh}
                    ItemSeparatorComponent={() => <View
                        style={{
                            width: '100%',
                            height: 1,
                            backgroundColor: '#D8D8D8'
                        }}>

                    </View>}
                    refreshing={loading}
                />
                <CircleActionButton icon={"note-plus"} handlePress={() => {
                    setItem(null);
                    setAddModal(true);
                }}/>
                <Modal
                    closeModal={() => setAddModal(false)}
                    modalVisible={addModal}
                    title={item ? "Edit Note" : "Add Note"}
                >
                    <NoteAddForm
                        hideModal={() => setAddModal(false)}
                        addNote={addNote}
                        updateNote={updateNote}
                        noteErrors={{}}
                        loading={adding}
                        folder_id={id}
                        item={item}
                    />
                </Modal>
                <ConfirmToast
                    show={deleteModal}
                    handClose={() => setDeleteModal(false)}
                    type={"info"}
                    message={"Do you sure to delete this note"}
                    item={item}
                    deleteHandle={deleteNote}
                    errors={noteErrors}
                />
            </View>
        </AuthenticatedScreenHOC>
    );
};

const mapStateToProps = ({lang, noteReducer}) => {
    return {
        lang,
        notes: noteReducer.notes,
        loading: noteReducer.fetching,
        deleting: noteReducer.deleting,
        adding: noteReducer.adding,
        noteErrors: noteReducer.errors,
        addFolderLoading: noteReducer.addFolderLoading,
    };
};

const mapDispatchToProps = {
    getNotes,
    addNote,
    updateNote,
    deleteNote
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Note);
