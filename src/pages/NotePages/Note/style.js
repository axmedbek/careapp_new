import {StyleSheet} from "react-native";
import {MAIN_COLOR} from "../../../constants/colors";
import {BOLD_FONT} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 6,
        paddingRight: 6,
        paddingTop: 10,
        paddingBottom: 80,
    },
    header: {
        flexDirection: 'row',
        backgroundColor: MAIN_COLOR,
        padding: 8,
        paddingLeft: 10,
        borderRadius: 4
    },
    headerIcon: {
        fontSize: 28,
        color: 'white'
    },
    headerTxt: {
        fontFamily: BOLD_FONT,
        letterSpacing: 0.4,
        fontSize: 20,
        marginLeft: 10,
        marginTop: 2,
        color: 'white'
    },
    headerCount: {
        position: 'absolute',
        right: 16,
        color: MAIN_COLOR,
        backgroundColor: 'white',
        width: 30,
        height: 30,
        textAlign: 'center',
        borderRadius: 15,
        top: 6,
        fontFamily: BOLD_FONT,
        paddingTop: 4,
        fontSize: 16
    }
});

export default styles;
