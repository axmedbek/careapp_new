import React, {useEffect, useState} from 'react';
import {FlatList, Text, TextInput, TouchableOpacity, View} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import styles from './style';
import ContactListItem from "../../../containers/ChatContainer/ContactListItem";
import {getAllContacts} from "../../../actions/chat.action";
import {connect} from "react-redux";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

let timeout = null;

const ContactList = ({navigation, contacts, getAllContacts}) => {

    const [searchValue, setSearchValue] = useState('');

    useEffect(() => {
        getAllContacts(searchValue);
    }, []);

    const refresh = () => {
        getAllContacts(searchValue);
    };

    const doSearch = (value) => {
        setSearchValue(value);
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            //search function
            getAllContacts(value);
        }, 1000);
    };

    return (
        <View>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <MaterialIcons style={styles.backIcon} name={"arrow-back"}/>
                </TouchableOpacity>
                <TextInput value={searchValue}
                           onChangeText={e => doSearch(e)}
                           style={styles.contactInput}
                           placeholder={"Search contact any name"}/>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('CreateChatGroup')} style={styles.createGroup}>
                <Icon style={styles.createGroupIcon} name={"account-group"}/>
                <Text style={styles.createGroupText}>Create new group</Text>
            </TouchableOpacity>
            <FlatList
                data={contacts}
                renderItem={({item}) => <ContactListItem navigation={navigation} contact={item}/>}
                contentContainerStyle={{flexGrow: 1, paddingBottom: 80}}
                ListEmptyComponent={<ListItemEmpty loading={false} text="No contact"/>}
                keyExtractor={item => `${item._id}`}
                ItemSeparatorComponent={() => <View
                    style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: '#D8D8D8'
                    }}>
                </View>}
                onRefresh={refresh}
                refreshing={false}
            />
        </View>
    );
};

const mapStateToProps = ({lang, chatReducer}) => {
    return {
        lang,
        contacts: chatReducer.contacts,
    };
};

const mapDispatchToProps = {
    getAllContacts
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ContactList);
