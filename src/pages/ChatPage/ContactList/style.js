import {StyleSheet} from "react-native";
import {
    BACK_ICON_COLOR,
    BLACK_COLOR,
    DIVIDER_COLOR,
    GRAY_COLOR,
    LIGHT_COLOR,
    MAIN_COLOR
} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";

const styles = StyleSheet.create({
    header: {
        paddingTop: 10,
        flexDirection: 'row',
        backgroundColor: MAIN_COLOR,
        height: 80
    },
    backIcon: {
        fontSize: 28,
        color: 'white',
        marginTop: 15,
        marginLeft: 10
    },
    contactInput: {
        backgroundColor: 'white',
        height: 50,
        borderRadius: 4,
        width: '80%',
        marginTop: 4,
        marginLeft: 10,
        paddingLeft: 15
    },
    createGroup: {
        borderBottomWidth: 1,
        borderBottomColor: DIVIDER_COLOR,
        flexDirection: 'row',
        height: 80,
        paddingLeft: 15,
        paddingTop: 10
    },
    createGroupIcon: {
        fontSize: 40,
        backgroundColor: '#0D8ABC',
        height: 60,
        width: 60,
        borderRadius: 30,
        paddingTop: 5,
        textAlign: 'center',
        color: 'white'
    },
    createGroupText: {
        fontSize: 20,
        marginLeft: 10,
        marginTop: 15,
        fontWeight: 'bold',
        color: BLACK_COLOR
    }
});

export default styles;
