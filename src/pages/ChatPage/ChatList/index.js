import React, {useEffect, useState} from 'react';
import {FlatList, Modal, Text, TextInput, TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/Entypo";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import styles from './style';
import ListItemEmpty from "../../../containers/ListItemEmpty";
import ChatListitem from "../../../containers/ChatContainer/ChatListitem";
import {connect} from "react-redux";
import {getAllChats} from "../../../actions/chat.action";
import {changeNotificationStatus} from "../../../actions/setting.action";

let timeout = null;

const ChatList = ({navigation, fetching, socket_connection,changeNotificationStatus, getAllChats, chats}) => {

    const [operationModal, setOperationModal] = useState(false);
    const [searchValue, setSearchValue] = useState('');

    useEffect(() => {
        changeNotificationStatus(false);
        navigation.addListener('focus', () => {
            changeNotificationStatus(false);
        });
        getAllChats(searchValue);

        if (socket_connection.socket) {
            socket_connection.socket.on('reload chat', response => {
                getAllChats('');
            });
        }
    }, []);

    const refresh = () => {
        getAllChats(searchValue);
    };

    const doSearch = (value) => {
        setSearchValue(value);
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            getAllChats(value);
        }, 1000);
    };

    // console.log(chats);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TextInput style={styles.search}
                           value={searchValue}
                           onChangeText={e => doSearch(e)}
                           placeholder={"Search contact or group name"}/>
                <View style={styles.menuContainer}>
                    <TouchableOpacity onPress={() => setOperationModal(true)}>
                        <Icon style={styles.menu} name={"dots-three-vertical"}/>
                    </TouchableOpacity>
                </View>
            </View>
            <FlatList
                data={chats}
                renderItem={({item}) => <ChatListitem navigation={navigation} chat={item}/>}
                contentContainerStyle={{flexGrow: 1, paddingBottom: 30}}
                ListEmptyComponent={<ListItemEmpty loading={fetching} text="No dialogue"/>}
                keyExtractor={item => `${item._id}`}
                ItemSeparatorComponent={() =>
                    <View style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: '#D8D8D8'
                    }}>
                    </View>}
                onRefresh={refresh}
                refreshing={false}
            />
            <TouchableOpacity style={styles.contactBtn} onPress={() => navigation.navigate('ContactList')}>
                <AntDesign style={styles.contactIcon} name={"contacts"}/>
            </TouchableOpacity>

            <Modal
                animationType="slide"
                visible={operationModal}
                onRequestClose={() => setOperationModal(false)}
                closeOnClick={true}
                transparent={true}
            >
                <View style={styles.modal}>
                    <View style={styles.modalContent}>
                        <TouchableOpacity style={styles.modalClose} onPress={() => setOperationModal(false)}>
                            <AntDesign name={"close"} style={styles.modalCloseBtn}/>
                        </TouchableOpacity>
                        <View style={{marginLeft: 20, marginBottom: 25}}>
                            <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => {
                                setOperationModal(false);
                                navigation.navigate('CreateChatGroup')
                            }}>
                                <MaterialCommunityIcons name={"account-group-outline"} style={styles.chatIcon}/>
                                <Text style={styles.chatText}>Create group</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flexDirection: 'row', marginTop: 10}}
                                              onPress={() => {
                                                  setOperationModal(false);
                                                  navigation.navigate('ContactList')
                                              }}>
                                <MaterialCommunityIcons name={"emoticon"} style={styles.chatIcon}/>
                                <Text style={styles.chatText}>New chat</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const mapStateToProps = ({lang, chatReducer, socket_connection}) => {
    return {
        lang,
        chats: chatReducer.chats,
        fetching: chatReducer.fetching,
        socket_connection: socket_connection
    };
};

const mapDispatchToProps = {
    getAllChats,
    changeNotificationStatus
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ChatList);
