import {StyleSheet} from "react-native";
import {BACK_ICON_COLOR, BLACK_COLOR, DIVIDER_COLOR, MAIN_COLOR, TEXT_COLOR} from "../../../constants/colors";
import {SCREEN_WIDTH} from "../../../constants/setting";
import {increase_brightness} from "../../../helpers/standard.helper";

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flexDirection: 'row',
        backgroundColor: MAIN_COLOR,
        paddingLeft: 10,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 10
    },
    search: {
        backgroundColor: 'white',
        width: '85%',
        height: 45,
        borderRadius: 4,
        paddingLeft: 10,
        marginLeft: 5
    },
    menuContainer: {
        width: '15%',
        justifyContent: 'center',
        textAlign: 'center'
    },
    menu: {
        color: 'white',
        fontSize: 30,
        textAlign: 'center'
    },
    contactBtn: {
        position: 'absolute',
        bottom: 20,
        backgroundColor: MAIN_COLOR,
        width: 60,
        height: 60,
        justifyContent: 'center',
        right: 10,
        borderRadius: 30,

        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    contactIcon: {
        textAlign: 'center',
        color: 'white',
        fontSize: 30
    },
    chatContainer: {
        flexDirection: 'row',
        padding: 10,
        paddingLeft: 15,
        paddingRight: 20
    },
    chatContent: {
        marginLeft: 10,
        paddingTop: 8,
        width: '90%'
    },
    chatHeader: {
        flexDirection: 'row',
        width: '100%'
    },
    chatTitle: {
        width: '60%',
        fontSize: 16,
        fontWeight: 'bold',
        color: increase_brightness(BLACK_COLOR,10)
    },
    chatDate: {
        position: 'absolute',
        right: 20,
        width: 100,
        marginTop: 4,
        color: BACK_ICON_COLOR,
        fontStyle: 'italic'
    },
    chatImage: {
        width: 60,
        height: 60,
        borderRadius: 30,
        marginTop: 2
    },
    desc: {
        paddingTop: 2,
        color: BACK_ICON_COLOR,
        fontSize: 15
    },
    modal: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(134,133,116,0.7)'
    },
    modalContent: {
        backgroundColor: 'white',
        width: SCREEN_WIDTH,
        borderRadius: 4
    },
    modalClose: {
        alignItems: 'flex-end',
        marginTop: 8,
        marginRight: 10
    },
    modalCloseBtn: {
        fontSize: 20
    },
    chatIcon: {
        fontSize: 30,
        marginRight: 10,
        backgroundColor: MAIN_COLOR,
        padding: 10,
        borderRadius: 20,
        color: 'white'
    },
    chatText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: BLACK_COLOR,
        marginTop: 10,
        marginLeft: 10
    }
});

export default styles;
