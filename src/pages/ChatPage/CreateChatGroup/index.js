import React, {useState} from 'react';
import {Image, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import styles from "./style";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Icon from "react-native-vector-icons/Entypo";
import StandardButton from "../../../components/buttons/StandardButton";
import {concatObjects} from "../../../helpers/standard.helper";
import {createChatGroup} from "../../../services/chat.service";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import {ActionSheet, Toast} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import SimpleAvatar from "../../../components/avatars/SimpleAvatar";


const CreateChatGroup = ({navigation}) => {
    const [selectedUsers, setSelectedUsers] = useState([]);
    const [title, setTitle] = useState('');
    const [loading, setLoading] = useState(false);
    const [avatar, setAvatar] = useState('');

    const handleCreateGroup = () => {
        setLoading(true);

        let users = [];
        selectedUsers.map(item => (
            users.push(item.id)
        ));

        let formData = new FormData();
        formData.append('title', title);
        users.map(user => (
            formData.append('receivers[]',user)
        ));

        if (avatar) {
            formData.append('image', {
                uri: avatar,
                type: 'image/jpg',
                name: 'uploaded_group_image.jpg'
            });
        }

        createChatGroup(formData, response => {
            console.log(response);
            setLoading(false);
            navigation.navigate('ChatInbox', {
                id: response.id,
                title: response.data.data.title,
                avatar: response.data.data.image ? response.data.data.image : 'group',
                users: response.data.data.users,
                dialogue: response.data.data._id,
                is_group: !!response.data.data.image
            });
        }, error => {
            console.log(error.response);
            Toast.show({
                position: 'top',
                text: "Group could not create.",
                buttonText: "OK",
                type: 'danger',
                textStyle: {marginLeft: 10},
                duration: 4000
            });
            setLoading(false);
        })
    };

    const updateSelectedUsers = (users) => {
        setSelectedUsers(users)
    };

    const handleClearUser = (user) => {
        setSelectedUsers(selectedUsers.filter(item => item.id !== user.id));
    };

    const openOptions = () => {
        const BUTTONS = ['Open Camera', 'Open Gallery', 'Cancel'];
        const CANCEL_INDEX = 2;

        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                title: 'Select image to upload',
            },
            buttonIndex => {
                if (buttonIndex === 0) {
                    handleOpenCamera();
                } else if (buttonIndex === 1) {
                    handleOpenGallery();
                }
            },
        );
    };

    const handleOpenGallery = () => {
        ImagePicker.openPicker({
            cropping: false
        }).then(image => {
            navigation.navigate('ChatGroupImageCropper', {setAvatar: setAvatar, image: image});
        });
    };


    const handleOpenCamera = () => {
        ImagePicker.openCamera({
            compressImageQuality: 0.5,
            cropping: false
        }).then(image => {
            navigation.navigate('ChatGroupImageCropper', {setAvatar: setAvatar, image: image});
        });
    };

    return (
        <View style={{flex: 1}}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <MaterialIcons style={styles.backIcon} name={"arrow-back"}/>
                </TouchableOpacity>
                <Text style={styles.title}>Create new group</Text>
            </View>
            <View style={styles.content}>
                <Text style={concatObjects(styles.label, {marginTop: 10})}>Add group image</Text>
                <TouchableOpacity onPress={openOptions} style={{alignSelf: 'center'}}>
                    {
                        avatar
                            ? <SimpleAvatar uri={{uri: avatar}}
                                            style={{marginBottom: 20, width: 100, height: 100, borderRadius: 50}}/>
                            : <MaterialCommunityIcons name={"account-group-outline"} style={styles.groupImage}/>
                    }
                </TouchableOpacity>
                <Text style={styles.label}>Group name</Text>
                <TextInput placeholder={"Group name"}
                           style={styles.input}
                           value={title}
                           onChangeText={e => setTitle(e)}
                />
                <Text style={styles.label}>Add users</Text>
                <View style={styles.users}>
                    <TouchableOpacity onPress={() => navigation.navigate('AddGroupUser',
                        {
                            selectedUsers: selectedUsers,
                            updateSelectedUsers: updateSelectedUsers
                        })}>
                        <Icon name={"plus"} style={styles.plus}/>
                    </TouchableOpacity>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        {
                            selectedUsers.map(user => {
                                let userAvatar = user.avatar.small;

                                return (
                                    <View>
                                        <Image source={{uri: userAvatar}} style={styles.userImage}/>
                                        <TouchableOpacity onPress={() => handleClearUser(user)}>
                                            <MaterialIcons name={"close"}
                                                           style={styles.closeIcon}/>
                                        </TouchableOpacity>
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <StandardButton text={"Create"} handleButton={handleCreateGroup}/>
            </View>
            <DataLoadingComponent loading={loading}/>
        </View>
    );
};

export default CreateChatGroup;
