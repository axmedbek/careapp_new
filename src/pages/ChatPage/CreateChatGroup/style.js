import {StyleSheet} from "react-native";
import {DIVIDER_COLOR, LIGHT_COLOR, MAIN_COLOR, RED_COLOR} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";

const styles = StyleSheet.create({
    header: {
        paddingTop: 10,
        flexDirection: 'row',
        backgroundColor: MAIN_COLOR,
        height: 80
    },
    backIcon: {
        fontSize: 28,
        color: 'white',
        marginTop: 15,
        marginLeft: 10
    },
    title: {
        fontSize: 20,
        color: 'white',
        marginLeft: 30,
        marginTop: 15
    },
    content: {
        marginTop: 20,
        marginLeft: 15,
        marginRight: 15
    },
    input: {
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 10
    },
    label: {
        marginTop: 30,
        marginBottom: 10,
        fontSize: 16,
        color: LIGHT_COLOR
    },
    users: {
        marginBottom: 15,
        flexDirection: 'row'
    },
    userImage: {
        width: 60,
        height: 60,
        borderRadius: 30,
        marginLeft: 10
    },
    plus: {
        width: 60,
        height: 60,
        borderRadius: 30,
        textAlign: 'center',
        justifyContent: 'center',
        backgroundColor: MAIN_COLOR,
        fontSize: 30,
        paddingTop: 14,
        color: 'white'
    },
    closeIcon: {
        fontSize: 20,
        textAlign: 'center',
        marginLeft: 10,
        backgroundColor: increase_brightness(RED_COLOR,80),
        borderRadius: 10,
        color: 'tomato'
    },
    groupImage: {
        fontSize: 50,
        backgroundColor: DIVIDER_COLOR,
        width: 100,
        paddingRight: 25,
        paddingTop: 18,
        color: LIGHT_COLOR,
        height: 100,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default styles;
