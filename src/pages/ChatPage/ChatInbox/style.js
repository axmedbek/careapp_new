import {StyleSheet} from "react-native";
import {BLACK_COLOR, MAIN_COLOR} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";
import {SCREEN_HEIGHT, SCREEN_WIDTH} from "../../../constants/setting";

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        backgroundColor: MAIN_COLOR,
        paddingTop: 10,
        paddingBottom: 4,
        height: 70
    },
    headerImage: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    backIcon: {
        fontSize: 30,
        color: 'white',
        marginTop: 10,
        marginLeft: 8,
        marginRight: 4
    },
    title: {
        marginLeft: 8,
        color: 'white',
        marginTop: 2,
        fontSize: 18
    },
    status: {
        marginLeft: 8,
        color: 'white'
    },
    menuContainer: {
        position: 'absolute',
        top: 22,
        right: 15
    },
    menu: {
        fontSize: 25,
        color: 'white'
    },
    content: {
        backgroundColor: increase_brightness(MAIN_COLOR,90)
    },
    messagesContainer: {
        height: SCREEN_HEIGHT - 310
    },
    messages: {
        flex: 1,
        backgroundColor: increase_brightness(MAIN_COLOR,90),
        marginBottom: 60
    },
    bottom: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 10,
        left: 10,
        backgroundColor: 'white',
        borderRadius: 30,
        width: '95%',
        height: 'auto',

        shadowColor: '#000',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    messageBox: {
        paddingLeft: 15,
        width: '70%',
        maxHeight: 150
    },
    operationBtn: {
        position: 'absolute',
        right: 10,
        bottom: 10,
        flexDirection: 'row'
    },
    fileContainer: {
        marginRight: 12
    },
    file: {
        fontSize: 28
    },
    sendContainer: {
        marginRight: 4
    },
    send: {
        fontSize: 28,
        color: MAIN_COLOR
    },
    modal: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(134,133,116,0.7)'
    },
    modalContent: {
        backgroundColor: 'white',
        width: SCREEN_WIDTH,
        borderRadius: 4
    },
    modalClose: {
        alignItems: 'flex-end',
        marginTop: 8,
        marginRight: 10
    },
    modalCloseBtn: {
        fontSize: 20
    },
    chatIcon: {
        fontSize: 30,
        marginRight: 10,
        backgroundColor: MAIN_COLOR,
        padding: 10,
        borderRadius: 20,
        color: 'white'
    },
    chatText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: BLACK_COLOR,
        marginTop: 10,
        marginLeft: 10
    }
});

export default styles;
