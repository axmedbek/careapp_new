import React, {useEffect, useReducer, useRef, useState} from 'react';
import {FlatList, Keyboard, ScrollView, View} from "react-native";
import styles from "./style";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import MessageItem from "../../../containers/ChatContainer/MessageItem";
import {SCREEN_HEIGHT} from "../../../constants/setting";
import {concatObjects, uuidv4} from "../../../helpers/standard.helper";
import {chatFileSend, chatMessageSend} from "../../../services/chat.service";
import {addMessageChatReducer, getChatMessages, getChatMessagesWithUserId} from "../../../actions/chat.action";
import {connect} from "react-redux";
import ImagePicker from "react-native-image-crop-picker";
import DocumentPicker from 'react-native-document-picker';
import {getDeviceUser} from "../../../config/caching/actions/setting";
import ChatUploadModal from "../../../modals/ChatUploadModal";
import ChatInboxHeader from "../../../containers/ChatContainer/ChatInboxHeader";
import ChatBubble from "../../../containers/ChatContainer/ChatBubble";
import moment from "moment";


const ChatInbox = ({navigation, socket_connection, addMessageChatReducer,messageLoading,
                       getChatMessagesWithUserId, getChatMessages, messages, route
                   }) => {

    const scrollRef = useRef();

    const forceUpdate = useReducer(bool => !bool)[1];

    const {id, title, avatar, users, dialogue, is_group} = route.params;
    const [isKeyboardVisible, setKeyboardVisible] = useState(false);
    const [inputHeight, setInputHeight] = useState(0);
    const [messageValue, setMessageValue] = useState('');
    const [selectedFile, setSelectedFile] = useState(null);

    const [bottomHeight, setBottomHeight] = useState(SCREEN_HEIGHT - 30);

    const [operationModal, setOperationModal] = useState(false);


    useEffect(() => {
        if (selectedFile) {
            let formData = new FormData();

            users.map(user => (
                formData.append('receivers[]', user)
            ));

            formData.append("file", {
                uri: selectedFile,
                type: 'image/jpg',
                name: 'uploaded_chat_image.jpg'
            });

            formData.append("type", "file");
            formData.append("chat_type", is_group ? 'group' : 'private');

            if (dialogue) {
                formData.append("dialogue_id", dialogue);
            }

            chatFileSend(formData, response => {
                let message = response.data.data;

                message.own = true;

                addMessageChatReducer(response.data.data);

                setSelectedFile(null);

                forceUpdate();
            }, error => {
                console.log(error)
            })
        }
    }, [selectedFile]);


    useEffect(() => {
        if (isKeyboardVisible) {
            setBottomHeight(SCREEN_HEIGHT - 310);
        } else {
            setBottomHeight(SCREEN_HEIGHT - 30);
        }
    }, [isKeyboardVisible]);

    useEffect(() => {
        if (dialogue) {
            getChatMessages(dialogue);
        } else {
            getChatMessagesWithUserId(id);
        }

        if (socket_connection.socket) {
            socket_connection.socket.on('new message', response => {
                if (parseInt(response.created_by) !== parseInt(getDeviceUser().user_id) &&
                    dialogue === response.dialogue) {
                    response.own = false;
                    addMessageChatReducer(response);
                    forceUpdate();
                }
            });
        }

        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                setKeyboardVisible(true); // or some other action
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setKeyboardVisible(false); // or some other action
            }
        );

        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };

    }, []);

    const refresh = () => {
        getChatMessages(dialogue);
    };

    const sendMessage = () => {
        if (messageValue.length < 1) {
            return "";
        }
        // Keyboard.dismiss();
        setMessageValue('');
        // setInputHeight(0);


        addMessageChatReducer({
            _id: uuidv4(),
            body: messageValue,
            created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
            created_by: getDeviceUser().user_id,
            dialogue: dialogue,
            is_deleted: 0,
            own: true,
            type: "text",
            updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
        });

        forceUpdate();

        let data = {
            receivers: users,
            message: messageValue,
            type: 'text',
            chat_type: is_group ? 'group' : 'private'
        };

        if (dialogue) {
            data.dialogue_id = dialogue;
        }

        chatMessageSend(data, response => {
            console.log(response);
        }, error => {
            console.log(error);
        })
    };

    const openOptions = () => {
        setOperationModal(true);
    };

    const handleOpenGallery = () => {
        setOperationModal(false);

        ImagePicker.openPicker({
            cropping: false
        }).then(image => {
            navigation.navigate('ChatGroupImageCropper', {setAvatar: setSelectedFile, image: image});
        });
    };

    const handleOpenCamera = () => {
        setOperationModal(false);

        ImagePicker.openCamera({
            compressImageQuality: 0.5,
            cropping: false
        }).then(image => {
            navigation.navigate('ChatGroupImageCropper', {setAvatar: setSelectedFile, image: image});
        });
    };

    const handleOpenDocument = async () => {
        setOperationModal(false);

        try {
            const res = await DocumentPicker.pick();
            let formData = new FormData();

            users.map(user => (
                formData.append('receivers[]', user)
            ));

            formData.append("file", {
                uri: res.uri,
                type: res.type,
                name: res.name
            });

            formData.append("type", "file");
            formData.append("chat_type", is_group ? 'group' : 'private');

            if (dialogue) {
                formData.append("dialogue_id", dialogue);
            }

            chatFileSend(formData, response => {
                let message = response.data.data;

                message.own = true;

                addMessageChatReducer(response.data.data);
                forceUpdate();
            }, error => {
                console.log(error)
            });
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    };

    return (
        <View style={styles.content}>
            <ChatInboxHeader dialogue={dialogue} avatar={avatar}
                             is_group={is_group} title={title} navigation={navigation}
            />
            <ScrollView keyboardShouldPersistTaps={'always'}>
                <View style={concatObjects(styles.messagesContainer, {height: bottomHeight})}>
                    <FlatList
                        ref={scrollRef}
                        inverted={-1}
                        style={styles.messages}
                        data={messages}
                        renderItem={({item}) => <MessageItem message={item}/>}
                        contentContainerStyle={{flexGrow: 1, paddingTop: 20}}
                        ListEmptyComponent={<ListItemEmpty loading={false} text="No message"/>}
                        keyExtractor={item => `${item._id}`}
                        onRefresh={refresh}
                        refreshing={messageLoading}
                    />
                </View>
                <ChatBubble inputHeight={inputHeight} messageValue={messageValue}
                            openOptions={openOptions} sendMessage={sendMessage}
                            setInputHeight={setInputHeight} setMessageValue={setMessageValue}
                />
            </ScrollView>
            <ChatUploadModal
                handleOpenCamera={handleOpenCamera} handleOpenDocument={handleOpenDocument}
                handleOpenGallery={handleOpenGallery} operationModal={operationModal}
                setOperationModal={setOperationModal}
            />
        </View>
    );
};

const mapStateToProps = ({lang, chatReducer, socket_connection}) => {
    return {
        lang,
        messages: chatReducer.messageList,
        messageLoading: chatReducer.messageFetching,
        currentPage: chatReducer.currentPage,
        totalPage: chatReducer.totalPage,
        socket_connection: socket_connection
    };
};

const mapDispatchToProps = {
    getChatMessagesWithUserId,
    getChatMessages,
    addMessageChatReducer
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ChatInbox);
