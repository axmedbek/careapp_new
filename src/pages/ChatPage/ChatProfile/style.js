import {StyleSheet} from "react-native";
import {BACK_ICON_COLOR, BLACK_COLOR, LIGHT_COLOR} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";

const styles = StyleSheet.create({
    backIcon: {
        fontSize: 35,
        position: 'absolute',
        left: 10,
        top: 10
    },
    groupName: {
        paddingTop: 0,
        padding: 10,
        fontSize: 18,
        color: BLACK_COLOR,
        fontWeight: 'bold'
    },
    label: {
        padding: 10,
        paddingBottom: 4,
        fontSize: 18,
        color: LIGHT_COLOR,
        fontStyle: 'italic'
    },
    chatContainer: {
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10
    },
    chatContent: {
        marginLeft: 10,
        width: '90%',
        flexDirection: 'row'
    },
    chatHeader: {
        width: '100%',
        paddingLeft: 10,
        paddingTop: 6
    },
    chatDesc: {
        paddingTop: 2,
        color: BACK_ICON_COLOR,
        fontSize: 16
    },
    chatTitle: {
        width: '65%',
        fontSize: 17,
        fontWeight: 'bold',
        color: increase_brightness(BLACK_COLOR,10)
    },
    chatImage: {
        width: 60,
        height: 60,
        borderRadius: 30,
        marginTop: 2
    },
    editBtn: {
        position: 'absolute',
        right: 2,
        width: 50,
        height: 50,
        margin: 15,
        backgroundColor: 'white',
        borderRadius: 25,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    editBtnTxt: {
        fontWeight: 'bold',
        fontSize: 24
    }
});

export default styles;
