import React, {useEffect} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from "react-native";
import styles from "./style";
import BackButton from "../../../components/buttons/BackButton";
import {getChatDialogue} from "../../../actions/chat.action";
import {connect} from "react-redux";
import DataLoadingComponent from "../../../components/loadings/DataLoadingComponent";
import {getDeviceUser} from "../../../config/caching/actions/setting";
import Icon from "react-native-vector-icons/AntDesign";

const ChatProfile = ({route, navigation, getChatDialogue, dialogueItem, fetching}) => {
    const {dialogue} = route.params;

    useEffect(() => {
        getChatDialogue(dialogue);
    }, []);

    console.log(dialogueItem);

    return (
        <View style={{flex: 1}}>
            {
                dialogueItem &&
                <>
                    <Image
                        source={{uri: dialogueItem.image ? dialogueItem.image : 'http://placehold.it/250x250'}}
                        style={{width: '100%', height: 250, resizeMode: "cover"}}/>

                    <View style={{paddingLeft: 10}}>
                        <Text style={styles.label}>Title</Text>
                        {
                            dialogueItem.title
                                ? <Text style={styles.groupName}>{dialogueItem.title ? dialogueItem.title : 'Group'}</Text>
                                : dialogueItem.users.map(user => {
                                    if (getDeviceUser().user_id !== user.id) {
                                        return (
                                            <Text style={styles.groupName}>{user.firstname} {user.lastname}</Text>
                                        )
                                    }
                                })
                        }
                        {
                            dialogueItem.title &&
                            <Text style={styles.label}>Users</Text>
                        }
                    </View>
                    {
                        dialogueItem.title &&
                        <ScrollView style={{paddingLeft: 8}}>
                            {
                                dialogueItem.users.map(item => {
                                    let userAvatar = `https://ui-avatars.com/api/?name=${item.firstname} ${item.lastname}&background=0D8ABC&color=fff&bold=true`;
                                    return (
                                        <TouchableOpacity style={styles.chatContainer}
                                                          onPress={() => navigation.navigate('ChatInbox', {
                                                              id: item.id,
                                                              title: item.firstname + " " + item.lastname,
                                                              avatar: userAvatar,
                                                              users: [getDeviceUser().user_id, item.id],
                                                              dialogue: 0
                                                          })}>
                                            <View style={styles.chatContent}>
                                                <Image
                                                    source={{uri: userAvatar}}
                                                    style={styles.chatImage}/>
                                                <View style={styles.chatHeader}>
                                                    <Text style={styles.chatTitle}>{item.firstname} {item.lastname}</Text>
                                                    <Text style={styles.chatDesc}>{item.type}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                    }
                    <BackButton handleSignIn={() => navigation.goBack()}/>
                    {
                        dialogueItem.type === "group" &&
                        <TouchableOpacity onPress={() => alert("coming soon")} style={styles.editBtn}>
                            <Icon style={styles.editBtnTxt} name={"edit"}/>
                        </TouchableOpacity>
                    }
                </>
            }
            <DataLoadingComponent loading={fetching}/>
        </View>
    );
};

const mapStateToProps = ({lang, chatReducer}) => {
    return {
        lang,
        dialogueItem: chatReducer.dialogue,
        fetching: chatReducer.dialogueFetching,
    };
};

const mapDispatchToProps = {
    getChatDialogue
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatProfile);
