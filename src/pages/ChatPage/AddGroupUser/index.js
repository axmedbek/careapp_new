import React, {useEffect, useState} from 'react';
import {FlatList, TextInput, TouchableOpacity, View} from "react-native";
import styles from "./style";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import {getAllContacts} from "../../../actions/chat.action";
import {connect} from "react-redux";
import ListItemEmpty from "../../../containers/ListItemEmpty";
import AddGroupUserItem from "./AddGroupUserItem";

let timeout = null;

const AddGroupUser = ({route, contacts, fetching, getAllContacts, navigation}) => {
    const {selectedUsers, updateSelectedUsers} = route.params;

    const [searchValue, setSearchValue] = useState('');
    const [selectedGroupUsers, setSelectedGroupUsers] = useState(selectedUsers);


    useEffect(() => {
        getAllContacts(searchValue);
    }, []);

    const refresh = () => {
        getAllContacts(searchValue);
    };

    const doSearch = (value) => {
        setSearchValue(value);
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            //search function
            getAllContacts(value);
        }, 1000);
    };

    const handleSelectUser = (user, is_added) => {
        if (is_added) {
            setSelectedGroupUsers(selectedGroupUsers.filter(item => item.id !== user.id));
            updateSelectedUsers(selectedGroupUsers.filter(item => item.id !== user.id));
        } else {
            setSelectedGroupUsers([...selectedGroupUsers, user]);
            updateSelectedUsers([...selectedGroupUsers, user]);
        }
    };

    return (
        <View>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <MaterialIcons style={styles.backIcon} name={"arrow-back"}/>
                </TouchableOpacity>
                <TextInput style={styles.input}
                           value={searchValue}
                           onChangeText={e => doSearch(e)}
                           placeholder={"Search user"}/>
                <TouchableOpacity style={styles.addContainer} onPress={() => navigation.goBack()}>
                    <MaterialIcons style={styles.addIcon} name={"add"}/>
                </TouchableOpacity>
            </View>
            <FlatList
                data={contacts}
                renderItem={({item}) => <AddGroupUserItem
                    item={item}
                    handleSelectUser={handleSelectUser}
                    selectedGroupUsers={selectedGroupUsers}/>}
                contentContainerStyle={{flex: 1, paddingBottom: 30}}
                ListEmptyComponent={<ListItemEmpty loading={false} text="No contact"/>}
                keyExtractor={item => `${item._id}`}
                ItemSeparatorComponent={() => <View
                    style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: '#D8D8D8'
                    }}>
                </View>}
                onRefresh={refresh}
                refreshing={fetching}
            />
        </View>
    );
};

const mapStateToProps = ({lang, chatReducer}) => {
    return {
        lang,
        contacts: chatReducer.contacts,
        fetching: chatReducer.contactFetching
    };
};

const mapDispatchToProps = {
    getAllContacts
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AddGroupUser);
