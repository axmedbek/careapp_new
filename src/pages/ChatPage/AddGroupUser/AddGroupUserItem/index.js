import React from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import {checkSelectedGroupUser, concatObjects, increase_brightness} from "../../../../helpers/standard.helper";
import styles from "../style";
import {MAIN_COLOR} from "../../../../constants/colors";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const AddGroupUserItem = ({handleSelectUser, selectedGroupUsers, item}) => {

    let userAvatar = item.avatar.small;

    return (
        <TouchableOpacity style={concatObjects(styles.listItem,
            checkSelectedGroupUser(selectedGroupUsers, item.id) ? {
                backgroundColor: increase_brightness(MAIN_COLOR, 80)
            } : {})}
                          onPress={() => handleSelectUser(item, checkSelectedGroupUser(selectedGroupUsers, item.id))}>
            <Image source={{uri: userAvatar}} style={styles.image}/>
            <View style={styles.itemContent}>
                <Text style={styles.userTitle}>{item.firstname} {item.lastname}</Text>
                <Text style={styles.userDesc}>{item.type}</Text>
            </View>
            {
                checkSelectedGroupUser(selectedGroupUsers, item.id) &&
                <MaterialIcons name={"check"} style={styles.checkIcon}/>
            }
        </TouchableOpacity>
    )
};

export default AddGroupUserItem;
