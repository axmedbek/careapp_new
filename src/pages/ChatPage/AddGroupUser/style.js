import {StyleSheet} from "react-native";
import {BACK_ICON_COLOR, DIVIDER_COLOR, LIGHT_COLOR, MAIN_COLOR} from "../../../constants/colors";

const styles = StyleSheet.create({
    header: {
        paddingTop: 10,
        flexDirection: 'row',
        backgroundColor: MAIN_COLOR,
        height: 80
    },
    backIcon: {
        fontSize: 28,
        color: 'white',
        marginTop: 15,
        marginLeft: 10
    },
    input: {
        backgroundColor: 'white',
        height: 50,
        borderRadius: 4,
        width: '60%',
        marginLeft: 10,
        marginTop: 5,
        paddingLeft: 10
    },
    content: {
        marginTop: 20,
        marginLeft: 15,
        marginRight: 15,
        height: '100%'
    },
    addContainer: {
        height: 50,
        width: 50,
        position: 'absolute',
        right: 15,
        top: 15,
        justifyContent: 'center',
        textAlign: 'center',
        borderWidth: 2,
        borderColor: 'white',
        padding: 4,
        borderRadius: 4
    },
    add: {
        fontSize: 20,
        color: 'white'
    },
    addIcon: {
        marginTop: 2,
        fontSize: 35,
        color: 'white',
        textAlign: 'center'
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    listItem: {
        flexDirection: 'row',
        paddingBottom: 10,
        justifyContent: 'center',
        paddingTop: 15,
        paddingLeft: 10
    },
    itemContent: {
        width: '80%',
        marginTop: 2,
        marginLeft: 10,
        paddingTop: 2,
        // borderBottomWidth: 1,
        // borderBottomColor: DIVIDER_COLOR
    },
    userTitle: {
        fontSize: 15
    },
    userDesc: {
        marginTop: 2,
        color: BACK_ICON_COLOR
    },
    checkIcon: {
        fontSize: 30,
        position: 'absolute',
        right: 30,
        top: 20,
        color: 'green'
    }
});

export default styles;
