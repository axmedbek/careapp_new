import React, {useEffect, useState, useRef} from 'react';
import AuthenticatedScreenHOC from "../../HOCs/AuthenticatedScreenHOC";
import {
    concatObjects,
    convertToSelectDataForValue, errorMessage, getCategoriesData,
    stringConvertDate,
    successMessage
} from "../../helpers/standard.helper";
import SimplePicker from "../../components/selects/SimpleSelect";
import {connect} from "react-redux";
import {statuses, types} from "../../constants/data";
import {View, ScrollView} from "react-native";
import StandardInputComponent from "../../components/inputs/StandardInputComponent";
import {Textarea, Toast} from "native-base";
import {BACK_ICON_COLOR, DIVIDER_COLOR, LIGHT_COLOR, MAIN_COLOR, RED_COLOR} from "../../constants/colors";
import StandardButton from "../../components/buttons/StandardButton";
import SelectableOptionItem from "../../components/selects/SelectableOptions/SelectableOptionItem";
import SelectableOptions from "../../components/selects/SelectableOptions";
import {getMinCases} from "../../actions/case.action";
import {getCategories, getCitizens, getEmployee, getModerators, getPartners} from "../../actions/common.action";
import SelectableCategoryItem from "../../components/selects/SelectableOptions/SelectableCategoryItem";
import SimpleDateTimePicker from "../../components/datepickers/SimpleDateTimePicker";
import {createCalendarEvent, getCalendarInfo} from "../../services/calendar.service";
import moment from "moment";
import {getEvents} from "../../actions/calendar.action";
import DataLoadingComponent from "../../components/loadings/DataLoadingComponent";
import MultiSelect from "../../components/selects/MultiSelect";
import AntDesign from "react-native-vector-icons/AntDesign";
import {getDeviceUser} from "../../config/caching/actions/setting";

const CreateEvent = ({
                         route, navigation, lang, cases, caseLoading,
                         getMinCases, categories, getCategories,
                         categoryLoading, moderatorList, getModerators,
                         employeeList, partnerList, citizenList,
                         getEmployee, getCitizens, getPartners, getEvents
                     }) => {


    const scrollRef = useRef();
    const {event, dateString} = route.params;
    const selectedDate = stringConvertDate(dateString);

    const [selectedType, setSelectedType] = useState("event");
    const [selectedTypeError, setSelectedTypeError] = useState(false);

    const [selectedStatus, setSelectedStatus] = useState(null);

    const [title, setTitle] = useState('');
    const [titleError, setTitleError] = useState(false);

    const [desc, setDesc] = useState('');
    const [descError, setDescError] = useState(false);

    const [selectedCase, setSelectedCase] = useState(null);
    const [selectedCaseError, setSelectedCaseError] = useState(false);

    const [category, setCategory] = useState(null);
    const [categoryError, setCategoryError] = useState(false);

    const [moderators, setModerators] = useState([]);
    const [employee, setEmployee] = useState([]);
    const [partners, setPartners] = useState([]);
    const [citizens, setCitizens] = useState([]);

    const [startDate, setStartDate] = useState(event ? moment(event.date_raw) : new Date());
    const [startTime, setStartTime] = useState(event ? moment(event.date_time, "HH:mm") : new Date());

    const [endDate, setEndDate] = useState(event ? moment(event.date_deadline) : new Date());
    const [endTime, setEndTime] = useState(event ? moment(event.deadline_time, "HH:mm") : new Date());

    const [loading, setLoading] = useState(false);


    useEffect(() => {
        if (event) {
            setLoading(true);
            getCalendarInfo(event.id, response => {
                setSelectedType(response.data.data.type);
                setSelectedCase(response.data.data.case);

                setModerators(response.data.data.moderator);
                setCitizens(response.data.data.citizen);
                setEmployee(response.data.data.employee);
                setPartners(response.data.data.contact_person);

                setTitle(response.data.data.title);
                setDesc(response.data.data.description);
                setSelectedStatus(response.data.data.status.value);
                setCategory(response.data.data.category.id);
                setStartDate(moment(response.data.data.date_raw));
                setStartTime(moment(response.data.data.date_time, "HH:mm"));
                setEndDate(moment(response.data.data.date_deadline));
                setEndTime(moment(response.data.data.deadline_time, "HH:mm"));

                setLoading(false);
            }, error => {
                console.log(error.response);
                alert("Calendar Info Problem");
                setLoading(false);
            })
        }


        getMinCases();
        getCategories();
        getModerators();
        getEmployee();
        getCitizens();
        getPartners();
    }, []);


    // console.log(event);

    const handleSubmitEvent = () => {
        let validation = false;
        if (title.length < 3) {
            setTitleError(true);
            validation = true;
        }
        else {
            setTitleError(false);
        }

        if (desc.length < 1) {
            setDescError(true);
            validation = true;
        }
        else {
            setDescError(false);
        }

        if (!selectedType) {
            setSelectedTypeError(true);
            validation = true;
        }
        else {
            setSelectedTypeError(false);
        }

        if (!selectedCase) {
            setSelectedCaseError(true);
            validation = true;
        } else {
            setSelectedCaseError(false);
        }

        if (!category) {
            setCategoryError(true);
            validation = true;
        } else {
            setCategoryError(false);
        }

        if (!validation) {
            let data = {};

            if (event) {
                data.id = event.id;
            }

            data.type = selectedType;
            data.title = title;
            data.description = desc;
            data.category = category;
            data.case = selectedCase;
            data.status = selectedStatus;

            data.moderator = moderators;
            data.citizen = citizens;
            data.employee = employee;
            data.partner = partners;

            data.startdate = moment(startDate).format("YYYY-MM-DD");
            data.starttime = moment(startTime).format("HH:mm");

            data.enddate = moment(endDate).format("YYYY-MM-DD");
            data.endtime = moment(endTime).format("HH:mm");

            data.timezone = 'da';

            createCalendarEvent(data, response => {
                successMessage(response.data.description);
                navigation.navigate('Calendar');
                getEvents();
            }, error => {
                console.log(error.response);
                errorMessage("Oops.Event couldn't add.");
            })
        } else {
            scrollRef.current?.scrollTo({
                y: 0,
                animated: true
            });
        }
    };

    return (
        <AuthenticatedScreenHOC title={event ? "Edit Event" : "Create Event"} navigation={navigation} backIcon={true}>
            <DataLoadingComponent loading={loading}/>
            <ScrollView ref={scrollRef} style={{paddingLeft: 10, marginTop: 10, backgroundColor: 'white'}}>
                <SimplePicker data={types}
                              placeHolder={lang.messages.common.choose_type}
                              selectedValue={selectedType}
                              setSelectedValue={setSelectedType}
                              hasError={selectedTypeError}
                              style={{width: '100%'}}
                />
                <StandardInputComponent
                    placeholder={"Skriv titel her"}
                    inputStyle={{width: '100%'}}
                    setValue={setTitle}
                    value={title}
                    setValueError={setTitleError}
                    valueError={titleError}
                />
                <Textarea
                    style={concatObjects({
                            marginLeft: 2,
                            marginRight: 10,
                            borderRadius: 4,
                            marginTop: 20,
                            marginBottom: 15
                        },
                        descError ? {borderWidth: 2, borderColor: RED_COLOR} : {
                            borderWidth: 1,
                            borderColor: DIVIDER_COLOR
                        })}
                    placeholderTextColor={BACK_ICON_COLOR}
                    rowSpan={5}
                    placeholder="Skriv beskrivelse her"
                    value={desc}
                    onChangeText={e => setDesc(e)}
                />
                <SelectableOptions
                    label={"Sag"}
                    options={cases}
                    loading={caseLoading}
                    selectedOption={selectedCase}
                    onOptionSelected={value => setSelectedCase(value)}
                    onOptionUnselected={() => setSelectedCase(null)}
                    renderItem={(item, isSelected) => <SelectableOptionItem item={item} isSelected={isSelected}/>}
                    hasError={selectedCaseError}
                />
                {
                    categories.length > 0 &&
                    <SelectableOptions
                        label={"Kategori"}
                        options={getCategoriesData(categories)}
                        loading={categoryLoading}
                        selectedOption={category}
                        onOptionSelected={value => setCategory(value)}
                        onOptionUnselected={() => setCategory(null)}
                        renderItem={(item, isSelected) => <SelectableCategoryItem item={item} isSelected={isSelected}/>}
                        hasError={categoryError}
                    />
                }
                {
                    getDeviceUser().type !== "employee" &&
                    <MultiSelect
                        title={'Moderators'}
                        data={moderatorList}
                        ids={moderators}
                        select={data => setModerators(data)}
                    />
                }
                <MultiSelect
                    title={'Medarbejdere'}
                    data={employeeList}
                    ids={employee}
                    select={data => setEmployee(data)}
                />
                <MultiSelect
                    title={'Samarbejdspartner'}
                    data={partnerList}
                    ids={partners}
                    setSelect={partners}
                    select={data => setPartners(data)}
                />
                <MultiSelect
                    title={'Borgere'}
                    data={citizenList}
                    ids={citizens}
                    setSelect={citizens}
                    select={data => setCitizens(data)}
                />

                {
                    selectedType === "todo" &&
                    <SimplePicker data={convertToSelectDataForValue(statuses)}
                                  placeHolder={"Choose status"}
                                  selectedValue={selectedStatus}
                                  setSelectedValue={setSelectedStatus}
                                  hasError={false}
                                  style={{width: '100%'}}
                    />
                }

                <View style={{flexDirection: 'row', width: '98%', marginTop: 10}}>
                    <AntDesign name="calendar" size={30} color={LIGHT_COLOR} style={{marginTop: 12, width: '10%'}}/>
                    <SimpleDateTimePicker date={startDate} setDate={setStartDate} label={"Start dato"}
                                          style={{width: '43%'}} format={"DD-MM-YYYY"}/>
                    <SimpleDateTimePicker date={startTime} setDate={setStartTime} label={"Tid"}
                                          mode={"time"} format={"hh:mm"} style={{width: '44%', marginLeft: 10}}/>
                </View>

                <View style={{flexDirection: 'row', width: '98%'}}>
                    <AntDesign name="calendar" size={30} color={LIGHT_COLOR} style={{marginTop: 12, width: '10%'}}/>
                    <SimpleDateTimePicker date={endDate} setDate={setEndDate} label={"Slut dato"}
                                          minimumDate={new Date(startDate)}
                                          style={{width: '43%'}} format={"DD-MM-YYYY"}/>
                    <SimpleDateTimePicker date={endTime} setDate={setEndTime} label={"Tid"}
                                          minimumDate={new Date(endTime)}
                                          mode={"time"} format={"hh:mm"} style={{width: '44%', marginLeft: 10}}/>
                </View>

                <StandardButton text={"Gem"} handleButton={handleSubmitEvent}
                                style={{width: '97%', marginLeft: 4, marginBottom: 20}}/>
                {
                    event &&
                    <StandardButton text={"Notes"}
                                    handleButton={() => navigation.navigate('EventNote', {
                                        id: event.id,
                                        title: `${event.title} Notes`
                                    })}
                                    textStyle={{color: MAIN_COLOR}}
                                    style={{
                                        width: '97%', marginLeft: 4,
                                        marginBottom: 30, backgroundColor: 'white',
                                        borderWidth: 2, borderColor: MAIN_COLOR
                                    }}/>
                }
            </ScrollView>
        </AuthenticatedScreenHOC>
    );
};


const mapStateToProps = ({lang, caseReducer, commonReducer}) => {
    return {
        lang,
        cases: caseReducer.minCases,
        caseLoading: caseReducer.fetching,
        categoryLoading: commonReducer.fetching,
        categories: commonReducer.categories,
        moderatorList: commonReducer.moderators,
        employeeList: commonReducer.employee,
        partnerList: commonReducer.partners,
        citizenList: commonReducer.citizens,
    };
};

const mapDispatchToProps = {
    getMinCases,
    getCategories,
    getModerators,
    getEmployee,
    getPartners,
    getCitizens,
    getEvents
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CreateEvent);
