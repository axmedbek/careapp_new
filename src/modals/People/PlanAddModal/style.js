import {StyleSheet} from 'react-native';
import {SCREEN_WIDTH} from "../../../constants/setting";

const styles = StyleSheet.create({
    container: {
        width: 350,
        height: 'auto',
        padding: 10
    }
});

export default styles;
