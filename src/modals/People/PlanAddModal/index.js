import React, {useState} from 'react';
import {View} from "react-native";
import styles from './style';
import StandardInputComponent from "../../../components/inputs/StandardInputComponent";
import SimplePicker from "../../../components/selects/SimpleSelect";
import StandardButton from "../../../components/buttons/StandardButton";
import {connect} from "react-redux";
import {
    convertGoalsForSelect,
    convertPlanForSelect,
    getToastMessage
} from "../../../helpers/standard.helper";
import {createPlan} from "../../../services/case.service";
import {getCasePlans} from "../../../actions/case.action";

const PlanAddModal = ({ case_id,lang,goals,trading_plans_statuses,getCasePlans,setLoading,setModal }) => {
    const [title, setTitle] = useState('');
    const [titleError, setTitleError] = useState(false);
    const [relationship, setRelationship] = useState(null);
    const [relationshipError, setRelationshipError] = useState(false);
    const [status, setStatus] = useState(null);
    const [statusError, setStatusError] = useState(false);


    const handlePlanAdd = () => {
        if (!relationship) {
            setRelationshipError(true);
        }
        else {
            setRelationshipError(false);
        }

        if (!status) {
            setStatusError(true);
        }
        else {
            setStatusError(false);
        }

        if (title.length > 0 && status && relationship) {
            setLoading(true);
            createPlan(case_id,relationship,title,status,(response) => {
                getToastMessage("Successfully added","success");
                setLoading(false);
                setModal(false);
                getCasePlans(case_id);
            },() => {
                getToastMessage("Oops.Something went wrong!");
                setLoading(false);
            })
        }
    };

    return (
        <View style={styles.container}>
            <StandardInputComponent placeholder={lang.messages.common.description}
                                    value={title}
                                    setValue={setTitle}
                                    labelTxt={lang.messages.common.description}
                                    valueError={titleError}
                                    setValueError={setTitleError}
                                    containerInputStyle={{marginLeft: 8,width: 315}}
                                    inputStyle={{width: 315}}
            />

            <SimplePicker placeHolder={lang.messages.common.question_ratio_choose}
                          data={convertGoalsForSelect(goals)}
                          labelText={lang.messages.common.question_ratio}
                          selectedValue={relationship}
                          setSelectedValue={setRelationship}
                          hasError={relationshipError}/>

            <SimplePicker placeHolder={lang.messages.common.choose_status}
                          data={convertGoalsForSelect(trading_plans_statuses)}
                          labelText={lang.messages.common.status}
                          selectedValue={status}
                          setSelectedValue={setStatus}
                          hasError={statusError}/>
            <StandardButton text={"Add"} handleButton={handlePlanAdd}/>
        </View>
    );
};


const mapStateToProps = ({caseReducer, lang}) => {
    return {
        lang,
        trading_plans_statuses: caseReducer.trading_plans_statuses,
        goals: caseReducer.goals
    };
};

const mapDispatchToProps = {
    getCasePlans
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PlanAddModal);
