import React from 'react';
import CoreModal from "../../../components/modals/CoreModal";
import {View, Text, Image, TouchableOpacity} from "react-native";
import {REGULAR_FONT, SCREEN_HEIGHT, SCREEN_WIDTH} from "../../../constants/setting";
import {BACK_ICON_COLOR, BLUE_COLOR, RED_COLOR} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";

const ImagePreviewModal = ({modalLoading, setModalLoading, image,handleDeleteImage,continueEditingImage}) => {
    return (
        <CoreModal visible={modalLoading} hideModal={() => setModalLoading(false)}>
            <View style={{
                paddingTop: 30,
                paddingBottom: 10,
                width: SCREEN_WIDTH,
                height: SCREEN_HEIGHT,
                alignItems: 'center',
                backgroundColor: increase_brightness(BACK_ICON_COLOR,60)
            }}>
                <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity onPress={() => handleDeleteImage(image)}
                                      style={{backgroundColor: RED_COLOR,width: 60,marginRight: 10,borderRadius: 4,padding: 4}}>
                        <Text style={{textAlign: 'center',color: 'white',fontFamily: REGULAR_FONT}}>Delete</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity onPress={() => console.log("---")}*/}
                    {/*                  style={{backgroundColor: BLUE_COLOR,width: 120,borderRadius: 4,padding: 4}}>*/}
                    {/*    <Text style={{textAlign: 'center',color: 'white',fontFamily: REGULAR_FONT}}>Continue editing</Text>*/}
                    {/*</TouchableOpacity>*/}
                </View>
                <View style={{marginTop: 6,marginBottom: 4,width: SCREEN_WIDTH,height: 1,
                    backgroundColor: increase_brightness(BACK_ICON_COLOR,50)}}/>
                <Image source={{uri: image}} style={{width: '100%', height: SCREEN_HEIGHT - 70,
                    resizeMode: 'contain'}}/>
            </View>
        </CoreModal>
    );
};

export default ImagePreviewModal;
