import React, {useEffect, useState} from 'react';
import {View} from "react-native";
import Text from "../../../components/text/Text";
import {SCREEN_WIDTH} from "../../../constants/setting";
import {Textarea} from "native-base";
import StandardButton from "../../../components/buttons/StandardButton";
import Icon from "react-native-vector-icons/AntDesign";
import {MAIN_COLOR, RED_COLOR} from "../../../constants/colors";

const SurveyCaseModal = ({handleSurveyUpdate, surveys}) => {

    const [currentIndex, setCurrentIndex] = useState(0);
    const [currentSurvey, setCurrentSurvey] = useState(surveys[currentIndex]);
    const [btnText, setBtnText] = useState(surveys.length === 1 ? "Finish" : "Next");
    const [editor, setEditor] = useState("");
    const [data, setData] = useState([]);

    useEffect(() => {
        data[currentIndex] = {question_id: surveys[currentIndex].id,answer: editor,plan_id: surveys[currentIndex].plan_id};
        setData(data);
    },[editor]);

    const handleNext = () => {
        if (surveys[currentIndex + 1]) {
            setCurrentSurvey(surveys[currentIndex + 1]);
            setCurrentIndex(currentIndex + 1);

            setEditor(data[currentIndex + 1] ? data[currentIndex + 1].answer : "");
            if ((currentIndex+2) === surveys.length) {
                setBtnText("Finish")
            }
        } else {
            handleSurveyUpdate(data);
        }
    };

    const handleBack = () => {
        if (surveys[currentIndex - 1]) {
            setCurrentSurvey(surveys[currentIndex - 1]);
            setCurrentIndex(currentIndex - 1);
            setEditor(data[currentIndex - 1] ? data[currentIndex - 1].answer : "");
            if ((currentIndex+2) !== surveys.length) {
                setBtnText("Next")
            }
        }
    };

    return (
        <View style={{width: SCREEN_WIDTH}}>
            <View style={{flexDirection: 'row'}}>
                <Text style={{
                    backgroundColor: MAIN_COLOR,
                    marginTop: 15,
                    color: 'white',
                    borderRadius: 4,
                    width: 20, height: 25,
                    justifyContent: 'center', textAlign: 'center'
                }}>{currentIndex + 1}</Text>
                <Text style={{padding: 10,marginRight: 4}}>{currentSurvey.title}</Text>
            </View>
            <Text style={{marginLeft: 10,padding: 6,color: RED_COLOR}}>Use min 4 word</Text>
            <Textarea rowSpan={10} bordered underline style={{margin: 10}}
                      value={editor}
                      onChangeText={e => setEditor(e)}
            />
            <View style={{flexDirection: 'row',justifyContent: 'center'}}>
                {
                    currentIndex > 0 &&
                    <StandardButton
                        handleButton={handleBack}
                        text={<Text style={{color: 'white', fontWeight: 'bold', letterSpacing: 0.8}}><Icon
                            name={"caretleft"} style={{fontSize: 16, paddingLeft: 20}}/> Back</Text>}/>
                }
                <StandardButton
                    disabled={editor.split(" ").length < 4}
                    handleButton={handleNext}
                    text={<Text style={{color: 'white', fontWeight: 'bold', letterSpacing: 0.8}}>{btnText} <Icon
                        name={"caretright"} style={{fontSize: 16, paddingLeft: 20}}/></Text>}/>
            </View>
        </View>
    );
};

export default SurveyCaseModal;
