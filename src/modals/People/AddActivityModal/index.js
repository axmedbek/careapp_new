import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import {BOLD_FONT, REGULAR_FONT, SCREEN_WIDTH} from "../../../constants/setting";
import {BACK_ICON_COLOR, MAIN_COLOR} from "../../../constants/colors";
import {increase_brightness} from "../../../helpers/standard.helper";
import NewFeatureIcon from "../../../components/icons/NewFeatureIcon";
import CoreModal from "../../../components/modals/CoreModal";
import StandardButton from "../../../components/buttons/StandardButton";

const AddActivityModal = ({modalLoading, setModalLoading, handleOpenCamera, handleOpenGallery, handleGetDirectlyReceipt}) => {
    return (
        <CoreModal visible={modalLoading} hideModal={() => setModalLoading(false)}>
            <View style={{paddingTop: 30, paddingBottom: 10, width: SCREEN_WIDTH, alignItems: 'center'}}>
                <Text style={{
                    fontFamily: REGULAR_FONT, color: BACK_ICON_COLOR,
                    fontSize: 16, paddingBottom: 10, fontStyle: 'italic'
                }}>
                    Choose a method to upload images
                </Text>
                <StandardButton text={<Text>Get receipt directly <NewFeatureIcon /></Text>} style={{width: '100%',borderRadius: 0}}
                                handleButton={handleGetDirectlyReceipt}/>
                <StandardButton text={"Open camera"} style={{width: '100%',borderRadius: 0}}
                                handleButton={handleOpenCamera}/>
                <StandardButton text={"Open gallery"} style={{width: '100%',borderRadius: 0}}
                                handleButton={handleOpenGallery}/>
            </View>
        </CoreModal>
    );
};

export default AddActivityModal;
