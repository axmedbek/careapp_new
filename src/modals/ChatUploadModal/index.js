import React from 'react';
import {Modal, Text, TouchableOpacity, View} from "react-native";
import styles from "../../pages/ChatPage/ChatInbox/style";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {concatObjects} from "../../helpers/standard.helper";

const ChatUploadModal = ({ operationModal,setOperationModal,handleOpenDocument,handleOpenCamera,handleOpenGallery }) => {
    return (
        <Modal
            animationType="slide"
            visible={operationModal}
            onRequestClose={() => setOperationModal(false)}
            closeOnClick={true}
            transparent={true}
        >
            <View style={styles.modal}>
                <View style={styles.modalContent}>
                    <TouchableOpacity style={styles.modalClose} onPress={() => setOperationModal(false)}>
                        <AntDesign name={"close"} style={styles.modalCloseBtn}/>
                    </TouchableOpacity>
                    <View style={{marginLeft: 20, marginBottom: 25}}>
                        <TouchableOpacity style={{flexDirection: 'row'}} onPress={handleOpenDocument}>
                            <MaterialCommunityIcons name={"file-document"} style={concatObjects(styles.chatIcon, {
                                marginBottom: 10
                            })}/>
                            <Text style={styles.chatText}>Upload document</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flexDirection: 'row'}} onPress={handleOpenCamera}>
                            <MaterialCommunityIcons name={"camera"} style={styles.chatIcon}/>
                            <Text style={styles.chatText}>Upload with camera</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flexDirection: 'row', marginTop: 10}}
                                          onPress={handleOpenGallery}>
                            <MaterialCommunityIcons name={"folder-image"} style={styles.chatIcon}/>
                            <Text style={styles.chatText}>Upload from gallery</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    );
};

export default ChatUploadModal;
