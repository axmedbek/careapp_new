import axios from "axios";
import {getAccessToken} from "../config/caching/actions/token";

const API_URL = 'http://rcrmapi.wowwer.com/api/v1';


export function httpClient(dispatch, types, url, method = "get", data = null) {
    const config = {
        method: method,
        url: `${API_URL}/${url}`,
        data: data,
        headers: {
            "Authorization": `Bearer ${getAccessToken()}`
        }
    };

    dispatch({
        type: types.pending
    });

    return axios(config)
        .then(response => {
            dispatch({
                type: types.success,
                payload: response
            });
        })
        .catch(error => {
            dispatch({
                type: types.reject,
                payload: error
            });
        })
}

export function httpClientWithoutDispatch(url, method = 'get', data = null) {
    const config = {
        method: method,
        url: `${API_URL}/${url}`,
        data: data,
        headers: {
            "Authorization": `Bearer ${getAccessToken()}`
        }
    };

    return axios(config);
}


export function httpClientWithoutDispatchForm(url, method = 'get', data = null) {
    const config = {
        method: method,
        url: `${API_URL}/${url}`,
        data: data,
        headers: {
            "Authorization": `Bearer ${getAccessToken()}`,
            "Content-Type": "multipart/form-data"
        }
    };

    return axios(config);
}


export function httpClientPostWithoutDispatch(url, data = null) {
    const config = {
        method: 'post',
        url: `${API_URL}/${url}`,
        data: data,
        headers: {
            "Authorization": `Bearer ${getAccessToken()}`
        }
    };

    return axios(config);
}


export function httpClientFileUpload(url, params) {
    return  axios({
        method: 'post',
        url: `${API_URL}/${url}`,
        data: params,
        headers: {
            "Authorization": `Bearer ${getAccessToken()}`,
            "Content-Type": "multipart/form-data"
        },
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
    })
}
