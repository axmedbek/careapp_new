import {getDeviceUser, getLanguage} from "../config/caching/actions/setting";
import {BLACK_COLOR, BLUE_COLOR, MAIN_COLOR, RED_COLOR, YELLOW_COLOR} from "../constants/colors";
import {Alert, Linking, Dimensions, Platform, LayoutAnimation, UIManager, Image} from "react-native";
import React from "react";
import {Toast} from "native-base";
import MultiImageAvatar from "../components/avatars/MultiImageAvatar";


export const getLanguageMessages = () => {
    let messages = {};
    switch (getLanguage()) {
        case 'da':
            messages = Object.assign(
                messages,
                require('../config/lang/i18n/locales/da/translation'),
            );
            break;
        case 'en':
            messages = Object.assign(
                messages,
                require('../config/lang/i18n/locales/en/translation'),
            );
            break;
    }
    return messages;
};

export function isEmptyObject(obj) {
    for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            return false;
        }
    }

    return JSON.stringify(obj) === JSON.stringify({});
}

export function isEmptyAvatar(image) {
    return image === "http://placehold.it/80x80";
}

export function getBottomTitleName(messages, label) {
    return messages[label] ? messages[label]['title'] : label;
}


export function concatObjects(o1, o2, o3) {
    return Object.assign({}, Object.assign({}, o1, o2), o3);
}

export function getToastStyleFromType(type) {
    switch (type) {
        case 'success' : {
            return {color: MAIN_COLOR};
        }
        case 'danger' : {
            return {color: RED_COLOR};
        }
        case 'warning' : {
            return {color: YELLOW_COLOR};
        }
        case 'info' : {
            return {color: BLUE_COLOR}
        }
    }
}


export function increase_brightness(hex, percent) {
    // strip the leading # if it's there
    hex = hex.replace(/^\s*#|\s*$/g, '');

    // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
    if (hex.length === 3) {
        hex = hex.replace(/(.)/g, '$1$1');
    }

    let r = parseInt(hex.substr(0, 2), 16),
        g = parseInt(hex.substr(2, 2), 16),
        b = parseInt(hex.substr(4, 2), 16);

    return '#' +
        ((0 | (1 << 8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
        ((0 | (1 << 8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
        ((0 | (1 << 8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
}

export function convertGoalsForSelect(data) {
    let selectedData = [];
    if (data) {
        data.map(item => {
            if (item.titles.en) {
                selectedData.push({id: item.id, title: item.titles.en})
            }
        });
    }
    return selectedData;
}


export function convertToSelectData(data, hasUserId) {
    let selectedData = [];
    if (data) {
        data.map(item => {
            if (hasUserId) {
                if (item.id) {
                    selectedData.push({id: item.id, title: item.title})
                } else {
                    selectedData.push({id: item.user_id, title: item.title})
                }
            } else {
                selectedData.push({id: item.id, title: item.title})
            }
        });
    }
    return selectedData;
}


export function convertToSelectDataForValue(data) {
    let selectedData = [];
    data.map(item => {
        selectedData.push({id: item.value, title: item.label})
    });
    return selectedData;
}


export const getEventsActiveDate = (events) => {
    return events.filter(item => !item.needHide);
};


export function getToastMessage(message, type = 'danger') {
    Toast.show({
        position: 'top',
        text: message,
        buttonText: "Okey",
        type: type,
        textStyle: {marginLeft: 10},
        duration: 3000
    });
}


export function getSpecificPartFromText(text, len) {
    return text.length > len ? text.substring(0, len) + '...' : text;
}

export function deleteElementFromArray(list, image) {
    let arrayList = [];
    list.map(item => {
        if (item !== image) {
            arrayList.push(item);
        }
    });

    return arrayList;
}


export function subsElementFromArray(list, image, newImage) {
    let arrayList = [];
    list.map(item => {
        if (item !== image) {
            arrayList.push(item);
        } else {
            arrayList.push(newImage);
        }
    });

    return arrayList;
}


export function getOnlyImageUrlsFromActivity(images) {
    let arrayList = [];
    images.map(item => {
        arrayList.push(item.url.replace('medium', 'org'));
    });

    return arrayList;
}


export const callNumber = phone => {
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
        phoneNumber = `telprompt:${phone}`;
    } else {
        phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
        .then(supported => {
            if (!supported) {
                Alert.alert('Phone Warning', "Phone number is not available");
            } else {
                return Linking.openURL(phoneNumber);
            }
        })
        .catch(err => console.log(err));
};


export const extractDatesForCalendar = (events: Array): Object => {
    return events.reduce((result, event) => {
        const datesObj = result;
        if (datesObj[event.date_raw] === undefined || datesObj[event.date_raw].dots.length < 3) {
            datesObj[event.date_raw] = {
                selected: true,
                marked: true,
                selectedColor: event.category.bg_color,
                dots: [
                    ...(datesObj[event.date_raw]?.dots || []),
                    {
                        key: event.category.title,
                        color: event.category.color,
                    },
                ],
            };
        }
        return datesObj;
    }, {});
};

export function stringConvertDate(dateString) {
    if (dateString.length < 8)
        return false;
    let p = dateString.split(/\D/g);
    return [p[2], p[1], p[0]].join("-");
}


export type File = {
    url: string,
    file_name?: string,
    file_size?: string,
    file_type?: string,
    data?: string,
};


export const truncateText = (string: string, length: number = 40): string => {
    if (string) {
        const formattedString = string.trim().replace(/\n/g, " ");

        if (formattedString.length > length) {
            return `${formattedString.substr(0, length)}..`;
        }

        return formattedString;
    }
};


export function startAnimation() {
    if (Platform.OS === 'android') {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
}

export async function setStateWithAnimation(values) {
    if (Platform.OS === 'android') {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    await this.setState(values);
    return true;
}

export function hasSafeArea() {
    const dim = Dimensions.get('window');
    return (
        Platform.OS === 'ios' &&
        (dim.height === 812 ||
            dim.width === 812 ||
            dim.height === 896 ||
            dim.width === 896)
    );
}

export function getTopHeightSafeArea() {
    if (Platform.OS === 'ios') {
        return hasSafeArea() ? 33 : 15;
    }
    return 0;
}

export function screenHeight() {
    const dim = Dimensions.get('window');
    return dim.height;
}

export function screenWidth() {
    const dim = Dimensions.get('window');
    return dim.width;
}

export function getUnixtime() {
    var time = new Date().getTime();
    time = Math.floor(time / 1000);
    return time;
}

export function getHI(time) {
    if (!time) {
        time = getUnixtime();
    }
    var D = new Date(time * 1000);
    var hour = D.getHours();
    if (hour < 10) {
        hour = '0' + hour;
    }
    var min = D.getMinutes();
    if (min < 10) {
        min = '0' + min;
    }
    var formattedTime = hour + ':' + min;
    return formattedTime;
}

export const getInitials = (string: string): string => {
    const parts = string.split(" ");

    if (parts.length > 1) {
        return `${parts[0].substring(0, 1).toUpperCase()}${parts[1].substring(0, 1).toUpperCase()}`;
    }

    return `${parts[0].substring(0, 2).toUpperCase()}`;
};


export function getDataWithSelectedCount(array, count) {
    let result = [];
    array.map(item => {
        if (result.length < count) {
            result.push(item);
        }
    });

    return result;
}

export function getOnlyUniqueData(data) {
    let result = [];
    data.map(item => {
        if (!result.includes(item)) {
            result.push(item);
        }
    });
    return result;
}

export function uniqueID() {
    function chr4() {
        return Math.random()
            .toString(16)
            .slice(-4);
    }

    return chr4() + '' + chr4() + '' + chr4() + '' + chr4();
}


export function getActivityListWithIndex(activities, comingIndex) {
    let result = [];
    activities.map((activity, index) => {
        if (index === comingIndex) {
            result = activity.list;
        }
    });
    return result;
}


export const generateKey = (pre) => {
    return `${pre}_${new Date().getTime()}`;
};


export function getErrorsFromResponse(errors) {
    let result = [];
    errors.map(item => (
        item.map(error => (
            result.push(error)
        ))
    ));

    return result;
}

export function getChatTitle(chat) {
    let result = "";
    if (chat.title) {
        return chat.title;
    } else {
        chat.users.map(user => {
            result = `${user.firstname} ${user.lastname}`;
        });
    }
    return result;
}

export function getChatAvatar(chat) {
    if (chat.type !== "dialogue") {
        if (chat.image_url) {
            return <Image source={{uri: chat.image_url}} style={{width: 60, height: 60, borderRadius: 30}}/>
        }
        return <MultiImageAvatar images={[]}/>
    } else {
        let userAvatar = chat.image_url
            ? chat.image_url
            : `https://ui-avatars.com/api/?name=${chat.title}&background=CEF2EF&color=169286&bold=true`;
        return <Image source={{uri: userAvatar}} style={{width: 60, height: 60, borderRadius: 30}}/>
    }
}

export function getTitleFromChat(chat) {
    if (chat.type) {
        if (chat.type === "group") {
            return "group"
        } else {
            let userAvatar = '';
            let userFullname = '';
            chat.users.map(user => {
                if (user.id !== getDeviceUser().id) {
                    userAvatar = user.avatar ? user.avatar.large : 'http://placehold.it/80x80';
                    userFullname = user.firstname + " " + user.lastname;
                }
            });
            userAvatar = userAvatar === 'http://placehold.it/80x80'
                ? `https://ui-avatars.com/api/?name=${userFullname}&background=D8D8D8&bold=true`
                : userAvatar;
            return userAvatar;
        }
    } else {
        if (chat.title) {
            return chat.title;
        }
        let userFullname = '';
        chat.users.map(user => {
            if (user.id !== getDeviceUser().id) {
                userFullname = user.firstname + " " + user.lastname;
            }
        });

        return userFullname;
    }
}


export function getAvatarFromChat(chat) {
    if (chat.title) {
        return chat.image ? chat.image : "group";
    }
    let userAvatar = '';
    let userFullname = '';
    chat.users.map(user => {
        if (user.id !== getDeviceUser().id) {
            userAvatar = user.avatar;
            userFullname = user.firstname + " " + user.lastname;
        }
    });

    userAvatar = userAvatar === 'http://placehold.it/80x80'
        ? `https://ui-avatars.com/api/?name=${userFullname}&background=D8D8D8&bold=true`
        : userAvatar;

    return userAvatar;
}

export function getUserArrayFromChat(chat) {
    let result = [];
    chat.users.map(user => {
        if (user.id !== getDeviceUser().user_id) {
            result.push(user.id)
        }
    });

    return result;
}

export function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

export function getRandomColor() {
    let colors = ['#AA0000', '#1d1160', '#800080', '#0093E9', '#FEC163'];
    return colors[getRndInteger(1, 5)];
}

export function checkSelectedGroupUser(list, id) {
    let result = false;
    list.map(user => {
        if (!result) {
            if (parseInt(user.id) === parseInt(id)) {
                result = true;
            }
        }
    });
    return result;
}


export function errorMessage(message = 'Oops.Something went wrong.') {
    Toast.show({
        position: 'top',
        text: message,
        buttonText: 'OK',
        type: 'error',
        textStyle: {marginLeft: 10},
        duration: 4000
    });
}


export function successMessage(message = 'Operation is successfull.') {
    Toast.show({
        position: 'top',
        text: message,
        buttonText: 'OK',
        type: 'success',
        textStyle: {marginLeft: 10},
        duration: 4000
    });
}

export function getCategoriesData(data) {
    let result = [];
    data.map(item => {
        let select = item;
        select.title = item.titles.da;
        result.push(select);
    });

    return result;
}

export function checkImageOrFile(path) {
    const extension = path.split('.').pop();
    return extension === "png" || extension === "jpg" || extension === "jpeg";
}

export function checkMessageExistList(list, message) {
    let result = false;
    list.map(item => {
        if (item._id === message._id) {
            result = true;
        }
    });

    return result;
}

export function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}