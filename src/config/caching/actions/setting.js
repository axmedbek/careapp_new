import {repository} from "../repository";

export function checkLangExists() {
    const langObj = repository.objects('Lang');
    return langObj.isEmpty();
}

export function saveSetting(key, value) {
    const settingObj = repository.objects('Setting').filtered('key == $0', key);
    if (settingObj.isEmpty()) {
        repository.write(() => {
            repository.create('Setting', {
                id: repository.objects('Setting').length + 1,
                key: key,
                value: value
            });
        });
    } else {
        repository.write(() => {
            settingObj[0].value = value;
        });
    }
}

export function getValueFromSettingWithKey(key) {
    const settingObj = repository.objects('Setting').filtered('key == $0', key);
    if (settingObj.isEmpty()) {
        return null;
    } else {
        return settingObj[0].value
    }
}


export function deleteValueFromSettingWithKey(key) {
    const settingObj = repository.objects('Setting').filtered('key == $0', key);
    if (settingObj.isEmpty()) {
        return null;
    } else {
        repository.write(() => {
            repository.delete(settingObj)
        });
    }
}

export function saveLang(lang) {
    let response;
    const langObj = repository.objects('Lang');
    if (langObj.isEmpty()) {
        repository.write(() => {
            repository.create('Lang', {
                id: 1,
                lang: lang
            });
            response = 'SUCCESS';
        });
    } else {
        response = updateLang(lang);
    }
    return response;
}

export function updateLang(lang) {
    if (!lang) {
        let langObj = repository.objects('Lang');
        repository.write(() => {
            langObj['0'].lang = undefined;
        });
    } else {
        let langObj = repository.objects('Lang');
        repository.write(() => {
            langObj['0'].lang = lang;
        });
    }
    return 'SUCCESS';
}


export function getLanguage() {
    const langObj = repository.objects('Lang');
    if (langObj.isEmpty() || langObj['0'].lang === null) {
        return 'da';
    } else {
        return langObj['0'].lang;
    }
}

export function deleteDeviceUserFromStorage() {
    const userObj = repository.objects('User');
    if (!userObj.isEmpty()) {
        repository.write(() => {
            repository.delete(userObj[0])
        });
    }
}

export function setDeviceUser(user) {
    const userObj = repository.objects('User');
    if (userObj.isEmpty()) {
        repository.write(() => {
            repository.create('User', {
                user_id: parseInt(user.id),
                _id: user._id,
                email: user.email,
                firstname: user.firstname,
                lastname: user.lastname,
                type: user.type,
                phone: user.phone,
                gender: user.gender,
                avatar: user.avatar.small,
                username: user.username !== null ? user.username : undefined
            });
        });
    } else {
        repository.write(() => {
            userObj[0].user_id = parseInt(user.id);
            userObj[0]._id = user._id;
            userObj[0].email = user.email;
            userObj[0].firstname = user.firstname;
            userObj[0].lastname = user.lastname;
            userObj[0].type = user.type;
            userObj[0].phone = user.phone;
            userObj[0].gender = user.gender;
            userObj[0].avatar = user.avatar.small;
            userObj[0].username = user.username !== null ? user.username : undefined;
        });
    }
    return 'SUCCESS';
}

export function getDeviceUser() {
    const userObj = repository.objects('User');
    if (userObj.isEmpty()) {
        repository.write(() => {
            repository.create('User', {
                id: 0,
                _id: '',
                user_id: 0,
                email: '',
                firstname: '',
                lastname: '',
                type: '',
                phone: '',
                gender: '',
                avatar: '',
                username: ''
            });
        });
    }
    return userObj['0'];
}

export function deleteDeviceUser() {
    const userObj = repository.objects('User');
    userObj[0].id = undefined;
    userObj[0]._id = undefined;
    userObj[0].user_id = undefined;
    userObj[0].email = undefined;
    userObj[0].firstname = undefined;
    userObj[0].lastname = undefined;
    userObj[0].type = undefined;
    userObj[0].phone = undefined;
    userObj[0].gender = undefined;
    userObj[0].avatar = undefined;
    userObj[0].username = undefined;
}

