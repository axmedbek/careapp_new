import {repository} from "../repository";

export function saveTokens(accessToken) {
    let response;
    const token = repository.objects('Token');
    if (token.isEmpty()) {
        repository.write(() => {
            repository.create('Token', {
                id: 1,
                accessToken: accessToken
            });
            response = 'SUCCESS';
        });
    } else {
        response = updateTokens(accessToken);
    }
    return response;
}
export function updateTokens(accessToken) {
    if (!accessToken) {
        let token = repository.objects('Token');
        repository.write(() => {
            token['0'].accessToken = undefined;
        });
    } else {
        let token = repository.objects('Token');
        repository.write(() => {
            token['0'].accessToken = accessToken;
        });
    }
    return 'SUCCESS';
}


export function getAccessToken() {
    const token = repository.objects('Token');
    if (token.isEmpty() || token['0'].accessToken === null) {
        return null;
    } else {
        return token['0'].accessToken;
    }
}
