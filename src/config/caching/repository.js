import Realm from 'realm';

const TokenSchema = {
    name: 'Token',
    primaryKey: 'id',
    properties: {
        id: 'int',
        accessToken: 'string?',
        deviceToken: 'string?',
    },
};

const SettingSchema = {
    name: 'Setting',
    primaryKey: 'id',
    properties: {
        id: 'int',
        key: 'string?',
        value: 'string?',
    },
};


const LangSchema = {
    name: 'Lang',
    primaryKey: 'id',
    properties: {
        id: 'int',
        lang: 'string?'
    },
};

const UserSchema = {
    name: 'User',
    primaryKey: 'id',
    properties: {
        id: 'int',
        user_id: 'int',
        _id: 'string?',
        firstname: 'string?',
        lastname: 'string?',
        username: 'string?',
        phone: 'string?',
        email: 'string?',
        gender: 'string?',
        avatar: 'string?',
        type: 'string?',
    },
};

export const repository = new Realm({
    schema: [TokenSchema,LangSchema,UserSchema,SettingSchema],
    schemaVersion: 38,
});
