import io from "socket.io-client";
import {getAccessToken} from "./caching/actions/token";

export function createSocket(){
    let socket = io('http://95.216.42.62:2222', {
        query: `token=Bearer ${getAccessToken()}`,
        transports: ['websocket'],
        reconnection: true,
        secure: true,
        jsonp: false
    });

    socket.on('connect', () => {
        console.log("connected");
    });

    socket.on('disconnect', () => {
        console.log("disconnect");
    });

    return socket;
}
