import CryptoJS from "react-native-crypto-js";


export const encryptString = (string) => {
    return CryptoJS.AES.encrypt(string, 'care compagniet').toString();
};

export const compareString = (hashed,string) => {
    if (hashed === null) {
        return false;
    }
    else {
        let bytes  = CryptoJS.AES.decrypt(hashed, 'care compagniet');
        return bytes.toString(CryptoJS.enc.Utf8) === string;
    }
};
